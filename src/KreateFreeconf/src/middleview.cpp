/***************************************************************************
 *   Copyright (C) 2009 by Jan Ehrenberger   *
 *   lankvil@seznam.cz   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "middleview.h"
#include <qscrollview.h>
#include <qhbox.h>
#include <qpushbutton.h>
#include <kpopupmenu.h>
#include <kxmlguiclient.h>
#include <kxmlguifactory.h>

MiddleView::MiddleView(QWidget *parent)
 : QVBox(parent)
{
    QScrollView *scroll = new QScrollView(this);
    m_list = new KListView(scroll->viewport());
    m_list->setSorting(-1);
    m_list->addColumn("Entry");
    scroll->addChild(m_list);
    scroll->setResizePolicy(QScrollView::AutoOneFit);

    QHBox *buttons = new QHBox(this);
    QPushButton *up = new QPushButton("Up", buttons);
    QPushButton *down = new QPushButton("Down", buttons);

    connect(up, SIGNAL(pressed()), this, SLOT(entryUp()));
    connect(down, SIGNAL(pressed()), this, SLOT(entryDown()));
    connect(m_list, SIGNAL(rightButtonPressed(QListViewItem *, const QPoint &, int)),
            this, SLOT(slotRightButtonPressed(QListViewItem *, const QPoint &, int)));

    // Create popup menu
    m_popup_menu = new KPopupMenu(m_list);
    m_popup_menu->insertItem("Delete", this, SLOT(slotDeleteItem()));
    m_popup_menu->insertSeparator();
    m_popup_menu->insertItem("Move Up", this, SLOT(entryUp()));
    m_popup_menu->insertItem("Move Down", this, SLOT(entryDown()));
}

void MiddleView::insertEntry(const QString &entry)
{
    new KListViewItem(m_list, entry);
}

void MiddleView::insertEntry(KHTMLPart *html, DOM::Range range)
{
    QString str = range.toString().string();
    if(!m_list->findItem(str, 0))
        new MiddleViewListItem(m_list, html, range);
}

/** Entry Up
 * Move currently selected entry up in the list.
 */
void MiddleView::entryUp()
{
    QListViewItem *item = m_list->selectedItem();
    if(item) {
        QListViewItem *above = item->itemAbove();
        if(above)
            above->moveItem(item);
    }        
}

/** Entry Down
 * Move currently selected entry down in the list.
 */
void MiddleView::entryDown()
{
    QListViewItem *item = m_list->selectedItem();
    if(item) {
        QListViewItem *below = item->itemBelow();
        if(below)
            item->moveItem(below);
    }        
}

MiddleView::~MiddleView()
{
}

void MiddleView::slotRightButtonPressed(QListViewItem *item, const QPoint &point, int column)
{
    if(item)
        m_popup_menu->exec(point);
}

void MiddleView::slotDeleteItem()
{
    QListViewItem *item = m_list->currentItem();
    if(item)
        delete item;
}


