/***************************************************************************
                          fcTKString.cpp  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "fcTKString.h"

//--------------------------------------------------------------------------
fcTKString :: fcTKString( )
   : user_values( true )
{
}
//--------------------------------------------------------------------------
fcTKString :: fcTKString( const fcTKString& entry )
   : fcTKEntry( entry ),
     strings( entry. strings ),
     user_values( entry. user_values )
{
}
//--------------------------------------------------------------------------
fcTKString :: ~fcTKString()
{
   // do not call deep erase for strings !!!
}
//--------------------------------------------------------------------------
Fc::EntryTypes fcTKString :: Type() const
{
   return Fc::String;
}
//--------------------------------------------------------------------------
fcBaseEntry* fcTKString :: NewEntry() const
{
   return new fcTKString( * this );
}
//--------------------------------------------------------------------------
const fcStringEntry& fcTKString :: GetString( fc_uint i ) const
{
   return strings[ i ];
}
//--------------------------------------------------------------------------
void fcTKString :: AppendStrings( fcList< fcStringEntry >& new_strings )
{
   fc_int i;
   for( i = 0; i < new_strings. Size(); i ++ )
      strings. Append( new_strings[ i ] ); 
}
//--------------------------------------------------------------------------
void fcTKString :: SetDependencyValue( const fc_char* val ) {
    dependency_value.SetString( val );
}
//--------------------------------------------------------------------------
const fc_char* fcTKString :: GetDependencyValue() const {
    return dependency_value.Data();
}
//--------------------------------------------------------------------------
#ifdef DEBUG
void fcTKString :: Print( ostream& stream ) const
{
   stream << "STRING ";
   fcTEntry :: Print( stream );
}
#endif
