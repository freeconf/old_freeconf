/***************************************************************************
                          fcTSEntry.h  -  description
                             -------------------
    begin                : 2004/04/10 16:20
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCTSENTRY_H__
#define __FCTSENTRY_H__

#include "fcSectionEntry.h"
#include "fcTypes.h"
#include "fcTEntry.h"
#include "fcList.h"

//! This is basic class for deriving group of entries - section etc.
class fcTSEntry : public fcSectionEntry, public fcTEntry
{
   public:

   //! Basic constructor
   fcTSEntry();

   //! Copy constructor
   fcTSEntry( const fcTSEntry& section_entry );
   
   //! Destructor
   ~fcTSEntry();
   
   //! Entry type getter
   Fc::EntryTypes Type() const;

   //! Return true if it is a section
   fc_bool IsSection() const { return true; };

   //! Return true if it is a kewyord
   fc_bool IsKeyword() const { return false; };

   //! Return section entry pointer
   fcSectionEntry* GetSectionEntryPointer();

   //! Allocate new instance and return pointer
   virtual fcBaseEntry* NewEntry() const;

   //! Indexing operator
   fcTEntry* operator[] ( fc_uint pos );
   
   //! Find entry with given name starting at given position
   /*! If the entry is found 'pos' is set to its position
    */
   fcTEntry* FindTEntry( const fc_char* name,
                         fc_uint& pos );
   
   //! Find entry with given name
   fcTEntry* FindTEntry( const fc_char* name );
  
   //! Return section entry pointer for constant instances
   const fcSectionEntry* GetSectionEntryPointer() const;

   //! Indexing operator for constant instances
   const fcTEntry* operator[] ( fc_uint pos ) const;
   
   //! Find entry with given name starting at given position for constant instances
   const fcTEntry* FindTEntry( const fc_char* name,
                               fc_uint& pos ) const;
   
   //! Find entry with given name for constant instances
   const fcTEntry* FindTEntry( const fc_char* name ) const;
   
#ifdef DEBUG
   //! Print this section
   void Print( ostream& stream ) const;
#endif 
};

// operator << is defined in fcBaseEntry

#endif
