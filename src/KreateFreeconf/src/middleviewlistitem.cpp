/***************************************************************************
 *   Copyright (C) 2009 by Jan Ehrenberger   *
 *   lankvil@seznam.cz   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "middleviewlistitem.h"
#include <iostream>

MiddleViewListItem::MiddleViewListItem(QListView *list, KHTMLPart *html, const DOM::Range &range)
 : KListViewItem(list), m_html(html), m_range(range)
{
    setText(0, m_range.toString().string());
    std::cout << "Add string: '" << m_range.toString().string() << "'" << std::endl;
}

void MiddleViewListItem::setSelection()
{
    m_html->setSelection(m_range);
}


MiddleViewListItem::~MiddleViewListItem()
{
}


#include "middleviewlistitem.moc"
