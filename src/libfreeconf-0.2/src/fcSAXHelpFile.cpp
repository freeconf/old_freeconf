/***************************************************************************
                          fcSAXHelpFile.cpp  -  description
                             -------------------
    begin                : 2004/08/20
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include "debug.h"
#include "fcSAXHelpFile.h"
#include "fcTEntry.h"
#include "fcTSEntry.h"
#include "fcMessageHandler.h"

//---------------------------------------------------------------------------
fcSAXHelpFile :: fcSAXHelpFile() : label_element( false ),
                                   help_element( false ),
                                   enclosing_tag( false ),
                                   current_entry( NULL )
{
}
//---------------------------------------------------------------------------
fcSAXHelpFile :: ~fcSAXHelpFile()
{
}
//---------------------------------------------------------------------------
void fcSAXHelpFile :: startElement(  const   XMLCh* const    uri,
                                     const   XMLCh* const    localname,
                                     const   XMLCh* const    qname,
                                     const   Attributes&     attributes )
{
   DBG_FUNCTION_NAME( "fcSAXHelpFile", "startElement" );  

   const fc_char* const name = XMLString :: transcode( localname );
   // so far there can be only one attribute - 'name' - entry name
   const fc_char* const attr_name = 
      XMLString :: transcode( attributes. getLocalName( ( unsigned int ) 0 ) );
   const fc_char* const attr_val = 
      XMLString :: transcode( attributes. getValue( ( unsigned int ) 0 ) );

   DBG_EXPR( name );  
   if( ! enclosing_tag && 
         strcmp( name, "freeconf-help" ) == 0 )
   {
      enclosing_tag = true;
      return;
   }
   if( ! enclosing_tag )
   {
      MessageHandler. Error( "You must enclose the Help File with <freeconf-help> and </freeconf-help>." );
      return;
   }
   
   fcTSEntry* t_section = t_section_stack. Top();
   if( strcmp( name, "section" ) == 0 )
   {
      // We store the section in the current_entry because
      // we also want to know its label a help.
      fc_uint attr_len = attributes. getLength();
      if( attr_len > 1 )
      {
         MessageHandler. Error( "Too many atributes in help file." );
         return;
      }

      if( strcmp( attr_name, "name" ) != 0 ) 
      {
         MessageHandler. Error( fcString( "Attribute name expected but " ) +
               fcString( attr_name ) +
               fcString( " found." ) );
         return;
      }
      DBG_COUT( "Looking for section " << attr_val <<
                " in " << t_section -> GetEntryName() );
      current_entry = t_section ->  FindTEntry( attr_val );
      if( ! current_entry )
      {
         MessageHandler. Error(
               fcString( "No section with name " ) +
               fcString( attr_val ) +
               fcString( " in tempate file." ) );
         return;
      }
      if( ! current_entry -> IsSection() )
      {
         MessageHandler. Error(
               fcString( "Entry " ) +
               fcString( attr_val ) +
               fcString( " is not a section." ) );
         return;
      }
      t_section_stack. Add( ( fcTSEntry* ) current_entry );
      DBG_EXPR( current_entry );
      return;
   }
   
   if( strcmp( name, "entry" ) == 0 )
   {
      DBG_COUT( "Looking for keyword " << name <<
                " in " << t_section -> GetEntryName() );
      fc_uint attr_len = attributes. getLength();
      if( attr_len > 1 )
      {
         MessageHandler. Error( "Too many atributes in help file." );
         return;
      }

      if( strcmp( attr_name, "name" ) != 0 ) 
      {
         MessageHandler. Error( fcString( "Attribute name expected but " ) +
               fcString( attr_name ) +
               fcString( " found." ) );
         return;
      }
      current_entry = t_section -> FindTEntry( attr_val );
      if( ! current_entry )
      {
         MessageHandler. Error( fcString( "There is no such entry ( " ) +
               fcString( attr_val ) +
               fcString( " in the template file." ) );
      }
      return;
   }
      
   assert( current_entry );
   if( strcmp( name, "label" ) == 0 )
   {
      label_element = true;
      return;
   }
   if( strcmp( name, "help" ) == 0 )
   {
      help_element = true;
      return;
   }
   MessageHandler. Error( fcString( " Uknown tag " )+
                          fcString(  name ) + 
                          fcString( "." ) );
   // TODO throw some exception here
}
//---------------------------------------------------------------------------
void fcSAXHelpFile :: endElement( const XMLCh* const uri, 
                                      const XMLCh* const localname, 
                                      const XMLCh* const qname )
{
   DBG_FUNCTION_NAME( "fcSAXHelpFile", "endElement" );  

   const fc_char* const name = XMLString :: transcode( localname );

   if( strcmp( name, "entry" ) == 0 )
   {
      current_entry = NULL;
      return;
   }
   if( strcmp( name, "section" ) == 0 )
   {
      current_entry = NULL;
      t_section_stack. Remove();
      DBG_COUT( "Going back to section " << 
            t_section_stack. Top() -> GetEntryName() );
      return;
   }
   if( strcmp( name, "label" ) == 0 )
   {
      label_element = false;
      return;
   }
   if( strcmp( name, "help" ) == 0 )
   {
      help_element = false;
      return;
   }
   if( strcmp( name, "freeconf-help" ) != 0 )
   {
      MessageHandler. Error( fcString( "ERROR: Uknown tag " ) +
            fcString( name ) +
            fcString( "." ) );
      // TODO throw some exception here
   }
}
//---------------------------------------------------------------------------
void fcSAXHelpFile :: characters( const XMLCh* const chars, 
                                      const unsigned int length )
{
   DBG_FUNCTION_NAME( "fcSAXHelpFile", "characters" );  
   const fc_char*  data = XMLString :: transcode( chars );
   
   if( label_element )
   {
      assert( current_entry );

      DBG_COUT( "setting LABEL: " << current_entry -> GetEntryName()
            << " -> " << data );
      current_entry -> SetLabel( data );
   }
   if( help_element )
   {
      assert( current_entry );

      DBG_COUT( "setting HELP: " << current_entry -> GetEntryName()
           << " -> " << data );
      
      current_entry -> SetHelp( data );
   }
}
//---------------------------------------------------------------------------
fc_bool fcSAXHelpFile :: ParseHelpFile( const fc_char* file_name,
                                        fcTSEntry* t_section )
{
   enclosing_tag = false;
   current_entry = NULL;
   t_section_stack. EraseAll();
   t_section_stack. Add( t_section );
   return Parse( file_name );
}
