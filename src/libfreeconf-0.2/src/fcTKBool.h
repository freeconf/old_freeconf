/***************************************************************************
                          fcTKBool.h  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcTKBoolH
#define fcTKBoolH

#include "fcTKEntry.h"

class fcTKBool : public fcTKEntry
{
   //! Dependency value
   fc_bool dependency_value;

   public:
   //! Basic constructor
   fcTKBool();

   //! Copy constructor
   fcTKBool( const fcTKBool& entry );

   //! Destructor
   ~fcTKBool(){};

   //! Entry type getter
   Fc::EntryTypes Type() const;

   //! Allocate new instance and return pointer
   virtual fcBaseEntry* NewEntry() const;

   //! Dependency_value setter
   void SetDependencyValue( const fc_char* val );

   //! Dependency_value getter
   fc_bool GetDependencyValue() const;

#ifdef DEBUG
   //! Print this keyword
   virtual void Print( ostream& stream ) const;
#endif

};

#endif
