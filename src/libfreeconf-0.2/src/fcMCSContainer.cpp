/***************************************************************************
                          fcMCSContainer.cpp  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "fcMCSContainer.h"

//--------------------------------------------------------------------------
fcMCSContainer :: fcMCSContainer()
   : default_values( 0 )
{
}
//--------------------------------------------------------------------------
fcMCSContainer :: fcMCSContainer( const fcMCSContainer& container )
   : fcCSEntry( container ),
     default_values( 0 )
{
   if( container. default_values )
      default_values = 
         new fcCSEntry( *container. default_values );
}
//--------------------------------------------------------------------------
fcMCSContainer :: fcMCSContainer( const fcTEntry* t_entry )
   : fcCSEntry( t_entry ),
     default_values( 0 )
{
}
//--------------------------------------------------------------------------
fcMCSContainer :: ~fcMCSContainer()
{
   if( default_values ) delete default_values;
}
//--------------------------------------------------------------------------
fc_bool fcMCSContainer :: IsMultipleEntryContainer() const
{
   return true;
}
