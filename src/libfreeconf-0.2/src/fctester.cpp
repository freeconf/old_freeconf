/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : 2004/04/12 14:18
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include <ctime>
#include <iostream>
#include <cstdlib>
#include <getopt.h>
#include <fcSAXTemplateFile.h>
#include <fcString.h>
#include <fcFiles.h>
#include "debug.h"
//------------------------------------------------------------------------------------------
static void Print_Usage( const char* program_name )
{
   std::cout << "Usage: " << program_name << " options [-i input_file] " << std::endl;
   std::cout << "Basic options:" << std::endl;
   std::cout << "    -h      --help                          Display this usage informations." << std::endl;
   std::cout << "    -v      --verbose N                     Set verbose mode to N." << std::endl;
   std::cout << "    -o      --output filename               Target confguratio file. " << std::endl;
   std::cout << "    -i      --input filename                Input Template File." << std::endl;
   std::cout << std::endl;
}
//------------------------------------------------------------------------------------------
static int Get_Options( int argc, char* argv[], int& _optind, char* input_file )
{
   int next_option;
   const char* short_options="hv:o:i:";
   const option long_options[] =
   {
      { "help",                 0, NULL, 'h' },
      { "verbose",              1, NULL, 'v' },
      { "output",               1, NULL, 'o' },
      { "input",                1, NULL, 'i' },
      { NULL,                   0, NULL, 0   }
   };
   const char* program_name = argv[ 0 ];
   double tmp_num;
   if( argc == 1 )
   {
      std::cerr << "There is no command line argument." << std::endl;
      Print_Usage( program_name );
   }
   do  
   {
      next_option = getopt_long_only( argc, argv, short_options, long_options, NULL );
      switch( next_option )
      {
         case 'h':
            Print_Usage( program_name );
            break;
         case 'v':
            //Parse_Number( optarg, tmp_num, "-v", "--verbose", program_name );
            //verbose = ( char ) tmp_num;
            break;
         case 'o':
            //output_file = optarg;
            break;
         case 'i':
            input_file = optarg;
            break;
         case '?':
            Print_Usage( program_name );
            return 0;
         case -1:
            break;
         default:
            return 0;
      }
   } while( next_option != -1 );
   _optind = optind;
   if( ! input_file && _optind < argc ) input_file = argv[ _optind ++ ];
   else
   {
      std::cerr << "There is given no input file." << std::endl;
      return 0;
   }
   //if( ! output_file ) Set_Output_File();
   return 1;
}
//------------------------------------------------------------------------------------------
static int Test_fcString()
{
   fcString str1, str2, str3;
   char text[ 1024 ];
   fc_uint len = strlen( text );
   //cout << text << endl;
   //str1 = fcString( text, len );
   std::cout << str1. Data() << std::endl;
   std::cout << str1 << std::endl;
   time_t start_time, end_time;
   time( &start_time );
   /*for( unsigned int i = 0; i < 10000; i ++ )
      for( unsigned int j = 1; j < 1024; j ++ )
      {
         //str2 = fcString( text, j );
         //str2. Set_String( text, j );
         //str2 = str3;
      }*/
   time( &end_time );
   std::cout << end_time - start_time << std::endl;
   return 1;
}
//------------------------------------------------------------------------------------------
static void TestList()
{
   fcList< fc_uint > list;
   for( fc_uint i = 0; i < 10; i ++ )
      list. Append( i );
   for( fc_uint i = 0; i < 10; i ++ )
      list. Prepend( i );
   std::cout << list;
   for( fc_uint i = 1; i <= 10; i ++ )
   {
      std::cout << "Erasing index " << list. Size() - i << std::endl;
      list. Erase( list. Size() - i );
   }
   std::cout << list;
}
//------------------------------------------------------------------------------------------
static void TestFiles()
{
   fcList< fcString > dirs;
   dirs. Append( "/home/oberhuber/.freeconf/strings" );
   dirs. Append( "/usr/local/share/freeconf/strings" );
   dirs. Append( "/usr/share/freeconf/strings" );

   fcList< fcString > langs;
   langs. Append( "cs" );
   langs. Append( "en" );

   fcList< fcString > result_files;

   fc_bool f = fcCheckFiles( dirs, 
                             &langs,
                             "code-pages.xml",
                             false,
                             0,
                             0,
                             &result_files,
                             0);
}
//------------------------------------------------------------------------------------------
int main( int argc, char *argv[] )
{
   DBG_INIT( "debug.xml" );
   std::cout << "Freeconf Tester:" << std::endl;
   fcString test( fcString( "test1" ) + "test2" );
   std::cout << test << std::endl;
   //if( ! Test_fcString() ) return 0;
   //TestList();
   //TestFiles();
}
