/***************************************************************************
 *   Copyright (C) 2009 by Jan Ehrenberger   *
 *   lankvil@seznam.cz   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef PACKAGETREEVIEW_H
#define PACKAGETREEVIEW_H

#include <klistview.h>

/**
Tree View of Freeconf package.

	@author Jan Ehrenberger <lankvil@seznam.cz>
*/
class PackageTreeView : public KListView
{
    Q_OBJECT
public:
    PackageTreeView(QWidget *);
    ~PackageTreeView();

public slots:
    void rename(QListViewItem *item, int c);

protected:
    bool acceptDrag(QDropEvent *) const;
    void contentsDropEvent(QDropEvent *);
};



class PackageTreeViewItem : public KListViewItem
{
public:
    PackageTreeViewItem(PackageTreeView *list, const QString &name, const QString &value = "");
    PackageTreeViewItem(PackageTreeViewItem *item, const QString &name, const QString &value = "");
    ~PackageTreeViewItem();

    /// Sets order of this item compared to others. -1 = no order, sort by text
    virtual int order() const { return -1; }
    int compare (QListViewItem * i, int col, bool ascending) const;
    
    /** Is Renamebale
     * Is given column of this item renameable ? Override this method in subclass.
     */
    virtual bool isRenameable(int column);
    /** Drop Text Event
     * This event is called when user drops text over this item. Retuns true if the drop was accepted,
     * false if it was not accepted.
     */
    virtual bool dropTextEvent(const QString &text);

protected:
    int m_order; 
};

#endif
