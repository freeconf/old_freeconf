/***************************************************************************
 *   Copyright (C) 2009 by Jan Ehrenberger   *
 *   lankvil@seznam.cz   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "packagetreeview.h"
#include "kcckentry.h"
#include <qdragobject.h>

PackageTreeView::PackageTreeView(QWidget *parent)
 : KListView(parent)
{
    setAcceptDrops(true);
    setRootIsDecorated(true);
    setAllColumnsShowFocus(true);

    addColumn("Name");
    addColumn("Value");
    setItemsRenameable(true);
    setRenameable(1, true);

    kcCKEntry *sekce = new kcCKEntry(this, "Sekce", "section");
    kcCKEntry *a = new kcCKEntry(sekce, "A", "string");
    kcCKEntry *B = new kcCKEntry(sekce, "B", "string");
}


PackageTreeView::~PackageTreeView()
{
}


void PackageTreeView::rename(QListViewItem *item, int c)
{
    // Call KListView rename function only if renaming of item is enabled
    if((dynamic_cast<PackageTreeViewItem*>(item)) -> isRenameable(c))
        KListView::rename(item, c);
}

bool PackageTreeView::acceptDrag(QDropEvent *event) const
{
    QString text;

    if(!QTextDrag::decode(event, text))
        return false;
    return true;
}

void PackageTreeView::contentsDropEvent (QDropEvent *event)
{
    QString text;
    QListViewItem *item;

    KListView::contentsDropEvent(event);
    if(!QTextDrag::decode(event, text))
        return;
    // Get item where the text was dropped
    item = itemAt(event->pos());
    if(item) {
        PackageTreeViewItem *pitem = dynamic_cast<PackageTreeViewItem *>(item);
        if(pitem->dropTextEvent(text))
            return;
        // Item did not accept drop event, maybe it's parent will be more hospitable.
        item = pitem->parent();
        if(item) {
            pitem = dynamic_cast<PackageTreeViewItem *>(item);
            pitem->dropTextEvent(text);
        }
        return;
    }
                
    // Insert new entry into the list
    kcCKEntry *entry = new kcCKEntry(this, text, "string");
}



//////// PackageTreeViewItem ////////

PackageTreeViewItem::PackageTreeViewItem(PackageTreeView *list, const QString &name, const QString &value)
    :KListViewItem(list, name, value)
{
}

PackageTreeViewItem::PackageTreeViewItem(PackageTreeViewItem *item, const QString &name, const QString &value)
    :KListViewItem(item, name, value)
{
}

PackageTreeViewItem::~PackageTreeViewItem()
{
}

/// Compare
/// Override default compare function to implement priorities using m_order.
int PackageTreeViewItem::compare (QListViewItem * i, int col, bool ascending) const
{
    PackageTreeViewItem *p = dynamic_cast<PackageTreeViewItem *>(i);
    
    if(order() == p->order())
        // If both items do not have order, use default comparison
        return QListViewItem::compare(i, col, ascending);
    if(order() == -1) // Item has no explicit order => this > i
        return 1;
    if(p->order() == -1) // i has no explicit order => i > this
        return -1;
    // Compare orders
    return order() - p->order();
}

bool PackageTreeViewItem::isRenameable(int column)
{
    return false;
}

bool PackageTreeViewItem::dropTextEvent(const QString &text)
{
    return false;
}

