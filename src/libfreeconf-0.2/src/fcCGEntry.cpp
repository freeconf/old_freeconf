/***************************************************************************
author           : 2009 by David Fabian
***************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/


#include "fcCGEntry.h"
#include "fcTEntry.h"
#include "fcCEntry.h"

fcCGEntry::fcCGEntry (fcCGEntry *parent)
  :_configBuddy(0),
  _parent(parent)
{

}


fcCGEntry::fcCGEntry (fcCEntry *configBuddy, fcCGEntry *parent)
  :_configBuddy(configBuddy),
  _parent(parent)
{

}


fcCGEntry::~fcCGEntry ()
{
  
}


void fcCGEntry::setConfigBuddy (fcCEntry *configBuddy)
{
  _configBuddy = configBuddy;
}


fcCGEntry* fcCGEntry::parent () const
{
  return _parent;
}

void fcCGEntry::setParent (fcCGEntry *parent)
{
  _parent = parent;
}


fcCEntry* fcCGEntry::configBuddy () const
{
  return _configBuddy;
}

void fcCGEntry::setName (const fcString &name)
{
  _name = name;
  // implicit fallback
  if (!_label) _label = name;
}


const fcString& fcCGEntry::name () const
{
  return _name;
}


Fc::EntryTypes fcCGEntry::type () const
{
  if (_configBuddy) return _configBuddy->Type();
  return Fc::UnknownEntry;
}


void fcCGEntry::setLabel (const fcString &label)
{
  _label = label;
}


const fcString& fcCGEntry::label () const
{
  if (_configBuddy)
  {
    const fcTEntry *tEntry = _configBuddy->GetTEntry();
    return tEntry->GetLabel();
  }  
  return _label;
}



