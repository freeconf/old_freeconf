/************************************************************************
 *                      fcCDependency.h
 *      author: Frantisek Rolinek
 ************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCCDEPENDENCY_H__
#define __FCCDEPENDENCY_H__


#include <ostream>
#include "fcTypes.h"
#include "fcString.h"
#include "fcList.h"
#include "fcSAXTemplateConst.h"
#include "fcTDependency.h"

class fcCDependencyListItem 
{
    //! Constructors & destructor
    public:
        fcCDependencyListItem();
        fcCDependencyListItem(const fcTDependencyListItem& item);
        fcCDependencyListItem(const fcCDependencyListItem& item);
        virtual ~fcCDependencyListItem();

    //! Pure virtual functions
    public:
        //! Allocates and returns new instance of this class
        virtual fcCDependencyListItem* NewDependencyListItem() const = 0;
        //! Returns true, if it is a keyword
        virtual fc_bool IsDependencyListItemKeyword() const = 0;
        //! Returns true, if it is a relation
        virtual fc_bool IsDependencyListItemRelation() const = 0;
};

class fcCDRelationListItem : public fcCDependencyListItem 
{
    //! Class members 
    private:
        //! Relation
        fcRelationEnum m_relation;

    //! Constructors & destructor
    public:
        fcCDRelationListItem();
        fcCDRelationListItem(const fcRelationEnum& _enum);
        fcCDRelationListItem(const fcTDRelationListItem& item);
        fcCDRelationListItem(const fcCDRelationListItem& item);
        virtual ~fcCDRelationListItem();

    //! Overrided functions from base class
    public:
        //! Allocates and returns new instance of this class
        virtual fcCDependencyListItem* NewDependencyListItem() const;
        //! Returns true, if it is a keyword
        virtual fc_bool IsDependencyListItemKeyword() const;  // return false;
        //! Returns true, if it is a relation
        virtual fc_bool IsDependencyListItemRelation() const; // return true;

    //! Overrided operator
    public:
        const fcCDRelationListItem& operator=(const fcCDRelationListItem& item);
        friend std::ostream& operator<< (std::ostream& stream, const fcCDRelationListItem& fcRel);

    //! Setter
    public:
        //! Relation setter
        void SetRelation(const fcRelationEnum& relation); 

    //! Getter
    public:
        //! Relation getter
        const fcRelationEnum& GetRelation() const;
};

enum fcDKeywordTypes { fcDK_BOOL,
                       fcDK_NUMBER,
                       fcDK_STRING,
};

class fcCDKeywordListItem : public fcCDependencyListItem
{
    //! Class members
    protected: 
        //! Name of entry
        /*! Name of entry, on which this one is dependent
         */
        fcString m_strEntryName;

    //! Constructors & destructor
    public: 
        fcCDKeywordListItem();
        fcCDKeywordListItem(const fc_char* name);
        fcCDKeywordListItem(const fcString& name);
        fcCDKeywordListItem(const fcTDKeywordListItem& item);
        fcCDKeywordListItem(const fcCDKeywordListItem& item);
        virtual ~fcCDKeywordListItem();

    //! Overrided functions from base class
    public: 
        //! Allocates and returns new instance of this class
        //virtual fcCDependencyListItem* NewDependencyListItem() const;
        //! Returns true, if it is a keyword
        virtual fc_bool IsDependencyListItemKeyword() const; // return true
        //! Returns true, if it is a relation
        virtual fc_bool IsDependencyListItemRelation() const; //return false

    //! Pure virtual functions
    public:
        //! Dependency keyword type getter
        virtual fcDKeywordTypes DKeywordType() const = 0;

    //! Overrided operators
    public: 
        //const fcCDKeywordListItem& operator=(const fcCDKeywordListItem& item);
        //friend std::ostream& operator<< (std::ostream& stream, const fcCDKeywordListItem& fcKey);

    //! Setters
    public:
        //! Entry name setter
        void SetKeywordName(const fc_char* name);
        //! Entry name setter
        void SetKeywordName(const fcString& name);

    //! Getters
    public:
        //! Entry name getter
        const fcString& GetKeywordName() const;
};

class fcCDependency
{
    //! Class members
    private:
        //! List of dependency items
        /*! This list represent the whole dependency condition for this entry.
         *  It's structure is similar to reverse polish notation's structure.
         */
        fcList< fcCDependencyListItem* >* m_pDep;
        
    //! Constructors & destructor
    public:
        fcCDependency();
        fcCDependency(fcList< fcCDependencyListItem* > *pDep, fc_bool bDeepCopy = true);
        fcCDependency(const fcCDependency& item);
        ~fcCDependency();

    //! Overrided operators
    public: 
        const fcCDependency& operator= (const fcCDependency& item);
        friend std::ostream& operator<< (std::ostream& stream, const fcCDependency& fcCDep);

    //! Adders
    public:
        //! fcCDependencyListItem adder
        void AddDependencyListItem(fcCDependencyListItem* pItem);

    //! Setters
    public:
        //! fcCDependencyListItem setter
        void SetDependencyListItem(fcCDependencyListItem* pItem, fc_uint pos);

    //! Getters
    public:
        //! fcCDependencyListItem getter (from a given position)
        fcCDependencyListItem* GetDependencyListItem(fc_uint pos);
        //! fcCDependencyListItem const getter (from a given position)
        fcCDependencyListItem* GetDependencyListItem(fc_uint pos) const;
        //! Dependency list size getter
        fc_uint GetDependencyListSize() const;

    //! Other functions
    public:
        //! Returns true, if the given position is valid
        fc_bool IsPositionValid(fc_uint pos) const;
        //! Returns true, if the given position is not valid
        fc_bool IsPositionNotValid(fc_uint pos) const;

    //! Erase functions
    public:
        //! Erase dependency list (list od dependency items)
        void EraseDependencyList();
};

#endif
