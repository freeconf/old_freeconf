#!/usr/bin/python

from PyQt4 import QtCore, QtGui

class MyLineEdit (QtGui.QLineEdit):
	"""This class is overridden from QLineEdit. It can change color of background on invalid input"""
	def __init__ (self, parent):
		QtGui.QLineEdit.__init__(self, parent)

	def validateInput (self, state):
		if state == True:
			self.setPalette(self.parent().palette());
		else:
			p = self.palette()
			p.setColor(QtGui.QPalette.Base, QtGui.QColor(255, 172, 142))
			self.setPalette(p);
