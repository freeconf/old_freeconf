/***************************************************************************
                          fcDependencyStruct.cpp  -  description
                             -------------------
    copyright            : (C) 2009 by Frantisek Rolinek
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <cstring>
#include <cstdlib>
#include "debug.h"
#include "fcDependencyStruct.h"

//---------------------------------------------------------------------------
struRPNRead :: struRPNRead()
{
    Init();
}
//---------------------------------------------------------------------------
struRPNRead :: struRPNRead(fcRelationEnum _enum, fc_bool _is_registrable)
{
    relation_enum  = _enum;
    is_registrable = _is_registrable;
}
//---------------------------------------------------------------------------
struRPNRead :: struRPNRead(const struRPNRead& stru)
{
    relation_enum  = stru.relation_enum;
    is_registrable = stru.is_registrable;
}
//---------------------------------------------------------------------------
struRPNRead :: ~struRPNRead()
{
}
//---------------------------------------------------------------------------
const struRPNRead& struRPNRead :: operator=(const struRPNRead& stru)
{
    relation_enum  = stru.relation_enum;
    is_registrable = stru.is_registrable;

    return *this;
}
//---------------------------------------------------------------------------
void struRPNRead :: Init()
{
    relation_enum  = fcNO_RELATION;
    is_registrable = false;
}
//---------------------------------------------------------------------------
struProperties :: struProperties()
{
    Init();
}
//---------------------------------------------------------------------------
struProperties :: struProperties(const fc_char* _max,  const fc_char* _min, const fc_char* _step,
                                 const fc_char* _type, const fc_char* _data)
{
    max.SetString(_max);
    min.SetString(_min);
    step.SetString(_step);
    type.SetString(_type);
    data.SetString(_data);
}
//---------------------------------------------------------------------------
struProperties :: struProperties(const fcString& _max,  const fcString& _min, const fcString& _step,
                                 const fcString& _type, const fcString& _data)
{
    max  = _max;
    min  = _min;
    step = _step;
    type = _type;
    data = _data;
}
//---------------------------------------------------------------------------
struProperties :: struProperties(const struProperties& stru)
{
    max  = stru.max;
    min  = stru.min;
    step = stru.step;
    type = stru.type;
    data = stru.data;
}
//---------------------------------------------------------------------------
const struProperties& struProperties :: operator=(const struProperties& stru)
{
    max  = stru.max;
    min  = stru.min;
    step = stru.step;
    type = stru.type;
    data = stru.data;

    return *this;
}
//---------------------------------------------------------------------------
void struProperties :: Init()
{
    max.SetString("");
    min.SetString("");
    step.SetString("");
    type.SetString("");
    data.SetString("");
}
//---------------------------------------------------------------------------
struDependencyValue :: struDependencyValue()
{
    Init();
}
//---------------------------------------------------------------------------
struDependencyValue :: struDependencyValue(const struDependencyValue& item)
{
    dep_value_type = item.dep_value_type;
    dep_values     = item.dep_values;
}
//---------------------------------------------------------------------------
struDependencyValue :: struDependencyValue(fcList<fcString>& depValues, fcValueType& depValType)
{
    dep_values     = depValues;
    dep_value_type = depValType;
}
//---------------------------------------------------------------------------
const struDependencyValue& struDependencyValue :: operator=(const struDependencyValue& item)
{
    dep_values     = item.dep_values;
    dep_value_type = item.dep_value_type;

    return *this;
}
//---------------------------------------------------------------------------
void struDependencyValue :: Init()
{
    dep_values.EraseAll();
    dep_value_type = fcVAL_EMPTY;
}
//---------------------------------------------------------------------------
struDBoolValue :: struDBoolValue()
{
    Init();
}
//---------------------------------------------------------------------------
struDBoolValue :: struDBoolValue(const struDependencyValue& item)
{
    if(item.dep_values.Size() != 1)
    {
        std::cerr << "fcDependencyStruct.cpp " << "struDBoolValue::struDBoolValue(const struDependencyValue& item) " <<
            "list count differ from 1" << std::endl;
        return;
    }
    if(item.dep_value_type != fcVAL_EQUAL && item.dep_value_type != fcVAL_NOT_EQUAL)
    {
        std::cerr << "fcDependencyStruct.cpp " << "struDBoolValue::struDBoolValue(const struDependencyValue& item) " <<
            "values type differ from \"equal\" and \"not-equal\"!" << std::endl;
        return;
    }

    const fc_char* str = item.dep_values[0].Data();
    fc_bool bValue = false;
    if( strcmp(str, "yes") == 0 || strcmp(str, "true") == 0 || strcmp(str, "1") == 0)
        bValue = true;

    m_bValue       = bValue;
    m_depValueType = item.dep_value_type;
}
//---------------------------------------------------------------------------
struDBoolValue :: struDBoolValue(const struDBoolValue& item)
{
    m_bValue       = item.m_bValue;
    m_depValueType = item.m_depValueType;
}
//---------------------------------------------------------------------------
struDBoolValue :: struDBoolValue(fc_bool bValue, fcValueType& depValueType)
{
    m_bValue       = bValue;
    m_depValueType = depValueType;
}
//---------------------------------------------------------------------------
const struDBoolValue& struDBoolValue :: operator=(const struDBoolValue& item)
{
    m_bValue       = item.m_bValue;
    m_depValueType = item.m_depValueType;

    return *this;
}
//---------------------------------------------------------------------------
void struDBoolValue :: Init()
{
    m_bValue       = false;
    m_depValueType = fcVAL_EMPTY;
}
//---------------------------------------------------------------------------
struDNumberValue :: struDNumberValue()
{
    Init();
}
//---------------------------------------------------------------------------
struDNumberValue :: struDNumberValue(const struDependencyValue& item)
{
    for(fc_int i = 0; i < item.dep_values.Size(); i++)
    {
        const fc_char* str = item.dep_values[i].Data();
        fc_int nValue = atoi(str);

        m_lstValues.Append(nValue);
    }

    m_depValueType = item.dep_value_type;
}
//---------------------------------------------------------------------------
struDNumberValue :: struDNumberValue(const struDNumberValue& item)
{
    m_lstValues    = item.m_lstValues;
    m_depValueType = item.m_depValueType;
}
//---------------------------------------------------------------------------
struDNumberValue :: struDNumberValue(fcList< fc_int >& lstValues, fcValueType& depValueType)
{
    m_lstValues    = lstValues;
    m_depValueType = depValueType;
}
//---------------------------------------------------------------------------
struDNumberValue :: struDNumberValue(const fcList< fc_int >& lstValues, fcValueType& depValueType)
{
    m_lstValues    = lstValues;
    m_depValueType = depValueType;
}
//---------------------------------------------------------------------------
const struDNumberValue& struDNumberValue :: operator=(const struDNumberValue& item)
{
    m_lstValues    = item.m_lstValues;
    m_depValueType = item.m_depValueType;

    return *this;
}
//---------------------------------------------------------------------------
void struDNumberValue :: Init()
{
    m_lstValues.EraseAll();
    m_depValueType = fcVAL_EMPTY;
}
//---------------------------------------------------------------------------
struDStringValue :: struDStringValue()
{
    Init();
}
//---------------------------------------------------------------------------
struDStringValue :: struDStringValue(const struDependencyValue& item)
{
    for(fc_int i = 0; i < item.dep_values.Size(); i++)
    {
        m_lstValues.Append(item.dep_values[i]);
    }

    m_depValueType = item.dep_value_type;
}
//---------------------------------------------------------------------------
struDStringValue :: struDStringValue(const struDStringValue& item)
{
    m_lstValues    = item.m_lstValues;
    m_depValueType = item.m_depValueType;
}
//---------------------------------------------------------------------------
struDStringValue :: struDStringValue(fcList< fcString >& lstValues, fcValueType& depValueType)
{
    m_lstValues    = lstValues;
    m_depValueType = depValueType;
}
//---------------------------------------------------------------------------
struDStringValue :: struDStringValue(const fcList< fcString >& lstValues, fcValueType& depValueType)
{
    m_lstValues    = lstValues;
    m_depValueType = depValueType;
}
//---------------------------------------------------------------------------
const struDStringValue& struDStringValue :: operator=(const struDStringValue& item)
{
    m_lstValues    = item.m_lstValues;
    m_depValueType = item.m_depValueType;

    return *this;
}
//---------------------------------------------------------------------------
void struDStringValue :: Init()
{
    m_lstValues.EraseAll();
    m_depValueType = fcVAL_EMPTY;
}
//---------------------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const struRPNRead& rpn)
{
    if(rpn.relation_enum == fcRELATION_AND)
    {
        stream << "&& [" << rpn.is_registrable << "]";
    } else
    if(rpn.relation_enum == fcRELATION_OR)
    {
        stream << "|| [" << rpn.is_registrable << "]";
    } else
    if(rpn.relation_enum == fcRELATION_NOT)
    {
        stream << "! [" << rpn.is_registrable << "]";
    } else {
        stream << "relation??????";
    }

    return stream;
}
//---------------------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const struProperties& prop)
{
    if(prop.max != "" || prop.min != "" || prop.step != "" || prop.type != "" || prop.data != "")
        stream << " { ";
    if(prop.max != "")
        stream << "max="  << prop.max  << ", ";
    if(prop.min != "")
        stream << "min="  << prop.min  << ", ";
    if(prop.step != "")
        stream << "step=" << prop.step << ", ";
    if(prop.type != "")
        stream << "type=" << prop.type << ", ";
    if(prop.data != "")
        stream << "data=" << prop.data ;
    if(prop.max != "" || prop.min != "" || prop.step != "" || prop.type != "" || prop.data != "")
        stream << " } ";

    return stream;
}
//---------------------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const struDependencyValue& stru)
{
    switch(stru.dep_value_type)
    {
        case fcVAL_EQUAL:
            stream << "EQUAL: ";
            break;
        case fcVAL_NOT_EQUAL:
            stream << "NOT EQUAL: ";
            break;
        case fcVAL_GREATER:
            stream << "GREATER: ";
            break;
        case fcVAL_GREATER_EQ:
            stream << "GREATER_EQ: ";
            break;
        case fcVAL_LOWER:
            stream << "LOWER: ";
            break;
        case fcVAL_LOWER_EQ:
            stream << "LOWER_EQ: ";
            break;
        case fcVAL_GREATER_LOWER:
            stream << "GREATER_LOWER: ";
            break;
        case fcVAL_GREATER_LOWER_EQ:
            stream << "GREATER_LOWER_EQ: ";
            break;
        case fcVAL_GREATER_EQ_LOWER:
            stream << "GREATER_EQ_LOWER: ";
            break;
        case fcVAL_GREATER_EQ_LOWER_EQ:
            stream << "GREATER_EQ_LOWER_EQ: ";
            break;
        case fcVAL_LOWER_GREATER:
            stream << "LOWER_GREATER: ";
            break;
        case fcVAL_LOWER_GREATER_EQ:
            stream << "LOWER_GREATER_EQ: ";
            break;
        case fcVAL_LOWER_EQ_GREATER:
            stream << "LOWER_EQ_GREATER: ";
            break;
        case fcVAL_LOWER_EQ_GREATER_EQ:
            stream << "LOWER_EQ_GREATER_EQ: ";
            break;
        case fcVAL_IN:
            stream << "IN: ";
            break;
        case fcVAL_NOT_IN:
            stream << "NOT IN: ";
            break;
        default:
            stream << "VALUE NOT KNOWN: ";
            break;
    }
    for(fc_uint i = 0; i < stru.dep_values.Size(); i++)
    {
        fcString str = stru.dep_values[i];
        if(i == 0)
            stream << str;
        else 
            stream << " -> " << str;
    }

    return stream;
}
//---------------------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const struDBoolValue& stru)
{
    stream  << "Bool_C ";
    switch(stru.m_depValueType)
    {
        case fcVAL_EQUAL:
            stream << "EQUAL: ";
            break;
        case fcVAL_NOT_EQUAL:
            stream << "NOT EQUAL: ";
            break;
        case fcVAL_GREATER:
            stream << "GREATER: ";
            break;
        case fcVAL_GREATER_EQ:
            stream << "GREATER_EQ: ";
            break;
        case fcVAL_LOWER:
            stream << "LOWER: ";
            break;
        case fcVAL_LOWER_EQ:
            stream << "LOWER_EQ: ";
            break;
        case fcVAL_GREATER_LOWER:
            stream << "GREATER_LOWER: ";
            break;
        case fcVAL_GREATER_LOWER_EQ:
            stream << "GREATER_LOWER_EQ: ";
            break;
        case fcVAL_GREATER_EQ_LOWER:
            stream << "GREATER_EQ_LOWER: ";
            break;
        case fcVAL_GREATER_EQ_LOWER_EQ:
            stream << "GREATER_EQ_LOWER_EQ: ";
            break;
        case fcVAL_LOWER_GREATER:
            stream << "LOWER_GREATER: ";
            break;
        case fcVAL_LOWER_GREATER_EQ:
            stream << "LOWER_GREATER_EQ: ";
            break;
        case fcVAL_LOWER_EQ_GREATER:
            stream << "LOWER_EQ_GREATER: ";
            break;
        case fcVAL_LOWER_EQ_GREATER_EQ:
            stream << "LOWER_EQ_GREATER_EQ: ";
            break;
        case fcVAL_IN:
            stream << "IN: ";
            break;
        case fcVAL_NOT_IN:
            stream << "NOT IN: ";
            break;
        default:
            stream << "VALUE NOT KNOWN: ";
            break;
    }
    stream << stru.m_bValue;

    return stream;
}
//---------------------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const struDNumberValue& stru)
{
    stream  << "Number_C ";
    switch(stru.m_depValueType)
    {
        case fcVAL_EQUAL:
            stream << "EQUAL: ";
            break;
        case fcVAL_NOT_EQUAL:
            stream << "NOT EQUAL: ";
            break;
        case fcVAL_GREATER:
            stream << "GREATER: ";
            break;
        case fcVAL_GREATER_EQ:
            stream << "GREATER_EQ: ";
            break;
        case fcVAL_LOWER:
            stream << "LOWER: ";
            break;
        case fcVAL_LOWER_EQ:
            stream << "LOWER_EQ: ";
            break;
        case fcVAL_GREATER_LOWER:
            stream << "GREATER_LOWER: ";
            break;
        case fcVAL_GREATER_LOWER_EQ:
            stream << "GREATER_LOWER_EQ: ";
            break;
        case fcVAL_GREATER_EQ_LOWER:
            stream << "GREATER_EQ_LOWER: ";
            break;
        case fcVAL_GREATER_EQ_LOWER_EQ:
            stream << "GREATER_EQ_LOWER_EQ: ";
            break;
        case fcVAL_LOWER_GREATER:
            stream << "LOWER_GREATER: ";
            break;
        case fcVAL_LOWER_GREATER_EQ:
            stream << "LOWER_GREATER_EQ: ";
            break;
        case fcVAL_LOWER_EQ_GREATER:
            stream << "LOWER_EQ_GREATER: ";
            break;
        case fcVAL_LOWER_EQ_GREATER_EQ:
            stream << "LOWER_EQ_GREATER_EQ: ";
            break;
        case fcVAL_IN:
            stream << "IN: ";
            break;
        case fcVAL_NOT_IN:
            stream << "NOT IN: ";
            break;
        default:
            stream << "VALUE NOT KNOWN: ";
            break;
    }
    for(fc_uint i = 0; i < stru.m_lstValues.Size(); i++)
    {
        fc_int nVal = stru.m_lstValues[i];
        if(i == 0)
            stream << nVal;
        else 
            stream << " -> " << nVal;
    }

    return stream;
}
//---------------------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const struDStringValue& stru)
{
    stream  << "String_C ";
    switch(stru.m_depValueType)
    {
        case fcVAL_EQUAL:
            stream << "EQUAL: ";
            break;
        case fcVAL_NOT_EQUAL:
            stream << "NOT EQUAL: ";
            break;
        case fcVAL_GREATER:
            stream << "GREATER: ";
            break;
        case fcVAL_GREATER_EQ:
            stream << "GREATER_EQ: ";
            break;
        case fcVAL_LOWER:
            stream << "LOWER: ";
            break;
        case fcVAL_LOWER_EQ:
            stream << "LOWER_EQ: ";
            break;
        case fcVAL_GREATER_LOWER:
            stream << "GREATER_LOWER: ";
            break;
        case fcVAL_GREATER_LOWER_EQ:
            stream << "GREATER_LOWER_EQ: ";
            break;
        case fcVAL_GREATER_EQ_LOWER:
            stream << "GREATER_EQ_LOWER: ";
            break;
        case fcVAL_GREATER_EQ_LOWER_EQ:
            stream << "GREATER_EQ_LOWER_EQ: ";
            break;
        case fcVAL_LOWER_GREATER:
            stream << "LOWER_GREATER: ";
            break;
        case fcVAL_LOWER_GREATER_EQ:
            stream << "LOWER_GREATER_EQ: ";
            break;
        case fcVAL_LOWER_EQ_GREATER:
            stream << "LOWER_EQ_GREATER: ";
            break;
        case fcVAL_LOWER_EQ_GREATER_EQ:
            stream << "LOWER_EQ_GREATER_EQ: ";
            break;
        case fcVAL_IN:
            stream << "IN: ";
            break;
        case fcVAL_NOT_IN:
            stream << "NOT IN: ";
            break;
        default:
            stream << "VALUE NOT KNOWN: ";
            break;
    }
    for(fc_uint i = 0; i < stru.m_lstValues.Size(); i++)
    {
        fcString str = stru.m_lstValues[i];
        if(i == 0)
            stream << str;
        else 
            stream << " -> " << str;
    }

    return stream;
}
//---------------------------------------------------------------------------
