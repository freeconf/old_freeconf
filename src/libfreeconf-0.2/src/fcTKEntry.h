/***************************************************************************
                          fcTKEntry.h  -  description
                             -------------------
    begin                : 2004/04/10 16:22
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCTKENTRY_H__
#define __FCTKENTRY_H__

#include "fcTEntry.h"
#include "fcList.h"
#include "fcString.h"

//! This is a class for keyword entries from Template File
/*! Although the xml element enclosing all this structure has name entry,
    in fact it is a description of particular keyword of a configuration. 
 */
class fcTKEntry : public fcTEntry
{
   protected:
   
   //! Possible aliases of the keyword
   fcList< fcString >* aliases;

   //! Command line alternative of this keyword
   fcString command_line;

   public:

   //! Basic constructor
   fcTKEntry();

   protected:
   //! Copy constructor is not necesary
   fcTKEntry( const fcTKEntry& entry );

   public:
   //! Destructor
   ~fcTKEntry();
   
   //! Say this is not a section
   fc_bool IsSection() const;

   //! Return true if it is a kewyord
   fc_bool IsKeyword() const;

   //! Return true if it is a template entry
   fc_bool IsTEntry() const;

   //! Return true if it is a config entry
   fc_bool IsCEntry() const;
   
   //! DependencyValue setter
   /*! This function set the (dependency) value of given entry. 
    *  This value is used for setting entry value in case, that
    *  the dependency on other entries says, that this entry
    *  is not visible (not editable). 
    *  In this case, we use this value, not the value set in the
    *  entry. 
    */
   virtual void SetDependencyValue( const fc_char* val ) =  0;
   
   //! Add possible alias 
   void AddAlias( const fc_char* str );
   
   //! Set command line alternative
   void SetCommandLine( const fc_char* str );

#ifdef DEBUG
   //! Print this keyword entry
   void Print( ostream& stream );
   // TODO: make it const
#endif
};

#endif
