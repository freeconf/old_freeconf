/***************************************************************************
                          fcCKList.h  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcCKListH
#define fcCKListH

#include "fcCKEntry.h"
#include "fcList.h"
#include "fcString.h"

class fcCKList : public fcCKEntry, public fcList< fcString >
{
   public:
   //! Basic constructor
   fcCKList();

   //! Copy constructor
   fcCKList( const fcCKList& config_value );

   //! Constructor template entry pointer
   fcCKList( const fcTEntry* t_entry )
      : fcCKEntry( t_entry ){};

   //! Destructor
   ~fcCKList();
   
   //! Allocate new instance and return pointer
   virtual fcBaseEntry* NewEntry() const;

   //! Value type getter
   Fc::EntryTypes Type() const;
   
   //! Update DOM node
   /*void UpdateDOMNode( fcDOMNode* parent_node,
                       fcDOMDocument* doc );*/

#ifdef DEBUG
   //! Print this structure
   void Print( ostream& stream ) const;
#endif
};


#endif
