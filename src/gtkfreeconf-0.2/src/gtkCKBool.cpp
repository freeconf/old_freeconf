/***************************************************************************
                          gtkCKBool.cpp  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <gtk/gtk.h>
#include <fcTEntry.h>
#include "gtkCKBool.h"
#include "gtkCSEntry.h"
#include "debug.h"
#include "widget.h"

//--------------------------------------------------------------------------
static gint cb_bool_set( GtkWidget *widget,
                         GdkEvent *event,
                         gtkCKBoolStructure* struc )
{
   DBG_FUNCTION_NAME( "", "gtk_fc_bool_set" );
   assert( (struc -> entry) -> Type() == fcBOOL );
   DBG_EXPR( struc -> entry -> GetEntryName() );
   if( GTK_TOGGLE_BUTTON( widget ) -> active ) {
      struc -> entry -> SetValue( false );
      gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), false );
   }
   else {
      struc -> entry -> SetValue( true );
      gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( widget ), true );
   }
   DBG_EXPR( struc -> entry -> GetValue() );
   for( fc_int i = 0; i < ((fcCEntry*)(struc->entry))->GetDependentsSize() ; i++) {
       fcCEntry* c_entry = ((fcCEntry*)(struc->entry))->GetDependents( i );
       fc_bool isSensitive =  c_entry -> CEntryIsActive( struc->main_entry );
       compileEntry( isSensitive, c_entry );
   }
   return true;
}
//--------------------------------------------------------------------------
gtkCKBool :: gtkCKBool() {
    struc = new gtkCKBoolStructure();
}
//--------------------------------------------------------------------------
gtkCKBool :: gtkCKBool( const fcCKBool& ck_bool )
   : fcCKBool( ck_bool )
{
    struc = new gtkCKBoolStructure();
}
//--------------------------------------------------------------------------
gtkCKBool :: gtkCKBool( const fcTEntry* t_entry )
   : fcCKBool( t_entry )
{
    struc = new gtkCKBoolStructure();
}
//--------------------------------------------------------------------------
gtkCKBool :: ~gtkCKBool() {
    if( struc )
        delete struc;
}
//--------------------------------------------------------------------------
void gtkCKBool :: SetWidget( GtkWidget* _widget) {
    widget = _widget;
}
//--------------------------------------------------------------------------
GtkWidget* gtkCKBool :: GetWidget() {
    return widget;
}
//--------------------------------------------------------------------------
void gtkCKBool :: Render( GtkWidget* parent_widget,
                          GtkTooltips* tooltips,
                          const fc_char* help, 
                          fcCSEntry* main_c_section )
{
   DBG_FUNCTION_NAME( "gtkCKBool", "Render" );
   const fcTEntry* template_entry = ((fcCEntry*)this) -> GetTEntry();
   const fc_char* label = template_entry -> GetLabel(). Data(); 

   GtkWidget* value_widget = gtk_check_button_new_with_label( label );

   if( GetValue() ) 
      gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( value_widget ), true );

   this -> SetWidget( value_widget );
   fcCEntry* current_entry = (fcCEntry*)(this);
   fc_bool isSensitive = current_entry -> CEntryIsActive( main_c_section );
   //gtk_widget_set_sensitive( value_widget, isSensitive );
   compileEntry( isSensitive, current_entry );

   struc -> entry = this;
   struc -> main_entry = main_c_section;

   gtk_box_pack_start( GTK_BOX( parent_widget ),
                       value_widget,
                       false,
                       false,
                       0 );
   gtk_signal_connect( GTK_OBJECT( value_widget ),
                       "button_press_event",
                       GTK_SIGNAL_FUNC( cb_bool_set ),
                       struc );
   // Set tooltips
   DBG_COUT( " setting tooltips to ... " << help );
   gtk_tooltips_set_tip( tooltips,
                         value_widget,
                         help,
                         NULL );
}
//--------------------------------------------------------------------------
