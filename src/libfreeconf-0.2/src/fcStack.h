/***************************************************************************
                          fcStack.h  -  description
                             -------------------
    begin                : 2004/04/11 14:35
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCSTACK_H__
#define __FCSTACK_H__

#include "fcDataElement.h"

//! Template for simple stack
template< class T > class fcStack
{
   //! Pointer to the first element
   fcDataElement< T >* first;

   //! Size of the stack
   fc_uint size;

   public:
   //! Basic constructor
   fcStack() : first( NULL ), size ( 0 ) {};

   //! Destructor
   ~fcStack() { EraseAll(); };

   fc_bool IsEmpty() { return first == NULL; };

   //! Adding data to the stack
   void Add( const T& data )
   {
      first = new fcDataElement< T >( data, 0, first );
      size++;
   };

   //! Return data from the top but do not remove it
   T& Top()
   {
      return first -> Data();
   };

   //! Return size of the stack
   fc_uint Size ()
   {
     return size;
   }

   //! Remove data from the top of the stack
   T Remove()
   {
      fcDataElement< T >* tmp = first;
      T data = first -> Data();
      first = first -> Next();
      size--;
      delete tmp;
      return data;
   };
   
   //! Erasing all elements in the stack
   void EraseAll()
   {
      fcDataElement< T >* tmp = first;
      while( first )
      {
         tmp = first -> Next();
         delete first;
         first = tmp;
      }
      size = 0;
   };
};

#endif
