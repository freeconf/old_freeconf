/***************************************************************************
                          fcBaseEntry.h  -  description
                             -------------------
    begin                : 2004/04/10 16:17
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCBASEENTRY_H__
#define __FCBASEENTRY_H__

#include <cassert>
#include "fcTypes.h"
#include "fcString.h"

class fcSectionEntry;

//! This is basic class for deriving all entries of freeconf files
class fcBaseEntry
{
   private:
   //! Keyword for section or keyword entry
   fcString entry_name;
   
   public:
   //! Basic constructor
   fcBaseEntry();

   //! Copy constructor
   fcBaseEntry( const fcBaseEntry& entry );

   //! Constructor with name
   fcBaseEntry( const fc_char* name );

   //! Destructor
   virtual ~fcBaseEntry();

   //! Entry type getter
   virtual Fc::EntryTypes Type() const = 0;

   //! Return true if it is a section
   virtual fc_bool IsSection() const = 0;

   //! Return true if it is a kewyord
   virtual fc_bool IsKeyword() const = 0;

   //! Return true if it is a template entry
   virtual fc_bool IsTEntry() const = 0;

   //! Return true if it is a config entry
   virtual fc_bool IsCEntry() const = 0;

   //! Allocate new instance and return pointer
   virtual fcBaseEntry* NewEntry() const = 0;

   //! Return section entry pointer
   virtual fcSectionEntry* GetSectionEntryPointer();
   
   //! Keyword name setter
   void SetEntryName( const fc_char* str );

   //! Keyword name getter
   const fcString& GetEntryName() const;

#ifdef DEBUG
   //! Print this entry
   virtual void Print( ostream& stream ) const = 0;
#endif

};

#ifdef DEBUG
inline ostream& operator << ( ostream& stream, const fcBaseEntry& entry ) 
{
   entry. Print( stream );
   return stream;
}
#endif

#endif
