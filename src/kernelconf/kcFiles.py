#!/usr/bin/python
#
# Freeconf Files
#
# begin: 11.11.2008 by Jan Ehrenberger <lankvil@seznam.cz>
#
# Create freeconf files
#

from xml.dom.minidom import Document
from xml.dom.ext import PrettyPrint


class XMLVisitor:
	def __init__(self, data, filename, parent_elt):
		self.data = data
		self.filename = filename
		self.buffer = []

		self.doc = Document()
		template = self.doc.createElement(parent_elt)
		self.doc.appendChild(template)
		self.buffer.append(template)
		
	def process(self):
		for a in self.data:
			a.accept(self)

	def enter(self, elt):
		"""Enter new section"""
		section = self.doc.createElement("section")
		section.setAttribute("name", elt.xml_name)
		self.buffer[-1].appendChild(section)
		self.buffer.append(section)
		return True

	def leave(self, elt):
		self.buffer.pop(-1)
		return True
		
	def write(self):
		PrettyPrint(self.doc, open(self.filename, "w"))
		

class Template(XMLVisitor):
	def __init__(self, data, filename):
		XMLVisitor.__init__(self, data, filename, "freeconf-template")
		# Set name of template
		name = self.doc.createElement("section-name")
		name.appendChild(self.doc.createTextNode("kernel-config"))
		self.buffer[0].appendChild(name)

		self.process()

	def enter(self, elt):
		"""Enter new section"""
		section = self.doc.createElement("section")
		# create entry-name
		name = self.doc.createElement("section-name")
		name.appendChild(self.doc.createTextNode(elt.xml_name))
		section.appendChild(name)

		self.buffer[-1].appendChild(section)
		self.buffer.append(section)
		return True

	def visit(self, elt):
		entry = self.doc.createElement(elt.elt_type)
		# set entry-name
		name = self.doc.createElement("entry-name")
		name.appendChild(self.doc.createTextNode(elt.xml_name))
		entry.appendChild(name)

		# Set entry properties
		properties = self.doc.createElement("properties")
		if elt.elt_type == "string" and elt.data != None:
			# set data
			d = self.doc.createElement("data")
			d.appendChild(self.doc.createTextNode(elt.data))
			properties.appendChild(d)
		# Append properties only if it has some data
		if properties.hasChildNodes():
			entry.appendChild(properties)

		self.buffer[-1].appendChild(entry)
		return True		
	

class Help(XMLVisitor):
	def __init__(self, data, filename, label):
		XMLVisitor.__init__(self, data, filename, "freeconf-help")
		# Append root section entry
		root_section = self.doc.createElement("section")
		root_section.setAttribute("name", "kernel-config")
		self.buffer[-1].appendChild(root_section)
		self.buffer.append(root_section)
		# Set main label
		self.add_label(self.buffer[-1], label)

		self.process()

	def add_label(self, parent, text):
		"""Create label element and append it to parent"""
		label = self.doc.createElement("label")
		label.appendChild(self.doc.createTextNode(text))
		parent.appendChild(label)

	def enter(self, elt):
		XMLVisitor.enter(self, elt)

		if elt.label:
			self.add_label(self.buffer[-1], elt.label)
		return True

	def visit(self, elt):
		entry = self.doc.createElement("entry")
		entry.setAttribute("name", elt.xml_name)

		if elt.label:
			self.add_label(entry, elt.label)
		if elt.help:
			help = self.doc.createElement("help")
			help.appendChild(self.doc.createTextNode(elt.help))
			entry.appendChild(help)

		self.buffer[-1].appendChild(entry)
		return True		


class Transform(XMLVisitor):
	def __init__(self, data, filename):
		XMLVisitor.__init__(self, data, filename, "xsl:stylesheet")
		# modify toplevel xml element
		self.buffer[-1].setAttribute("xmlns:xsl", "http://www.w3.org/1999/XSL/Transform")
		self.buffer[-1].setAttribute("version", "1.0")
		output = self.doc.createElement("xsl:output")
		output.setAttribute("method", "text")
		output.setAttribute("indent", "no")
		self.buffer[-1].appendChild(output)

		self.process()
	
	def enter(self, elt):
		"""Ignore enter events"""
		return True
	
	def leave(self, elt):
		"""Ignore leave events"""
		return True
	
	def visit(self, elt):
		template = self.doc.createElement("xsl:template")
		template.setAttribute("match", elt.xml_name)

		if elt.elt_type == "bool" or (elt.elt_type == "string" and elt.data == "tristate"):
			choose = self.doc.createElement("xsl:choose")
			template.appendChild(choose)

			when = self.doc.createElement("xsl:when")
			when.setAttribute("test", ". = 'yes'")
			when.appendChild(self.doc.createTextNode(elt.name + "=y"))
			choose.appendChild(when)

			if elt.elt_type == "string":
				when = self.doc.createElement("xsl:when")
				when.setAttribute("test", ". = 'module'")
				when.appendChild(self.doc.createTextNode(elt.name + "=m"))
				choose.appendChild(when)

			otherwise = self.doc.createElement("xsl:otherwise")
			otherwise.appendChild(self.doc.createTextNode("# " + elt.name + " is not set"))
			choose.appendChild(otherwise)
		else:
			template.appendChild(self.doc.createTextNode(elt.name + "="))
			value_of = self.doc.createElement("xsl:value-of")
			value_of.setAttribute("select", ".")
			template.appendChild(value_of)
		
		self.buffer[-1].appendChild(template)
		return True
	

class Default(XMLVisitor):
	def __init__(self, data, filename):
		XMLVisitor.__init__(self, data, filename, "kernel-config")
		self.process()

	def enter(self, elt):
		"""Enter new section"""
		section = self.doc.createElement(elt.xml_name)
		self.buffer[-1].appendChild(section)
		self.buffer.append(section)
		return True

	def visit(self, elt):
		if not elt.default:
			return True

		entry = self.doc.createElement(elt.xml_name)
		entry.appendChild(self.doc.createTextNode(elt.get_default_str()))

		self.buffer[-1].appendChild(entry)
		return True		


class StringLists:
	def __init__(self, data, filename):
		self.filename = filename
		# Data is just a dictionary mapping name -> list of strings
		self.data = data

	def write(self):
		doc = Document()
		strings_elt = doc.createElement("freeconf-strings")
		doc.appendChild(strings_elt)
		
		for list in self.data.keys():
			list_elt = doc.createElement("string-list")
			list_elt.setAttribute("name", list)
			strings_elt.appendChild(list_elt)
			for string in self.data[list]:
				string_elt = doc.createElement("string")
				string_elt.appendChild(doc.createTextNode(string))
				list_elt.appendChild(string_elt)

		PrettyPrint(doc, open(self.filename, "w"))
		

class Header:
	header = "header.xml"
	template = "kernel_template.xml"
	default_values = "kernel_default.xml"
	help = "kernel_help.xml"
	output = "kernel.xml"
	transform = "kernel_transform.xsl"
	native_output = "config"
	strings = "kernel_strings.xml"

	def __init__(self, package_path):
		self.package_path = package_path
		self.filename = self.get_path(self.header)
	
	def get_path(self, file):
		return self.package_path + "/" + file

	def write(self):
		doc = Document()
		header = doc.createElement("freeconf-header")
		doc.appendChild(header)

		def add_elt(name, value):
			elt = doc.createElement(name)
			elt.appendChild(doc.createTextNode(value))
			header.appendChild(elt)

		add_elt("strings", self.strings)
		add_elt("template", self.template)
		add_elt("default-values", self.default_values)
		add_elt("help", self.help)
		add_elt("output", self.output)
		add_elt("transform", self.transform)
		add_elt("native-output", self.native_output)

		PrettyPrint(doc, open(self.filename, "w"))
		
