/***************************************************************************
                          fcTEntry.cpp  -  description
                             -------------------
    begin                : 2004/08/28
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include "fcMessageHandler.h"
#include "fcTEntry.h"
#include "fcDependencyActions.h"
#include "fcTDependency.h"

//--------------------------------------------------------------------------
fcTEntry :: fcTEntry()
{
    m_bRequired = false;
    m_bMultiple = false;
    m_pTDep     = new fcTDependency;
}
//---------------------------------------------------------------------------
fcTEntry :: fcTEntry(fcTDependency* pDep, fc_bool bRequired /*=false*/, fc_bool bMultiple /*=false*/)
{
    m_bRequired = bRequired;
    m_bMultiple = bMultiple;

    if(pDep)
        m_pTDep = new fcTDependency(*pDep);
    else
        m_pTDep = new fcTDependency;
}
//---------------------------------------------------------------------------
fcTEntry :: fcTEntry(const fcTEntry& item)
    : fcBaseEntry(item)
{
    m_bRequired = item.m_bRequired;
    m_bMultiple = item.m_bMultiple;
    m_pTDep     = new fcTDependency(*item.m_pTDep);
}
//---------------------------------------------------------------------------
fcTEntry :: ~fcTEntry() {
    if (m_pTDep) 
        delete m_pTDep;
}
//---------------------------------------------------------------------------
fc_bool fcTEntry :: IsTEntry() const
{
    return true;
}
//---------------------------------------------------------------------------
fc_bool fcTEntry :: IsCEntry() const
{
    return false;
}
//---------------------------------------------------------------------------
void fcTEntry :: SetLabel(const fc_char* strLabel)
{
   m_strLabel = strLabel;
}
//---------------------------------------------------------------------------
void fcTEntry :: SetLabel(const fcString& strLabel)
{
   m_strLabel = strLabel;
}
//---------------------------------------------------------------------------
void fcTEntry :: SetHelp(const fc_char* strHelp)
{
    m_strHelp = strHelp;
}
//---------------------------------------------------------------------------
void fcTEntry :: SetHelp(const fcString& strHelp)
{
    m_strHelp = strHelp;
}
//---------------------------------------------------------------------------
void fcTEntry :: SetRequired(fc_bool bRequired)
{ 
   m_bRequired = bRequired;
}
//---------------------------------------------------------------------------
void fcTEntry :: SetMultiple(fc_bool bMultiple)
{
   m_bMultiple = bMultiple;
}
//---------------------------------------------------------------------------
const fcString& fcTEntry :: GetLabel() const
{
   return m_strLabel;
}
//---------------------------------------------------------------------------
const fcString& fcTEntry :: GetHelp() const
{
   return m_strHelp;
}
//---------------------------------------------------------------------------
fc_bool fcTEntry :: GetRequired() const
{ 
   return m_bRequired;
}
//---------------------------------------------------------------------------
fc_bool fcTEntry :: GetMultiple() const
{
   return m_bMultiple;
}
//---------------------------------------------------------------------------
void fcTEntry :: AddDependencyListItem(fcTDependencyListItem *pItem)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","AddDependencyListItem(fcTDependencyListItem* pItem)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    if(!pItem)
    {
        MessageHandler.Error("fcTEntry.cpp","AddDependencyListItem(fcTDependencyListItem* pItem)",
                "the given pointer (pItem) is not allocated!");
        return;
    }

    m_pTDep->AddDependencyListItem(pItem);
}
//---------------------------------------------------------------------------
void fcTEntry :: AddDependencyListItem(const fcRelationEnum& _enum)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","AddDependencyListItem(const fcRelationEnum& _enum)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    fcTDRelationListItem *pItem = new fcTDRelationListItem(_enum);

    m_pTDep->AddDependencyListItem(pItem);
}
//---------------------------------------------------------------------------
void fcTEntry :: AddDependencyListItem(const fc_char* name, const fcList<struDependencyValue>& lst)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp",
                "AddDependencyListItem(const fc_char* name, const fcList<struDependencyValue>& lst)",
                "m_pTDep pointer is not allocated!");
        return;
    }
    
    fcTDKeywordListItem * pItem = new fcTDKeywordListItem(name, lst);

    m_pTDep->AddDependencyListItem(pItem);
}
//---------------------------------------------------------------------------
void fcTEntry :: AddDependencyListItem(const fcString& name, const fcList<struDependencyValue>& lst)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp",
                "AddDependencyListItem(const fcString& name, const fcList<struDependencyValue>& lst)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    fcTDKeywordListItem * pItem = new fcTDKeywordListItem(name, lst);

    m_pTDep->AddDependencyListItem(pItem);
}
//---------------------------------------------------------------------------
void fcTEntry :: AddActionListItem(fcDActionBaseListItem *pItem)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","AddActionListItem(fcDActionBaseListItem *pItem)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    if(!pItem)
    {
        MessageHandler.Error("fcTEntry.cpp","AddActionListItem(fcDActionBaseListItem *pItem)",
                "the given pointer (pItem) is not allocated!");
        return;
    }

    m_pTDep->AddActionListItem(pItem);
}
//---------------------------------------------------------------------------
void fcTEntry :: AddActionListItem(fc_int nActionValue, fc_bool bDefValue)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","AddActionListItem(fc_int nActionValue, fc_bool bDefValue)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    fcDActionBoolListItem * pItem = new fcDActionBoolListItem(nActionValue, bDefValue);

    m_pTDep->AddActionListItem(pItem);
}
//---------------------------------------------------------------------------
void fcTEntry:: AddActionListItem(fc_int nActionValue, fc_int nDefValue)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","AddActionListItem(fc_int nActionValue, fc_int nDefValue)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    fcDActionNumberListItem * pItem = new fcDActionNumberListItem(nActionValue, nDefValue);

    m_pTDep->AddActionListItem(pItem);
}
//---------------------------------------------------------------------------
void fcTEntry :: AddActionListItem(fc_int nActionValue, fc_int nMin, fc_int nMax, fc_int nStep, const fc_char* strType)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp",
                "AddActionListItem(fc_int nActionValue, fc_int nMin, fc_int nMax, fc_int nStep, const fc_char* strType)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    fcDActionNumberListItem * pItem = new fcDActionNumberListItem(nActionValue, nMin, nMin, nStep, strType);

    m_pTDep->AddActionListItem(pItem);
}
//---------------------------------------------------------------------------
void fcTEntry :: AddActionListItem(fc_int nActionValue, fc_int nMin, fc_int nMax, fc_int nStep, const fcString& strType)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp",
                "AddActionListItem(fc_int nActionValue, fc_int nMin, fc_int nMax, fc_int nStep, const fcString& strType)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    fcDActionNumberListItem * pItem =  new fcDActionNumberListItem(nActionValue, nMin, nMin, nStep, strType);

    m_pTDep->AddActionListItem(pItem);
}
//---------------------------------------------------------------------------
void fcTEntry :: AddActionListItem(fc_int nActionValue, const fc_char* strDefValue)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","AddActionListItem(fc_int nActionValue, const fc_char* strDefValue)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    fcDActionStringListItem * pItem = new fcDActionStringListItem(nActionValue, strDefValue);

    m_pTDep->AddActionListItem(pItem);
}
//---------------------------------------------------------------------------
void fcTEntry :: AddActionListItem(fc_int nActionValue, const fcString& strDefValue)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","AddActionListItem(fc_int nActionValue, const fcString& strDefValue)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    fcDActionStringListItem * pItem =  new fcDActionStringListItem(nActionValue, strDefValue);

    m_pTDep->AddActionListItem(pItem);
}
//---------------------------------------------------------------------------
void fcTEntry :: AddActionListItem(fc_int nActionValue, const fcList< fcStringEntry >& lstData)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","AddActionListItem(fc_int nActionValue, const fc_char* strData, bool)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    fcDActionStringListItem * pItem =  new fcDActionStringListItem(nActionValue, lstData);

    m_pTDep->AddActionListItem(pItem);
}
//---------------------------------------------------------------------------
void fcTEntry :: SetDependencyListItem(fc_uint pos, fcTDependencyListItem *pItem)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","SetDependencyListItem(fc_uint pos, fcTDependencyListItem *pItem)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    if(m_pTDep->IsPositionInDependencyListNotValid(pos))
    {
        MessageHandler.Warning("fcTEntry.cpp","SetDependencyListItem(fc_uint pos, fcTDependencyListItem *pItem)",
                "pos index is out of range!");
        return;
    }

    SetDependencyListItem(pItem, pos);
}
//---------------------------------------------------------------------------
void fcTEntry :: SetDependencyListItem(fcTDependencyListItem *pItem, fc_uint pos)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","SetDependencyListItem(fcTDependencyListItem *pItem, fc_uint pos)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    if(m_pTDep->IsPositionInDependencyListNotValid(pos))
    {
        MessageHandler.Warning("fcTEntry.cpp","SetDependencyListItem(fcTDependencyListItem *pItem, fc_uint pos)",
                "pos index is out of range!");
        return;
    }

    SetDependencyListItem(pItem, pos);
}
//---------------------------------------------------------------------------
void fcTEntry :: SetActionListItem(fc_uint pos, fcDActionBaseListItem *pItem)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","SetActionListItem(fc_uint pos, fcDActionBaseListItem *pItem)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    if(m_pTDep->IsPositionInActionListNotValid(pos))
    {
        MessageHandler.Warning("fcTEntry.cpp","SetActionListItem(fc_uint pos, fcDActionBaseListItem *pItem)",
                "pos index is out of range!");
        return;
    }
    
    SetActionListItem(pItem, pos);
}
//---------------------------------------------------------------------------
void fcTEntry :: SetActionListItem(fcDActionBaseListItem *pItem, fc_uint pos)
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","SetActionListItem(fcDActionBaseListItem *pItem, fc_uint pos)",
                "m_pTDep pointer is not allocated!");
        return;
    }

    if(m_pTDep->IsPositionInActionListNotValid(pos))
    {
        MessageHandler.Warning("fcTEntry.cpp","SetActionListItem(fcDActionBaseListItem *pItem, fc_uint pos)",
                "pos index is out of range!");
        return;
    }
    
    SetActionListItem(pItem, pos);
}
//---------------------------------------------------------------------------
fcTDependencyListItem* fcTEntry :: GetDependencyListItem(fc_uint pos) const
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","GetDependencyListItem(fc_uint pos) const",
                "m_pTDep pointer is not allocated!");
        return (fcTDependencyListItem*) 0;
    }

    if(m_pTDep->IsPositionInDependencyListNotValid(pos))
    {
        MessageHandler.Warning("fcTEntry.cpp","GetDependencyListItem(fc_uint pos) const",
                "pos index is out of range!");
        return (fcTDependencyListItem*) 0;
    }

    return m_pTDep->GetDependencyListItem(pos);
}
//---------------------------------------------------------------------------
fcDActionBaseListItem* fcTEntry :: GetActionListItem(fc_uint pos) const
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","GetActionListItem(fc_uint pos) const",
                "m_pTDep pointer is not allocated!");
        return (fcDActionBaseListItem*) 0;
    }

    if(m_pTDep->IsPositionInActionListNotValid(pos))
    {
        MessageHandler.Warning("fcTEntry.cpp","GetActionListItem(fc_uint pos) const",
                "pos index is out of range!");
        return (fcDActionBaseListItem*) 0;
    }

    return m_pTDep->GetActionListItem(pos);
}
//---------------------------------------------------------------------------
fc_uint fcTEntry :: GetDependencyListSize() const
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","GetDependencyListSize() const",
                "m_pTDep pointer is not allocated!");
        return 0;
    }

    return m_pTDep->GetDependencyListSize();
}
//---------------------------------------------------------------------------
fc_uint fcTEntry :: GetActionListSize() const
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","GetActionListSize() const",
                "m_pTDep pointer is not allocated!");
        return 0;
    }

    return m_pTDep->GetActionListSize();
}
//---------------------------------------------------------------------------
void fcTEntry :: EraseDependency()
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","EraseDependency()",
                "m_pTDep pointer is not allocated!");
        return;
    }

    m_pTDep->EraseDependencyList();
    m_pTDep->EraseActionList();
}
//---------------------------------------------------------------------------
void fcTEntry :: PrintDependency() const 
{
    if(!m_pTDep)
    {
        MessageHandler.Error("fcTEntry.cpp","PrintDependency()",
                "m_pTDep pointer is not allocated!");
        return;
    }

    if(m_pTDep->GetDependencyListSize() > 0 || m_pTDep->GetActionListSize() > 0)
    {
        std::cout << std::endl;
        std::cout << GetEntryName() << std::endl;
        std::cout << "------------------------------" << std::endl;
        std::cout << (*m_pTDep) << std::endl;
        std::cout << std::endl;
        //std::cout << "************************************************************************************" << std::endl;
        //std::cout << std::endl;
    }
}
//---------------------------------------------------------------------------
#ifdef DEBUG
void fcTEntry :: Print( ostream& stream ) const
{
   stream << " name=\"" << GetEntryName() << "\"" <<
             " label=\"" << GetLabel() << "\"" <<
             " help=\"" << GetHelp() << "\"" <<
             " multiple=" << GetMultiple() <<
             " required=" << GetRequired();
}
#endif
