#!/usr/bin/python

import sys


import package

p = package.FcPackage()
p.loadPackage("apache")
#p.configFile.write(p.headerStructure.outputFile)
##p.transform()
#
#
## Print config tree:
def print_tree(e, i = ""):
	if e.isSection():
		print i + str(e)
		for x in e.entries:
			print_tree(x, i + " ")
	else:
		print "%s %-40s: %s" % (i, str(e), str(e.value))
print_tree(p.trees.configTree)

## Test recursive search
print
print "## Testing recursive search ##"
e = p.trees.configTree.recursiveFindCEntry('/apache-config/Files/IsER')
print "Result: " + str(e)

sys.exit(0)

from dependencies import DependencyParser
parser = DependencyParser()



vyraz = 'substr("Ahoj", 1, 2)'

print "Zpracovavam vraz:", vyraz
vysledek = parser.parse(vyraz, None, p.trees.configTree, p.data.lists)
print "Reprezentace vrazu:", vysledek
print "Hodnota vrazu:", vysledek.value().data


e = p.trees.configTree.recursiveFindCEntry('/vantage-config/GMI/MSGLGSUP')
e2 = p.trees.configTree.recursiveFindCEntry('/vantage-config/GMI/MSGBREAK')
e3 = p.trees.configTree.recursiveFindCEntry('/vantage-config/GMI/MSGNMBRS')
e4 = p.trees.configTree.recursiveFindCEntry('/vantage-config/GMI/MSGSHOTS')
e.value = 'yes'
print e, e.value
print e2, e2.templateBuddy.dynamicActive
print e3, e3.templateBuddy.dynamicActive
print e4, e4.templateBuddy.dynamicActive
e.value = 'no'
print e, e.value
print e2, e2.templateBuddy.dynamicActive
print e3, e3.templateBuddy.dynamicActive
print e4, e4.templateBuddy.dynamicActive


#
#e = p.configTree.recursiveFindCEntry("/mplayer-config/general-options/quiet")
#e.value = True


##from base import FcBaseEntry as pokus
##g = pokus()
##print g
#
#
#sys.exit()
#
