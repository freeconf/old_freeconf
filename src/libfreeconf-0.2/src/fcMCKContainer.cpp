/***************************************************************************
                          fcMCKContainer.cpp  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "fcMCKContainer.h"

//--------------------------------------------------------------------------
fcMCKContainer :: fcMCKContainer()
   : default_values( 0 )
{
}
//--------------------------------------------------------------------------
fcMCKContainer :: fcMCKContainer( const fcMCKContainer& container )
   : fcCSEntry( container ),
     default_values( 0 )
{
   assert( 0 );
   /*if( section_entry. default_values )
      default_values = 
         new fcCKEntry( *section_entry. default_values )*/
}
//--------------------------------------------------------------------------
fcMCKContainer :: fcMCKContainer( const fcTEntry* t_entry )
   : fcCSEntry( t_entry ),
     default_values( 0 )
{
}
//--------------------------------------------------------------------------
fcMCKContainer :: ~fcMCKContainer()
{
   if( default_values ) delete default_values;
}
//--------------------------------------------------------------------------
fc_bool fcMCKContainer :: IsMultipleEntryContainer() const
{
   return true;
}
