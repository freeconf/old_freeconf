/***************************************************************************
                          fcSAXHeaderFile.cpp  -  description
                             -------------------
    begin                : 2004/10/15
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include "fcSAXHeaderFile.h"
#include "debug.h"
#include "fcMessageHandler.h"

//--------------------------------------------------------------------------
fcSAXHeaderFile :: fcSAXHeaderFile() : header_structure( 0 ),
                                       enclosing_tag( false ),
                                       header_element( fcNO_HDR_ELEMENT )
{
}
//--------------------------------------------------------------------------
fcSAXHeaderFile :: ~fcSAXHeaderFile()
{
}
//--------------------------------------------------------------------------
void fcSAXHeaderFile :: startElement(  const   XMLCh* const    uri,
                                       const   XMLCh* const    localname,
                                       const   XMLCh* const    qname,
                                       const   Attributes&     attributes)
{
   DBG_FUNCTION_NAME( "fcSAXHeaderFile", "startElement" );

   const fc_char* const name = XMLString :: transcode( localname );
   DBG_EXPR( name );

   if( strcmp( name, "freeconf-header" ) == 0 )
   {
      enclosing_tag = true;
      return;
   }
   if( ! enclosing_tag )
   {
     MessageHandler. Error( "You must enclose the Header File with <freeconf-header> and </freeconf-header>." );
     return;
   }
   
   if( strcmp( name, "template" ) == 0 )
   {
      header_element = fcTEMPLATE_FILE;
      return;
   }
   if( strcmp( name, "gui-template" ) == 0 )
   {
     header_element = fcGUI_TEMPLATE_FILE;
     return;
   }
   if( strcmp( name, "help" ) == 0 )
   {
      header_element = fcHELP_FILE;
      return;
   }
   if( strcmp( name, "gui-label" ) == 0 )
   {
     header_element = fcGUI_LABEL_FILE;
     return;
   }   
   if( strcmp( name, "strings" ) == 0 )
   {
      header_element = fcSTRING_LISTS_FILE;
      return;
   }
   if( strcmp( name, "default-values" ) == 0 )
   {
      header_element = fcDEFAULT_VALUES_FILE;
      return;
   }
   if( strcmp( name, "transform" ) == 0 )
   {
      header_element = fcTRANSFORM_FILE;
      return;
   }
   if( strcmp( name, "output" ) == 0 )
   {
      header_element = fcOUTPUT_FILE;
      return;
      // Parsing attributes
   }
   if( strcmp( name, "native-output" ) == 0 )
   {
      header_element = fcNATIVE_OUTPUT_FILE;
      return;
   }
   std::cerr << "Uknown header element '" << name << "'" << std::endl;
   //abort();
   return;
}
//--------------------------------------------------------------------------
void fcSAXHeaderFile :: endElement( const XMLCh* const uri, 
                                    const XMLCh* const localname, 
                                    const XMLCh* const qname)
{
   DBG_FUNCTION_NAME( "fcSAXHeaderFile", "endElement" );

   const fc_char* const name = XMLString :: transcode( localname );
   
   DBG_COUT( name );

   header_element = fcNO_HDR_ELEMENT;
}
//--------------------------------------------------------------------------
void fcSAXHeaderFile :: characters( const XMLCh* const chars, 
                                    const unsigned int length)
{
   DBG_FUNCTION_NAME( "fcSAXHeaderFile", "characters" );

   const fc_char* data = XMLString :: transcode( chars );

   DBG_EXPR( data );

   switch( header_element )
   {
      case fcTEMPLATE_FILE:
         header_structure -> SetTemplateFile( data );
         break;
      case fcGUI_TEMPLATE_FILE:
        header_structure -> SetGuiTemplateFile( data );
        break;
      case fcSTRING_LISTS_FILE:
         header_structure -> StringListFiles(). Append( data );
         break;
      case fcHELP_FILE:
         header_structure -> SetHelpFile( data );
         break;
      case fcGUI_LABEL_FILE:
         header_structure -> SetGuiLabelFile( data );
         break;
      case fcDEFAULT_VALUES_FILE:
         header_structure -> SetDefaultValuesFile( data );
         break;
      case fcTRANSFORM_FILE:
         header_structure -> SetTransformFile( data );
         break;
      case fcOUTPUT_FILE:
         header_structure -> SetOutputFile( data );
         break;
      case fcNATIVE_OUTPUT_FILE:
         header_structure -> SetNativeFile( data );
         break;
      default:;
   }
}
//--------------------------------------------------------------------------
fc_bool fcSAXHeaderFile :: ParseHeaderFile( const fc_char* file_name,
                                            fcHeaderStructure* fc_header_structure )
{
   header_structure = fc_header_structure;
   enclosing_tag = false;
   header_element = fcNO_HDR_ELEMENT;
   return Parse( file_name );
}
