/***************************************************************************
                          gtkCSEntry.cpp  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "gtkCSEntry.h"
#include "gtkCKBool.h"
#include <fcCKList.h>
#include <gtkCKNumber.h>
#include <gtkCKString.h>
#include <gtkMCSContainer.h>
#include <fcMCKContainer.h>
#include <fcTEntry.h>
#include <fcTSEntry.h>
#include <fcMessageHandler.h>
#include "debug.h"

static const fc_char* no_help = "(No help)";
//--------------------------------------------------------------------------
gtkCSEntry :: gtkCSEntry()
{
}
//--------------------------------------------------------------------------
gtkCSEntry :: gtkCSEntry( const gtkCSEntry& section_entry )
   : fcCSEntry( section_entry )
{
}
//--------------------------------------------------------------------------
gtkCSEntry :: gtkCSEntry( const fcTEntry* t_entry )
   : fcCSEntry( t_entry )
{
}
//--------------------------------------------------------------------------
gtkCSEntry :: ~gtkCSEntry() {
}
//--------------------------------------------------------------------------
fcCEntry* gtkCSEntry :: CreateCEntry( const fcTEntry* t_entry, fc_bool ignore_multiple ) const
{
   fcCEntry* new_entry = 0;
   if( !ignore_multiple && t_entry -> GetMultiple() )
   {
      if( t_entry -> IsSection() )
         new_entry = new gtkMCSContainer( t_entry );
      else new_entry = new fcMCKContainer( t_entry );
   }
   else switch( t_entry -> Type() )
   {
      case fcSECTION:
         new_entry = new gtkCSEntry( t_entry );
         break;
      case fcBOOL:
         new_entry = new gtkCKBool( t_entry );
         break;
      case fcNUMBER:
         new_entry = new gtkCKNumber( t_entry );
         break;
      case fcLIST:
         new_entry = new fcCKList( t_entry );
         break;
      case fcSTRING:
         new_entry = new gtkCKString( t_entry );
         break;
      default:;
   }
   if( ! new_entry )
   {
      MessageHandler. Error( 
            fcString( "Unable to allocate new config entry " ) +
            t_entry -> GetEntryName() );
   }
   return new_entry;
}
//--------------------------------------------------------------------------
void gtkCSEntry :: Render( GtkWidget* parent_widget,
                           fc_bool create_frame, 
                           fcCSEntry* main_c_section )
{
   DBG_FUNCTION_NAME( "gtkCSEntry", "Render" );
   fcTSEntry* t_section = ( fcTSEntry* ) GetTEntry();
   assert( t_section );
   DBG_COUT( "******* Rendering template section " 
            << t_section -> GetEntryName() 
            << " (" << t_section
            << ") *************" );
   DBG_COUT( "Config section " << GetEntryName() );
   
   // create frame if necesary
   GtkWidget *section_frame;
   if( create_frame )
   {
      section_frame = gtk_frame_new( t_section -> GetLabel(). Data() );
      gtk_container_add( GTK_CONTAINER( parent_widget ),
                         section_frame );
   }
   
   GtkWidget *section_vbox = gtk_vbox_new( FALSE, 0 );
   if( create_frame )
     gtk_container_add( GTK_CONTAINER( section_frame ), section_vbox );
   else
      gtk_container_add( GTK_CONTAINER( parent_widget ), section_vbox );

   GtkTooltips* tooltips = gtk_tooltips_new();

   for( fc_uint i = 0; i < Size(); i ++ )
   {
      fcCEntry* c_entry = ( * this )[ i ];
      const fcTEntry* t_entry = c_entry -> GetTEntry();
      const fc_char* help = t_entry -> GetHelp(). Data();
      if( ! help ) help = no_help;

      if( t_entry -> IsKeyword() )
      {
         DBG_COUT( "------ rendering template entry: " << 
                   t_entry -> GetEntryName() << " ---------" );
         DBG_COUT( "Config entry is " << * c_entry );
         
         switch( t_entry -> Type() )
         {
            case fcBOOL:
               ( ( gtkCKBool* ) c_entry ) -> Render( section_vbox,
                                                     tooltips,
                                                     help, 
                                                     main_c_section );
               break;
            case fcNUMBER:
               ( ( gtkCKNumber* ) c_entry ) -> Render( section_vbox,
                                                       tooltips,
                                                       help,
                                                       main_c_section );
               break;
            case fcSTRING:
               ( ( gtkCKString* ) c_entry ) -> Render( section_vbox,
                                                       tooltips,
                                                       help,
                                                       main_c_section );
               break;
            case fcLIST:
               break;
            default:;
         }
      }
      else
      {
         DBG_EXPR( c_entry -> GetEntryName() );
         DBG_EXPR( t_entry -> GetEntryName() );
         assert( t_entry -> IsSection() );
         assert( c_entry -> IsSection() );
         if( t_entry -> GetMultiple() )
         {
            ( ( gtkMCSContainer* ) c_entry ) -> Render( section_vbox,
                                                        tooltips,
                                                        help,
                                                        main_c_section);
         }
         else
            ( ( gtkCSEntry* ) c_entry ) -> Render( section_vbox, true, main_c_section );
      }
      DBG_COUT( "---------------- rendering entry done --------------------" );
   }
   DBG_COUT( "***************** rendering section done ******************" );
}
//--------------------------------------------------------------------------
//void gtkCSEntry :: RecastingWidgets( fcCSEntry* main_c_section ) {
//    for( fc_int i = 0 ; i < Size() ; i++ ) {
//        fcCEntry* c_entry = (*this)[i];
//        if( c_entry -> Type() == fcBOOL ) {
//            fc_bool isSensitive = main_c_section -> CEntryIsVisible( c_entry );
//            gtk_widget_set_sensitive( ((gtkCKBool*)c_entry) -> GetWidget() , isSensitive );
//        }
//        else if( c_entry -> Type() == fcSECTION ) 
//            ((gtkCSEntry*)c_entry) -> RecastingWidgets( main_c_section );
//    }
//}
