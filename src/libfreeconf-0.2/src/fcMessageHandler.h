/***************************************************************************
                          fcMessageHandler.h  -  description
                             -------------------
    begin                : 2004/10/21
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCMESSAGEHANDLER_H__
#define __FCMESSAGEHANDLER_H__

#include "fcTypes.h"

class fcString;

//! Class for handling messages, warnings and errors
class fcMessageHandler
{
   //! Number of handled messages
   fc_uint messages_num;

   //! Number of handled warnings
   fc_uint warnings_num;

   //! Number of handled errors
   fc_uint errors_num;
   
   public:

   //! Basic constructor
   fcMessageHandler();

   //! Destructor
   virtual ~fcMessageHandler(){};

   //! Method for handling messages
   /*! Returned value is 'true'.
    */
   virtual fc_bool Message( const fc_char* message );
   
   //! Method for handling messages
   /*! Returned value is 'true'.
    */
   virtual fc_bool Message( const fc_char* file, const fc_char* function, const fc_char* message );
   
   //! Method for handling messages
   /*! Returned value is 'true'.
    */
   virtual fc_bool Message( const fc_char* file, fc_uint line, const fc_char* message );
   
   //! Method for handling messages
   /*! Returned value is 'true'.
    */
   virtual fc_bool Message( const fcString& message );

   //! Method for handling messages
   /*! Returned value is 'true'.
    */
   virtual fc_bool Message( const fcString& file, const fcString& function, const fcString& message );

   //! Method for handling messages
   /*! Returned value is 'true'.
    */
   virtual fc_bool Message( const fcString& file, fc_uint line, const fcString& message );

   //! Method for handling warnings
   /*! Returned value is 'true'.
    */
   virtual fc_bool Warning( const fc_char* warning );

   //! Method for handling warnings
   /*! Returned value is 'true'.
    */
   virtual fc_bool Warning( const fc_char* file, const fc_char* function, const fc_char* warning );

   //! Method for handling warnings
   /*! Returned value is 'true'.
    */
   virtual fc_bool Warning( const fc_char* file, fc_uint line, const fc_char* warning );

   //! Method for handling warnings
   /*! Returned value is 'true'.
    */
   virtual fc_bool Warning( const fcString& warning );

   //! Method for handling warnings
   /*! Returned value is 'true'.
    */
   virtual fc_bool Warning( const fcString& file, const fcString& function, const fcString& warning );

   //! Method for handling warnings
   /*! Returned value is 'true'.
    */
   virtual fc_bool Warning( const fcString& file, fc_uint line, const fcString& warning );

   //! Method for handling messages
   /*! Returned value is 'false'.
    */
   virtual fc_bool Error( const fc_char* error );

   //! Method for handling messages
   /*! Returned value is 'false'.
    */
   virtual fc_bool Error( const fc_char* file, const fc_char* function, const fc_char* error );

   //! Method for handling messages
   /*! Returned value is 'false'.
    */
   virtual fc_bool Error( const fc_char* file, fc_uint line, const fc_char* error );

   //! Method for handling messages
   /*! Returned value is 'false'.
    */
   virtual fc_bool Error( const fcString& error );

   //! Method for handling messages
   /*! Returned value is 'false'.
    */
   virtual fc_bool Error( const fcString& file, const fcString& function, const fcString& error );

   //! Method for handling messages
   /*! Returned value is 'false'.
    */
   virtual fc_bool Error( const fcString& file, fc_uint line, const fcString& error );

   //! Messages number getter
   fc_uint GetMessagesNumber();

   //! Warnings number getter
   fc_uint GetWarningsNumber();

   //! Errors number getter
   fc_uint GetErrorsNumber();

   //! Init the message handler
   /*! Reset all counters.
    */
   virtual void Init();
};

extern fcMessageHandler MessageHandler;
#endif
