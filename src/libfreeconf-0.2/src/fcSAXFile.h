/***************************************************************************
                          fcSAXFile.h  -  description
                             -------------------
    begin                : 2004/04/11 11:37
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCSAXFILE_H__
#define __FCSAXFILE_H__

#include <xercesc/sax2/DefaultHandler.hpp>
#include <xercesc/sax2/Attributes.hpp>
XERCES_CPP_NAMESPACE_USE

#include "fcTypes.h"


//! Basic class for reading xml files via SAX Xerces-C
class fcSAXFile : public DefaultHandler
{
   //! Errors appearing during parsing
   fc_uint errors;

   public:
   fcSAXFile();

   ~fcSAXFile();
   
   protected:
   //! Implementations of the SAX ErrorHandler warning
   void warning(const SAXParseException& exception);

   //! Implementations of the SAX ErrorHandler error
   void error(const SAXParseException& exception);
   
   //! Implementations of the SAX ErrorHandler fatal error
   void fatalError(const SAXParseException& exception);

   //! Method for reading xml file
   fc_bool Parse( const fc_char* file_name );

   //! Errors accesor
   fc_uint& Errors() { return errors; };


};

#endif
