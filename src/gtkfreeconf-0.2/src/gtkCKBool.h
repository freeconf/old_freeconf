/***************************************************************************
                          gtkCKBool.h  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef gtkCKBoolH
#define gtkCKBoolH

#include <gtk/gtk.h>
#include <fcCKBool.h>
#include <fcCSEntry.h>

class gtkCKBool;

struct gtkCKBoolStructure {
    gtkCKBool* entry;
    fcCSEntry* main_entry;
};
//--------------------------------------------------------------------------
class gtkCKBool : public fcCKBool
{
   //! Widget for this entry
   GtkWidget* widget; 

   //! Pointer to gtkCKBoolStructure structure
   gtkCKBoolStructure *struc; 

   public:

   //! Basic constructor
   gtkCKBool();

   //! Copy constructor
   gtkCKBool( const fcCKBool& ck_bool );

   //! Constructor template entry pointer
   gtkCKBool( const fcTEntry* t_entry );

   //! Destructor
   ~gtkCKBool();
  
   //! Render the boolean GUI widget
   void Render( GtkWidget* parent_widget,
                GtkTooltips* tooltips,
                const fc_char* help,
                fcCSEntry* main_c_section );

   //! GtkWidget setter
   void SetWidget( GtkWidget* _widget );

   //! GtkWidget getter
   GtkWidget* GetWidget();
};

#endif
