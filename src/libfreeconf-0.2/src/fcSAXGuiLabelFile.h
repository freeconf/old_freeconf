/***************************************************************************
author           : 2009 by David Fabian
***************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/


#ifndef _FCSAXGUIHELPFILE_H_
#define _FCSAXGUIHELPFILE_H_

#include "fcSAXFile.h"
#include "fcStack.h"

class fcCGSEntry;
class fcGTab;

class fcSAXGuiLabelFile : public fcSAXFile
{
  private:
    enum Element
    {
      NoElement,
      RootElement,
      Window,
      Title,
      Tab,
      TabName,
      TabLabel,
      TabDescription,
      TabSection,
      TabSectionName,
      TabSectionLabel
    } _currentElement;
    
    fcGWindow *_window;
    
    fc_bool _rootElementOpened;
    fc_bool _sectionElementOpened;
    fcGTab* _currentTab;
    
    fcStack<fcCGSEntry*> _sectionStack;
    
  protected:

    void startElement(const XMLCh* const uri,
                      const XMLCh* const localName,
                      const XMLCh* const qName,
                      const Attributes& attributes);

    void endElement(const XMLCh* const uri,
                    const XMLCh* const localName,
                    const XMLCh* const qName);


   void characters(const XMLCh* const chars, const unsigned int length);
   
  public:
    
    fcSAXGuiLabelFile();
    
    ~fcSAXGuiLabelFile();

    fc_bool ParseGuiLabelFile(const fc_char* fileName, fcGWindow* window);
                           
};

#endif
