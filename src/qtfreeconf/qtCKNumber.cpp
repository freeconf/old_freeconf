#include <fcTEntry.h>
#include <fcDOMConfigFile.h>
#include <fcPackage.h>
#include <fcTKNumber.h>
#include <fcCKNumber.h>
#include <fcCGEntry.h>

#include <QDoubleSpinBox>
#include <QLabel>
#include <QDebug>
#include <QBoxLayout>
#include <QHBoxLayout>

#include "qtCSEntry.h"
#include "qtCKNumber.h"

qtCKNumber::qtCKNumber (fcPackage *package, const qtCWidget *parent)
  :qtCWidget(package, parent),
  _spin(0)
{
  
}


qtCKNumber::qtCKNumber (const fcCGEntry *guiEntry, fcPackage *package, const qtCWidget *parent)
  :qtCWidget(guiEntry, package, parent),
  _spin(0)
{
  
}


void qtCKNumber::render ()
{
  if (!_parent || !_parent->fcLayout() || !_guiEntry || !_guiEntry->configBuddy())
  {
    qDebug() << "Widget's parent, config model and layout must be set before render";
    return;
  }
  
  const fcTKNumber *tEntry = dynamic_cast<const fcTKNumber*>(_guiEntry->configBuddy()->GetTEntry());
  if (!tEntry)
  {
    qDebug() << "No template entry associated";
    return;
  }
  
  QHBoxLayout *hLayout = new QHBoxLayout();
  QLabel *label = new QLabel(tEntry->GetLabel().Data());
  if (!_spin) _spin = new QDoubleSpinBox();
  _spin->setRange(tEntry->GetMin(), tEntry->GetMax());
  _spin->setSingleStep(tEntry->GetStep());
  _spin->setDecimals(4);
  _spin->setValue(dynamic_cast<fcCKNumber*>(_guiEntry->configBuddy())->GetValue());
  label->setToolTip(tEntry->GetHelp().Data());
  label->setBuddy(_spin);
  hLayout->addWidget(label);
  hLayout->addWidget(_spin);
  
  _parent->fcLayout()->addLayout(hLayout);

  if (_package)
      _guiEntry->configBuddy()->GetCEntryAction(_package->configFile()->GetConfigSection());
  //  _spin->setEnabled(_guiEntry->configBuddy()->CEntryIsActive(_package->mainCSection()));
  //   compileEntry( isSensitive, current_entry );

  _rendered = true;
  
  attach(_attached);
  //connect(spin, SIGNAL(valueChanged(double)), this, SLOT(valueChanged(double)));
}


void qtCKNumber::valueChanged (double value)
{
  dynamic_cast<fcCKNumber*>(_guiEntry->configBuddy())->SetValue(value);
/*  for (fc_int i = 0; i < ((fcCEntry*)(this))->GetDependentsSize(); i++)
{
  fcCEntry *c_entry = ((fcCEntry*)(this))->GetDependents(i);
  fc_bool isSensitive =  c_entry->CEntryIsActive(_mainCSection);*/
//     compileEntry( isSensitive, c_entry );
//   }
}


void qtCKNumber::invalidateGui()
{
  if (_spin) delete _spin;
  _spin = 0;
}


void qtCKNumber::attach (fc_bool attached)
{
  if (!_guiEntry->configBuddy()) return;
  qtCWidget::attach(attached);
  
  if (!attached)
  {
    if (!_spin) return;
    disconnect(_spin, SIGNAL(valueChanged(double)), 0, 0);
  }
  else
  {
    if (!_spin) return;
    connect(_spin, SIGNAL(valueChanged(double)), this, SLOT(valueChanged(double)));
    dynamic_cast<fcCKNumber*>(_guiEntry->configBuddy())->SetValue(_spin->value());
  }
}

                        
