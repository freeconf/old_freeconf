/***************************************************************************
                          gtk_render.h  -  description
                             -------------------
    begin                : 2004/06/07 14:34
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __GTK_RENDER_H__
#define __GTK_RENDER_H__

class fcHeaderStructure;

//! Function for rendering GUI via GTK library
fc_bool Gtk_Render( const fcTSEntry* t_section,
                    const fcHeaderStructure* header_structure,
                    fcDOMConfigFile *fc_config_file );

#endif
