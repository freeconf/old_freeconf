#include <fcTEntry.h>
#include <fcTKBool.h>
#include <fcCKBool.h>
#include <fcCGEntry.h>
#include <fcDOMConfigFile.h>
#include <fcPackage.h>
#include <QDebug>
#include <QBoxLayout>
#include <QCheckBox>

#include "qtCKBool.h"
#include "qtCSEntry.h"

qtCKBool::qtCKBool (fcPackage *package, const qtCWidget *parent)
  :qtCWidget(package, parent),
  _check(0)
{

}


qtCKBool::qtCKBool (const fcCGEntry *guiEntry, fcPackage *package, const qtCWidget *parent)
  :qtCWidget(guiEntry, package, parent),
  _check(0)
{
  
}


void qtCKBool::render ()
{
  if (!_parent || !_parent->fcLayout() || !_guiEntry || !_guiEntry->configBuddy())
  {
    qDebug() << "Widget's parent, config model and layout must be set before render";
    return;
  }
  
  const fcTEntry *tEntry = _guiEntry->configBuddy()->GetTEntry();
  if (!tEntry)
  {
    qDebug() << "No template entry associated";
    return;
  }
  
  if (!_check) _check = new QCheckBox();
  _check->setText(tEntry->GetLabel().Data());
  _check->setToolTip(tEntry->GetHelp().Data());
  _parent->fcLayout()->addWidget(_check);

  if(dynamic_cast<fcCKBool*>(_guiEntry->configBuddy())->GetValue())
    _check->setCheckState(Qt::Checked);
  else
    _check->setCheckState(Qt::Unchecked);

  if (_package)
      _guiEntry->configBuddy()->GetCEntryAction(_package->configFile()->GetConfigSection());
//    _check->setEnabled(_guiEntry->configBuddy()->CEntryIsActive(_package->mainCSection()));
//   compileEntry( isSensitive, current_entry );
  _rendered = true;
  attach(_attached);
}


void qtCKBool::stateChanged (int state)
{
  if (!_guiEntry->configBuddy()) return;
  
  if (state == Qt::Checked)
    dynamic_cast<fcCKBool*>(_guiEntry->configBuddy())->SetValue(true);
  else
    dynamic_cast<fcCKBool*>(_guiEntry->configBuddy())->SetValue(false);
  
/*  for (fc_int i = 0; i < ((fcCEntry*)(this))->GetDependentsSize(); i++)
  {
    fcCEntry *c_entry = ((fcCEntry*)(this))->GetDependents(i);
    fc_bool isSensitive =  c_entry->CEntryIsActive(_mainCSection);*/
//     compileEntry( isSensitive, c_entry );
//   }
}


void qtCKBool::invalidateGui()
{
  if (_check) delete _check;
  _check = 0;
}


void qtCKBool::attach (fc_bool attached)
{
  if (!_guiEntry->configBuddy()) return;
  qtCWidget::attach(attached);

  if (!_check) return;
  if (!attached)
  {
    disconnect(_check, SIGNAL(stateChanged(int)), 0, 0);
  }
  else
  {
    connect(_check, SIGNAL(stateChanged(int)), this, SLOT(stateChanged(int)));
    dynamic_cast<fcCKBool*>(_guiEntry->configBuddy())->SetValue(_check->checkState() == Qt::Checked);
  }
}

                                                   
