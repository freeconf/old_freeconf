/***************************************************************************
                          fcCKBool.cpp  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "fcCKBool.h"

//--------------------------------------------------------------------------
fcCKBool :: fcCKBool()
{
}
//--------------------------------------------------------------------------
fcCKBool :: fcCKBool( const fcCKBool& entry  )
   : fcCKEntry( entry ),
     value( entry. value )
{
}
//--------------------------------------------------------------------------
fcCKBool :: ~fcCKBool()
{
}
//--------------------------------------------------------------------------
fcBaseEntry* fcCKBool :: NewEntry() const
{
   return new fcCKBool( * this );
}
//--------------------------------------------------------------------------
Fc::EntryTypes fcCKBool :: Type() const
{ 
   return Fc::Bool;
}
//--------------------------------------------------------------------------
fc_bool fcCKBool :: GetValue() const 
{ 
   return value;
}
//--------------------------------------------------------------------------
void fcCKBool :: SetValue( bool val )
{
   value = val;
   value_set = true;
}
//--------------------------------------------------------------------------
#ifdef DEBUG
void fcCKBool :: Print( ostream& stream ) const
{
   stream << "BOOL " << GetEntryName() << " = " << value;
}
#endif
