/***************************************************************************
                          fcSectionEntry.h  -  description
                             -------------------
    begin                : 2004/08/14
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCSECTIONENTRY_H__
#define __FCSECTIONENTRY_H__

#include "fcList.h"
#include "fcBaseEntry.h"

class fcSectionEntry : public fcList< fcBaseEntry* >
{
   public:

   //! Basic constructor
   fcSectionEntry(){};

   //! Copy constructor
   fcSectionEntry( const fcSectionEntry& section_entry );
   // TODO: make it const

   //! Constructor
   virtual ~fcSectionEntry();
   
   //! Find entry with given name starting at given position
   /*! If the entry is found its 'pos' is set to its position
    */
   fcBaseEntry* FindEntry( const fc_char* name,
                           fc_uint& pos );
   
   //! Find entry with given name
   fcBaseEntry* FindEntry( const fc_char* name );
  
   //! Erase all entries with given name starting at given position
   /*void EraseEntries( const fc_char* name,
                      fc_uint pos = 0 );*/

   //! Find entry with given name starting at given position for constant instances
   const fcBaseEntry* FindEntry( const fc_char* name,
                                 fc_uint& pos ) const;
   
   //! Find entry with given name for constant instances
   const fcBaseEntry* FindEntry( const fc_char* name ) const;
   
#ifdef DEBUG
   //! Print this section
   void Print( ostream& stream ) const; 
   // TODO: make it const
#endif
};

#ifdef DEBUG
inline ostream& operator << ( ostream& stream, const fcSectionEntry& entry )
   // TODO: make it const ( fcBaseEntry& )
{
   entry. Print( stream );
   return stream;
}
#endif

#endif
