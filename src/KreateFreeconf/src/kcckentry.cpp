/***************************************************************************
 *   Copyright (C) 2009 by Jan Ehrenberger   *
 *   lankvil@seznam.cz   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "kcckentry.h"

kcCKEntry::kcCKEntry(PackageTreeView *tree, const QString &name, const QString &type)
    :PackageTreeViewItem(tree, name)
{
    m_type = new kcCKEntryProperty(this, "Type", type, 1);
    m_label = new kcCKEntryText(this, "Label", "", 2);
    m_description = new kcCKEntryText(this, "Description", "", 3);
    setOpen(true);
}

kcCKEntry::kcCKEntry(kcCKEntry *section, const QString &name, const QString &type)
    :PackageTreeViewItem(section, name)
{
    m_type = new kcCKEntryProperty(this, "Type", type, 1);
    m_label = new kcCKEntryText(this, "Label", "", 2);
    m_description = new kcCKEntryText(this, "Description", "", 3);
    setOpen(true);
}

kcCKEntry::~kcCKEntry()
{
}

bool kcCKEntry::isRenameable(int column)
{
    if(column == 0) // Enable renaming of the first column(that's where name of the entry is)
        return true;
    return false;
}


bool kcCKEntry::dropTextEvent(const QString &text)
{
    if(m_type->getValue() != "section")
        return false;
    // Drop text event on section will create new entry in this section
    kcCKEntry *entry = new kcCKEntry(this, text, "string");
}

//////// kcCKEntryProperty ////////
kcCKEntryProperty::kcCKEntryProperty(kcCKEntry *entry, const QString &name, const QString &value, int order)
    :PackageTreeViewItem(entry, name, value), m_order(order)
{
}

kcCKEntryProperty::~kcCKEntryProperty()
{
}

QString kcCKEntryProperty::getName() const
{
    return text(0);
}

QString kcCKEntryProperty::getValue() const
{
    return text(1);
}

void kcCKEntryProperty::setValue(const QString &value)
{
    setText(1, value);
}

bool kcCKEntryProperty::isRenameable(int column)
{
    if(column == 1) // Enable renaming of the second column(that's where value of the property value is)
        return true;
    return false;
}


//////// kcCKEntryText ////////
kcCKEntryText::kcCKEntryText(kcCKEntry *entry, const QString &name, const QString &value, int order)
    :kcCKEntryProperty(entry, name, value, order)
{
}

kcCKEntryText::~kcCKEntryText()
{
}

bool kcCKEntryText::dropTextEvent(const QString &text)
{
    setValue(text);
}
