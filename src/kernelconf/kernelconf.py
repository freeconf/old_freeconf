#!/usr/bin/python
#
# Kernel Config
#
# begin: 27.10.2008 by Jan Ehrenberger <lankvil@seznam.cz>
#
# Parse kernel configuration files and create freeconf package
#
# Arguments: [kernel_src]
#    kernel_src - Location of kernel source code. Default directory is /usr/src/linux directory
#

import kcPackage

try:
	pkg = kcPackage.Package('/usr/src/linux', 'i386')
except IOError, e:
	pkg = kcPackage.Package('/usr/src/linux', 'x86')
pkg.write()

