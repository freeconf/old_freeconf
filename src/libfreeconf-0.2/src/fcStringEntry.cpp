/***************************************************************************
                          fcStringEntry.cpp  -  description
                             -------------------
    copyright            : (C) 2009 by Frantisek Rolinek
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "fcStringEntry.h"

//-------------------------------------------------------------
fcStringEntry :: fcStringEntry() 
{
}
//-------------------------------------------------------------
fcStringEntry :: fcStringEntry(const fc_char* strValue, const fc_char* strHelp/*=0*/, const fc_char* strLabel/*=0*/)
{
    m_strValue = strValue;
    m_strHelp  = strHelp;
    m_strLabel = strLabel;
}
//-------------------------------------------------------------
fcStringEntry :: fcStringEntry(const fcString& strValue, const fcString& strHelp, const fcString& strLabel)
{
    m_strValue = strValue;
    m_strHelp  = strHelp;
    m_strLabel = strLabel;
}
//-------------------------------------------------------------
fcStringEntry :: fcStringEntry(const fcStringEntry& item)
{
    m_strValue = item.m_strValue;
    m_strHelp  = item.m_strHelp;
    m_strLabel = item.m_strLabel;
}
//-------------------------------------------------------------
fcStringEntry :: ~fcStringEntry()
{
}
//-------------------------------------------------------------
const fcStringEntry& fcStringEntry :: operator=(const fcStringEntry& item)
{
    m_strValue = item.m_strValue;
    m_strHelp  = item.m_strHelp;
    m_strLabel = item.m_strLabel;

    return *this;
}
//-------------------------------------------------------------
void fcStringEntry :: SetValue(const fc_char* strValue)
{
    m_strValue = strValue;
}
//-------------------------------------------------------------
void fcStringEntry :: SetValue(const fcString& strValue)
{
    m_strValue = strValue;
}
//-------------------------------------------------------------
void fcStringEntry :: SetHelp(const fc_char* strHelp)
{
    m_strHelp = strHelp;
}
//-------------------------------------------------------------
void fcStringEntry :: SetHelp(const fcString& strHelp)
{
    m_strHelp = strHelp;
}
//-------------------------------------------------------------
void fcStringEntry :: SetLabel(const fc_char* strLabel)
{
    m_strLabel = strLabel;
}
//-------------------------------------------------------------
void fcStringEntry :: SetLabel(const fcString& strLabel)
{
    m_strLabel = strLabel;
}
//-------------------------------------------------------------
const fcString& fcStringEntry :: GetValue() const
{
    return m_strValue;
}
//-------------------------------------------------------------
const fcString& fcStringEntry :: GetHelp() const
{
    return m_strHelp;
}
//-------------------------------------------------------------
const fcString& fcStringEntry :: GetLabel() const
{
    return m_strLabel;
}
//-------------------------------------------------------------
