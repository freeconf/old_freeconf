/***************************************************************************
                          gtk_render.cpp  -  description
                             -------------------
    begin                : 2004/06/07 14:38
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <gtk/gtk.h>
#include <fcDOMConfigFile.h>
#include <fcTransformer.h>
#include <fcHeaderStructure.h>
#include <fcTSEntry.h>
#include "debug.h"
#include "gtkCSEntry.h"
#include "gtk_render.h"

struct gtkCbApplyInfo
{
   const fcHeaderStructure* header_structure;
   fcDOMConfigFile* config_file;
};

static gint cb_cancel( GtkWidget *widget,
                       GdkEvent *event,
                       void* info )
{
   gtk_main_quit();
   return true;
}

static gint cb_apply( GtkWidget *widget,
                      GdkEvent *event,
                      gtkCbApplyInfo* info )
{
   DBG_FUNCTION_NAME( "", "gtk_apply" );
   fcDOMConfigFile* config_file = info -> config_file;
   const fcHeaderStructure* header_structure = 
      info -> header_structure;
   DBG_EXPR( * config_file );

   std::cout << "Writing config file [" << header_structure -> GetOutputFile().Data() << "] ..." << std::endl;
   config_file -> Write( header_structure -> GetOutputFile().Data() );   
   fcTransformer transformer;
   std::cout << "Transforming config file [" << header_structure -> GetOutputFile().Data() << "] "
             << "to native format [" <<  header_structure -> GetNativeFile(). Data() << "] ..." << std::endl;
   transformer. Transform( 
         header_structure -> GetOutputFile(). Data(),
         header_structure -> GetTransformFile(). Data(),
         header_structure -> GetNativeFile(). Data() );
   gtk_main_quit();
   return true;
}

fc_bool Gtk_Render( const fcTSEntry* t_section,
                    const fcHeaderStructure* header_structure,
                    fcDOMConfigFile* fc_config_file )
{
   DBG_FUNCTION_NAME( "", "Gtk_Render" );
   std::cout << "GTK renderer" << std::endl;
 
   GtkWidget *window;

   // Create a new window 
   window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

   gtk_window_set_title (GTK_WINDOW (window), "GTK client for FreeConf");

   // It's a good idea to do this for all windows.
   gtk_signal_connect( GTK_OBJECT( window ),
                       "destroy",
                       GTK_SIGNAL_FUNC( gtk_main_quit ),
                       NULL);

   gtk_signal_connect( GTK_OBJECT(window),
                       "delete_event",
                       GTK_SIGNAL_FUNC( gtk_main_quit ),
                       NULL );
   
   // Sets the border width of the window. 
   gtk_container_set_border_width (GTK_CONTAINER (window), 0);
  
   // Create new vbox for menu
   GtkWidget *main_vbox = gtk_vbox_new( false, 5 );
   gtk_container_add( GTK_CONTAINER( window ), main_vbox );

   // Create new vbox for workspace
   GtkWidget *work_vbox = gtk_vbox_new( false, 5 );
   gtk_container_set_border_width( GTK_CONTAINER( work_vbox ), 10 );
   gtk_container_add( GTK_CONTAINER( main_vbox ), work_vbox );

   // Separator
   GtkWidget *sep0 = gtk_hseparator_new();
   gtk_box_pack_start( GTK_BOX( work_vbox ), sep0, false, false, false );

   // Get the first template section the label of which is used for a title
   const fcTSEntry* first_t_section = ( const fcTSEntry* ) ( *t_section )[ 0 ];
   assert( first_t_section );
   GtkWidget *label = gtk_label_new( first_t_section -> GetLabel(). Data() );
   gtk_box_pack_start( GTK_BOX( work_vbox ), label, false, false, false );

   // Separator
   GtkWidget *sep1 = gtk_hseparator_new();
   gtk_box_pack_start( GTK_BOX( work_vbox ), sep1, false, false, false );

   // Scrolled window for main configuration rendering
   GtkWidget *scrolled_window = gtk_scrolled_window_new( NULL, NULL );
   gtk_scrolled_window_set_policy( GTK_SCROLLED_WINDOW( scrolled_window ), 
                                   GTK_POLICY_NEVER,
                                   GTK_POLICY_AUTOMATIC );
   gtk_box_pack_start( GTK_BOX( work_vbox ), scrolled_window, true, true, false );
   
   // Separator
   GtkWidget *sep2 = gtk_hseparator_new();
   gtk_box_pack_start( GTK_BOX( work_vbox ), sep2, false, false, false );

   // Two buttons at the bottom OK and Cancel
   GtkWidget *buttons = gtk_hbutton_box_new( );
   gtk_hbutton_box_set_spacing_default( 10 );
   gtk_hbutton_box_set_layout_default( GTK_BUTTONBOX_SPREAD );
   gtk_box_pack_start( GTK_BOX( work_vbox ), buttons, false, false, false );
 
   GtkWidget *ok_button = gtk_button_new();
   GtkWidget *ok_label = gtk_label_new( "OK" );
   gtk_container_add( GTK_CONTAINER( ok_button ), ok_label );
   
   GtkWidget *cancel_button = gtk_button_new();
   GtkWidget *cancel_label = gtk_label_new( "Cancel" );
   gtk_container_add( GTK_CONTAINER( cancel_button ), cancel_label );
   gtkCbApplyInfo info;
   info. config_file = fc_config_file;
   info. header_structure = header_structure;
   gtk_signal_connect( GTK_OBJECT( cancel_button ),
                       "button_press_event",
                       GTK_SIGNAL_FUNC( cb_cancel ),
                       0 );
   gtk_signal_connect( GTK_OBJECT( ok_button ),
                       "button_press_event",
                       GTK_SIGNAL_FUNC( cb_apply ),
                       &info );
   gtk_container_add( GTK_CONTAINER( buttons ), ok_button );
   gtk_container_add( GTK_CONTAINER( buttons ), cancel_button );

   // New vbox into scrolled window
   GtkWidget* window_vbox = gtk_vbox_new( false, 0 );
   gtk_scrolled_window_add_with_viewport( GTK_SCROLLED_WINDOW( scrolled_window ), window_vbox );


   DBG_EXPR( * fc_config_file );
   gtkCSEntry* first_c_section = ( gtkCSEntry* ) 
      ( * fc_config_file -> GetConfigSection() )[ 0 ];
   assert( first_c_section );
   fcCSEntry* main_c_section = fc_config_file -> GetConfigSection();
   first_c_section -> Render( window_vbox, false , main_c_section );
   // Gtk_Render_Section( first_c_section, window_vbox, false );

   gtk_widget_show_all( window );

   gtk_main();
  
   return 1;
}
