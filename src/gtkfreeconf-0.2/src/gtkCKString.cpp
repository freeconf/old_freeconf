/***************************************************************************
                          gtkCKString.cpp  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <gtk/gtk.h>
#include <fcTEntry.h>
#include <fcTKString.h>
#include "gtkCKString.h"
#include "debug.h"
#include "widget.h"

//--------------------------------------------------------------------------
static void cb_string_set( GtkWidget *widget,
                           gtkCKStringStructure* struc )
{
   DBG_FUNCTION_NAME( "", "cb_string_set" );
   assert( struc -> entry -> Type() == fcSTRING );
   DBG_EXPR( struc -> entry -> GetEntryName() );
   DBG_EXPR( struc -> entry -> GetValue() );
   const fc_char *entry_text;
   entry_text = gtk_entry_get_text( GTK_ENTRY( widget ) );
   DBG_COUT( "STRING value set to '" << entry_text << "'" );
   (struc -> entry) -> SetValue( entry_text );
   for( fc_int i = 0; i < ((fcCEntry*)(struc->entry))->GetDependentsSize() ; i++) {
       fcCEntry* c_entry = ((fcCEntry*)(struc->entry))->GetDependents( i );
       fc_bool isSensitive =  c_entry -> CEntryIsActive( struc->main_entry );
       compileEntry( isSensitive, c_entry );
   }
}
//--------------------------------------------------------------------------
gtkCKString :: gtkCKString()
   : g_strings( 0 )
{
    struc = new gtkCKStringStructure();
}
//--------------------------------------------------------------------------
gtkCKString :: gtkCKString( const gtkCKString& entry  )
   : fcCKString( entry ),
     g_strings( 0 )
{
    struc = new gtkCKStringStructure();
}
//--------------------------------------------------------------------------
gtkCKString :: gtkCKString( const fcTEntry* t_entry )
   : fcCKString( t_entry ),
     g_strings( 0 )
{
    struc = new gtkCKStringStructure();
}
//--------------------------------------------------------------------------
gtkCKString :: ~gtkCKString()
{
   if( g_strings ) g_list_free( g_strings );
   if( struc ) delete struc;
}
//--------------------------------------------------------------------------
void gtkCKString :: Render( GtkWidget* parent_widget,
                            GtkTooltips* tooltips,
                            const fc_char* help,
                            fcCSEntry* main_c_section )
{
   DBG_FUNCTION_NAME( "gtkCKString", "Render" );

   GtkWidget* hbox = gtk_hbox_new( false, 0 );
   gtk_box_pack_start( GTK_BOX( parent_widget ),
                       hbox,
                       false,
                       false,
                       0 );

   const fcTKString& t_entry = * ( fcTKString* ) GetTEntry();
   const fc_char* label = t_entry. GetLabel(). Data();
   GtkWidget* label_widget = gtk_label_new( label );
   gtk_label_set_justify( GTK_LABEL( label_widget ),
                          GTK_JUSTIFY_LEFT );
   
   const fc_char* text = GetValue(). Data();
   
   struc -> entry = this;
   struc -> main_entry = main_c_section;

   fcCEntry* current_entry = (fcCEntry*)(this);
   fc_bool isSensitive = current_entry -> CEntryIsActive( main_c_section );

   GtkWidget* value_widget;
   if( t_entry. Size() ) 
   {
      value_widget = gtk_combo_new();

      this -> isCombo = true;
      
      for( fc_uint i = 0; i < t_entry. Size(); i ++ )
      {
         fc_char* str = const_cast< fc_char* > (
                  t_entry. GetString( i ). GetValue(). Data() );
         DBG_EXPR( str );
         DBG_EXPR( g_strings );
         g_strings = g_list_append( g_strings, str );
         DBG_COUT( "Adding to combo box: " <<  str );
      }

      gtk_combo_set_popdown_strings( GTK_COMBO( value_widget ),
                                     g_strings );

      gtk_entry_set_text( 
            GTK_ENTRY( GTK_COMBO( value_widget ) -> entry ),
            text );

      this -> SetWidget( value_widget );
      compileEntry( isSensitive, current_entry );

      gtk_signal_connect( GTK_OBJECT( GTK_COMBO( value_widget ) -> entry ),
                          "changed",
                          GTK_SIGNAL_FUNC ( cb_string_set ),
                          struc );
   }
   else
   {
      value_widget = gtk_entry_new();
      this -> isCombo = false;
      gtk_entry_set_text( GTK_ENTRY( value_widget ),
                          text );

      this -> SetWidget( value_widget );
      compileEntry( isSensitive, current_entry );
      
      gtk_signal_connect( GTK_OBJECT( value_widget ),
                          "changed",
                          GTK_SIGNAL_FUNC( cb_string_set ),
                          struc );
   }

   gtk_box_pack_start( GTK_BOX( hbox ),
                       label_widget,
                       false,
                       false,
                       10 );
   
   gtk_box_pack_start( GTK_BOX( hbox ),
                       value_widget,
                       true,
                       true,
                       10 );
   
   // Set tooltips
   DBG_COUT( " setting tooltips to ... " << help );
   gtk_tooltips_set_tip( tooltips,
                         value_widget,
                         help,
                         NULL );
}
//--------------------------------------------------------------------------
void gtkCKString :: SetWidget( GtkWidget* _widget ) {
    widget = _widget;
}
//--------------------------------------------------------------------------
GtkWidget* gtkCKString :: GetWidget() {
    return widget;
}
//--------------------------------------------------------------------------
fc_bool gtkCKString :: GetIsCombo() {
    return isCombo;
}
