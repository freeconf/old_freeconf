/***************************************************************************
                          fcCKNumber.cpp  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "fcCKNumber.h"

//--------------------------------------------------------------------------
fcCKNumber :: fcCKNumber()
   : value( 0 )
{
}
//--------------------------------------------------------------------------
fcCKNumber :: fcCKNumber( const fcCKNumber& entry  )
   : fcCKEntry( entry ),
     value( entry. value )
{
}
//--------------------------------------------------------------------------
fcCKNumber :: ~fcCKNumber()
{
}
//--------------------------------------------------------------------------
fcBaseEntry* fcCKNumber :: NewEntry() const
{
   return new fcCKNumber( * this );
}
//--------------------------------------------------------------------------
Fc::EntryTypes fcCKNumber :: Type() const
{ 
   return Fc::Number;
}
//--------------------------------------------------------------------------
const fc_real& fcCKNumber :: GetValue() const 
{ 
   return value;
}
//--------------------------------------------------------------------------
void fcCKNumber :: SetValue( fc_real val )
{
   value = val;
   value_set = true;
}
//--------------------------------------------------------------------------
/*void fcCKNumber :: UpdateDOMNode( fcDOMNode* parent_node,
                                  fcDOMDocument* doc )
{
   assert( 0 );
}*/
//--------------------------------------------------------------------------
#ifdef DEBUG
void fcCKNumber :: Print( ostream& stream ) const
{
   stream << "NUMBER " << GetEntryName() << " = " << value;
}
#endif
