/***************************************************************************
                          fcSectionEntry.cpp  -  description
                             -------------------
    begin                : 2004/08/14
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "debug.h"
#include "fcCEntry.h"
#include "fcSectionEntry.h"

//--------------------------------------------------------------------------
fcSectionEntry :: fcSectionEntry( const fcSectionEntry& section_entry )
   : fcList< fcBaseEntry* >()
{
   fc_uint size = section_entry. Size();
   for( fc_uint i = 0; i < size; i ++ )
   {
     Append( section_entry[ i ] -> NewEntry () );
   }
}
//--------------------------------------------------------------------------
fcSectionEntry :: ~fcSectionEntry()
{
  DeepEraseAll();
}
//---------------------------------------------------------------------------
fcBaseEntry* fcSectionEntry :: FindEntry( const fc_char* name,
                                          fc_uint& pos )
{
   DBG_FUNCTION_NAME( "fcSectionEntry", "FindEntry" );
   DBG_COUT( "Looking for entry " << name );
   DBG_EXPR( Size() );
   DBG_EXPR( pos );
   for( fc_int i = pos; i < Size(); i ++ )
   {
      fcBaseEntry* entry = (*this)[i];
      DBG_EXPR( entry -> GetEntryName() );
      if( entry -> GetEntryName() == name ) 
      {
         DBG_COUT( "Entry found." );
         pos = i;
         return entry;
      }
   }
   return NULL;
}
//---------------------------------------------------------------------------
fcBaseEntry* fcSectionEntry :: FindEntry( const fc_char* name )
{
   fc_uint pos = 0;
   return FindEntry( name, pos );
}
//---------------------------------------------------------------------------
/*void fcSectionEntry :: EraseEntries( const fc_char* name,
                                     fc_uint pos )
{
   DBG_FUNCTION_NAME( "fcSectionEntry", "EraseEntries" );
   for( fc_uint i = pos; i < Size(); i ++ )
   {
      fcBaseEntry* entry = operator[]( i );
      if( entry -> GetEntryName() == name )
         EraseEntry( i );
   }
}*/
//---------------------------------------------------------------------------
const fcBaseEntry* fcSectionEntry :: FindEntry( const fc_char* name,
                                                fc_uint& pos ) const{ 
   return const_cast< fcSectionEntry* >( this ) ->
      FindEntry( name, pos );
}
//---------------------------------------------------------------------------
const fcBaseEntry* fcSectionEntry :: FindEntry( const fc_char* name ) const
{
   return const_cast< fcSectionEntry* >( this ) ->
      FindEntry( name );
}
//---------------------------------------------------------------------------
#ifdef DEBUG
void fcSectionEntry :: Print( ostream& stream ) const
{
   stream << " Section at: " << this << "  size: " << Size() << endl;
   for( fc_int i = 0; i < Size(); i ++ )
   {
      operator[]( i ) -> Print( stream );
      stream << endl;
   }
}
#endif
