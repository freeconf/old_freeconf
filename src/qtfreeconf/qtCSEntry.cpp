#include <fcPackage.h>
#include <fcCGEntry.h>
#include <fcCGSEntry.h>
#include <vector>

#include <QGroupBox>
#include <QFrame>
#include <QVBoxLayout>
#include <QDebug>

#include "qtCSEntry.h"
#include "qtCSRootEntry.h"
#include "qtCKBool.h"
#include "qtCKNumber.h"
#include "qtCKString.h"
#include "qtMCSContainer.h"


qtCSEntry::qtCSEntry (fcPackage *package, const qtCWidget *parent)
  :qtCWidget(package, parent),
   _frameLess(false)
{
  _children.clear();
}


qtCSEntry::qtCSEntry (const fcCGEntry *guiEntry, fcPackage *package, const qtCWidget *parent)
  :qtCWidget(guiEntry, package, parent),
   _frameLess(false)
{
  fillChildren();
}


void qtCSEntry::fillChildren()
{
  // smazat podstrom
  if (_children.size())
  {
    destroyChildren();
  }
  _children.clear();
  if (_guiEntry)
  {
    const fcCGSEntry *tempEntry = dynamic_cast<const fcCGSEntry*>(_guiEntry);
    fcCGEntry *tempGuiEntry = 0;
    tempEntry->skipToFirst();
    while ((tempGuiEntry = tempEntry->next()) != 0)
    {
      qtCWidget *newEntry = 0;
      switch (tempGuiEntry->type())
      {
        case Fc::Section:
          newEntry = new qtCSEntry(tempGuiEntry, _package, this);
          break;
        case Fc::Bool:
          newEntry = new qtCKBool(tempGuiEntry, _package, this);
          break;
        case Fc::Number:
          newEntry = new qtCKNumber(tempGuiEntry, _package, this);
          break;
        case Fc::List:
          //newEntry = new fcCKList();
          break;
        case Fc::String:
          newEntry = new qtCKString(tempGuiEntry, _package, this);
          break;
        default:;
       }
       if (newEntry)
       {
         _children.push_back(newEntry);
       }
       else
       {
         qDebug() << "Unknown config type";
       }
    }
  } 
}


void qtCSEntry::destroyChildren()
{
  for (std::vector<qtCWidget*>::iterator iter = _children.begin(); iter != _children.end(); iter++)
  {
    delete *iter;
  }
}


qtCSEntry::~qtCSEntry ()
{
  destroyChildren();
}


int qtCSEntry::numberOfChildren() const
{
  return _children.size();
}


void qtCSEntry::setGuiEntry (fcCGEntry *guiEntry)
{
  _guiEntry = guiEntry;
  fillChildren();
}


void qtCSEntry::render ()
{
  if (!_parent || !_parent->fcLayout() || !_guiEntry)
  {
    qDebug() << "Widget's parent, config model and layout must be set before render";
    return;
  }
  
  _layout = new QVBoxLayout();

  
  if (_frameLess)
  {
    QFrame *sectionFrame = new QFrame();
    sectionFrame->setLayout(_layout);
    _parent->fcLayout()->addWidget(sectionFrame);
  }
  else
  {
    //QGroupBox *sectionFrame = new QGroupBox(_guiEntry->label().Data());
    QGroupBox *sectionFrame = new QGroupBox(_guiEntry->label().Data());
    sectionFrame->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
    sectionFrame->setLayout(_layout);
    _parent->fcLayout()->addWidget(sectionFrame);
  }
  renderChildren();
}


void qtCSEntry::renderChildren ()
{
  for (std::vector<qtCWidget*>::iterator iter = _children.begin(); iter != _children.end(); iter++)
    (*iter)->render();
}


void qtCSEntry::invalidateGui()
{
  for (std::vector<qtCWidget*>::iterator iter = _children.begin(); iter != _children.end(); iter++)
    (*iter)->invalidateGui();
}


void qtCSEntry::setFrameLess (fc_bool frameLess)
{
  _frameLess = frameLess;
}


fc_bool qtCSEntry::frameLess () const
{
  return _frameLess;
}

void qtCSEntry::attach (fc_bool attached)
{
  if (!_guiEntry->configBuddy()) return;
  qtCWidget::attach(attached);

/*  for(fc_int i = 0; i < Size(); i ++)
  {
    fcCEntry *cEntry = (*this)[i];
    dynamic_cast<qtCWidget*>(cEntry)->attach(this->attached());
  }*/
}


