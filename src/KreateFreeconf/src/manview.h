/***************************************************************************
 *   Copyright (C) 2009 by Jan Ehrenberger   *
 *   lankvil@seznam.cz   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef MANVIEW_H
#define MANVIEW_H

#include <ktabwidget.h>
#include <khtml_part.h>

class ManViewTab;


/**
	@author Jan Ehrenberger <lankvil@seznam.cz>
*/
class ManView : public KTabWidget
{
Q_OBJECT
public:
    ManView(QWidget *parent = 0, const char *name = 0);
    ~ManView();
    
    void newTab(const QString &url);
    void setCurrentURL(const QString &url);

    ManViewTab *currentTab();

signals:
    void selectionChanged(KHTMLPart *part, const DOM::Range &range);
    
private slots:
    void slotSelectionChanged(KHTMLPart *part, const DOM::Range &range);
};

#endif
