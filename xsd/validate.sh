#!/bin/bash
# 
# validate.sh
# begin: 20.12.2008 by Jan Ehrenberger
#
# Validation script for Freeconf packages
#

PACKAGE="$1"

if [ -z "$PACKAGE" ]
then
	echo "usage: `basename $0` FREECONF_PACKAGE" >&2
	exit 66
fi

DIR=~/.freeconf/packages/$PACKAGE
HEADER=$DIR/header.xml


function validate
{
	local XSD="$1"
	local XML="$2"

	echo "Valildating `basename $XML` against `basename $XSD` ..."
	xmlstarlet val -e -s "$XSD" "$XML"
	RET=$?
	if [ $RET -ne 0 ]
	then
		echo "Validation failed!" >&2
		exit $RET
	fi
	echo "OK"
}


validate "header.xsd" "$HEADER"

TEMPLATE=`xmlstarlet sel -t -v "//template" "$HEADER"`
validate "template.xsd" "$DIR/$TEMPLATE"

HELP=`xmlstarlet sel -t -v "//help" "$HEADER"`
if [ -n "$HELP" ]
then
	for HELP_FILE in `find "$DIR/help" -name "$HELP"`
	do
		validate "help.xsd" "$HELP_FILE"
	done
fi

exit 0
