# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_groupsdialog.ui'
#
# Created: Sun Oct 16 21:28:09 2011
#      by: PyQt4 UI code generator 4.8.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_GroupsDialog(object):
    def setupUi(self, GroupsDialog):
        GroupsDialog.setObjectName(_fromUtf8("GroupsDialog"))
        GroupsDialog.setWindowModality(QtCore.Qt.WindowModal)
        GroupsDialog.resize(400, 300)
        GroupsDialog.setModal(True)
        self.verticalLayout = QtGui.QVBoxLayout(GroupsDialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.groupsList = QtGui.QListWidget(GroupsDialog)
        self.groupsList.setObjectName(_fromUtf8("groupsList"))
        self.horizontalLayout.addWidget(self.groupsList)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.addGroup = QtGui.QPushButton(GroupsDialog)
        self.addGroup.setObjectName(_fromUtf8("addGroup"))
        self.verticalLayout_2.addWidget(self.addGroup)
        self.deleteGroup = QtGui.QPushButton(GroupsDialog)
        self.deleteGroup.setObjectName(_fromUtf8("deleteGroup"))
        self.verticalLayout_2.addWidget(self.deleteGroup)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.buttonBox = QtGui.QDialogButtonBox(GroupsDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Close)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(GroupsDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), GroupsDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), GroupsDialog.reject)
        QtCore.QObject.connect(self.addGroup, QtCore.SIGNAL(_fromUtf8("clicked()")), GroupsDialog.addGroup)
        QtCore.QObject.connect(self.deleteGroup, QtCore.SIGNAL(_fromUtf8("clicked()")), GroupsDialog.deleteGroup)
        QtCore.QObject.connect(self.groupsList, QtCore.SIGNAL(_fromUtf8("itemActivated(QListWidgetItem*)")), GroupsDialog.editGroup)
        QtCore.QMetaObject.connectSlotsByName(GroupsDialog)

    def retranslateUi(self, GroupsDialog):
        GroupsDialog.setWindowTitle(QtGui.QApplication.translate("GroupsDialog", "Groups", None, QtGui.QApplication.UnicodeUTF8))
        self.addGroup.setText(QtGui.QApplication.translate("GroupsDialog", "Add", None, QtGui.QApplication.UnicodeUTF8))
        self.deleteGroup.setText(QtGui.QApplication.translate("GroupsDialog", "Delete", None, QtGui.QApplication.UnicodeUTF8))

