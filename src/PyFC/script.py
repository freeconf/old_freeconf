#!/usr/bin/python
#
# script.py
# begin: 20.11.2011 by Jan Ehrenberger
#
# Representation of script
#

from log import log
from group import FcFileLocation


class FcTriggers:
	ON_LOAD = 1


class FcTriggerBase:
	"""Base class for all script triggers."""
	id = None
	name = None

	def __init__(self, script = None):
		self.script = script

class FcOnLoadTrigger(FcTriggerBase):
	id = FcTriggers.ON_LOAD # ID of the trigger
	name = "on-load" # Name of the trigger as it is written in script XML file.

	def __init__(self, script = None):
		FcTriggerBase(self, script)


class FcScript:
	def __init__(self):
		self.path = FcFileLocation() # Path to script
		self.arguments = [] # List of strings
		self.triggers = {} # Table of triggers
	
	def setTrigger(self, trigger):
		assert isinstance(trigger, FcTriggerBase)
		self.triggers[trigger.id] = trigger
		trigger.script = self
	
	def getTrigger(self, id):
		if id in self.triggers:
			return self.triggers[id]
		else:
			return None
	
	def execute(self, trigger = None):
		"""Execute the script."""
		pass

