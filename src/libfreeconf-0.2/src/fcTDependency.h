/************************************************************************
 *                      fcTDependency.h
 *                      -------------------
 *   copyright          : (C) 2009 by Frantisek Rolinek
 ************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCTDEPENDENCY_H__
#define __FCTDEPENDENCY_H__

#include <ostream>
#include "fcTypes.h"
#include "fcString.h"
#include "fcList.h"
#include "fcSAXTemplateConst.h"
#include "fcDependencyActions.h"
#include "fcDependencyStruct.h"

class fcTDependencyListItem 
{
    //! Constructors & destructor
    public:
        fcTDependencyListItem();
        fcTDependencyListItem(const fcTDependencyListItem& item);
        virtual ~fcTDependencyListItem();

    //! Pure virtual functions
    public:
        //! Allocates and returns new instance of this class
        virtual fcTDependencyListItem* NewDependencyListItem() const = 0;
        //! Returns true, if it is a keyword
        virtual fc_bool IsDependencyListItemKeyword() const = 0;
        //! Returns true, if it is a relation
        virtual fc_bool IsDependencyListItemRelation() const = 0;
};

class fcTDRelationListItem : public fcTDependencyListItem 
{
    //! Class members 
    private:
        //! Relation
        fcRelationEnum m_relation;

    //! Constructors & destructor
    public:
        fcTDRelationListItem();
        fcTDRelationListItem(const fcRelationEnum& _enum);
        fcTDRelationListItem(const fcTDRelationListItem& item);
        virtual ~fcTDRelationListItem();

    //! Overrided functions from base class
    public:
        //! Allocates and returns new instance of this class
        virtual fcTDependencyListItem* NewDependencyListItem() const;
        //! Returns true, if it is a keyword
        virtual fc_bool IsDependencyListItemKeyword() const;  // return false;
        //! Returns true, if it is a relation
        virtual fc_bool IsDependencyListItemRelation() const; // return true;

    //! Overrided operator
    public:
        const fcTDRelationListItem& operator=(const fcTDRelationListItem& item);
        friend std::ostream& operator<< (std::ostream& stream, const fcTDRelationListItem& fcRel);

    //! Setter
    public:
        //! Relation setter
        void SetRelation(const fcRelationEnum& relation); 

    //! Getter
    public:
        //! Relation getter
        const fcRelationEnum& GetRelation() const;
};

class fcTDKeywordListItem : public fcTDependencyListItem
{
    //! Class members
    private: 
        //! Name of entry
        /*! Name of entry, on which this one is dependent
         */
        fcString m_strEntryName;

        //! List of struDependencyValue structure
        fcList<struDependencyValue> m_dependencyValues;

    //! Constructors & destructor
    public: 
        fcTDKeywordListItem();
        fcTDKeywordListItem(const fc_char* name, const fcList<struDependencyValue>& lst);
        fcTDKeywordListItem(const fcString& name, const fcList<struDependencyValue>& lst);
        fcTDKeywordListItem(const fcTDKeywordListItem& item);
        virtual ~fcTDKeywordListItem();

    //! Overrided functions from base class
    public: 
        //! Allocates and returns new instance of this class
        virtual fcTDependencyListItem* NewDependencyListItem() const;
        //! Returns true, if it is a keyword
        virtual fc_bool IsDependencyListItemKeyword() const; // return true
        //! Returns true, if it is a relation
        virtual fc_bool IsDependencyListItemRelation() const; //return false

    //! Overrided operators
    public: 
        const fcTDKeywordListItem& operator=(const fcTDKeywordListItem& item);
        friend std::ostream& operator<< (std::ostream& stream, const fcTDKeywordListItem& fcKey);

    //! Setters
    public:
        //! Class members setter
        void SetKeyword(const fc_char* name, const fcList<struDependencyValue>& lst); 
        //! Class members setter
        void SetKeyword(const fcString& name, const fcList<struDependencyValue>& lst); 
        //! Entry name setter
        void SetKeywordName(const fc_char* name);
        //! Entry name setter
        void SetKeywordName(const fcString& name);
        //! Dependency values setter
        void SetKeywordValues(const fcList<struDependencyValue>& lst);

    //! Getters
    public:
        //! Entry name getter
        const fcString& GetKeywordName() const;
        //! Values getter
        const fcList<struDependencyValue>& GetDependencyValuesList() const;
};

class fcTDependency
{
    //! Class members
    private:
        //! List of dependency items
        /*! This list represents the whole dependency condition for this entry.
         *  It's structure is similar to reverse polish notation's structure.
         */
        fcList< fcTDependencyListItem* >* m_pDep;

        //! List of actions for the current entry
        /*! This list represents the whole set of possible actions, that can 
         *  occur in dependence on dependency condition. 
         */
        fcList< fcDActionBaseListItem* >* m_pAction;
        
    //! Constructors & destructor
    public:
        fcTDependency();
        fcTDependency(fcList<fcTDependencyListItem*> *pDep, fcList<fcDActionBaseListItem*> *pAction, 
                      fc_bool bDeepCopy = true);
        fcTDependency(const fcTDependency& item);
        ~fcTDependency();

    //! Overrided operators
    public: 
        const fcTDependency& operator= (const fcTDependency& item);
        friend std::ostream& operator<< (std::ostream& stream, const fcTDependency& fcTDep);

    //! Adders
    public:
        //! fcTDependencyListItem adder
        void AddDependencyListItem(fcTDependencyListItem* pItem);
        //! fcDActionBaseListItem adder
        void AddActionListItem(fcDActionBaseListItem* pItem);

    //! Setters
        //! fcTDependencyListItem setter
        void SetDependencyListItem(fcTDependencyListItem* pItem, fc_uint pos);
        //! fcDActionBaseListItem setter
        void SetActionListItem(fcDActionBaseListItem* pItem, fc_uint pos);

    //! Getters
    public:
        //! fcTDependencyListItem getter (from a given position)
        fcTDependencyListItem* GetDependencyListItem(fc_uint pos);
        //! fcTDependencyListItem getter (from a given position)
        fcTDependencyListItem* GetDependencyListItem(fc_uint pos) const;
        //! fcDActionBaseListItem getter (from a given positon)
        fcDActionBaseListItem* GetActionListItem(fc_uint pos);
        //! fcDActionBaseListItem getter (from a given positon)
        fcDActionBaseListItem* GetActionListItem(fc_uint pos) const;
        //! Dependency list size getter
        fc_uint GetDependencyListSize() const;
        //! Actions list size getter
        fc_uint GetActionListSize() const;

    //! Other functions
    public:
        //! Returns true, if the given position for m_pDep list is valid
        fc_bool IsPositionInDependencyListValid(fc_uint pos) const;
        //! Returns true, if the given position for m_pDep list is not valid
        fc_bool IsPositionInDependencyListNotValid(fc_uint pos) const;
        //! Returns true, if the given position for m_pAction list is valid
        fc_bool IsPositionInActionListValid(fc_uint pos) const;
        //! Returns true, if the given position for m_pAction list is not valid
        fc_bool IsPositionInActionListNotValid(fc_uint pos) const;

    //! Erase functions
    public:
        //! Erase dependency list (list of dependency items)
        void EraseDependencyList();
        //! Erase action list (list of actions)
        void EraseActionList();
};

#endif
