#!/usr/bin/python
#
# Kernel Config Parser
#
# begin: 27.10.2008 by Jan Ehrenberger <lankvil@seznam.cz>
#
# Parse kernel configuration files 
#


import types
import re
import kcFreeconf
import kcDependency
from kcLog import log


class Error:
	pass

class SyntaxError(Error):
	def __init__(self, msg):
		self.msg = msg
	def __str__(self):
		return 'KernelConfig Syntax Error: ' + msg


class Parser:
	"Abstract class for parsing with body of parse loop"
	# Define return cosntants for parse_cmd
	PARSE_NEXT_LINE=1 # Get next line from the buffer and parse it
	PARSE_THIS_LINE=2 # Use current line and parse it
	PARSE_STOP_NOW=3 # Stop parsing at this exact line
	PARSE_STOP_NEXT=4 # Delete one line form the buffer and then stop parsing

	def __init__(self, lines):
		self.lines = lines

	def parse_cmd(self, cmd, rest):
		raise TypeError('Abstract method parse_cmd called!')

	def strip_comment(self, line):
		p = line.find('#')
		return line[0:p].strip()
	
	def next_line(self):
		return self.lines.pop(0)
	
	def current_line(self):
		"""Return current line of code to parse. Strips comments and concatenates lines divided by \ """
		line = self.lines[0]
		while True:
			line = self.strip_comment(line)
			if len(self.lines) <= 1 or len(line) == 0 or line[-1] != '\\': 
				break
			# Ending character of the line is '\' => Delete it and append next line
			line = line[:-1] + self.lines.pop(1)
		return line
	
	def parse(self):
		while self.lines and len(self.lines) > 0:
			line = self.current_line()

			if line == '': # Skip empty lines
				self.next_line()
				continue

			rest = None
			tokens = line.split(None, 1)
			if len(tokens) > 0:
				cmd = tokens[0]
				if len(tokens) > 1:
					rest = tokens[1]
			try:
				## Call parse_cmd function
				# this function is abstract here. Has to be redefined.
				res = self.parse_cmd(cmd, rest)
				if res == self.PARSE_STOP_NOW:
					break
				elif res == self.PARSE_STOP_NEXT:
					self.next_line()
					break
				elif res == self.PARSE_THIS_LINE:
					continue
				elif res == self.PARSE_NEXT_LINE: # By default parse next line
					self.next_line()
			except IndexError:
				break

	def analyze(self):
		return None
	

class Dependent:
	dependency_parser = kcDependency.DependencyParser()

	def __init__(self):
		self.dependency = None
	
	def add_dependency(self, str):
		"""Add dependency expression"""
		dep = self.dependency_parser.parse(str)
		self.dependency = self.dependency_parser.concatenate_and(self.dependency, dep)

	def parse_depends(self, cmd, rest):	
		"""Parse 'depends...' expression. Returns True if the line was correctly parsed, False if the line did not look like dependency."""
		if cmd != "depends":
			return False
	
		if rest:
			tokens = rest.split(None, 1)
		else:
			tokens = None
		
		if len(tokens) > 0 and tokens[0] == "on":
			tokens = tokens[1:]
		if len(tokens) > 0: # depends ...
			self.add_dependency(" ".join(tokens))
			return True
		return False


#class Default(Dependent):
#	def __init__(self, value):
#		self.value = value
#
#	def __str__(self):
#		return 'Default('+self.value+')'
#		

class Range:
	from_val = None
	to_val = None

	def __init__(self, from_val = None, to_val = None):
		self.from_val = from_val
		self.to_val = to_val

	def parse_range(self, cmd, rest):
		"""Parse 'range...' expression. Returns True if the line was correctly parsed, False if the line did not look like dependency."""
		if cmd != "range":
			return False
		tokens = rest.split(None, 2)
		if len(tokens) < 2:
			raise SyntaxError("Range is missing some arguments")
		self.from_val = tokens[0]
		self.to_val = tokens[1]
		return True

	def __str__(self):
		return 'Range(' + self.from_val + ', ' + self.to_val + ')'
		

class Help:
	text = None
	
	def __init__(self, text = None):
		self.text = text
		self.help_match = re.compile('-{0,3}\\s?help\\s?-{0,3}')

	def get_indent(self, line):
		"""Return number of identation spaces. Tab counts as 8."""
		i = 0
		for c in line:
			if c == ' ':
				i = i + 1
			elif c == '\t':
				i = (i / 8 + 1) * 8
			else:
				break
		return i

	def parse_help(self, cmd, rest, lines):
		cmd = cmd + (rest or '')
		if not self.help_match.match(cmd):
			return False
			
		# Determine initial indent and then compare all other lines with it.
		self.text = ""
		lines.pop(0)
		start_indent = None
		while lines and len(lines) > 0:
			line = lines[0]
			indent = self.get_indent(line)
			line = line.strip()
			if start_indent == None:
				start_indent = indent
			elif len(line) > 0 and start_indent > indent:
				self.text = self.text.strip()
				return True
			self.text = self.text + line + '\n'
			lines.pop(0)

	def __str__(self):
		if self.text == None:
			return ""
		return self.text



class Config(Dependent, Parser):
	type = None
	label = None
	default = None
	range = None

	def __init__(self, name, lines = None):
		log.debug("Config name: " + name)
		self.name = name
		Parser.__init__(self, lines)
		Dependent.__init__(self)
		self.help = Help()
	
	
	def parse_cmd(self, cmd, rest):
		if self.parse_depends(cmd, rest): # depends on ...
			pass

		# Parse default value
		elif cmd == "default":
			# TODO: zatim nepodporuje podminku za tim
			if rest:
				self.default = rest[0:rest.find(" if ")].strip('"')

		# Read help text
		elif self.help.parse_help(cmd, rest, self.lines):
			return self.PARSE_THIS_LINE

		# Parse type definition
		elif cmd in ["bool", "boolean", "tristate", "int", "string", "hex"]:
			if cmd == "boolean":
				self.type = "bool"
			else:
				self.type = cmd
			log.debug("Type: " + self.type)
			if rest:
				# TODO: Process dependencies
				self.label = rest.strip('"')
		
		# Parse type definition + default value
		elif cmd in ["def_bool", "def_tristate"]:
			# TODO - default value and dependency
			self.type = cmd[4:]

		# Parse prompt
		elif cmd == "prompt":
			self.label = rest.strip('"')

		# Parse range
		elif cmd == "range":
			self.range = Range(cmd, rest)

		# Parse select
		elif cmd == "select":
			# TODO
			pass

		elif cmd == "option":
			# TODO
			pass 

		else:
			return self.PARSE_STOP_NOW
		return self.PARSE_NEXT_LINE
	
	def clear_value(self, value):
		"""Clear the value according to config's type"""
		if self.type == "tristate":
			if value in ['y', 'm', 'n']:
				return value
		elif self.type == "bool":
			if value == 'y':
				return True
			elif value == 'n':
				return False
		elif self.type == "integer":
			return int(value)
		else:
			return value

		return None

	def analyze(self):
		if self.type == "tristate":
			# Ulozit tristate jako string list y/m/n
			e = kcFreeconf.String(self.name, data="tristate")

		elif self.type == "bool":
			e = kcFreeconf.Bool(self.name)

		elif self.type == "int":
			e = kcFreeconf.Number(self.name)
			e.type = "integer"
			if self.range:
				e.min = self.range.from_val
				e.max = self.range.to_val

		elif self.type == "string":
			e = kcFreeconf.String(self.name)
		else:
			return None

		e.help = str(self.help)
		e.label = self.label
		if self.default:
			e.default = self.clear_value(self.default)
		return e
		

	def __str__(self):
		s = '## ' + self.__class__.__name__ + ' ##\n'
		s = s + 'Name: ' + str(self.name) + '\n'
		s = s + 'Type: ' + str(self.type) + '\n'
		s = s + 'Label: ' + str(self.label) + '\n'
		s = s + 'Default: ' + str(self.default) + '\n'
		s = s + 'Dependency: ' + str(self.dependency) + '\n'
		s = s + 'Help: ' + str(self.help) + '\n'
		return s


class MenuConfig(Config):
	def __init__(self, name, lines = None):
		Config.__init__(self, name, lines)


class Comment(Parser, Dependent):
	text = None

	def __init__(self, text, lines = None):
		self.text = text
		Parser.__init__(self, lines)
		Dependent.__init__(self)
	
	def parse_cmd(self, cmd, rest):
		if self.parse_depends(cmd, rest): # depends on ...
			return self.PARSE_NEXT_LINE
		else:
			return self.PARSE_STOP_NOW


class Tree(Parser):
	content = None
	
	def __init__(self, lines):
		self.content = []
		Parser.__init__(self, lines)

	def parse_cmd(self, cmd, rest):
		if cmd == "config":
			block = Config(rest, self.lines)
		elif cmd == "menuconfig":
			block = MenuConfig(rest, self.lines)
		elif cmd == "menu":
			block = Menu(rest.strip('"'), self.lines)
		elif cmd == "mainmenu":
			block = MainMenu(rest.strip('"'), self.lines)
		elif cmd == "choice":
			block = Choice(self.lines)
		elif cmd == "if":
			block = If(self.lines)
		elif cmd == "comment":
			block = Comment(rest, self.lines)
		elif cmd == "source":
			# Append contents of the file to our list
			file = rest.strip('"')
			source = Source(file)
			source.parse()
			self.content = self.content + source.content
			return self.PARSE_NEXT_LINE
		else:
			log.error('Syntax error: Unidentified source line: %s %s'  % (cmd, rest or ''))
			return self.PARSE_STOP_NOW
		# Parse the block
		self.next_line()
		block.parse()
		self.content.append(block)
		return self.PARSE_THIS_LINE

	def analyze(self):
		"""Default Tree analyzer. Return list of analyzed objects"""
		list = []
		for c in self.content:
			elt = c.analyze()
			if isinstance(elt, types.ListType):
				list = list + elt
			elif elt != None:
				list.append(elt)
		return list

	def __str__(self):
		s = "## " + self.__class__.__name__ + " ##"
		for a in self.content:
			s = s + str(a) + "\n"
		return s


class Choice(Tree, Dependent):
	default = None
	optional = False
	
	def __init__(self, lines):
		Tree.__init__(self, lines)
		Dependent.__init__(self)
		self.help = Help()

	def parse_cmd(self, cmd, rest):
		if self.parse_depends(cmd, rest):
			pass
		elif self.help.parse_help(cmd, rest, self.lines):
			return self.PARSE_THIS_LINE
		# Parse type definition
		elif cmd in ["bool", "boolean", "tristate"]:
			if cmd == "boolean":
				self.type = "bool"
			else:
				self.type = cmd
			# TODO: parsovat zavoslosti
			if rest:
				self.label = rest.strip('"')
		elif cmd == "prompt":
			self.label = rest
		elif cmd == "optional":
			self.optional = True
		elif cmd == "default":
			self.default = rest
		elif cmd == "endchoice":
			return self.PARSE_STOP_NEXT
		else:
			return Tree.parse_cmd(self, cmd, rest)
		return self.PARSE_NEXT_LINE


class If(Tree):
	def __init__(self, lines):
		Tree.__init__(self, lines)

	def parse_cmd(self, cmd, rest):
		if cmd == "endif":
			return self.PARSE_STOP_NEXT
		else:
			return Tree.parse_cmd(self, cmd, rest)


class Menu(Tree, Dependent):

	class Counter:
		cnt = 0
		def add(self):
			self.cnt = self.cnt + 1
			return self.cnt
		def __str__(self):
			return str(self.cnt)


	label = None
	counter = Counter() # Static variable to count menu entries
	count = 0

	def __init__(self, label, lines):
		self.label = label
		Tree.__init__(self, lines)
		Dependent.__init__(self)
		self.count = self.counter.add()
	
	def parse_cmd(self, cmd, rest):
		if self.parse_depends(cmd, rest):
			return self.PARSE_NEXT_LINE
		elif cmd == "endmenu":
			return self.PARSE_STOP_NEXT
		else:
			return Tree.parse_cmd(self, cmd, rest)
	
	def gen_name(self):
		"""Generate valid name from menu label"""
		name = ''
		for i in range(0, len(self.label)):
			if self.label[i].isalnum():
				name = name + self.label[i]
			elif len(name) > 0 and name[-1] != '_':
				name = name + '_'
		if len(name) == 0:
			name = 'menu_' + self.count
		return name

	def analyze(self):
		section = kcFreeconf.Section(self.gen_name())
		section.label = self.label
		for c in self.content:
			elt = c.analyze()
			if isinstance(elt, types.ListType):
				section.concat(elt)
			elif elt != None:
				section.append(elt)
		return section

	def __str__(self):
		s = '## Menu ##\n'
		s = s + 'Name: ' + str(self.label) + '\n'
		s = s + 'Dependency: ' + str(self.dependency) + '\n'
		s = s + Tree.__str__(self)
		return s

class MainMenu(Tree):
	label = None

	def __init__(self, label, lines):
		self.label = label
		Tree.__init__(self, lines)
	

class Source(Tree):
	def __init__(self, file_name):
		self.file_name = file_name
		f = open(file_name)
		try:
			lines = f.readlines()
		finally:
			f.close()
		Tree.__init__(self, lines)
	
	
