AC_INIT()
AC_CONFIG_AUX_DIR(config)
AM_INIT_AUTOMAKE(libfreeconf,0.2)
AM_CONFIG_HEADER( config.h )
dnl AM_PROG_LIBTOOL
AC_PROG_LIBTOOL

dnl -------------------------------
dnl ---- check for programs -------
dnl -------------------------------

AC_PROG_CXX
AC_PROG_CC
AC_CHECK_PROG(DOXYGEN,doxygen, doxygen)

dnl ---------------------------------------------------------------------
dnl --- Checks for typedefs, structures, and compiler characteristics ---
dnl ---------------------------------------------------------------------
AC_C_CONST
AC_C_INLINE
AC_TYPE_SIZE_T
AC_STRUCT_TM

dnl ----------------------------------------
dnl ----- check for standard headers -------
dnl ----------------------------------------

AC_HEADER_STDC
AC_CHECK_HEADERS([float.h stdlib.h string.h])
AC_CHECK_HEADERS(string.h)
AC_CHECK_HEADERS(unistd.h)

dnl ------------------------------------
dnl --- Checks for library functions ---
dnl ------------------------------------

AC_FUNC_ERROR_AT_LINE
AC_FUNC_STRTOD
AC_CHECK_FUNCS([bzero memset pow sqrt strtol])

dnl ----------------------------
dnl --- check for xml engine ---
dnl ----------------------------

AC_LANG_SAVE
AC_LANG_CPLUSPLUS
AC_CHECK_HEADERS(xercesc/util/XMLString.hpp,,AC_MSG_ERROR(Unable to find Xerces headers ) )

TMP_LIBS=$LIBS
XERCESC_LIBS="-lxerces-c"
available_xercesc=no
LIBS="$LIBS $XERCESC_LIBS"
AC_MSG_CHECKING([for lxerces-c])
AC_TRY_LINK( [#include<xercesc/util/PlatformUtils.hpp>
               XERCES_CPP_NAMESPACE_USE],
             [XMLPlatformUtils::Initialize()],
             available_xercesc=yes,
             available_xercesc=no )
AC_MSG_RESULT($available_xercesc)
   if test x"$available_xercesc" = xno; then
      AC_MSG_ERROR( Unable to link with -lxerces-c )
   fi
LIBS=$TMP_LIBS

dnl AC_CHECK_HEADERS(xalanc/XalanTransformer/XalanTransformer.hpp,,AC_MSG_ERROR(Unable to find Xalanc headers ) )
dnl 
dnl XALANC_LIBS="-lxalan-c"
dnl available_xalanc=no
dnl LIBS="$LIBS $XALANC_LIBS"
dnl AC_MSG_CHECKING([for lxalan-c])
dnl AC_TRY_LINK( [#include<xalanc/XalanTransformer/XalanTransformer.hpp>
dnl              XALAN_USING_XALAN(XalanTransformer) ],
dnl              [XalanTransformer::initialize()],
dnl              available_xalanc=yes,
dnl              available_xalanc=no )
dnl AC_MSG_RESULT($available_xalanc)
dnl if test x"$available_xalanc" = xno; then
dnl       AC_MSGuto_ERROR( Unable to link with -lxalan-c )
dnl    fi
dnl LIBS=$TMP_LIBS
dnl 
AC_CHECK_HEADERS(xalanc/XalanTransformer/XalanTransformer.hpp,,AC_MSG_ERROR(Unable to find Xalanc headers ) )

XALANC_LIBS="-lxalan-c"
available_xalanc=no
LIBS="$LIBS $XALANC_LIBS"
AC_MSG_CHECKING([for lxalan-c])
AC_TRY_LINK( [#include<xalanc/XalanTransformer/XalanTransformer.hpp>
             XALAN_USING_XALAN(XalanTransformer) ],
             [XalanTransformer::initialize()],
             available_xalanc=yes,
             available_xalanc=no )
AC_MSG_RESULT($available_xalanc)
if test x"$available_xalanc" = xno; then
      AC_MSG_ERROR( Unable to link with -libxalan-c )
   fi
LIBS=$TMP_LIBS

AC_LANG_RESTORE

dnl -----------------------------------------
dnl --------- check for debuging ------------
dnl -----------------------------------------

AC_ARG_ENABLE(debug,[  --enable-debug=[no/yes/full]     turn on debugging [default=no]] )
if test x"$enable_debug" = xyes; then
   CXXFLAGS="$CXXFLAGS -O0 -DDEBUG -g3 -Wall -W -Wstrict-prototypes -Wno-unused"

   dnl check for Pound
   AC_CHECK_HEADERS(pound.h)
   PKG_CHECK_MODULES( POUND, pound )
   LDFLAGS="$LDFLAGS $POUND_LIBS"
   CXXFLAGS="$CXXFLAGS $POUND_CFLAGS"
   CXXFLAGS="$CXXFLAGS -Wno-deprecated"
fi
if test x"$enable_debug" = xfull; then
   CXXFLAGS="$CXXFLAGS -O0 -DDEBUG -g3 -Wall -W -Wstrict-prototypes -Wno-unused -ansi -pedantic"

   dnl check for Pound
   AC_CHECK_HEADERS(pound.h)
   PKG_CHECK_MODULES( POUND, pound )
   LDFLAGS="$LDFLAGS $POUND_LIBS"
   CXXFLAGS="$CXXFLAGS $POUND_CFLAGS"
fi

dnl -----------------------------------------
dnl --------- check for dmalloc -------------
dnl -----------------------------------------

AC_ARG_WITH(dmalloc, [  --with-dmalloc=[no/yes/full]    compile with Debug Malloc Library])
if test x"$with_dmalloc" = xfull;
then
  CXXFLAGS="$CXXFLAGS -DDMALLOC_FUNC_CHECK"
  with_dmalloc=yes
fi
if test x"$with_dmalloc" = xyes;
then
  AC_CHECK_LIB(dmalloc,dmalloc_error,,AC_MSG_ERROR(Unable to find Debug malloc library))
  CXXFLAGS="$CXXFLAGS -DGAG_DEBUG_MEM"
  LIBS="$LIBS -ldmalloc"
fi

dnl -----------------------------------------
dnl --------- check for efence --------------
dnl -----------------------------------------

AC_ARG_WITH(malloc, [  --with-efence    compile with Electric Fence Library])
if test x"$with_efence" = xyes;
then
dnl  AC_CHECK_LIB(efence,malloc,,AC_MSG_ERROR(Unable to find Electric Fence library))
  LIBS="$LIBS -lefence"
fi

dnl -----------------------------------------
dnl ---------- check for gprof --------------
dnl -----------------------------------------

AC_ARG_WITH(gprof, [  --with-gprof      compile with gprof extra code])
if test x"$with_gprof" = xyes;
then
   CXXFLAGS="$CXXFLAGS -pg"

fi

AC_SUBST(LIBXML2_LIBS)
AC_SUBST(XERCESC_LIBS)
dnl AC_SUBST(XALANC_LIBS)
AC_SUBST(XALANC_LIBS)
AC_OUTPUT( Makefile
           config/Makefile
           src/Makefile
           DTD/Makefile
           doc/Makefile)

echo "Generating file libfreeconf.pc ..."

if [ test libfreeconf.pc ]
   then
      rm -f libfreeconf.pc;
      fi
      echo "prefix=$prefix" > libfreeconf.pc
      echo "exec_prefix=\${prefix}/bin" >> libfreeconf.pc
      echo "libdir=\${prefix}/lib" >> libfreeconf.pc
      echo "includedir=\${prefix}/include/freeconf" >> libfreeconf.pc
      echo >> libfreeconf.pc
      echo "Name: libfreeconf" >> libfreeconf.pc
      echo "Description: Freeconf Library" >> libfreeconf.pc
      echo "Requires:" >> libfreeconf.pc
      echo "Version: 0.2.0" >> libfreeconf.pc
      echo "Libs:  -L\${libdir} -lfreeconf-0.2 -lxerces-c" >> libfreeconf.pc
      echo "Cflags: -I\${includedir} " >> libfreeconf.pc



echo
echo Configure results:
echo -=-=-=-=-=-=-=-=-=-=-
echo "CXXFLAGS": $CXXFLAGS
echo "LIBS": $LIBS $XERCESC_LIBS $XALANC_LIBS
echo Install dir: ${prefix}

dnl if test "x${prefix} = x";
dnl then
dnl    echo "${ac_default_prefix}"
dnl else
dnl    echo "${prefix}"
dnl fi

echo Now you can type \'make\' following by \'make install\'
