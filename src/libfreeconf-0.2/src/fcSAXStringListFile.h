/***************************************************************************
                          fcSAXStringListFile.h  -  description
                             -------------------
    begin                : 2004/12/29
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcSAXStringListFileH
#define fcSAXStringListFileH

#include "fcSAXFile.h"
#include "fcStringEntry.h"
#include "fcHash.h"

enum fcStringListFileEnum { fcNO_STR_LIST_ELEMENT,
                            fcSTRING_LIST,
                           // fcSTRING_LIST_NAME,
                            fcSTRING_LIST_STRING,
                            fcSTRING_LIST_HELP,
                            fcSTRING_LIST_LABEL };

//! String lists parser
/*! This parser reads string lists in given file and
    insert them into given string list container.
*/
class fcSAXStringListFile : public fcSAXFile
{
   //! Pointer to string lists container the new string lists will be inserted in.
   /*! It is set in ParseStringListFile() method.
    */
   fcHash< fcList< fcStringEntry > >* string_lists;

   //! Current string list
   fcList< fcStringEntry >* current_list;

   //! Current string entry
   fcStringEntry current_entry;

   //! Flag for the tag freeconf-header
   fc_bool enclosing_tag;

   //! Id of current xml element
   fcStringListFileEnum xml_element;

   public:
   //! Basic constructor
   fcSAXStringListFile();
   
   //! Destructor
   ~fcSAXStringListFile();
   
   protected:
   //! Implementations of the SAX DocumentHandler start element
   void startElement(  const   XMLCh* const    uri,
                       const   XMLCh* const    localname,
                       const   XMLCh* const    qname,
                       const   Attributes&     attributes);

   //! Implementations of the SAX DocumentHandler end element
   void endElement( const XMLCh* const uri, 
                    const XMLCh* const localname, 
                    const XMLCh* const qname);

   //! Implementations of the SAX DocumentHandler characters
   void characters( const XMLCh* const chars, 
                    const unsigned int length);

   public:
   //! Method for parsing header file
   /*! This method just sets pointer this -> header_structure
       and the calls fcSAXFile :: Parse()
    */
   fc_bool ParseStringListFile( const fc_char* file_name,
                                fcHash< fcList< fcStringEntry > >* _string_lists );

};

#endif
