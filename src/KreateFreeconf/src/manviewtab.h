/***************************************************************************
 *   Copyright (C) 2009 by Jan Ehrenberger   *
 *   lankvil@seznam.cz   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef MANVIEWTAB_H
#define MANVIEWTAB_H

#include <qhbox.h>
#include <khtml_part.h>

class ManView;

class ManPart : public KHTMLPart
{
Q_OBJECT
    QPoint m_dragStartPoint;
    bool m_drag;
    
public:
    ManPart(QWidget *parent, const char *name = 0);
    ~ManPart();
    
protected:
    void khtmlMousePressEvent(khtml::MousePressEvent *event);
    void khtmlMouseMoveEvent(khtml::MouseMoveEvent *event);

private slots:
    void slotSelectionChanged();
};

/**
	@author Jan Ehrenberger <lankvil@seznam.cz>
*/
class ManViewTab : public QHBox
{
Q_OBJECT
        
    KHTMLPart *m_html;
    ManView *m_man_view; // Pointer to parent ManView
    
public:
    ManViewTab(const QString &url, ManView *man_view);
    ~ManViewTab();

    KHTMLPart *getHTMLPart();
    void openLocation(const QString &location);

signals:
    void selectionChanged(KHTMLPart *part, const DOM::Range &range);

private slots:
    void slotSetTitle(const QString& title);
    void slotSelectionChanged();
    void slotOpenURLRequest(const KURL &url, const KParts::URLArgs &args);
};

#endif
