/************************************************************************
 *                      widget.h
 *      author: Frantisek Rolinek
 ************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/

#ifndef _WIDGET_H_
#define _WIDGET_H_

#include <fcCEntry.h>

void compileEntry( fc_bool isSensitive, fcCEntry* c_entry ); 

#endif
