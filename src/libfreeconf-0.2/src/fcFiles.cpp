/***************************************************************************
                          fcFiles.cpp  -  description
                             -------------------
    begin                : 2005/08/18
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <unistd.h>
#include <iostream>
#include <cerrno>
#include "fcFiles.h"
#include "fcTypes.h"
#include "debug.h"

//--------------------------------------------------------------------------
static fc_bool CheckExist( const fc_char* file_name )
{
   if( access( file_name, F_OK ) != 0 )
   {
      if( errno == ENOENT )
         std::cout << file_name << " does not exist. " << std::endl;
      if( errno == EACCES )
         std::cout << file_name << " is not accessible. " << std::endl;
      return false;
   }
   return true;
}
//--------------------------------------------------------------------------
static fc_bool CheckRead( const fc_char* file_name )
{
   if( access( file_name, R_OK ) != 0 )
   {
         std::cout << file_name << " is not readable. " << std::endl;
      return false;
   }
   return true;
}
//--------------------------------------------------------------------------
static fc_bool CheckWrite( const fc_char* file_name )
{
   if( access( file_name, W_OK ) != 0 )
   {
      if( errno == EACCES )
         std::cout << file_name << " is not writable ( access denied ). " << std::endl;
      if( errno == EROFS )
         std::cout << file_name << "is not writable (read-only filesystem )." << std::endl;
      return false;
   }
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcCheckFiles( const fcList< fcString >& dirs,
                      const fcList< fcString >* langs, 
                      const fc_char* file_name,
                      fc_bool write_access,
                      fcString* result_file,
                      fcString* result_dir,
                      fcList< fcString >* result_files,
                      fcList< fcString >* result_dirs )
{
   DBG_FUNCTION_NAME( "", "fcCheckFiles" );
   assert( file_name );
   assert( result_file || result_files );
   assert( ! dirs. IsEmpty() );
   fc_int i;
   for( i = 0; i < dirs. Size(); i ++ )
   {
      DBG_EXPR( dirs[ i ] );
      if( langs )
      {
         fc_int j;
         for( j = 0; j < langs -> Size(); j ++ )
         {
            fcString current_file( dirs[ i ] );
            current_file += "/";
            current_file += ( * langs )[ j ];
            current_file += "/";
            current_file += file_name;
            DBG_EXPR( current_file );
            if( ! CheckExist( current_file. Data() ) )
               continue;
            if( ! CheckRead( current_file. Data() ) )
               continue;
            if( write_access && ! CheckWrite( current_file. Data() ) )
               continue;
            DBG_COUT( "File is OK." );
            if( result_file )
            {
               result_file -> SetString( current_file. Data() );
               if( result_dir )
                  * result_dir = dirs[ i ] + "/" + ( * langs )[ j ] ;
               return true;
            }
            assert( result_files );
            result_files -> Append( current_file );
            if( result_dirs )
               result_dirs -> Append( dirs[ i ] + "/" + ( *langs )[ j ] );
         }
      }
      else
      {
         fcString current_file( dirs[ i ] );
         current_file += "/";
         current_file +=  file_name;
         DBG_EXPR( current_file );
         if( ! CheckExist( current_file. Data() ) )
            continue;
         if( ! CheckRead( current_file. Data() ) )
            continue;
         if( write_access && ! CheckWrite( current_file. Data() ) )
            continue;
         if( result_file )
         {
            result_file -> SetString( current_file. Data() );
            if( result_dir )
               result_dir -> SetString( dirs[ i ]. Data() );
            return true;
         }
         assert( result_files );
         result_files -> Append( current_file );
         if( result_dirs )
            result_dirs -> Append( dirs[ i ] );
      }
   }
   if( result_file ) return false;
   assert( result_files );
   if( result_files -> IsEmpty() ) return false;
   return true;
}
//--------------------------------------------------------------------------
void fcAppendSubdir( fcList< fcString >& dirs,
                     const fc_char* subdir )
{
   DBG_FUNCTION_NAME( "", "fcAppendSubdir" );
   fc_int i;
   for( i = 0; i < dirs. Size(); i ++ )
   {
      DBG_EXPR( dirs[ i ] );
      fcString& dir = dirs[ i ];
      dir += "/";
      dir += subdir;
      DBG_EXPR( dirs[ i ] );
   }
}
//--------------------------------------------------------------------------
void fcExpandFileName( fcString& file_name,
                       const fcString& home_dir,
                       const fcString& package_dir )
{
   DBG_FUNCTION_NAME( "" ,"ExpandFileName" );
   const fc_char* ch = file_name. Data();
   if( ch[ 0 ] == '~' ) // replace it by home dir;
   {
      fcString str( &ch[ 1 ] );
      file_name = home_dir;
      file_name += str;
      return;
   }
   if( ch[ 0 ] == '$' ) // replace it by package dir;
   {
      fcString str( &ch[ 1 ] );
      file_name = package_dir;
      file_name += str;
      return;
   }
}
   
