/***************************************************************************
                          fcString.h  -  description
                             -------------------
    begin                : 2004/04/10 16:35
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCSTRING_H__
#define __FCSTRING_H__

#include <ostream>
#include "fcTypes.h"

//! Class for managing strings
class fcString
{
   //! Pointer to char ended with zero
   fc_char* string;

   //! Length of the alocated array
   fc_uint capacity;

   public:

   //! Basic constructor
   fcString();

   //! Constructor with fc_char pointer and length
   fcString( const fc_char* c, fc_int len = 0 );

   //! Copy constructor
   fcString( const fcString& str );

   //! Destructor
   ~fcString();

   //! Set string from given fc_char pointer
   void SetString( const fc_char* c );

   //! Operator =
   fcString& operator = ( const fc_char* str );

   //! Operator =
   fcString& operator = ( const fcString& str );

   //! Operator +=
   fcString& operator += ( const fc_char* str );

   //! Operator +=
   fcString& operator += ( const fc_char c );

   //! Operator +=
   fcString& operator += ( const fcString& str );
 
   //! Operator +
   fcString operator + ( const fc_char* str );

   //! Operator +
   fcString operator + ( const fc_char c );
   
   //! Operator +
   fcString operator + ( const fcString& str );

   //! Comparison operator 
   fc_bool operator == ( const fcString& str ) const;

   //! Comparison operator 
   fc_bool operator != ( const fcString& str ) const;

   //! Comparison operator
   fc_bool operator == ( const fc_char* ) const;

   //! Comparison operator
   fc_bool operator != ( const fc_char* ) const;
  
   //! Retyping operator
   operator bool () const;

   //! Return length of the string
   fc_uint Length() const;

   //! Return true if the string is empty
   fc_bool Empty() const;

   //! Removes characters from the string
   void Erase();
   
   //! Return pointer to data
   const fc_char* Data() const;

   friend std::ostream& operator << ( std::ostream& stream, const fcString& str );
};


#endif
