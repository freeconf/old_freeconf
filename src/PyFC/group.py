#!/usr/bin/python
#
# group.py
# begin: 30.8.2011 by Jan Ehrenberger
#
# Group of records
#

import re
from log import log
from exception import FcTransformError


## XSL Transform
# Help function for FcGroup
def XSLTransform(xslString, inputString, outputFile):
	"""Transform config file to native format using xsl transformation."""
	import libxml2
	import libxslt

	xslDoc = libxml2.parseDoc(xslString)
	xsl = libxslt.parseStylesheetDoc(xslDoc)
	inputXmlDoc = libxml2.parseDoc(inputString)
	resultDoc = xsl.applyStylesheet(inputXmlDoc, None)
	xsl.saveResultToFilename(outputFile, resultDoc, 0)

	xsl.freeStylesheet()
	inputXmlDoc.freeDoc()
	resultDoc.freeDoc()



class FcFileLocation:
	"""Structure storing name of file and it's full path."""
	# TODO: Tahle trida by asi nemela byt definovana tady
	def __init__(self, name = None):
		self.name = name
		self.fullPath = None
	
	def __nonzero__(self):
		return self.name != None and self.name != ""
	
	def __repr__(self):
		return "%s(%s)" % (self.name, self.fullPath)


class FcIncludeFileLocation(FcFileLocation):
	"""Extension of FcFileLocation: reference to plugin."""
	# TODO: Tahle trida by asi nemela byt definovana tady
	def __init__(self, plugin, name = None):
		FcFileLocation.__init__(self, name)
		self.plugin = plugin


class FcGroup:
	"""This class represents group of entries and it's settings."""
	def __init__(self, name):
		self.__name = name
		## Initialize properties
		# Main XSL file
		self.transformFile = FcFileLocation()
		# List of included transform files - transform files added in plugins that enhance functionality of main XSL file
		self.includedTransformFiles = []
		# Output file in native format
		self.nativeFile = FcFileLocation()
		# Output defaults settings
		self.outputDefaults = True
	
	@property
	def name (self):
		return self.__name
	
	@name.setter
	def name(self, name):
		self.__name = name
	
	def includeTransform(self, filelocation):
		"""Add transform file to list of included transform files."""
		assert isinstance(filelocation, FcIncludeFileLocation)
		self.includedTransformFiles.append(filelocation)
	
	def transform(self, config_root):
		"""Create native config file for the group."""
		if not self.nativeFile:
			log.warning("Native configuration file for group %s was not defined." % (self.name,))
			return

		log.debug("Writing native config file %s for group %s." % (self.nativeFile.fullPath, self.name))

		if not self.transformFile:
			raise FcTransformError("Transform file is missing for group %s" % (self.name,))

		# Create XSL transformation
		xslString = '<?xml version="1.0" ?>\n<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">\n'
		# Include main XSL file
		xslString += ('<xsl:include href="%s"/>\n' % (self.transformFile.fullPath,))

		# Insert include
		for i in self.includedTransformFiles:
			assert i.fullPath != None
			xslString += ('<xsl:include href="%s"/>\n' % (i.fullPath,))
		xslString += '</xsl:stylesheet>\n'

		# Transform config file
		try:
			# Blank the output file
			open(self.nativeFile.fullPath, "w")
			# Generate file contents
			XSLTransform(
				xslString,
				config_root.output(groupName = self.name), #self.paths.configFileFullPath,
				self.nativeFile.fullPath
			)
		except:
			raise FcTransformError("Cannot transform the output config file " + self.nativeFile.fullPath)

