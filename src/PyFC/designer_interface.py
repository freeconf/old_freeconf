#!/usr/bin/python

from constants import *
import package
import base
import group
from log import log

from exception import FcPackageLoadError, FcInconsistencyError


class PackageInterface (object):
	"""Designers's entry point to the package"""
	def __init__ (self):
		self.__p = None


	def loadPackage (self, name):
		"""Loads the package by name"""
		self.__p = package.FcPackage()
		try:
			self.__p.loadPackage(name, False, True)
		except FcPackageLoadError:
			self.__p = None
			raise


	def closePackage (self):
		"""Closes the package"""
		self.__p = None


	def savePackage (self):
		"""Saves all changes made to the respective files"""
		try:
			self.__p.writePackage()
		except FcInconsistencyError, e:
			# We don't care about inconsistency error here.
			pass
	
	def createPackage(self):
		"""Create new package."""
		# TODO

	@property
	def template (self):
		"""Returns the proxy to the top-level entry of the template tree"""
		return self.__p.trees.templateTree.proxy

	@property
	def name (self):
		"""Returns name of the package"""
		return self.__p.paths.packageName

	@property
	def loaded (self):
		"""Returns True if the package has been loaded"""
		return self.__p != None
		
	@property
	def languages (self):
		"""Returns list of all languages available in the package"""
		return self.__p.data.languagesInPackage
	
	@property
	def groups (self):
		"""Return list of communicators for all groups in the package."""
		return [FcGroupCommunicator(group) for group in self.__p.data.groups.values()]

	def createGroup(self, name):
		"""Create new group in the packate"""
		g = group.FcGroup(name)
		self.__p.addGroup(g)
		return FcGroupCommunicator(g)
	
	def removeGroup(self, group):
		"""Remove existing group. Takes group communicator as argument."""
		self.__p.removeGroup(group.name)
		group.disconnect()


class FcGroupCommunicator(object):
	"""Class that handles communication between the library and FcGroup object."""
	def __init__(self, group):
		self.__group = group
	
	def disconnect(self):
		self.__group = None
	
	@property
	def _reference(self):
		"""Return reference to library object. Needed in template communicator."""
		return self.__group
	
	@property
	def name(self):
		return self.__group.name
	
	@name.setter
	def name(self, name):
		self.__group.name = name
	
	@property
	def outputDefaults(self):
		return self.__group.outputDefaults
	
	@outputDefaults.setter
	def outputDefaults(self, o):
		self.__group.outputDefaults = o
	
	@property
	def transformFile(self):
		return self.__group.transformFile.name
	
	@transformFile.setter
	def transformFile(self, name):
		self.__group.transformFile = group.FcFileLocation(name)
	
	@property
	def nativeFile(self):
		return self.__group.nativeFile.name
	
	@nativeFile.setter
	def nativeFile(self, name):
		self.__group.nativeFile = group.FcFileLocation(name)
	



class FcTemplateCommunicator (object):
	"""Class that handles communication between the library and the designer."""

	def __init__ (self, templateEntry):
		self.__template = templateEntry
		self.__designerEntry = None
		self.__children = []

	def connectDesigner (self, designerEntry):
		"""Connects the designer object"""
		self.__designerEntry = designerEntry

	def disconnectFromTemplate (self):
		"""Disconnects the designer object"""
		self.__template = None

	@property
	def children (self):
		"""Returns list of section's children proxies"""
		if isinstance(self.__template, base.FcSEntry):
			if len(self.__children) == 0:
				for child in self.__template.entries:
					self.__children.append(child.proxy)
			return self.__children
		else:
			return None

	@property
	def parent (self):
		"""Returns parent proxy of this entry"""
		if self.__template.parent:
			return self.__template.parent.proxy
		else:
			return None

	@property
	def name (self):
		"""Returns name of the entry"""
		return self.__template.name

	def setName (self, newName):
		"""Sets name of the entry. Returns False if an object of the same name already exists in the container"""
		if newName == "": return False
		if newName == self.__template.name: return True
		if self.__template.parent:
			if self.__template.parent.findEntry(newName) != (None, None): return False
		self.__template.name = newName
		return True

	@property
	def type (self):
		"""Returns type of the entry"""
		if isinstance(self.__template, base.FcTEntry):
			if self.__template.isReference():
				return FcTypes.REFERENCE
			else:
				return self.__template.type
		else:
			return None


	def setType (self, type):
		"""Sets type of the entry. Returns False if it is not possible to change the type"""
		if type == self.type: return True
		if isinstance(self.__template, base.FcTSEntry) == True or type == FcTypes.SECTION: return False
		if self.__template.parent == None: return False
		node = self.__generateNode(type, self)
		node.name = self.__template.name
		state = True
		# creates a new config node without parent, otherwise it would	 add the buddy to the config tree
		# swaps nodes in the config tree
#		if self.__template.parent.configBuddy != None:
#			state = self.__template.parent.configBuddy.replaceEntry(self.__template.configBuddy, node.configBuddy)
		if state:
			# swap nodes in the template tree
			state = self.__template.parent.replaceEntry(self.__template, node)
			# this should not happen
			assert state == True
			self.__template = node
		return state

	@property
	def active (self):
		"""Returns True if the entry is active"""
		if isinstance(self.__template, base.FcTEntry):
			return self.__template.staticActive
		else:
			return None

	@active.setter
	def active (self, active):
		"""Sets the static activity of the entry"""
		if isinstance(self.__template, base.FcTEntry):
			self.__template.active = active

	@property
	def mandatory (self):
		"""Returns True if the entry is mandatory"""
		if isinstance(self.__template, base.FcTEntry):
			return self.__template.staticMandatory
		else:
			return None

	@mandatory.setter
	def mandatory (self, mandatory):
		"""Sets the static mandatority of the entry"""
		if isinstance(self.__template, base.FcTEntry):
			self.__template.staticMandatory = mandatory

	@property
	def multiple (self):
		"""Returns True if the entry is multiple"""
		if isinstance(self.__template, base.FcTEntry):
			return self.__template.multiple
		else:
			return None

	@multiple.setter
	def multiple(self, multiple):
		"""Sets property multiple of the entry to given value"""
		if isinstance(self.__template, base.FcTEntry):
			self.__template.multiple = multiple
	
	@property
	def group(self):
		"""Returns communicator to entry's group."""
		if isinstance(self.__template, base.FcTEntry) and self.__template.group:
			return FcGroupCommunicator(self.__template.group)
		else:
			return None

	@group.setter
	def group(self, group):
		if isinstance(self.__template, base.FcTEntry):
			self.__template.group = group._reference()

	@property
	def regExp (self):
		"""Returns regular expression string of the string entry"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.STRING:
			return self.__template.regExp
		else:
			return None

	@regExp.setter
	def regExp (self, regExp):
		"""Sets regular expression string"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.STRING:
			self.__template.regExp = regExp


	@property
	def precision (self):
		"""Returns precision of the number entry"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.NUMBER:
			return self.__template.precision
		else:
			return None

	@precision.setter
	def precision (self, precision):
		"""Sets precision"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.NUMBER:
			self.__template.precision = precision

	@property
	def printSign (self):
		"""Returns True if sign should be printed in the output"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.NUMBER:
			return self.__template.printSign
		else:
			return None

	@printSign.setter
	def printSign (self, printSign):
		"""Sets print sign state"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.NUMBER:
			self.__template.printSign = printSign

	@property
	def leadingZeros (self):
		"""Returns True if value of this entry should be printed with leading zeros"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.NUMBER:
			return self.__template.leadingZeros
		else:
			return None

	@leadingZeros.setter
	def leadingZeros (self, leadingZeros):
		"""Sets leading zeros state"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.NUMBER:
			self.__template.leadingZeros = leadingZeros

	@property
	def min(self):
		"""Minimum of this number entry"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.NUMBER:
			return self.__template.min
		else:
			return None

	@min.setter
	def min (self, min):
		"""Sets minimum"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.NUMBER:
			self.__template.min = min

	@property
	def max(self):
		"""Maximum of this number entry"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.NUMBER:
			return self.__template.max
		else:
			return None

	@max.setter
	def max (self, max):
		"""Sets maximum"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.NUMBER:
			self.__template.max = max

	@property
	def step(self):
		"""Step of this number entry"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.NUMBER:
			return self.__template.step
		else:
			return None

	@step.setter
	def step (self, step):
		"""Sets step"""
		if isinstance(self.__template, base.FcTEntry) and self.__template.type == FcTypes.NUMBER:
			self.__template.step = step

	
	@property
	def defaultValue (self):
		"""Returns default value"""
		if isinstance(self.__template, base.FcTKBool):
			return self.__template.defaultGrade
		elif isinstance(self.__template, base.FcTKEntry):
			return self.__template.defaultValue
		else:
			return None
		
		
	@defaultValue.setter
	def defaultValue (self, value):
		"""Sets default value"""
		if isinstance(self.__template, base.FcTKBool):
			self.__template.defaultGrade = value
		elif isinstance(self.__template, base.FcTKEntry):
			self.__template.defaultValue = value
	
	@property
	def target(self):
		"""Retruns address of reference's target."""
		if isinstance(self.__template, base.FcTReference) and self.__template.target != None:
			return self.__template.target.getFullPath()
		else:
			return None
	
	@target.setter
	def target(self, target_str):
		if isinstance(self.__template, base.FcTReference):
			# Get template object for given reference address
			target = self.__template.root.recursiveFindTEntry(target_str)
			self.__template.target = target

	def keyHelp (self, language):
		"""Returns help in specified language"""
		if isinstance(self.__template, base.FcTKEntry):
			return self.__template.keyHelp(language)
		else:
			return None
			
			
	def setKeyHelp (self, language, help):
		"""Set help in specified language"""
		if isinstance(self.__template, base.FcTKEntry):
			self.__template.setKeyHelp(language, escapeXML(help))


	def keyLabel (self, language):
		"""Returns label in specified language"""
		if isinstance(self.__template, base.FcTEntry):
			return self.__template.keyLabel(language)
		else:
			return None
			
			
	def setKeyLabel (self, language, label):
		"""Set label in specified language"""
		if isinstance(self.__template, base.FcTEntry):
			self.__template.setKeyLabel(language, label)


	def __generateNode (self, type, proxy = None):
		if type == FcTypes.SECTION:
			return base.FcTSEntry(proxy)
		if type == FcTypes.BOOL:
			return base.FcTKBool(proxy)
		if type == FcTypes. NUMBER:
			return base.FcTKNumber(proxy)
		if type == FcTypes.STRING:
			return base.FcTKString(proxy)
		if type == FcTypes.REFERENCE:
			return base.FcTReference(proxy)

	def moveToPosition (self, destination, position = -1):
		"""Moves this entry into the new position in the specified container"""
		if destination.type != FcTypes.SECTION: return False
		result = self.__template.moveToPosition(destination.__template, position)
		if result == True:
			destination.invalidateChildrenCache()
		return result



	def appendNode (self, name, type):
		"""Appends a new entry to this entry"""
		if isinstance(self.__template, base.FcTSEntry) == False:
			return None
		if self.__template.findEntry(name) != (None, None): return None
		node = self.__generateNode(type)
		node.name = name
		self.__template.append(node)
		#buddy = self.__template.configBuddy
		self.invalidateChildrenCache()
		return node.proxy


	#def __appendExistingNode (self, node):
		#if node.parent != None:
			#node.__disconnectFromTree()
		#self.__template.append(node.__template)
		#self.invalidateChildrenCache()


	#def __insertExistingNode (self, node, position):
		#if node.parent != None:
			#node.__disconnectFromTree()
		#self.__template.insertEntry(node.__template, position)
		#self.__invalidateChildrenCache()


	def __disconnectFromTree (self):
		if self.__template.parent == None: return False
		return self.__template.parent.disconnect(self.__template)


	def invalidateChildrenCache (self):
		"""Invalidates children cache. Must be done when the children list is changed"""
		self.__children = []


	def deleteNode (self):
		"""Removes this entry from the template tree"""
		parent = self.__template.parent # Save parent
		if parent == None: return
		if self.__disconnectFromTree() == False:
			log.error("Something is wrong, invalid parent of the node " + self.name)
		else:
			parent.proxy.invalidateChildrenCache()


