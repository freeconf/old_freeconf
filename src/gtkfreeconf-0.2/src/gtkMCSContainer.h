/***************************************************************************
                          gtkMCSContainer.h  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef gtkMCSContainerH
#define gtkMCSContainerH

#include <gtk/gtk.h>
#include <fcMCSContainer.h>
#include "gtkCSEntry.h"

class gtkMCSContainer : public fcMCSContainer
{
   //! Gtk clist widget - list of the sections 
   GtkCList* gtk_clist;

   //! Selected row in the list of the entries
   fc_uint select_row;

   //! The config section being just edited
   gtkCSEntry* edited_section;

   public:

   //! Basic constructor
   gtkMCSContainer();

   //! Copy constructor
   gtkMCSContainer( const gtkMCSContainer& mcs_container );

   //! Constructor template entry pointer
   gtkMCSContainer( const fcTEntry* t_entry );

   //! Destructor
   ~gtkMCSContainer();
  
   //! Render the number GUI widget
   void Render( GtkWidget* parent_widget,
                GtkTooltips* tooltips,
                const fc_char* help,
                fcCSEntry* main_c_section );

   //! Setting the row selection
   void SetRowSelection( fc_uint row );

   //! Callback function for adding new section
   void CBAddSection( GtkWidget* widget , fcCSEntry* main_c_section );
   
   //! Callback function for pressing OK button while creating new section
   void CBAddSectionOK( GtkWidget* widget );
   
   //! Callback function for pressing CANCEL button while creating new section
   void CBAddSectionCancel( GtkWidget* widget );

   //! Callback function for modifying a section
   void CBModifySection( GtkWidget* widget , fcCSEntry* main_c_section );

   //! Callback function for modifying a section
   void CBModifySectionOK( GtkWidget* widget );

   //! Callback function for modifying a section
   void CBRemoveSection( GtkWidget* widget );

};

#endif
