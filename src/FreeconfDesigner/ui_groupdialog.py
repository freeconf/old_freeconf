# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_groupdialog.ui'
#
# Created: Sun Oct 16 21:19:21 2011
#      by: PyQt4 UI code generator 4.8.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_editGroupDialog(object):
    def setupUi(self, editGroupDialog):
        editGroupDialog.setObjectName(_fromUtf8("editGroupDialog"))
        editGroupDialog.setWindowModality(QtCore.Qt.WindowModal)
        editGroupDialog.resize(325, 192)
        editGroupDialog.setMinimumSize(QtCore.QSize(325, 192))
        editGroupDialog.setModal(True)
        self.verticalLayout = QtGui.QVBoxLayout(editGroupDialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.formLayout_2 = QtGui.QFormLayout()
        self.formLayout_2.setObjectName(_fromUtf8("formLayout_2"))
        self.label = QtGui.QLabel(editGroupDialog)
        self.label.setObjectName(_fromUtf8("label"))
        self.formLayout_2.setWidget(1, QtGui.QFormLayout.LabelRole, self.label)
        self.groupName = QtGui.QLineEdit(editGroupDialog)
        self.groupName.setObjectName(_fromUtf8("groupName"))
        self.formLayout_2.setWidget(1, QtGui.QFormLayout.FieldRole, self.groupName)
        self.label_2 = QtGui.QLabel(editGroupDialog)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.formLayout_2.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_2)
        self.label_3 = QtGui.QLabel(editGroupDialog)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.formLayout_2.setWidget(3, QtGui.QFormLayout.LabelRole, self.label_3)
        self.groupNativeFile = QtGui.QLineEdit(editGroupDialog)
        self.groupNativeFile.setObjectName(_fromUtf8("groupNativeFile"))
        self.formLayout_2.setWidget(2, QtGui.QFormLayout.FieldRole, self.groupNativeFile)
        self.groupTransformFile = QtGui.QLineEdit(editGroupDialog)
        self.groupTransformFile.setObjectName(_fromUtf8("groupTransformFile"))
        self.formLayout_2.setWidget(3, QtGui.QFormLayout.FieldRole, self.groupTransformFile)
        self.verticalLayout.addLayout(self.formLayout_2)
        self.groupOutputDefaults = QtGui.QCheckBox(editGroupDialog)
        self.groupOutputDefaults.setObjectName(_fromUtf8("groupOutputDefaults"))
        self.verticalLayout.addWidget(self.groupOutputDefaults)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.buttonBox = QtGui.QDialogButtonBox(editGroupDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(editGroupDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), editGroupDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), editGroupDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(editGroupDialog)

    def retranslateUi(self, editGroupDialog):
        editGroupDialog.setWindowTitle(QtGui.QApplication.translate("editGroupDialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("editGroupDialog", "Name", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("editGroupDialog", "Native File", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("editGroupDialog", "Transform File", None, QtGui.QApplication.UnicodeUTF8))
        self.groupOutputDefaults.setText(QtGui.QApplication.translate("editGroupDialog", "Write default values to output", None, QtGui.QApplication.UnicodeUTF8))

