/***************************************************************************
                          fcDependencyActions.cpp  -  description
                             -------------------
    copyright            : (C) 2009 by Frantisek Rolinek
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "fcMessageHandler.h"
#include "fcDependencyActions.h"

//-------------------------------------------------------------
fcDActionBaseListItem :: fcDActionBaseListItem()
{
    m_nActionValue = 0;
}
//-------------------------------------------------------------
fcDActionBaseListItem :: fcDActionBaseListItem(fc_int nActionValue)
{
    m_nActionValue = nActionValue;
}
//-------------------------------------------------------------
fcDActionBaseListItem :: fcDActionBaseListItem(const fcDActionBaseListItem& item)
{
    m_nActionValue = item.m_nActionValue;
}
//-------------------------------------------------------------
fcDActionBaseListItem :: ~fcDActionBaseListItem()
{
}
//-------------------------------------------------------------
void fcDActionBaseListItem :: SetActionValue(fc_int nActionValue)
{
    m_nActionValue =  nActionValue;
}
//-------------------------------------------------------------
fc_int fcDActionBaseListItem :: GetActionValue() const
{
    return m_nActionValue;
}
//-------------------------------------------------------------
fcDActionBoolListItem :: fcDActionBoolListItem()
{
    m_bDefValue     = false;
    m_bFlagDefValue = false;
}
//-------------------------------------------------------------
fcDActionBoolListItem :: fcDActionBoolListItem(fc_int nActionValue, fc_bool bDefValue)
    : fcDActionBaseListItem(nActionValue)
{
    m_bDefValue     = bDefValue;
    m_bFlagDefValue = true;
}
//-------------------------------------------------------------
fcDActionBoolListItem :: fcDActionBoolListItem(const fcDActionBoolListItem& item)
    : fcDActionBaseListItem(item)
{
    m_bDefValue     = item.m_bDefValue;
    m_bFlagDefValue = item.m_bFlagDefValue;
}
//-------------------------------------------------------------
fcDActionBoolListItem :: ~fcDActionBoolListItem()
{
}
//-------------------------------------------------------------
fcDActionBaseListItem* fcDActionBoolListItem :: NewActionListItem() const
{
    return new fcDActionBoolListItem(*this);
}
//-------------------------------------------------------------
fc_bool fcDActionBoolListItem :: IsDefaultValueSet() const
{
    return m_bFlagDefValue;
}
//-------------------------------------------------------------
fc_bool fcDActionBoolListItem :: IsPropertiesSet() const
{
    return false;
}
//-------------------------------------------------------------
fcActionType fcDActionBoolListItem :: ActionType() const
{
    return fcBOOL_ACTION;
}
//-------------------------------------------------------------
const fcDActionBoolListItem& fcDActionBoolListItem :: operator=(const fcDActionBoolListItem& item)
{
    m_nActionValue  = item.m_nActionValue;
    m_bDefValue     = item.m_bDefValue; 
    m_bFlagDefValue = item.m_bFlagDefValue;

    return *this;
}
//-------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const fcDActionBoolListItem& fcBoolAction)
{
    if(!fcBoolAction.m_bFlagDefValue)
        return stream;

    stream << " [ ACTION_BOOL: action_value=" << fcBoolAction.m_nActionValue << ";";
    stream << " default_value=" << fcBoolAction.m_bDefValue << "] ";

    return stream;
}
//-------------------------------------------------------------
void fcDActionBoolListItem :: SetDefaultValue(fc_bool bDefValue)
{
    m_bDefValue     = bDefValue;
    m_bFlagDefValue = true;
}
//-------------------------------------------------------------
fc_bool fcDActionBoolListItem :: GetDefaultValue() const
{
    return m_bDefValue;
}
//-------------------------------------------------------------
fcDActionNumberListItem :: fcDActionNumberListItem()
{
    m_nDefValue     = 0;
    m_bFlagDefValue = false;
    m_nMin          = 0;
    m_bFlagMin      = false;
    m_nMax          = 0;
    m_bFlagMax      = false;
    m_nStep         = 0;
    m_bFlagStep     = false;
    m_bFlagType     = false;
}
//-------------------------------------------------------------
fcDActionNumberListItem :: fcDActionNumberListItem(fc_int nActionValue)
    : fcDActionBaseListItem(nActionValue)
{
    m_nDefValue     = 0;
    m_bFlagDefValue = false;
    m_nMin          = 0;
    m_bFlagMin      = false;
    m_nMax          = 0;
    m_bFlagMax      = false;
    m_nStep         = 0;
    m_bFlagStep     = false;
    m_bFlagType     = false;
}
//-------------------------------------------------------------
fcDActionNumberListItem :: fcDActionNumberListItem(fc_int nActionValue, fc_int nDefValue)
    : fcDActionBaseListItem(nActionValue)
{
    m_nDefValue     = nDefValue;
    m_bFlagDefValue = true;
    m_nMin          = 0;
    m_bFlagMin      = false;
    m_nMax          = 0;
    m_bFlagMax      = false;
    m_nStep         = 0;
    m_bFlagStep     = false;
    m_bFlagType     = false;
}
//-------------------------------------------------------------
fcDActionNumberListItem :: fcDActionNumberListItem(fc_int nActionValue, fc_int nMin, fc_int nMax, 
                                                   fc_int nStep, const fc_char* strType)
    : fcDActionBaseListItem(nActionValue)
{
    m_nDefValue     = 0;
    m_bFlagDefValue = false;
    m_nMin          = nMin;
    m_bFlagMin      = true;
    m_nMax          = nMax;
    m_bFlagMax      = true;
    m_nStep         = nStep;
    m_bFlagStep     = true;
    m_strType       = strType;
    m_bFlagType     = true;
}
//-------------------------------------------------------------
fcDActionNumberListItem :: fcDActionNumberListItem(fc_int nActionValue, fc_int nMin, fc_int nMax, 
                                                   fc_int nStep, const fcString& strType)
    : fcDActionBaseListItem(nActionValue)
{
    m_nDefValue     = 0;
    m_bFlagDefValue = false;
    m_nMin          = nMin;
    m_bFlagMin      = true;
    m_nMax          = nMax;
    m_bFlagMax      = true;
    m_nStep         = nStep;
    m_bFlagStep     = true;
    m_strType       = strType;
    m_bFlagType     = true;
}
//-------------------------------------------------------------
fcDActionNumberListItem :: fcDActionNumberListItem(const fcDActionNumberListItem& item)
    : fcDActionBaseListItem(item)
{
    m_nDefValue     = item.m_nDefValue;
    m_bFlagDefValue = item.m_bFlagDefValue;
    m_nMin          = item.m_nMin;
    m_bFlagMin      = item.m_bFlagMin;
    m_nMax          = item.m_nMax;
    m_bFlagMax      = item.m_bFlagMax;
    m_nStep         = item.m_nStep;
    m_bFlagStep     = item.m_bFlagStep;
    m_strType       = item.m_strType;
    m_bFlagType     = item.m_bFlagType;
}
//-------------------------------------------------------------
fcDActionNumberListItem :: ~fcDActionNumberListItem()
{
}
//-------------------------------------------------------------
fcDActionBaseListItem* fcDActionNumberListItem :: NewActionListItem() const
{
    return new fcDActionNumberListItem(*this);
}
//-------------------------------------------------------------
fc_bool fcDActionNumberListItem :: IsDefaultValueSet() const
{
    return m_bFlagDefValue;
}
//-------------------------------------------------------------
fc_bool fcDActionNumberListItem :: IsPropertiesSet() const
{
    return m_bFlagMin || m_bFlagMax || m_bFlagStep || m_bFlagType;
}
//-------------------------------------------------------------
fcActionType fcDActionNumberListItem :: ActionType() const
{
    return fcNUMBER_ACTION;
}
//-------------------------------------------------------------
const fcDActionNumberListItem& fcDActionNumberListItem :: operator=(const fcDActionNumberListItem& item)
{
    m_nActionValue  = item.m_nActionValue;
    m_nDefValue     = item.m_nDefValue;
    m_bFlagDefValue = item.m_bFlagDefValue;
    m_nMin          = item.m_nMin;
    m_bFlagMin      = item.m_bFlagMin;
    m_nMax          = item.m_nMax;
    m_bFlagMax      = item.m_bFlagMax;
    m_nStep         = item.m_nStep;
    m_bFlagStep     = item.m_bFlagStep;
    m_strType       = item.m_strType;
    m_bFlagType     = item.m_bFlagType;

    return *this;
}
//-------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const fcDActionNumberListItem& fcNumberAction)
{
    stream << " [ ACTION_NUMBER: action_value=" << fcNumberAction.m_nActionValue;
    if(fcNumberAction.m_bFlagDefValue)
    {
        stream << "; default_value=" << fcNumberAction.m_nDefValue << "] ";
    } else {
        if(fcNumberAction.m_bFlagMin)
            stream << "; min=" << fcNumberAction.m_nMin << ";";
        if(fcNumberAction.m_bFlagMax)
            stream << "; max=" << fcNumberAction.m_nMax << ";";
        if(fcNumberAction.m_bFlagStep)
            stream << "; step=" << fcNumberAction.m_nStep << ";";
        if(fcNumberAction.m_bFlagType)
            stream << "; data=" << fcNumberAction.m_strType << ";";
        stream << "] ";
    }

    return stream;
}
//-------------------------------------------------------------
void fcDActionNumberListItem :: SetDefaultValue(fc_int nDefValue)
{
    m_nDefValue     = nDefValue;
    m_bFlagDefValue = true;
    m_bFlagMin      = false;
    m_bFlagMax      = false;
    m_bFlagStep     = false;
    m_bFlagType     = false;
}
//-------------------------------------------------------------
void fcDActionNumberListItem :: SetProperties(fc_int nMin, fc_int nMax, fc_int nStep, const fc_char* strType)
{
    m_nMin          = nMin;
    m_bFlagMin      = true;
    m_nMax          = nMax;
    m_bFlagMax      = true;
    m_nStep         = nStep;
    m_bFlagStep     = true;
    m_strType       = strType;
    m_bFlagType     = true;
    m_bFlagDefValue = false;
}
//-------------------------------------------------------------
void fcDActionNumberListItem :: SetProperties(fc_int nMin, fc_int nMax, fc_int nStep, const fcString& strType)
{
    m_nMin          = nMin;
    m_bFlagMin      = true;
    m_nMax          = nMax;
    m_bFlagMax      = true;
    m_nStep         = nStep;
    m_bFlagStep     = true;
    m_strType       = strType;
    m_bFlagType     = true;
    m_bFlagDefValue = false;
}
//-------------------------------------------------------------
void fcDActionNumberListItem :: SetMin(fc_int nMin)
{
    m_nMin          = nMin;
    m_bFlagMin      = true;
    m_bFlagDefValue = false;
}
//-------------------------------------------------------------
void fcDActionNumberListItem :: SetMax(fc_int nMax)
{
    m_nMax          = nMax;
    m_bFlagMax      = true;
    m_bFlagDefValue = false;
}
//-------------------------------------------------------------
void fcDActionNumberListItem :: SetStep(fc_int nStep)
{
    m_nStep         = nStep;
    m_bFlagStep     = true;
    m_bFlagDefValue = false;
}
//-------------------------------------------------------------
void fcDActionNumberListItem :: SetType(const fc_char* strType)
{
    m_strType       = strType;
    m_bFlagType     = true;
    m_bFlagDefValue = false;
}
//-------------------------------------------------------------
void fcDActionNumberListItem :: SetType(const fcString& strType)
{
    m_strType       = strType;
    m_bFlagType     = true;
    m_bFlagDefValue = false;
}
//-------------------------------------------------------------
fc_int fcDActionNumberListItem :: GetDefaultValue() const
{
    return m_nDefValue;
}
//-------------------------------------------------------------
void fcDActionNumberListItem :: GetProperties(fc_int& nMin, fc_int& nMax, fc_int& nStep, fcString& strType)
{
    nMin    = m_nMin;
    nMax    = m_nMax;
    nStep   = m_nStep;
    strType = m_strType;
}
//-------------------------------------------------------------
fc_int fcDActionNumberListItem :: GetMin() const
{
    return m_nMin;
}
//-------------------------------------------------------------
fc_int fcDActionNumberListItem :: GetMax() const
{
    return m_nMax;
}
//-------------------------------------------------------------
fc_int fcDActionNumberListItem :: GetStep() const
{
    return m_nStep;
}
//-------------------------------------------------------------
const fcString& fcDActionNumberListItem :: GetType() const
{
    return m_strType;
}
//-------------------------------------------------------------
fc_bool fcDActionNumberListItem :: IsMinSet() const
{
    return m_bFlagMin;
}
//-------------------------------------------------------------
fc_bool fcDActionNumberListItem :: IsMaxSet() const
{
    return m_bFlagMax;
}
//-------------------------------------------------------------
fc_bool fcDActionNumberListItem :: IsStepSet() const
{
    return m_bFlagStep;
}
//-------------------------------------------------------------
fc_bool fcDActionNumberListItem :: IsTypeSet() const
{
    return m_bFlagType;
}
//-------------------------------------------------------------
fcDActionStringListItem :: fcDActionStringListItem()
{
    m_bFlagDefValue = false;
    m_bFlagData     = false;
}
//-------------------------------------------------------------
fcDActionStringListItem :: fcDActionStringListItem(fc_int nActionValue, const fc_char* strDefValue)
    : fcDActionBaseListItem(nActionValue)
{
    m_strDefValue   = strDefValue;
    m_bFlagDefValue = true;
    m_bFlagData     = false;
}
//-------------------------------------------------------------
fcDActionStringListItem :: fcDActionStringListItem(fc_int nActionValue, const fcString& strDefValue)
    : fcDActionBaseListItem(nActionValue)
{
    m_strDefValue   = strDefValue;
    m_bFlagDefValue = true;
    m_bFlagData     = false;
}
//-------------------------------------------------------------
fcDActionStringListItem :: fcDActionStringListItem(fc_int nActionValue, const fcList< fcStringEntry >& lstData)
    : fcDActionBaseListItem(nActionValue)
{
    m_bFlagDefValue = false;
    m_lstData       = lstData;
    m_bFlagData     = true;
}
//-------------------------------------------------------------
fcDActionStringListItem :: fcDActionStringListItem(const fcDActionStringListItem& item)
    : fcDActionBaseListItem(item)
{
    m_strDefValue   = item.m_strDefValue;
    m_bFlagDefValue = item.m_bFlagDefValue;
    m_lstData       = item.m_lstData;
    m_bFlagData     = item.m_bFlagData;
}
//-------------------------------------------------------------
fcDActionStringListItem :: ~fcDActionStringListItem()
{
    EraseDataList();
}
//-------------------------------------------------------------
fcDActionBaseListItem* fcDActionStringListItem :: NewActionListItem() const
{
    return new fcDActionStringListItem(*this);
}
//-------------------------------------------------------------
fc_bool fcDActionStringListItem :: IsDefaultValueSet() const
{
    return m_bFlagDefValue;
}
//-------------------------------------------------------------
fc_bool fcDActionStringListItem :: IsPropertiesSet() const
{
    return m_bFlagData;
}
//-------------------------------------------------------------
fcActionType fcDActionStringListItem :: ActionType() const
{
    return fcSTRING_ACTION;
}
//-------------------------------------------------------------
const fcDActionStringListItem& fcDActionStringListItem :: operator=(const fcDActionStringListItem& item)
{
    m_nActionValue  = item.m_nActionValue;
    m_strDefValue   = item.m_strDefValue;
    m_bFlagDefValue = item.m_bFlagDefValue;
    m_lstData       = item.m_lstData;
    m_bFlagData     = item.m_bFlagData;

    return *this;
}
//-------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const fcDActionStringListItem& fcStringAction)
{
    stream << " [ ACTION_STRING: action_value=" << fcStringAction.m_nActionValue;
    if(fcStringAction.m_bFlagDefValue)
    {
        stream << "; default_value=" << fcStringAction.m_strDefValue << "] ";
    } else 
    if(fcStringAction.m_bFlagData)
    {
        for(fc_int i = 0; i < fcStringAction.m_lstData.Size(); i++)
        {
            fcStringEntry strEntry = fcStringAction.m_lstData.at(i);
            stream << "; data=" << strEntry.GetValue();
        }
        stream << "] ";
    }

    return stream;
}
//-------------------------------------------------------------
void fcDActionStringListItem :: AppendData(const fcStringEntry& strEntry)
{
    m_lstData.Append(strEntry);

    m_bFlagDefValue = false;
    m_bFlagData     = true;
}
//-------------------------------------------------------------
void fcDActionStringListItem :: AppendDataList(const fcList< fcStringEntry >& lstData)
{
    for(fc_int i = 0; i < lstData.Size(); i++)
        m_lstData.Append(lstData.at(i));

    m_bFlagDefValue = false;
    m_bFlagData     = true;
}
//-------------------------------------------------------------
void fcDActionStringListItem :: SetDefaultValue(const fc_char* strDefValue)
{
    m_strDefValue   = strDefValue;
    m_bFlagDefValue = true;
    m_bFlagData     = false;
}
//-------------------------------------------------------------
void fcDActionStringListItem :: SetDefaultValue(const fcString& strDefValue)
{
    m_strDefValue   = strDefValue;
    m_bFlagDefValue = true;
    m_bFlagData     = false;
}
//-------------------------------------------------------------
void fcDActionStringListItem :: SetData(const fcList< fcStringEntry >& lstData)
{
    m_bFlagDefValue = false;
    m_lstData       = lstData;
    m_bFlagData     = true;
}
//-------------------------------------------------------------
void fcDActionStringListItem :: SetData(const fcStringEntry& strEntry, fc_uint pos)
{
    if(IsPositionNotValid(pos))
    {
        MessageHandler.Warning("fcDependencyActions.cpp", "SetData(const fcStringEntry& strEntry, fc_uint pos)",
                "pos index is out of range!");
    }

    m_lstData.at(pos) = strEntry;
}
//-------------------------------------------------------------
void fcDActionStringListItem :: SetData(fc_uint pos, const fcStringEntry& strEntry)
{
    SetData(strEntry, pos);
}
//-------------------------------------------------------------
const fcString& fcDActionStringListItem :: GetDefaultValue() const
{
    return m_strDefValue;
}
//-------------------------------------------------------------
const fcList< fcStringEntry >& fcDActionStringListItem :: GetData() const
{
    return m_lstData;
}
//-------------------------------------------------------------
const fcStringEntry& fcDActionStringListItem :: GetData(fc_uint pos) const
{
    if(IsPositionNotValid(pos))
    {
        MessageHandler.Warning("fcDependencyActions.cpp", "SetData(const fcStringEntry& strEntry, fc_uint pos)",
                "pos index is out of range!");
    }

    return m_lstData.at(pos);
}
//-------------------------------------------------------------
void fcDActionStringListItem :: EraseDataList()
{
    m_lstData.EraseAll();
}
//-------------------------------------------------------------
fc_int fcDActionStringListItem :: GetDataListSize() const
{
    return m_lstData.Size();
}
//-------------------------------------------------------------
fc_bool fcDActionStringListItem :: IsPositionValid(fc_uint pos) const
{
    if( pos < 0 || pos > m_lstData.Size() -1 )
        return false;
    return true;
}
//-------------------------------------------------------------
fc_bool fcDActionStringListItem :: IsPositionNotValid(fc_uint pos) const
{
    if( pos < 0 || pos > m_lstData.Size() -1 )
        return true;
    return false;
}
//-------------------------------------------------------------
