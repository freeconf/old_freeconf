/***************************************************************************
                          fcDOMConfigFile.h  -  description
                             -------------------
    begin                : 2004/08/12
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCDOMCONFIGFILE_H__
#define __FCDOMCONFIGFILE_H__

#include <xercesc/dom/DOMDocument.hpp>
#include "fcCKEntry.h"
#include "fcCSEntry.h"
// #include "fcCreateCEntry.h"
#include "fcStack.h"
#include "fcTypes.h"

XERCES_CPP_NAMESPACE_USE

class fcTemplateStructure;

class fcDOMConfigFile
{
   protected:
   
   //! Related config section 
   /*! It is not deleted in destructor.
    */
   fcCSEntry* config_section;

   //! DOM structure for config file
   DOMDocument* config_file;

   //! Method for filling config structure from DOMDocument
   /*! We need to convert data from DOM document @param node
       to config section @param c_section. The template section 
       @param t_section is necessary to know configuration
       structure - section names, keyword types etc.
       We also need to know whether we are parsing a config file
       with default values or main configuration file. In the case of
       configuration file we have to set the flag present for each 
       keyword mentioned in the file.
       */
   fc_bool ProcessDOMNode( DOMNode* node,
                           fcCSEntry* c_section,
                           const fcTSEntry* t_section,
                           fc_bool default_setting );
   
   //! Processing element node
   void ProcessElementNode( fcDOMNode* node,                                                      fcCSEntry* c_section,
                            const fcTSEntry* t_section,
                            fc_bool default_setting );
                  

   //! Process just TEXT_NODE with a keyword value
   void SetKeywordValue( DOMNode* node, fcCKEntry* entry );
   public:

   //! Basic constructor
   fcDOMConfigFile();


   explicit fcDOMConfigFile(fcCSEntry *c_section);
   
   //! Destructor
   ~fcDOMConfigFile();
  
   //! Config section setter
   void SetConfigSection( fcCSEntry* c_section );
  
   //! Config section getter
   fcCSEntry* GetConfigSection();

   //! Method for parsing given fil
   fc_bool Parse( const fc_char* file_name,
                  const fcTSEntry* t_section,
                  fc_bool default_setting );

   //! Write the config file
   fc_bool Write(const fc_char* output_file);         

#ifdef DEBUG
   //! Print DOM node structure
   void PrintDOMNode( DOMNode* node );
#endif

   friend std::ostream& operator << (std::ostream& stream, fcDOMConfigFile& conf_file);
};


#endif
