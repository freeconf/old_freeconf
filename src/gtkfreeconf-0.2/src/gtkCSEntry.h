/***************************************************************************
                          gtkCSEntry.h  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef gtkCSEntryH
#define gtkCSEntryH

#include <gtk/gtk.h>
#include <fcCSEntry.h>

class gtkCSEntry : public fcCSEntry
{
   public:

   //! Basic constructor
   gtkCSEntry();

   //! Copy constructor
   gtkCSEntry( const gtkCSEntry& section_entry );

   //! Constructor template entry pointer
   gtkCSEntry( const fcTEntry* t_entry );

   //! Destructor
   ~gtkCSEntry();
     
   //! Creates new config entry by the type of template entry
   virtual fcCEntry* CreateCEntry( const fcTEntry* t_entry, fc_bool ignore_multiple ) const;

   //! Render GUI
   void Render( GtkWidget* parent_widget,
                fc_bool create_frame, 
                fcCSEntry* main_c_section );

   //! Recasting the displayed widgets
   void RecastingWidgets( fcCSEntry* main_c_section );

};

#endif
