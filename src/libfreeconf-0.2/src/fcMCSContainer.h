/***************************************************************************
                          fcMCSContainer.h  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcMCSContainerH
#define fcMCSContainerH

#include "fcCSEntry.h"
#include "fcTEntry.h"

//! Conatiner for multiple config sections
class fcMCSContainer : public fcCSEntry
{
   //! Section with default values 
   /*! It is usefull for example for creating a new section by user
       to have some initial setting.
    */
   fcCSEntry* default_values;

   public:

   //! Basic constructor
   fcMCSContainer();

   //! Copy constructor
   fcMCSContainer( const fcMCSContainer& container );

   //! Constructor template entry pointer
   fcMCSContainer( const fcTEntry* t_entry );

   //! Destructor
   ~fcMCSContainer();
   
   //! Returns true if it is a multiple config entries conatianer
   fc_bool IsMultipleEntryContainer() const;
};

#endif
