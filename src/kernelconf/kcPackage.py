#!/usr/bin/python
#
# Kernel Config Package
#
# begin: 11.11.2008 by Jan Ehrenberger <lankvil@seznam.cz>
#
# Create freeconf package
#

import kcFiles
import kcFreeconf
import kcParser
import kcVersion
import os
import os.path
from kcLog import log


def mkdir(dir):
	"""Helper function for os.makedirs. Checks existence of the dir first."""
	if not os.path.isdir(dir):
		os.makedirs(dir)

def touch(file):
	"""Helper function to touch the file."""
	if not os.path.isfile(file):
		open(file, 'a').close()


class Package:
	package_root = os.environ["HOME"] + "/.freeconf/packages"

	def __init__(self, path, arch = 'i386'):
		self.kernel_path = path
		self.arch = arch
		self.name = "kernel"
		self.data = None

		# Get version of our kernel
		self.version = kcVersion.Version(self.kernel_path)
		log.info('Kernel version: ' + str(self.version))
		self.name = "%s-%s-%s" % (self.name, self.version, self.arch)

		# Create label for help file
		self.label = "Linux Kernel " + str(self.version) + " - " + self.version.name
	
		# Set directories
		self.package_path = self.package_root + "/" + self.name
		self.help_path = self.package_path + "/help/en"
		self.strings_path = self.package_path + "/strings"

		# Set current working directory to kernel's directory
		# Files included from Kconf file are relative to this directory
		os.chdir(self.kernel_path)

		# Parse kernel config data
		source = kcParser.Source("arch/" + self.arch + "/Kconfig")
		source.parse()

		self.data = source.analyze()

		# Define string list data
		self.string_lists = {"tristate": ['yes', 'module', 'no']}
	
	def write(self):
		"""Write package files to disk"""
		# Create needed directories
		mkdir(self.package_path)
		mkdir(self.help_path)
		mkdir(self.strings_path)

		# Header file
		header = kcFiles.Header(self.package_path)
		header.write()

		# Write string lists
		strings = kcFiles.StringLists(self.string_lists, self.strings_path + "/" + header.strings)
		strings.write()
		# Help file
		help = kcFiles.Help(self.data, self.help_path + "/" + header.help, self.label)
		help.write()
		# Template file
		template = kcFiles.Template(self.data, self.package_path + "/" + header.template)
		template.write()
		# XST Transformation
		transform = kcFiles.Transform(self.data, self.package_path + "/" + header.transform)
		transform.write()
		# Default values
		default = kcFiles.Default(self.data, self.package_path + "/" + header.default_values)
		default.write()
		# Config file
		touch(self.package_path + "/" + header.output)

	
