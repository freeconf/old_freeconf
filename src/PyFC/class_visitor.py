#!/usr/bin/python
#
# class_visitor.py
# begin: 10.7.2011 by Jan Ehrenberger
#
# Support for hierarchical class visitor in Freeconf
# Read more about this pattern at http://c2.com/cgi/wiki?HierarchicalVisitorPattern
#


class FcVisitor:
	"""Interface for hierarchical visitor."""
	def enter(self, s):
		"""Enter a section. Redefine this method in subclass"""
		return True

	def leave(self, s):
		"""Leave a section. Redefine this method in subclass."""
		return True
	
	def visit(self, n):
		"""Visit node. Redefine this method in subclass."""
		return True

class FcVisitee:
	"""Interface for visited object in hierarchy. This class represents leaf objects(keywords)."""
	def accept(self, visitor):
		"""Accept visitor."""
		return visitor.visit(self)

class FcSVisitee:
	"""Interface for visited object in hierarchy. This class represents node objects(sections).
	   Subclassed classes must have attribute 'entries'."""
	def accept(self, visitor):
		if visitor.enter(self):
			for elt in self.entries:
				elt.accept(visitor)
		return visitor.leave(self)
