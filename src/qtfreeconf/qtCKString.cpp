#include <fcTEntry.h>
#include <fcDOMConfigFile.h>
#include <fcPackage.h>
#include <fcTKString.h>
#include <fcCKString.h>
#include <fcCGEntry.h>

#include <QLineEdit>
#include <QComboBox>
#include <QLabel>
#include <QDebug>
#include <QHBoxLayout>

#include "qtCSEntry.h"
#include "qtCKString.h"

qtCKString::qtCKString (fcPackage *package, const qtCWidget *parent)
  :qtCWidget(package, parent),
  _dropDown(false),
  _combo(0),
  _line(0)
{
 
}


qtCKString::qtCKString (const fcCGEntry *guiEntry, fcPackage *package, const qtCWidget *parent)
  :qtCWidget(guiEntry, package, parent),
  _dropDown(false),
  _combo(0),
  _line(0)
{

}


void qtCKString::render ()
{
  if (!_parent || !_parent->fcLayout() || !_guiEntry || !_guiEntry->configBuddy())
  {
    qDebug() << "Widget's parent, config model and layout must be set before render";
    return;
  }
  
  const fcTKString *tEntry = dynamic_cast<const fcTKString*>(_guiEntry->configBuddy()->GetTEntry());
  if (!tEntry)
  {
    qDebug() << "No template entry associated";
    return;
  }
  QHBoxLayout *hLayout = new QHBoxLayout();
  QLabel *label = new QLabel(tEntry->GetLabel().Data());
  label->setToolTip(tEntry->GetHelp().Data());
  hLayout->addWidget(label);

  if(_dropDown)
  {
    if (!_combo) _combo = new QComboBox();
    for (int i = 0; i < (int)tEntry->Size(); i++)
    {
      _combo->addItem(tEntry->GetString(i).GetValue().Data());
      if (dynamic_cast<fcCKString*>(_guiEntry->configBuddy())->GetValue() == tEntry->GetString(i).GetValue()) _combo->setCurrentIndex(i);
    }
    if (_package)
      _guiEntry->configBuddy()->GetCEntryAction(_package->configFile()->GetConfigSection());
    //  //_combo->setEnabled(_guiEntry->configBuddy()->CEntryIsActive(_package->mainCSection()));
    
    _combo->setEditText(dynamic_cast<fcCKString*>(_guiEntry->configBuddy())->GetValue().Data());
    label->setBuddy(_combo);
    hLayout->addWidget(_combo);
  }
  else
  {
    if (!_line) _line = new QLineEdit();
    _line->setText(dynamic_cast<fcCKString*>(_guiEntry->configBuddy())->GetValue().Data());
    if (_package)
      _guiEntry->configBuddy()->GetCEntryAction(_package->configFile()->GetConfigSection());
      //_line->setEnabled(_guiEntry->configBuddy()->CEntryIsActive(_package->mainCSection()));
    
    label->setBuddy(_line);
    hLayout->addWidget(_line);
  }

  _rendered = true;

  attach(_attached);
  
  _parent->fcLayout()->addLayout(hLayout);
  
  //   compileEntry( isSensitive, current_entry );
  
  /*                Set tooltips*/
  /*                DBG_COUT( " setting tooltips to ... " << help );*/
  /*                gtk_tooltips_set_tip( tooltips,
  value_widget,
  help,
  NULL );*/
}


void qtCKString::textChanged (const QString &text)
{
  dynamic_cast<fcCKString*>(_guiEntry->configBuddy())->SetValue(text.toStdString().c_str());
  /*  for (fc_int i = 0; i < ((fcCEntry*)(this))->GetDependentsSize(); i++)
  {
    fcCEntry *c_entry = ((fcCEntry*)(this))->GetDependents(i);
    fc_bool isSensitive =  c_entry->CEntryIsActive(_mainCSection);*/
  //     compileEntry( isSensitive, c_entry );
  //   }
}


void qtCKString::invalidateGui()
{
  if (_line) delete _line;
  if (_combo) delete _combo;
  _line = 0;
  _combo = 0;
}


void qtCKString::attach(fc_bool attached)
{
  if (!_guiEntry->configBuddy()) return;
  qtCWidget::attach(attached);
  
  if (!attached)
  {
    if (_dropDown)
    {
      if (!_combo) return;
      disconnect(_combo, SIGNAL(editTextChanged(const QString&)), 0, 0);
      disconnect(_combo, SIGNAL(currentIndexChanged(const QString&)), 0, 0);
    }
    else
    {
      if (!_line) return;
      disconnect(_line, SIGNAL(textChanged(const QString&)), 0, 0);
    }
  }
  else
  {
    if (_dropDown)
    {
      if (!_combo) return;
      connect(_combo, SIGNAL(editTextChanged(const QString&)), this, SLOT(textChanged(const QString&)));
      connect(_combo, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(textChanged(const QString&)));
      dynamic_cast<fcCKString*>(_guiEntry->configBuddy())->SetValue(_combo->currentText().toStdString().c_str());
    }
    else
    {
      if (!_line) return;
      connect(_line, SIGNAL(textChanged(const QString&)), this, SLOT(textChanged(const QString&)));
      dynamic_cast<fcCKString*>(_guiEntry->configBuddy())->SetValue(_line->text().toStdString().c_str());
    }
  }
  
}

