/***************************************************************************
                          fcTransformer.h  -  description
                             -------------------
    begin                : 2004/10/13
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCTRANSFORMER_H__
#define __FCTRANSFORMER_H__

#include "fcTypes.h"

//! XSL transformer from freeconf config format to native format
class fcTransformer
{
   public:
   //! Method for XSL transformation
   void Transform( const fc_char* config_file, 
                   const fc_char* xsl_file,
                   const fc_char* output_file );
};

#endif
