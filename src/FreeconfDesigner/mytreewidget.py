#!/usr/bin/python

from PyFC.designer_interface import *

from PyQt4 import QtCore, QtGui


class TreeItem (QtGui.QTreeWidgetItem):

	def __init__ (self, parent, communicator, translator):
		QtGui.QTreeWidgetItem.__init__(self, parent, QtCore.QStringList())
		self.communicator = communicator
		self.translator = translator

	def updateContent (self):
		if self.communicator == None: return
		self.updateName()
		self.updateType()

	def updateName (self):
		if self.communicator == None: return
		self.setText(0, self.communicator.name)

	def updateType (self):
		if self.communicator == None: return
		if self.communicator.type == FcTypes.SECTION:
			self.setText(1, self.translator.tr("Section"))
		if self.communicator.type == FcTypes.BOOL:
			self.setText(1, self.translator.tr("Boolean"))
		if self.communicator.type == FcTypes.NUMBER:
			self.setText(1, self.translator.tr("Number"))
		if self.communicator.type == FcTypes.STRING:
			self.setText(1, self.translator.tr("String"))
		if self.communicator.type == FcTypes.REFERENCE:
			self.setText(1, self.translator.tr("Reference"))


class MyTreeWidget (QtGui.QTreeWidget):

	def __init__ (self, parent = None):
		QtGui.QTreeWidget.__init__(self, parent)
		self.__package = None
		self.setDropIndicatorShown(True)
		self.counter = 1

	@property
	def package (self):
		return self.__package

	@package.setter
	def package (self, package):
		self.__package = package


	def dragEnterEvent (self, event):
		source = self.itemAt(event.pos())
		if source == None:
			event.ignore()
			return
		event.acceptProposedAction()


	def dragMoveEvent (self, event):
		source = self.currentItem()
		destination = self.itemAt(event.pos())
		if (destination == None) or (destination.communicator.type == FcTypes.SECTION) and (destination != source) and (destination != source.parent()):
			event.acceptProposedAction()
			QtGui.QTreeWidget.dragMoveEvent(self, event)
		else:
			event.ignore()


	def dropEvent (self, event):
		#self.emit(QtCore.SIGNAL("nodeMoved"), self.currentItem(), self.itemAt(event.pos()))
		source = self.currentItem()
		QtGui.QTreeWidget.dropEvent(self, event)
		destination = source.parent()
		if destination == None:
			if source.communicator.moveToPosition(self.__package.template, self.indexOfTopLevelItem(source)) == False:
				#print "failed1"
				event.ignore()
				return
		else:
			if source.communicator.moveToPosition(destination.communicator, destination.indexOfChild(source)) == False:
				#print "failed2"
				event.ignore()
				return


	def mousePressEvent (self, event):
		node = self.itemAt(event.pos())
		self.setCurrentItem(node)
		if event.button() == QtCore.Qt.RightButton:
			menu = QtGui.QMenu (self)
			if node == None or node.communicator.type == FcTypes.SECTION:
				QtCore.QObject.connect(menu.addAction(self.tr("Add section")), QtCore.SIGNAL("triggered()"), self.addSection)
				QtCore.QObject.connect(menu.addAction(self.tr("Add config key")), QtCore.SIGNAL("triggered()"), self.addKey)
				menu.addSeparator()
			QtCore.QObject.connect(menu.addAction(self.tr("Delete this node")), QtCore.SIGNAL("triggered()"), self.deleteNode)
			menu.exec_(self.mapToGlobal(event.pos()))
		else:
			QtGui.QTreeWidget.mousePressEvent(self, event)


	def addSection (self):
		node = self.currentItem()
		text = self.tr("New_section_" + str(self.counter))
		if node == None:
			communicator = self.__package.template.appendNode(text, FcTypes.SECTION)
			if communicator != None:
				item = TreeItem(self, communicator, self)
				item.updateContent()
				self.addTopLevelItem(item)
			else:
				QtGui.QMessageBox.warning(self, self.tr("Section already exists"), self.tr("The section <b>" + text + "</b> already exists. Please rename the existing section before you try again."))
				return
		else:
			communicator = node.communicator.appendNode(text, FcTypes.SECTION)
			if communicator != None:
				item = TreeItem(node, communicator, self)
				item.updateContent()
			else:
				QtGui.QMessageBox.warning(self, self.tr("Section already exists"), self.tr("The section <b>" + text + "</b> already exists. Please rename the existing section before you try again."))
				return
		self.counter+=1
		self.expandItem(item)
		self.setCurrentItem(item)


	def addKey (self):
		node = self.currentItem()
		text = self.tr("New_key_" + str(self.counter))
		if node == None:
			communicator = self.__package.template.appendNode(text, FcTypes.BOOL)
			if communicator != None:
				item = TreeItem(self, communicator, self)
				item.updateContent()
				self.addTopLevelItem(item)
			else:
				QtGui.QMessageBox.warning(self, self.tr("Key already exists"), self.tr("The key <b>" + text + "</b> already exists. Please rename the existing key before you try again."))
				return
		else:
			communicator = node.communicator.appendNode(text, FcTypes.BOOL)
			if communicator != None:
				item = TreeItem(node, communicator, self)
				item.updateContent()
			else:
				QtGui.QMessageBox.warning(self, self.tr("Key already exists"), self.tr("The key <b>" + text + "</b> already exists. Please rename the existing key before you try again."))
				return
		self.counter+=1
		self.expandItem(item)
		self.setCurrentItem(item)


	def deleteNode (self):
		node = self.currentItem()
		if QtGui.QMessageBox.question(self, self.tr("Delete node?"), \
			self.tr("Are you sure you want to delete the node <b>" + node.communicator.name + "</b>?"), \
			QtGui.QMessageBox.Yes or QtGui.QMessageBox.No, QtGui.QMessageBox.No) == QtGui.QMessageBox.Yes:
				node.communicator.deleteNode()
				if node.parent() != None:
					node.parent().removeChild(node)
				else:
					self.takeTopLevelItem(self.indexOfTopLevelItem(node))


	def changeKeyName (self, name):
		node = self.currentItem()
		if node == None: return
		state = node.communicator.setName(str(name))
		if state == True:
			node.updateName()
		self.emit(QtCore.SIGNAL("keyNameValid(bool)"), state)


	def changeSectionName (self, name):
		node = self.currentItem()
		if node == None: return
		state = node.communicator.setName(str(name))
		if state == True:
			node.updateName()
		self.emit(QtCore.SIGNAL("sectionNameValid(bool)"), state)


	def changeKeyType (self, type):
		node = self.currentItem()
		if node.communicator.type == type: return
		if node == None: return
		if type == 0:
			node.communicator.setType(FcTypes.BOOL)
		if type == 1:
			node.communicator.setType(FcTypes.NUMBER)
		if type == 2:
			node.communicator.setType(FcTypes.STRING)
		if type == 3:
			node.communicator.setType(FcTypes.REFERENCE)
		node.updateType()
		self.emit(QtCore.SIGNAL("typeChanged(QTreeWidgetItem*)"), node)

	def changeActivity (self, activity):
		node = self.currentItem()
		if node == None: return
		node.communicator.active = activity

	def changeMandatory (self, mandatory):
		node = self.currentItem()
		if node == None: return
		node.communicator.mandatory = mandatory
	
	def changeMultiple (self, multiple):
		node = self.currentItem()
		if node == None: return
		node.communicator.multiple = multiple

	def changePrecision (self, precision):
		if precision < 0: return
		node = self.currentItem()
		if node == None: return
		node.communicator.precision = precision


	def changeMinimum (self, minimum):
		node = self.currentItem()
		if node == None: return
		node.communicator.min = minimum


	def changeMaximum (self, maximum):
		node = self.currentItem()
		if node == None: return
		node.communicator.max = maximum


	def changeStep (self, step):
		node = self.currentItem()
		if node == None: return
		node.communicator.step = step

	def changeSignPrinting (self, printSign):
		node = self.currentItem()
		if node == None: return
		node.communicator.printSign = printSign

	def changeLeadingZeros (self, leadingZeros):
		node = self.currentItem()
		if node == None: return
		node.communicator.leadingZeros = leadingZeros

	def changeRegExp (self, regExp):
		node = self.currentItem()
		if node == None: return
		node.communicator.regExp = str(regExp)
		
		
	def changeHelp (self, help):
		node = self.currentItem()
		if node == None: return
		node.communicator.setKeyHelp("en", str(help))
	

	def changeLabel (self, label):
		node = self.currentItem()
		if node == None: return
		node.communicator.setKeyLabel("en", str(label))


	def changeBoolDefault (self, value):
		node = self.currentItem()
		if node == None: return
		node.communicator.defaultValue = value
	

	def changeNumberDefault (self, value):
		node = self.currentItem()
		if node == None: return
		node.communicator.defaultValue = value


	def changeStringDefault (self, value):
		node = self.currentItem()
		if node == None: return
		if value != None:
			node.communicator.defaultValue = str(value)
		else:
			node.communicator.defaultValue = None
		
	def changeReferenceTarget(self, value):
		node = self.currentItem()
		if node == None: return
		node.communicator.target = value

	def fillTree (self):
		#self.headerItem().setText(0, QtCore.QString(self.__package.template.name))
		self.__processNode(self.__package.template, None)
		if self.topLevelItemCount() > 0:
			self.setCurrentItem(self.topLevelItem(0))


	def __processNode (self, node, treeNode):
		for node in node.children:
			if treeNode == None:
				item = TreeItem(self, node, self)
				self.addTopLevelItem(item)
			else:
				item = TreeItem(treeNode, node, self)
			item.updateContent()
			if node.type == FcTypes.SECTION:
				self.__processNode(node, item)


