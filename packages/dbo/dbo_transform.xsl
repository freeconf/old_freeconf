<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text" indent="no"/>

<!-- ############################################################ -->
<!-- ##########################HEADER############################ -->

<xsl:variable name="page-width">80</xsl:variable>
<xsl:variable name="key-value-width">80</xsl:variable>
<xsl:variable name="key-value-separator" xml:space="preserve">=</xsl:variable>
<xsl:variable name="indent-key-value" xml:space="preserve">0</xsl:variable>
<xsl:variable name="fill-char" xml:space="preserve"> </xsl:variable>
<xsl:variable name="value-surround-left" xml:space="preserve"></xsl:variable>
<xsl:variable name="value-surround-right" xml:space="preserve"></xsl:variable>
<xsl:variable name="num-new-lines">2</xsl:variable>
<xsl:variable name="comment-sequence">#</xsl:variable>
<xsl:variable name="output-help">1</xsl:variable>

<!-- ############################################################ -->
<xsl:variable name="key-value-separator-length" select="string-length($key-value-separator)" />
<xsl:variable name="value-surround-left-length" select="string-length($value-surround-left)" />
<xsl:variable name="value-surround-right-length" select="string-length($value-surround-right)" />
<xsl:variable name="comment-length" select="string-length($comment-sequence)" />

<!-- ############################################################ -->
<!-- ####################HANDY TEMPLATES######################### -->

<!-- generate new lines, by default $num-new-lines times -->
<xsl:template name="new-lines">
<xsl:param name="repeat" select="number($num-new-lines)" />
  <xsl:if test="number($repeat) &gt;= 0">
    <xsl:call-template name="new-lines">
      <xsl:with-param name="repeat" select="$repeat - 1"/>
    </xsl:call-template>
    <xsl:text>&#x0A;</xsl:text>
  </xsl:if>
</xsl:template>

<!-- does one indentation step -->
<xsl:template name="indent-step">
  <xsl:param name="repeat">0</xsl:param>
  <xsl:if test="number($repeat) &gt;= 1">
    <xsl:call-template name="indent-step">
      <xsl:with-param name="repeat" select="$repeat - 1"/>
    </xsl:call-template>
    <xsl:value-of select="$fill-char" />
  </xsl:if>
</xsl:template>

<!-- compute space for indentation and generate right amount of fill-chars -->
<xsl:template name="indent-key-value">
  <xsl:param name="keyword-length">0</xsl:param>
  <xsl:param name="value-length">0</xsl:param>
  <!-- 2 spaces and $key-value-separator -->
  <xsl:variable name="content-width" select="$key-value-width - 2 - $key-value-separator-length - $value-surround-left-length - $value-surround-right-length" />
  <xsl:choose>
    <!--  not enough space to indent or do not indent   -->
    <xsl:when test="number($keyword-length) + number($value-length) &gt;= $content-width or $indent-key-value = 0">
      <xsl:value-of select="$fill-char" />
      <xsl:value-of select="$key-value-separator" />
      <xsl:value-of select="$fill-char" />
    </xsl:when>
    <!--  call indent template    -->
    <xsl:otherwise>
      <xsl:value-of select="$fill-char" />
      <xsl:value-of select="$key-value-separator" />
      <xsl:value-of select="$fill-char" />
      <xsl:call-template name="indent-step">
      <xsl:with-param name="repeat" select="$content-width - (number($keyword-length) + number($value-length))"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<xsl:template name="wrap">
  <xsl:param name="rest" />
  <xsl:param name="written">0</xsl:param>
  <xsl:param name="new-line">1</xsl:param>
  <!-- one space and the comment sequence -->
  <xsl:variable name="content-width" select="$page-width - 1 - $comment-length" />
  <!-- is there something to parse? -->
  <xsl:if test="$rest">
    <!-- beginning of the new line, need to fill the comment sequence -->
    <xsl:if test="number($new-line) = 1">
      <xsl:value-of select="$comment-sequence" />
      <xsl:value-of select="$fill-char" />
    </xsl:if>
    <!-- the first word from the left of the $rest, separated from $rest by ' ' -->
    <xsl:variable name="word" select="substring-before($rest, ' ')" />
    <xsl:variable name="word-length" select="string-length($word)" />
    
    <xsl:choose>
      <!-- no word found, that means... -->
      <xsl:when test="number($word-length) = 0">
        <xsl:choose>
          <!-- there are more spaces together -->
          <xsl:when test="substring($rest, 1, 1) = ' '">
            <xsl:call-template name="wrap">
              <xsl:with-param name="written" select="number($written)" />
              <xsl:with-param name="new-line">0</xsl:with-param>
              <xsl:with-param name="rest" select="substring($rest, 2)" />
            </xsl:call-template>
          </xsl:when>
          <!-- there is only one word remaining -->
          <xsl:otherwise>
            <!-- OK, the word will fit into the page  -->
            <xsl:if test="number($written) + string-length($rest) &lt; $content-width">
              <xsl:value-of select="$rest" />
            </xsl:if>
            <!-- too long word to fit  -->
            <xsl:if test="not(number($written) + string-length($rest) &lt; $content-width)">
              <xsl:call-template name="new-lines">
                <xsl:with-param name="repeat">0</xsl:with-param>
              </xsl:call-template>
              <xsl:value-of select="$comment-sequence" />
              <xsl:value-of select="$fill-char" />
              <xsl:value-of select="$rest" />
            </xsl:if>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <!-- too long word to fit, it has to overflow -->
      <xsl:when test="number($written) = 0 and number($word-length) &gt;= $content-width">
        <xsl:value-of select="$word" />
        <xsl:call-template name="new-lines">
          <xsl:with-param name="repeat">0</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="wrap">
          <xsl:with-param name="new-line">1</xsl:with-param>
          <xsl:with-param name="rest" select="substring($rest, $word-length + 2)" />
        </xsl:call-template>
      </xsl:when>
      <!-- OK, the word will fit into the page  -->
      <xsl:when test="number($written) + number($word-length) &lt; $content-width">
        <xsl:value-of select="$word" />
        <xsl:text> </xsl:text>
        <xsl:call-template name="wrap">
          <xsl:with-param name="written" select="number($written) + $word-length + 1" />
          <xsl:with-param name="new-line">0</xsl:with-param>
          <xsl:with-param name="rest" select="substring($rest, $word-length + 2)" />
        </xsl:call-template>
      </xsl:when>
      <!-- there is no more room, new line is needed  -->
      <xsl:otherwise>
        <xsl:call-template name="new-lines">
          <xsl:with-param name="repeat">0</xsl:with-param>
        </xsl:call-template>
        <xsl:call-template name="wrap">
          <xsl:with-param name="new-line">1</xsl:with-param>
          <xsl:with-param name="rest" select="$rest" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:if>
</xsl:template>

<!-- print help above the keyword as a comment, configurable by $output-help -->
<xsl:template name="output-help">
  <!-- print help  -->
  <xsl:if test="number($output-help) = 1">
    <xsl:choose>
      <!-- help fits in the page -->
      <xsl:when test="string-length(help) &lt;= $page-width">
        <xsl:value-of select="$comment-sequence" />
        <xsl:value-of select="$fill-char" />
        <xsl:value-of select="help" />
      </xsl:when>
      <!-- word wrapping is needed -->
      <xsl:otherwise>
        <xsl:call-template name="wrap">
          <xsl:with-param name="rest" select="help" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
    <!-- new line break -->
    <xsl:call-template name="new-lines">
      <xsl:with-param name="repeat">0</xsl:with-param>
    </xsl:call-template>
  </xsl:if>
</xsl:template>
<!-- ############################################################ -->



<!-- ############################################################ -->
<!-- ######################TEMPLATES CALLING##################### -->

<!-- this drops the whitespace before and after all source tree elements -->
<xsl:strip-space elements="*" />
<!-- calling template on the root element -->
<!-- does nothing, root element will be omitted from the output -->
<xsl:template match="/*">
  <!-- here paste the result from the transformation of the root's children-->
  <xsl:apply-templates />
</xsl:template>

<!--  calling template on every child of the root element -->
<!-- does nothing, section elements will be omitted from the output -->
<!-- <xsl:template match="/*/*"> -->
<!--   here paste the result from the transformation of the section's children -->
<!--   <xsl:apply-templates /> -->
<!-- </xsl:template> -->

<!-- calling template on all remaining elements -->
<xsl:template match="/*/*">
  <!-- print help if enabled -->
  <xsl:call-template name="output-help" />
  <!-- print keyword -->
  <xsl:value-of select="@name" />
  <!-- indent if necessary -->
  <xsl:call-template name="indent-key-value">
    <xsl:with-param name="keyword-length" select="string-length(@name)"/>
    <xsl:with-param name="value-length" select="string-length(value)"/>
  </xsl:call-template>
  <!-- print value -->
  <xsl:value-of select="$value-surround-left" />
  <xsl:value-of select="value" />
  <xsl:value-of select="$value-surround-right" />
  <!-- output line breaks -->
  <xsl:call-template name="new-lines" />
</xsl:template>
<!-- ############################################################ -->

</xsl:stylesheet>