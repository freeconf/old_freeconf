/***************************************************************************
                          fcDependencyStruct.h  -  description
                             -------------------
    copyright            : (C) 2009 by Frantisek Rolinek
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCDEPENDENCYSTRUCT_H__
#define __FCDEPENDENCYSTRUCT_H__

#include <ostream>
#include "fcTypes.h"
#include "fcString.h"
#include "fcList.h"
#include "fcSAXTemplateConst.h"

struct struRPNRead 
{
    fcRelationEnum relation_enum;
    fc_bool is_registrable;

    struRPNRead();
    struRPNRead(fcRelationEnum _enum, fc_bool _is_registrable);
    struRPNRead(const struRPNRead& stru);
    ~struRPNRead();

    const struRPNRead& operator=(const struRPNRead& stru);
    friend std::ostream& operator<< (std::ostream& stream, const struRPNRead& rpn);
    void Init();
};

struct struProperties 
{
    fcString max;
    fcString min;
    fcString step;
    fcString type;
    fcString data;

    struProperties();
    struProperties(const struProperties& stru);
    struProperties(const fc_char* _max,  const fc_char* _min, const fc_char* _step, 
                   const fc_char* _type, const fc_char* _data);
    struProperties(const fcString& _max,  const fcString& _min, const fcString& _step,
                   const fcString& _type, const fcString& _data);
    
    const struProperties& operator=(const struProperties& stru);
    friend std::ostream& operator<< (std::ostream& stream, const struProperties& prop);
    void Init();
};

struct struDependencyValue
{
    //! Dependency values for the current entry
    /*! The list of values occured in <value> element for
     *  the current entry. 
     */
    fcList< fcString > dep_values;
    
    //! Id of dependency value condition
    /*! The value conditions are:
     *  equal, not-equal, 
     *  greater, greater-or-equal,
     *  lower, lower-or-equal
     *  lower and greater , lower and greater-or-equal,
     *  lower-or-equal and greater, lower-or-equal and greater-or-equal,
     *  in, not-int
     */
    fcValueType dep_value_type;

    struDependencyValue();
    struDependencyValue(const struDependencyValue& item);
    struDependencyValue(fcList<fcString>& depValues, fcValueType& depValType);

    const struDependencyValue& operator=(const struDependencyValue& item);
    friend std::ostream& operator<< (std::ostream& stream, const struDependencyValue& stru);
    void Init();
};

struct struDBoolValue
{
    //! Dependency value for the current entry
    /*! The bool value occured in <equal> or <not-equal> 
     *  elements nested in <value> element in the 
     *  current entry
     */
    fc_bool     m_bValue;
    //! Id of dependency value condition
    /*! The value conditions are:
     *  equal, not-equal, 
     */
    fcValueType m_depValueType;

    struDBoolValue();
    struDBoolValue(const struDependencyValue& item);
    struDBoolValue(const struDBoolValue& item);
    struDBoolValue(fc_bool bValue, fcValueType& depValueType);

    const struDBoolValue& operator=(const struDBoolValue& item);
    friend std::ostream& operator<< (std::ostream& stream, const struDBoolValue& stru);
    void Init();
};

struct struDNumberValue
{
    //! Dependency values for the current entry
    /*! The list of number values occured in <equal>,
     *  <not-equal>, <grater>, <greater-or-equal>,
     *  <lower>, <lower-or-equal>, <in> or <not-in>
     *  elements nested in <value> element in the 
     *  current entry
     */
    fcList< fc_int > m_lstValues;
    //! Id of dependency value condition
    /*! The value conditions are:
     *  equal, not-equal, 
     *  greater, greater-or-equal,
     *  lower, lower-or-equal
     *  lower and greater , lower and greater-or-equal,
     *  lower-or-equal and greater, lower-or-equal and greater-or-equal,
     *  in, not-int
     */
    fcValueType      m_depValueType;

    struDNumberValue();
    struDNumberValue(const struDependencyValue& item);
    struDNumberValue(const struDNumberValue& item);
    struDNumberValue(fcList< fc_int >& lstValues, fcValueType& depValueType);
    struDNumberValue(const fcList< fc_int >& lstValues, fcValueType& depValueType);

    const struDNumberValue& operator=(const struDNumberValue& item);
    friend std::ostream& operator<< (std::ostream& stream, const struDNumberValue& stru);
    void Init();
};

struct struDStringValue
{
    //! Dependency values for the current entry
    /*! The list of string values occured in <equal>,
     *  <not-equal>, <in> or <not-in> 
     *  elements nested in <value> 
     *  element in the current entry
     */
    fcList< fcString > m_lstValues;
    //! Id of dependency value condition
    /*! The value conditions are:
     *  equal, not-equal, 
     *  in, not-int
     */
    fcValueType        m_depValueType;

    struDStringValue();
    struDStringValue(const struDependencyValue& item);
    struDStringValue(const struDStringValue& item);
    struDStringValue(fcList< fcString >& lstValues, fcValueType& depValueType);
    struDStringValue(const fcList< fcString >& lstValues, fcValueType& depValueType);

    const struDStringValue& operator=(const struDStringValue& item);
    friend std::ostream& operator<< (std::ostream& stream, const struDStringValue& stru);
    void Init();
};

#endif
