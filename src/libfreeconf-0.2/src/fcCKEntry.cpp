/***************************************************************************
                          fcCKEntry.cpp  -  description
                             -------------------
    begin                : 2004/08/13
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <xercesc/dom/DOM.hpp>
#include <cstdio>
// #include <sstream>
#include "debug.h"
#include "fcCKEntry.h"
#include "fcCKBool.h"
#include "fcCKString.h"
#include "fcCKNumber.h"

using namespace std;

XERCES_CPP_NAMESPACE_USE

//--------------------------------------------------------------------------
fcCKEntry :: fcCKEntry()
   : value_set( false )
{
}
//--------------------------------------------------------------------------
fcCKEntry :: fcCKEntry( const fcCKEntry& entry )
   : fcCEntry( entry ),
     value_set( entry. value_set )
{
}
//--------------------------------------------------------------------------
fcCKEntry :: ~fcCKEntry()
{
}
//---------------------------------------------------------------------------
fc_bool fcCKEntry :: IsSection() const
{ 
   return false;
}
//---------------------------------------------------------------------------
fc_bool fcCKEntry :: IsKeyword() const
{ 
   return true;
}
//---------------------------------------------------------------------------
fc_bool fcCKEntry :: IsTEntry() const 
{ 
   return false;
}
//---------------------------------------------------------------------------
fc_bool fcCKEntry :: IsCEntry() const
{ 
   return true;
}
//---------------------------------------------------------------------------
fc_bool fcCKEntry :: ValueSet()
{
   return value_set;
}
//--------------------------------------------------------------------------
void fcCKEntry :: UpdateDOMNode( DOMNode* parent_node,
                                 DOMDocument* doc )
{
   DBG_FUNCTION_NAME( "fcCKEntry", "UpdateDOMNode" );
   DBG_EXPR( * this );
   const fc_char* value( 0 );
   fc_char buf[ 1024 ];
//    stringstream stream( buf, ios :: out );
   switch( Type() )
   {     
     case Fc::Bool:
         if( ( ( fcCKBool* ) this ) -> GetValue() == true )
            value = "yes";
         else value = "no";
         break;

     case Fc::String:
         value = ( ( fcCKString* ) this ) -> GetValue(). Data();
         break;
     case Fc::Number:
//          stream << ( ( fcCKNumber* ) this ) -> GetValue()
//                 << ends;
         snprintf(buf, sizeof(buf), "%.0f", ( ( fcCKNumber* ) this ) -> GetValue());
         value = buf;
         break;
     case Fc::List:
         break;
      default:;
   }
   DBG_EXPR( value );
   DBG_EXPR( dom_node );
   if( ! dom_node )
   {
      DBG_COUT( "Creating new DOM node..." );
      dom_node = doc -> createElement( XMLString :: transcode( GetEntryName(). Data() ) );
      parent_node -> appendChild( dom_node );
      DOMText* value_node = doc -> createTextNode(
            XMLString :: transcode( value ) );
      dom_node -> appendChild( value_node );

   }
   else
   {
      DOMNode* node = dom_node -> getFirstChild();
      if( ! node )
      {
         DOMText* value_node = doc -> createTextNode(
            XMLString :: transcode( value ) );
         dom_node -> appendChild( value_node );
      }
      else
         node -> setNodeValue( XMLString :: transcode( value ) );
   }
   DBG_EXPR( XMLString :: transcode( dom_node -> getNodeValue() ) );        
}
//--------------------------------------------------------------------------
#ifdef DEBUG
void fcCKEntry :: Print( ostream& stream ) const
{
   stream << "CONFIG KEYWORD: " << GetEntryName() << " (" << this << ")  -> ";
   //stream << * GetConfigValue();
}
#endif
