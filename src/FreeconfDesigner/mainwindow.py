#!/usr/bin/python

import os
from PyQt4 import QtCore, QtGui
from ui_mainwindow import Ui_MainWindow
from PyFC.designer_interface import *
from PyFC.header_file import HeaderStructure, HeaderFileWriter
from mytreewidget import TreeItem
from groupsdialog import GroupsDialog

class MainWindow (QtGui.QMainWindow):

	def __init__ (self):
		QtGui.QWidget.__init__(self, None)
		self.ui = Ui_MainWindow()
		desktop = QtGui.QDesktopWidget()
		self.setGeometry(desktop.width()/2 - self.width()/2, desktop.height()/2 - self.height()/2, self.width(), self.height());

		self.ui.setupUi(self)

		self.package = PackageInterface()
		self.ui.templateTree.package = self.package
		self.ui.keySectionStack.setEnabled(False)
		self.ui.keyTypeStack.setCurrentIndex(0)

		self.ui.multipleKey.setVisible(False) # XXX: multiple keys zatim nejsou podporovany

	def precisionChanged (self, index):
		#spin-boxes must be disconnected first because setDecimals() invokes a valueChanged() signal which 
		#can send wrong values to the library due to rounding 
		QtCore.QObject.disconnect(self.ui.numberMin, QtCore.SIGNAL("valueChanged(double)"), self.ui.templateTree.changeMinimum)
		QtCore.QObject.disconnect(self.ui.numberMax, QtCore.SIGNAL("valueChanged(double)"), self.ui.templateTree.changeMaximum)
		QtCore.QObject.disconnect(self.ui.numberStep, QtCore.SIGNAL("valueChanged(double)"), self.ui.templateTree.changeStep)
		self.ui.numberMin.setDecimals(index)
		self.ui.numberMax.setDecimals(index)
		self.ui.numberStep.setDecimals(index)
		self.ui.numberDefault.setDecimals(index)
		QtCore.QObject.connect(self.ui.numberMin, QtCore.SIGNAL("valueChanged(double)"), self.ui.templateTree.changeMinimum)
		QtCore.QObject.connect(self.ui.numberMax, QtCore.SIGNAL("valueChanged(double)"), self.ui.templateTree.changeMaximum)
		QtCore.QObject.connect(self.ui.numberStep, QtCore.SIGNAL("valueChanged(double)"), self.ui.templateTree.changeStep)


	def changeType (self, current):
		self.updateDetails(current)
	

	def updateGroupComboBox(self):
		"""Update contents of combo boxes holding list of available groups."""
		self.ui.keyGroup.clear()
		self.ui.sectionGroup.clear()
		for group in self.package.groups:
			self.ui.keyGroup.addItem(group.name)
			self.ui.sectionGroup.addItem(group.name)
		

	def updateProperties (self, current, previous):
	
		if current == previous or current == None: return
		if current.communicator.type == FcTypes.SECTION:
			# Prepare section properties
			self.ui.keySectionStack.setCurrentIndex(1)
			self.ui.sectionName.setText(current.communicator.name)
			self.ui.sectionLabel.setText(prepareStr(current.communicator.keyLabel("en")))
			if current.communicator.multiple == True:
				self.ui.multipleSection.setChecked(QtCore.Qt.Checked)
			else:
				self.ui.multipleSection.setChecked(QtCore.Qt.Unchecked)
			self.updateGroupComboBox()
			if current.communicator.group:
				self.ui.sectionGroup.setCurrentIndex(
					self.ui.sectionGroup.findText(
						current.communicator.group.name,
						QtCore.Qt.MatchExactly)
				)
		else:
			# Prepare key properties
			self.ui.keySectionStack.setCurrentIndex(0)
			self.ui.keyName.setText(current.communicator.name)
			self.ui.keyLabel.setText(prepareStr(current.communicator.keyLabel("en")))
			self.ui.keyHelp.setPlainText(prepareStr(current.communicator.keyHelp("en")))
			if current.communicator.active == True:
				self.ui.activeKey.setChecked(QtCore.Qt.Checked)
			else:
				self.ui.activeKey.setChecked(QtCore.Qt.Unchecked)
			if current.communicator.mandatory == True:
				self.ui.mandatoryKey.setChecked(QtCore.Qt.Checked)
			else:
				self.ui.mandatoryKey.setChecked(QtCore.Qt.Unchecked)
			if current.communicator.multiple == True:
				self.ui.multipleKey.setChecked(QtCore.Qt.Checked)
			else:
				self.ui.multipleKey.setChecked(QtCore.Qt.Unchecked)
			# Set group
			self.updateGroupComboBox()
			self.ui.keyGroup.setCurrentIndex(
				self.ui.keyGroup.findText(
					current.communicator.group.name,
					QtCore.Qt.MatchExactly)
			)
		if current.communicator.type == FcTypes.BOOL:
			self.ui.keyType.setCurrentIndex(0)
		if current.communicator.type == FcTypes.NUMBER:
			self.ui.keyType.setCurrentIndex(1)
		if current.communicator.type == FcTypes.STRING:
			self.ui.keyType.setCurrentIndex(2)
		if current.communicator.type == FcTypes.REFERENCE:
			self.ui.keyType.setCurrentIndex(3)
		if previous != None and (previous.communicator.type == current.communicator.type or previous.communicator.type == FcTypes.SECTION):
			self.updateDetails(current)


	def updateDetails (self, current):
		if current.communicator.type == FcTypes.BOOL:
			value = current.communicator.defaultValue
			if value == None:
				self.ui.boolDefaultCheck.setChecked(QtCore.Qt.Unchecked)
				self.ui.boolDefault.setEnabled(False)
				self.ui.boolDefault.setCurrentIndex(0)
			else:
				self.ui.boolDefaultCheck.setChecked(QtCore.Qt.Checked)
				self.ui.boolDefault.setEnabled(True)
				self.ui.boolDefault.setCurrentIndex(int(value))
		if current.communicator.type == FcTypes.NUMBER:
			if self.ui.numberPrecision.currentIndex() == current.communicator.precision:
				self.precisionChanged(current.communicator.precision)
			else:
				self.ui.numberPrecision.setCurrentIndex(current.communicator.precision)
			self.ui.numberMin.setValue(current.communicator.min)
			self.ui.numberMax.setValue(current.communicator.max)
			self.ui.numberStep.setValue(current.communicator.step)
			self.ui.numberStep.setMaximum(self.ui.numberMax.value())
			value = current.communicator.defaultValue
			if value == None:
				self.ui.numberDefaultCheck.setChecked(QtCore.Qt.Unchecked)
				self.ui.numberDefault.setEnabled(False)
				self.ui.numberDefault.setValue(0.0)
			else:
				self.ui.numberDefaultCheck.setChecked(QtCore.Qt.Checked)
				self.ui.numberDefault.setEnabled(True)
				self.ui.numberDefault.setValue(value)
			self.ui.numberDefault.setMaximum(self.ui.numberMax.value())
			self.ui.numberDefault.setMinimum(self.ui.numberMin.value())
			if current.communicator.printSign == True:
				self.ui.numberSign.setChecked(QtCore.Qt.Checked)
			else:
				self.ui.numberSign.setChecked(QtCore.Qt.Unchecked)

			if current.communicator.leadingZeros == True:
				self.ui.numberZeros.setChecked(QtCore.Qt.Checked)
			else:
				self.ui.numberZeros.setChecked(QtCore.Qt.Unchecked)
		if current.communicator.type == FcTypes.STRING:
			self.ui.stringRegExp.setText(current.communicator.regExp)
			value = current.communicator.defaultValue
			if value == None:
				self.ui.stringDefaultCheck.setChecked(QtCore.Qt.Unchecked)
				self.ui.stringDefault.setEnabled(False)
				self.ui.stringDefault.setText("")
			else:
				self.ui.stringDefaultCheck.setChecked(QtCore.Qt.Checked)
				self.ui.stringDefault.setEnabled(True)
				self.ui.stringDefault.setText(value)
		if current.communicator.type == FcTypes.REFERENCE:
			self.ui.referenceText.setText(prepareStr(current.communicator.target))
			self.ui.referenceButton.setVisible(False) # TODO: Reference button: po kliknuti na nej se objevi stromecek objektu kde se da vybrat na co to ma ukazovat.

	def updateBoolDefault (self, value):
		if value == True:
			self.ui.templateTree.changeBoolDefault(bool(self.ui.boolDefault.currentIndex()))
		else:
			self.ui.templateTree.changeBoolDefault(None)

	def updateNumberDefault (self, value):
		if value == True:
			self.ui.templateTree.changeNumberDefault(self.ui.numberDefault.value())
		else:
			self.ui.templateTree.changeNumberDefault(None)
	
	def updateStringDefault (self, value):
		if value == True:
			self.ui.templateTree.changeStringDefault(self.ui.stringDefault.text())
		else:
			self.ui.templateTree.changeStringDefault(None)
	
	def __loadPackageByName (self, name):
		try:
			self.package.loadPackage(str(name))
			self.processTemplate()
			self.ui.statusBar.showMessage("Package " + self.package.name + " loaded")
			self.ui.templateTree.expandAll()
			self.ui.keySectionStack.setEnabled(True)
			self.ui.expandAllAction.setEnabled(True)
			self.ui.collapseAllAction.setEnabled(True)
			self.ui.savePackageAction.setEnabled(True)
			self.ui.closePackageAction.setEnabled(True)
			self.ui.editGroupsAction.setEnabled(True)
		except FcPackageLoadError as e:
			self.ui.statusBar.showMessage(self.tr(str(e)))
			QtGui.QMessageBox.critical(self, self.tr("Package load"), self.tr(str(e)))
		

	def loadPackage (self):
		if self.package.loaded:
			if QtGui.QMessageBox.question(self, self.tr("Save package"), \
			self.tr("Another package has already been loaded. Do you want to save it first?"), \
			QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.Yes) == QtGui.QMessageBox.Yes:
				self.package.savePackage()

		# the bug https://bugs.kde.org/show_bug.cgi?id=179032 shows itself here
		# the bug needs to be reopenned
		name = QtGui.QFileDialog.getExistingDirectory(self, self.tr("load package"), QtCore.QDir.home().absolutePath() + "/.freeconf/packages/", QtGui.QFileDialog.ShowDirsOnly | QtGui.QFileDialog.DontResolveSymlinks)
		#name = "/home/mosqua/.freeconf/packages/vantage"
		#name = "/home/mosqua/.freeconf/packages/mplayer_dep"
		#name = "/home/mosqua/.freeconf/packages/pureftpd"
		#name = "/home/lankvil/.freeconf/packages/apache"
		#if name.isEmpty():
			#return
		self.__loadPackageByName(name)


	def processTemplate (self):
		self.ui.templateTree.fillTree()


	def savePackage (self):
		self.package.savePackage()


	def closePackage (self):
		if self.package.loaded:
			if QtGui.QMessageBox.question(self, self.tr("Save package"), \
			self.tr("Do you want to save the package before closing it?"), \
			QtGui.QMessageBox.Yes | QtGui.QMessageBox.No, QtGui.QMessageBox.Yes) == QtGui.QMessageBox.Yes:
				self.package.savePackage()


		self.package.closePackage()
		self.ui.templateTree.clear()
		self.ui.expandAllAction.setEnabled(False)
		self.ui.collapseAllAction.setEnabled(False)
		self.ui.savePackageAction.setEnabled(False)
		self.ui.closePackageAction.setEnabled(False)
		self.ui.editGroupsAction.setEnabled(False)
		self.ui.keyName.clear()
		self.ui.keySectionStack.setEnabled(False)
		self.ui.keySectionStack.setCurrentIndex(0)
		self.ui.keyType.setCurrentIndex(0)


	def newPackage (self):
		self.closePackage()
		ok = False
		dir = QtCore.QDir(QtCore.QDir.home().absolutePath() + os.path.sep + ".freeconf" + os.path.sep + "packages")
		while not ok:
			name, test = QtGui.QInputDialog.getText(self, self.tr("New package"), self.tr("Set the name of the package"))
			name = name.toLower()
			if not test:
				name = ""
				break
			if name.isEmpty() == False and dir.exists(name) == False:
				ok = True
		
		if name != "":
			if dir.exists() == False:
				if dir.mkpath(name) == False:
					QtGui.QMessageBox.critical(self, self.tr("Writing problem"), self.tr("The package root directory is not writable"))
					return
			else:
				dir.mkdir(name)
			 
			## Generate Header File
			# Generate default header structure
			header_structure = HeaderStructure()
			header_structure.initDefaults(name)
			header_file = HeaderFileWriter(header_structure)
			self.createFile(dir.absolutePath() + os.path.sep + name + os.path.sep + "header.xml", header_file.generate())
			
			# template file
			self.createFile(dir.absolutePath() + os.path.sep + name + os.path.sep + name + "_template.xml", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n \
			<freeconf-template>\n \
			<section-name>" + name +"-config</section-name>\n \
			</freeconf-template>")
				
			# default file
			self.createFile(dir.absolutePath() + os.path.sep + name + os.path.sep + name + "_default.xml", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n \
				<section name=\"" + name + "-config\">\n</section>")
				
			#translation directory
			if dir.cd(name) == False:
				QtGui.QMessageBox.critical(self, self.tr("Problem accessing package directory"), self.tr("The package directory is not accessible"))
				return
			if dir.mkdir("L10n") == False:
				QtGui.QMessageBox.critical(self, self.tr("Writing problem"), self.tr("The package directory is not writable"))
				return
			
			print dir.absolutePath()
			self.__loadPackageByName(dir.absolutePath())
				
				
	def createFile(self, fileName, content):
		file = QtCore.QFile(fileName)
		print file.fileName()
		if file.open(QtCore.QIODevice.WriteOnly) == False:
			QtGui.QMessageBox.critical(self, self.tr("Writing problem"), self.tr("The package directory is not writable"))
			return
		stream = QtCore.QTextStream(file)
		stream << content
		file.close()
	

	def editGroups(self):
		"""Slot for action editGroupsAction."""
		dialog = GroupsDialog(self.package, self)
		dialog.show()



def prepareStr(s):
	"""Helpful function. Translates None into empty string."""
	if s == None:
		return ""
	return s

