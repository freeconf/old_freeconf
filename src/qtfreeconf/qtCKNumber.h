#ifndef _QT_CKNUMBER_H
#define _QT_CKNUMBER_H

#include "qtCWidget.h"

class QDoubleSpinBox;

class qtCKNumber: public qtCWidget
{
  Q_OBJECT
  private:
    //! pointer to Qt widget
    QDoubleSpinBox *_spin;
    
  public:

    //! constructor
    explicit qtCKNumber (fcPackage *package = 0, const qtCWidget *parent = 0);
    //! constructor
    explicit qtCKNumber (const fcCGEntry *guiEntry, fcPackage *package = 0, const qtCWidget *parent = 0);
    
    //! render function
    virtual void render ();
    //! removes any Qt visual widgets
    virtual void invalidateGui ();
    
  public slots:
    //! connects widget with GUI tree
    virtual void attach (fc_bool attached);
  protected slots:
    //! spinbox has changed it's value
    void valueChanged (double value);
};

#endif
