/***************************************************************************
 *   Copyright (C) 2009 by Jan Ehrenberger   *
 *   lankvil@seznam.cz   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#include "kreatefreeconfview.h"
#include "packagetreeview.h"

#include <qpainter.h>
#include <qlayout.h>
#include <qvbox.h>
#include <qhbox.h>
#include <qsplitter.h>
#include <qpushbutton.h>

#include <kurl.h>

#include <ktrader.h>
#include <klibloader.h>
#include <kmessagebox.h>
#include <krun.h>
#include <klocale.h>

KreateFreeconfView::KreateFreeconfView(QWidget *parent)
    : QWidget(parent),
      DCOPObject("KreateFreeconfIface")
{
    QLayout *layout = new QHBoxLayout(this);
    
    QSplitter *splitter = new QSplitter(this);
    layout->add(splitter);

    PackageTreeView *package_view = new PackageTreeView(splitter);
    //m_middle = new MiddleView(splitter);
    QVBox *manbox = new QVBox(splitter);
    
    // Location Box
    QHBox *location_box = new QHBox(manbox);
    m_man_location = new QLineEdit(location_box);
    connect(m_man_location, SIGNAL(returnPressed()),
            this, SLOT(slotOpenLocation()));
    // New Tab Button
    QPushButton *new_tab_button = new QPushButton("New Tab", location_box);
    connect(new_tab_button, SIGNAL(clicked()),
            this, SLOT(slotNewTab()));

    m_man_view = new ManView(manbox);
    /*connect(m_man_view, SIGNAL(selectionChanged(KHTMLPart *, const DOM::Range &)),
            this, SLOT(slotSelectionChanged(KHTMLPart *, const DOM::Range &)));
    */
}

KreateFreeconfView::~KreateFreeconfView()
{
}

void KreateFreeconfView::print(QPainter *p, int height, int width)
{
    // do the actual printing, here
    // p->drawText(etc..)
}

QString KreateFreeconfView::currentURL()
{
   //return m_man_html->url().url();
}

void KreateFreeconfView::slotOnURL(const QString& url)
{
    emit signalChangeStatusbar(url);
}

void KreateFreeconfView::slotSetTitle(const QString& title)
{
    emit signalChangeCaption(title);
}

void KreateFreeconfView::slotOpenLocation()
{
       m_man_view->setCurrentURL(m_man_location->text());
       m_man_location->setText("");
}


void KreateFreeconfView::slotSelectionChanged(KHTMLPart *part, const DOM::Range &range)
{
    m_middle->insertEntry(part, range);
}


void KreateFreeconfView::slotNewTab()
{
    m_man_view->newTab(m_man_location->text());
    m_man_location->setText("");
}



#include "kreatefreeconfview.moc"




