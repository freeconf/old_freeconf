#include <QApplication>
#include <QWidget>
#include <QLabel>
#include <QFrame>
#include <QDebug>
#include <QFont>
#include <QSizePolicy>
#include <QScrollArea>
#include <QListWidget>
#include <QListWidgetItem>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include <fcPackage.h>
#include <fcHeaderStructure.h>
#include <fcTransformer.h>
#include <fcGTab.h>
#include <fcGWindow.h>
#include "qtCSEntry.h"
#include "qtCSRootEntry.h"
#include "qtRender.h"
#include "fcDOMConfigFile.h"

qtRender::qtRender (fcPackage *package)
  :_package(package),
   _contentLayout(0),
   _scroll(0),
   _iconView(0),
   _currentConfigPage(0)
{
  
}

qtRender::~qtRender ()
{
}

void qtRender::render ()
{
  _main = new QWidget();
  setCentralWidget(_main);
  setWindowTitle(_package->window()->title().Data());
  
  setMinimumWidth(_package->window()->minWidth());
  setMinimumHeight(_package->window()->minHeight());
  if (_package->window()->maxWidth())
    setMaximumWidth(_package->window()->maxWidth());
  if (_package->window()->maxHeight())
    setMaximumHeight(_package->window()->maxHeight());
  
  QVBoxLayout *mainLayout = new QVBoxLayout();
  _main->setLayout(mainLayout);
  QHBoxLayout *collumnLayout = new QHBoxLayout();
  mainLayout->addLayout(collumnLayout);

  _iconView = new QListWidget();
  _iconView->setViewMode(QListView::IconMode);
  _iconView->setFixedWidth(128);
  _iconView->setIconSize(QSize(48, 48));
  _iconView->setUniformItemSizes(true);
  _iconView->setSpacing(0);
  _iconView->setMovement(QListView::Static);

  QFont font;
  font.setBold(true);
  
  for (int i = 0; i < _package->window()->tabsCount(); i++)
  {
    QListWidgetItem *configButton = new QListWidgetItem(_iconView);
    configButton->setSizeHint(QSize(122,70));
    configButton->setIcon(loadIcon(_package->window()->at(i)->icon()));
    configButton->setText(_package->window()->at(i)->label().Data());
    configButton->setFont(font);
    configButton->setTextAlignment(Qt::AlignHCenter);
    configButton->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
  }
  
  _tabLabel = new QLabel();
  _tabLabel->setFont(font);
  _tabLabel->setMinimumHeight(40);
  _tabLabel->setMargin(6);
  _tabLabel->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
  _tabLabel->setMargin(5);

  QVBoxLayout *vLayout = new QVBoxLayout();
  vLayout->addWidget(_tabLabel);
  collumnLayout->addWidget(_iconView);
  collumnLayout->addSpacing(4);
  collumnLayout->addLayout(vLayout, 1);

  _contentLayout = new QVBoxLayout();

  QFrame *fr = new QFrame();
  fr->setBackgroundRole(QPalette::Window);
  fr->setFrameStyle(QFrame::NoFrame | QFrame::Plain);
  fr->setLayout(_contentLayout);

  _scroll = new QScrollArea();
  _scroll->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
  _scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
  _scroll->setFrameStyle(QFrame::NoFrame | QFrame::Plain);
  _scroll->setWidgetResizable(true);
  _scroll->setWidget(fr);
  vLayout->addWidget(_scroll);

  QFrame *line = new QFrame();
  line->setFrameStyle(QFrame::HLine);
  line->setFixedHeight(2);
  line->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);

  mainLayout->addSpacing(4);
  mainLayout->addWidget(line);

  QHBoxLayout *hLayout = new QHBoxLayout();
  hLayout->addStretch(1);
  QPushButton *ok = new QPushButton(tr("&OK"));
  QPushButton *apply = new QPushButton(tr("&Apply"));
  QPushButton *cancel = new QPushButton(tr("&Cancel"));
  hLayout->addWidget(ok);
  hLayout->addWidget(apply);
  hLayout->addWidget(cancel);
  mainLayout->addLayout(hLayout);

  connect(ok, SIGNAL(clicked()), this, SLOT(ok()));
  connect(apply, SIGNAL(clicked()), this, SLOT(apply()));
  connect(cancel, SIGNAL(clicked()), qApp, SLOT(quit()));

  connect(_iconView,
           SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)),
           this,
           SLOT(tabChanged(QListWidgetItem*, QListWidgetItem*)));

  _currentConfigPage = new qtCSRootEntry(_package, _package->window()->at(0), _contentLayout);
  _currentConfigPage->render();
  _iconView->setCurrentRow(0);
  
  show();
}


void qtRender::transform ()
{
  const fcHeaderStructure *header = _package->header();
  std::cout << "Writing config file " << header->GetOutputFile().Data() << "(" << _package->configFile() << ")..." << std::endl;
  _package->configFile()->Write(header->GetOutputFile().Data());
  fcTransformer transformer;
  std::cout << "Transforming config file to native config file '" << header->GetNativeFile().Data() << "'..." << std::endl;
  
  transformer.Transform(header->GetOutputFile().Data(),
                        header->GetTransformFile().Data(),
                        header->GetNativeFile().Data());
}


void qtRender::ok ()
{
  transform();
  qApp->quit();
}


void qtRender::apply ()
{
  transform();
}


void qtRender::tabChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
  if (!current)
    current = previous;

  delete _currentConfigPage;
  _tabLabel->setText(_package->window()->at(_iconView->currentRow())->description().Data());
  _currentConfigPage = new qtCSRootEntry(_package, _package->window()->at(_iconView->row(current)), _contentLayout);
  _currentConfigPage->render();
  _contentLayout->addStretch(1);
  _scroll->repaint();
}


QIcon qtRender::loadIcon (Fc::Icon icon)
{
  QString path = "/usr/share/icons/oxygen/48x48/";
  switch (icon)
  {
    case Fc::NoIcon:
      return QIcon(path + "mimetypes/unknown.png");

    case Fc::Security:
      return QIcon(path + "actions/document-encrypt.png");

    case Fc::Password:
      return QIcon(path + "apps/preferences-desktop-user-password.png");
      
    case Fc::Network:
      return QIcon(path + "categories/applications-internet.png");

    case Fc::Terminal:
      return QIcon(path + "categories/applications-utilities.png");
      
    case Fc::User:
      return QIcon(path + "places/user-identity.png");

    case Fc::Tools:
      return QIcon(path + "categories/preferences-system.png");
      
    case Fc::Display:
      return QIcon(path + "devices/video-display.png");
         
    case Fc::Package:
      return QIcon(path + "apps/utilities-file-archiver.png");

    case Fc::File:
      return QIcon(path + "mimetypes/text-rtf.png");

    case Fc::Directory:
      return QIcon(path + "places/folder.png");

    default:
      return QIcon();
  }
      
}