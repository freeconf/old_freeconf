#ifndef _QT_CWIDGET_H
#define _QT_CWIDGET_H

#include <QWidget>
#include <fcTypes.h>

class fcPackage;
class fcCGEntry;
class QBoxLayout;

class qtCWidget: public QWidget
{
  Q_OBJECT
  
  protected:
    //! pointer to package object
    fcPackage *_package;
    //! parent of the widget
    const qtCWidget *_parent;
    //! widget's layout
    QBoxLayout *_layout;
    //! GUI tree node associated with this widget
    const fcCGEntry *_guiEntry;
    //! is this widget connected with it's GUI buddy
    fc_bool _attached;
    //! has this widget been rendered
    fc_bool _rendered;
    
  public:
    //! constructor
    explicit qtCWidget (fcPackage *package = 0, const qtCWidget *parent = 0);
    //! constructor
    explicit qtCWidget (const fcCGEntry *guiEntry, fcPackage *package = 0, const qtCWidget *parent = 0);

    //! destructor
    virtual ~qtCWidget ();

    //! rendering function
    virtual void render () = 0;

    //! removes any Qt visual widgets
    virtual void invalidateGui () = 0;

    //! fcPackage setter
    void setPackage (fcPackage *package);
    //! fcPackage getter
    fcPackage* package () const;

    //! GUI entry setter
    virtual void setGuiEntry (fcCGEntry *guiEntry);
    //! GUI entry getter
    const fcCGEntry* guiEntry () const;

    //! parent setter
    void setFcParent (const qtCWidget *parent);
    //! parent getter
    const qtCWidget* fcParent () const;

    //! layout setter
    void setFcLayout (QBoxLayout *layout);
    //! layout getter
    QBoxLayout* fcLayout () const;

    //! rendered getter
    fc_bool rendered () const;
    //! attached getter
    fc_bool attached () const;
    
  public slots:
    //! connects widget with GUI tree
    virtual void attach (fc_bool attached);
    
/*  signals:
    void attached ();
    void detached ();*/
};

#endif
