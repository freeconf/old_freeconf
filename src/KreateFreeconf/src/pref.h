/***************************************************************************
 *   Copyright (C) 2009 by Jan Ehrenberger   *
 *   lankvil@seznam.cz   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef _KREATEFREECONFPREF_H_
#define _KREATEFREECONFPREF_H_

#include <kdialogbase.h>
#include <qframe.h>

class KreateFreeconfPrefPageOne;
class KreateFreeconfPrefPageTwo;

class KreateFreeconfPreferences : public KDialogBase
{
    Q_OBJECT
public:
    KreateFreeconfPreferences();

private:
    KreateFreeconfPrefPageOne *m_pageOne;
    KreateFreeconfPrefPageTwo *m_pageTwo;
};

class KreateFreeconfPrefPageOne : public QFrame
{
    Q_OBJECT
public:
    KreateFreeconfPrefPageOne(QWidget *parent = 0);
};

class KreateFreeconfPrefPageTwo : public QFrame
{
    Q_OBJECT
public:
    KreateFreeconfPrefPageTwo(QWidget *parent = 0);
};

#endif // _KREATEFREECONFPREF_H_
