#!/usr/bin/python

from constants import *
from exception import *
import package
import base


class PackageInterface (object):
	"""Client's entry point to the package"""
	def __init__(self):
		self.__p = None

	def loadPackage (self, name):
		"""Loads the package by name"""
		self.__p = package.FcPackage()
		try:
			self.__p.loadPackage(name)
			return True
		except FcPackageLoadError:
			self.__p = None
			raise

	def saveConfiguration (self):
		"""Saves all value changes to the output and performs the XSL transformation"""
		if self.__p == None: return
		try:
			self.__p.writeOutput()
			self.__p.transform()
		except (FcInconsistencyError):
			raise
		
	@property
	def window (self):
		"""returns the proxy object of the top-level element of the GUI tree"""
		if self.__p == None: return
		return self.__p.trees.guiTree.proxy
		
	def showAllActiveWidgets (self, show):
		"""Turns showing advanced widgets on or off"""
		self.__p.trees.guiTree.showAll(show)
		



class FcGUICommunicator(object):
	"""Class that handles communication between the library and the client."""

	def __init__ (self, guiEntry):
		self.__guiEntry = guiEntry
		self.__clientEntry = None
		self.__tabs = []
		self.__children = []

	def processSignal (self, signal, value):
		"""Signal brigde between the client and the library"""
		if self.__clientEntry != None:
			self.__clientEntry.processSignal(signal, value)

	@property
	def value (self):
		"""Returns the corresponding value from the config tree"""
		if self.__guiEntry.type == FcTypes.BOOL:
			return self.__guiEntry.configBuddy.grade
		else:
			return self.__guiEntry.configBuddy.value

	@value.setter
	def value (self, value):
		"""Set value of associated config entry. Apllication should not call this metod while processing signal CHANGE_VALUE!"""
		if self.__guiEntry.type == FcTypes.BOOL:
			self.__guiEntry.configBuddy.grade = value
		else:
			self.__guiEntry.configBuddy.value = value
	
	@property
	def valueRepr (self):
		"""Returns string representation of value as it will be written to output file."""
		return self.__guiEntry.configBuddy.valueRepr

	@property
	def list (self):
		"""Returns True if the object is a list"""
		if isinstance(self.__guiEntry, base.FcCGEntry) and self.__guiEntry.type in (FcTypes.STRING, FcTypes.FUZZY):
			return self.__guiEntry.configBuddy.templateBuddy.list
		else:
			return None

	#@property
	#def grade (self):
		#if isinstance(self.__guiEntry, base.FcCGEntry) and self.__guiEntry.type in (FcTypes.FUZZY, FcTypes.BOOL):
			#return self.__guiEntry.configBuddy.grade
		#else:
			#return None

	def disconnect (self):
		"""Disconnects the client object""" 
		self.__clientEntry = None

	def connectClient (self, clientEntry):
		"""Connects the client object"""
		self.__clientEntry = clientEntry

	@property
	def minWidth (self):
		"""Min width of the config window"""
		if isinstance(self.__guiEntry, base.FcGWindow):
			return self.__guiEntry.minWidth
		else:
			return None

	@property
	def maxWidth (self):
		"""Max width of the config window"""
		if isinstance(self.__guiEntry, base.FcGWindow):
			return self.__guiEntry.maxWidth
		else:
			return None

	@property
	def minHeight (self):
		"""Min height of the config window"""
		if isinstance(self.__guiEntry, base.FcGWindow):
			return self.__guiEntry.minHeight
		else:
			return None

	@property
	def maxHeight (self):
		"""Max height of the config window"""
		if isinstance(self.__guiEntry, base.FcGWindow):
			return self.__guiEntry.maxHeight
		else:
			return None

	@property
	def tabs (self):
		"""Returns a list of tabs' proxies"""
		if isinstance(self.__guiEntry, base.FcGWindow):
			if len(self.__tabs) == 0:
				for tab in self.__guiEntry.entries:
					self.__tabs.append(tab.proxy)
			return self.__tabs
		else:
			return None

	@property
	def title (self):
		"""Returns title of the config window"""
		if isinstance(self.__guiEntry, base.FcGWindow):
				return self.__guiEntry.title
		else:
			return None

	@property
	def icon (self):
		"""Returns the tab's icon"""
		if isinstance(self.__guiEntry, base.FcGTab):
			return self.__guiEntry.icon
		else:
			return None

	@property
	def description (self):
		"""Returns the tab's description"""
		if isinstance(self.__guiEntry, base.FcGTab):
			return self.__guiEntry.description
		else:
			return None

	@property
	def content (self):
		"""Returns the tab's content proxy"""
		if isinstance(self.__guiEntry, base.FcGTab):
			return self.__guiEntry.content.proxy
		else:
			return None

	@property
	def empty (self):
		"""Returns True if the tab or section is empty"""
		if isinstance(self.__guiEntry, base.FcGTab) or isinstance(self.__guiEntry, base.FcCGSEntry):
			return self.__guiEntry.empty
		else:
			return None

	@property
	def name (self):
		"""Returns name of the entry"""
		return self.__guiEntry.name

	@property
	def help (self):
		"""Returns help of the entry"""
		if isinstance(self.__guiEntry, base.FcCGEntry):
			return self.__guiEntry.configBuddy.templateBuddy.keyHelp()
		else:
			return None


	@property
	def label (self):
		"""Returns label of the entry"""
		if isinstance(self.__guiEntry, base.FcCGEntry):
			return self.__guiEntry.label
		else:
			return None

	@property
	def children (self):
		"""Returns list of section's children proxies"""
		if isinstance(self.__guiEntry, base.FcCGSEntry):
			if len(self.__children) == 0:
				for child in self.__guiEntry._entries:
					self.__children.append(child.proxy)
			return self.__children
		else:
			return None
			
	@property
	def showAllChildren (self):
		"""Returns True if show all is enabled"""
		if isinstance(self.__guiEntry, base.FcCGSEntry):
			return self.__guiEntry._showAllChildren
		else:
			return None

	@property
	def type (self):
		"""Returns type of the entry"""
		if isinstance(self.__guiEntry, base.FcCGEntry):
			return self.__guiEntry.type
		else:
			return None
	
	@property
	def enabled (self):
		"""Returns True if the entry is enabled"""
		if isinstance(self.__guiEntry, base.FcCGEntry) and self.__guiEntry.configBuddy != None:
			return self.__guiEntry.configBuddy.enabled
		else:
			return None

	@property
	def min (self):
		"""Returns min of the number entry"""
		if isinstance(self.__guiEntry, base.FcCGEntry) and self.__guiEntry.type == FcTypes.NUMBER:
			return self.__guiEntry.configBuddy.templateBuddy.min
		else:
			return None

	@property
	def max (self):
		"""Returns max of the number entry"""
		if isinstance(self.__guiEntry, base.FcCGEntry) and self.__guiEntry.type == FcTypes.NUMBER:
			return self.__guiEntry.configBuddy.templateBuddy.max
		else:
			return None

	@property
	def step (self):
		"""Returns step of the number entry"""
		if isinstance(self.__guiEntry, base.FcCGEntry) and self.__guiEntry.type == FcTypes.NUMBER:
			return self.__guiEntry.configBuddy.templateBuddy.step
		else:
			return None

	@property
	def precision (self):
		"""Returns precision of the number entry"""
		if isinstance(self.__guiEntry, base.FcCGEntry) and self.__guiEntry.type == FcTypes.NUMBER:
			return self.__guiEntry.configBuddy.templateBuddy.precision
		else:
			return None

	@property
	def active (self):
		"""Returns True if the entry or section is active"""
		if isinstance(self.__guiEntry, base.FcCGEntry) and self.__guiEntry.configBuddy != None:
			return self.__guiEntry.configBuddy.templateBuddy.dynamicActive
		else:
			return None

	@property
	def mandatory (self):
		"""Returns True if the entry is mandatory"""
		if isinstance(self.__guiEntry, base.FcCGEntry) and self.__guiEntry.configBuddy != None:
			return self.__guiEntry.configBuddy.templateBuddy.dynamicMandatory
		else:
			return None
			
	@property
	def inconsistent (self):
		"""Returns True if the entry or the section or the tab or the window is mandatory"""
		if isinstance(self.__guiEntry, base.FcCGEntry):
			return self.__guiEntry.inconsistent
		else:
			return None
	
	@property
	def multiple (self):
		"""Returns True if entry is multiple."""
		if isinstance(self.__guiEntry, base.FcCGEntry) and self.__guiEntry.type == FcTypes.SECTION and self.__guiEntry.configBuddy != None:
			return self.__guiEntry.configBuddy.templateBuddy.multiple
		else:
			return None
	
	@property
	def isMultipleEntryContainer(self):
		"""Returns True if the entry is multiple entry container"""
		if isinstance(self.__guiEntry, base.FcCGEntry) and self.__guiEntry.type == FcTypes.SECTION and self.__guiEntry.configBuddy != None:
			return self.__guiEntry.configBuddy.isMultipleEntryContainer()
		else:
			return None
	
	def createEntry(self):
		"""Create new entry in multiple entry container."""
		if not self.isMultipleEntryContainer:
			return None

		centry = self.__guiEntry.configBuddy.createEntry()
		guiEntry = self.__guiEntry.appendCEntry(centry)
		self.__children.append(guiEntry.proxy)
		if centry.type == FcTypes.SECTION:
			# Fill config entry section
			centry.fill()
			# Fill GUI Entry section
			guiEntry.fill()
		guiEntry.initState() # Initialize state
		return guiEntry.proxy
	
	def deleteNode(self):
		"""Delete instance of multiple config entry."""
		if not self.multiple or self.isMultipleEntryContainer:
			# Entry is not deletable
			return None
		# Remove instance of communicator from parent's client interface
		if self.__guiEntry.parent != None:
			parentComm = self.__guiEntry.parent.proxy
			parentComm.__children.remove(self)
		# Disconnect instance of config entry from GUI tree and config tree
		centry = self.__guiEntry.configBuddy
		self.__guiEntry.parent.disconnect(self.__guiEntry)
		centry.parent.disconnect(centry)
		self.disconnect()
	
	def moveUp(self):
		"""Move config entry one position up in the section."""
		if not self.multiple or self.isMultipleEntryContainer:
			# Entry is not moveable 
			return None
		# Move GUI entry
		gentry = self.__guiEntry
		if gentry.parent != None:
			gentry.parent.moveUp(gentry)
		# Move config entry
		centry = self.__guiEntry.configBuddy
		if centry.parent != None:
			centry.parent.moveUp(centry)

	def moveDown(self):
		"""Move config entry one position down in the section."""
		if not self.multiple or self.isMultipleEntryContainer:
			# Entry is not moveable 
			return None
		# Move GUI entry
		gentry = self.__guiEntry
		if gentry.parent != None:
			gentry.parent.moveDown(gentry)
		# Move config entry
		centry = self.__guiEntry.configBuddy
		if centry.parent != None:
			centry.parent.moveDown(centry)
	
	@property
	def primaryChild(self):
		"""Return communicator of primary child in this section."""
		if self.__guiEntry.type != FcTypes.SECTION:
			return None
		primaryChild = self.__guiEntry.primaryChild
		if primaryChild != None:
			return primaryChild.proxy
		else:
			return None


class FcGEntry(object):

	"""Base class of all client widgets. The client widget classes must be sub-classes of this class"""

	def __init__ (self, communicator):
		self.communicator = communicator
		if self.communicator != None:
			self.communicator.connectClient(self)
			
	def __del__ (self):
		if self.communicator != None:
			self.communicator.disconnect()

	def processSignal (self, signal, value):
		raise NotImplementedError, "Abstract method was not defined!"

