/***************************************************************************
                          fcTKNumber.cpp  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cfloat>
#include <cstdlib>
#include "fcTKNumber.h"

//--------------------------------------------------------------------------
fcTKNumber :: fcTKNumber( )
   : max( DBL_MAX ),
     min( -DBL_MAX ),
     step( 1.0 )
{
}
//--------------------------------------------------------------------------
fcTKNumber :: fcTKNumber( const fcTKNumber& entry )
   : fcTKEntry( entry )
{
}
//--------------------------------------------------------------------------
Fc::EntryTypes fcTKNumber :: Type() const
{
   return Fc::Number;
}
//--------------------------------------------------------------------------
fcBaseEntry* fcTKNumber :: NewEntry() const
{
   return new fcTKNumber( * this );
}
//--------------------------------------------------------------------------
const fc_real& fcTKNumber :: GetMax() const 
{
   return max;
}
//--------------------------------------------------------------------------
void fcTKNumber :: SetMax( const fc_real& _max )
{
   max = _max;
}
//--------------------------------------------------------------------------
const fc_real& fcTKNumber :: GetMin() const
{ 
   return min;
}
//--------------------------------------------------------------------------
void fcTKNumber :: SetMin( const fc_real& _min )
{
   min = _min;
}
//--------------------------------------------------------------------------
const fc_real& fcTKNumber :: GetStep() const
{
   return step; 
}
//--------------------------------------------------------------------------
void fcTKNumber :: SetStep( const fc_real& _step )
{ 
   step = _step;
} 
//--------------------------------------------------------------------------
void fcTKNumber :: SetDependencyValue( const fc_char* val ) {
    dependency_value = atof( val );
}
//--------------------------------------------------------------------------
fc_real fcTKNumber :: GetDependencyValue() const {
    return dependency_value;  
}
//--------------------------------------------------------------------------
#ifdef DEBUG
void fcTKNumber :: Print( ostream& stream ) const
{
   stream << "NUMBER ";
   fcTEntry :: Print( stream );
}
#endif

