/***************************************************************************
author           : 2009 by David Fabian
***************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/


#include "fcGWindow.h"
#include "fcCSEntry.h"
#include "fcGTab.h"
#include "fcCGSEntry.h"
#include "fcMessageHandler.h"
#include "fcSAXGuiTemplateFile.h"


#include "fcTEntry.h"

fcSAXGuiTemplateFile::fcSAXGuiTemplateFile ()
  :_rootElementOpened(false),
   _tabElementOpened(false),
   _sectionElementOpened(false),
   _currentElement(NoElement)
{

}

    
fcSAXGuiTemplateFile::~fcSAXGuiTemplateFile ()
{

}


void fcSAXGuiTemplateFile::startElement(const XMLCh* const uri,
                                        const XMLCh* const localName,
                                        const XMLCh* const qName,
                                        const Attributes& attributes)
{
  const fc_char *const elementName = XMLString::transcode(localName);
  
  if(!_rootElementOpened && strcmp(elementName, "freeconf-gui-template") == 0)
    {
//       DBG_COUT( "Freeconf-template tag." );
      _rootElementOpened = true;
      _currentElement = RootElement;
      return;
    }
    
    if(!_rootElementOpened)
    {
      MessageHandler.Error("fcSAXGuiTemplateFile.cpp", "startElement", "You must enclose the Gui template file with <freeconf-gui-template> and </freeconf-gui-template>.");
      return;
    }
    
    if(!strcmp(elementName, "window"))
    {
//       MessageHandler.Message("window start");
      _currentElement = Window;
      return;
    }

    if(!strcmp(elementName, "min-width"))
    {
//       MessageHandler.Message("min-width start");
      _currentElement = MinWidth;
      return;
    }
    
    if(!strcmp(elementName, "min-height"))
    {
//       MessageHandler.Message("min-height start");
      _currentElement = MinHeight;
      return;
    }
    
    if(!strcmp(elementName, "max-width"))
    {
//       MessageHandler.Message("min-width start");
      _currentElement = MaxWidth;
      return;
    }
    
    if(!strcmp(elementName, "max-height"))
    {
//       MessageHandler.Message("max-height start");
      _currentElement = MaxHeight;
      return;
    }

    if(!strcmp(elementName, "max-height"))
    {
      //       MessageHandler.Message("max-height start");
      _currentElement = MaxHeight;
      return;
    }

    if(!strcmp(elementName, "tab"))
    {
      //       MessageHandler.Message("tab start");
      _sectionStack.EraseAll();
      fcCGSEntry *rootSection = new fcCGSEntry();
      fcGTab *tab = new fcGTab();
      _window->addTab(tab);
      tab->setContent(rootSection);
      _sectionStack.Add(rootSection);
      
      _tabElementOpened = true;
      _currentElement = Tab;
      return;
    }

    if(!strcmp(elementName, "tab-name"))
    {
      if (!_tabElementOpened || _currentElement != Tab)
      {
        MessageHandler.Warning("Element <tab-name> is only allowed as first child of the <tab> element");
        return;
      }
      //       MessageHandler.Message("tab-name start");
      _currentElement = TabName;
      return;
    }

    if(!strcmp(elementName, "tab-icon"))
    {
      if (!_tabElementOpened)
      {
        MessageHandler.Warning("Element <tab-icon> is only allowed inside the <tab> element");
        return;
      }
      //       MessageHandler.Message("tab-icon start");
      _currentElement = TabIcon;
      return;
    }

    if(!strcmp(elementName, "tab-section"))
    {
      if (!_tabElementOpened)
      {
        MessageHandler.Warning("Element <tab-section> is only allowed inside the <tab> element");
        return;
      }
      //       MessageHandler.Message("tab-section start");
      _currentElement = TabSection;
      _sectionElementOpened = true;
      fcCGSEntry *section = new fcCGSEntry();
      _sectionStack.Top()->addChild(section);
      _sectionStack.Add(section);
      return;
    }

    if(!strcmp(elementName, "tab-section-name"))
    {
      if (!_sectionElementOpened || _currentElement != TabSection)
      {
        MessageHandler.Warning("Element <tab-section-name> is only allowed inside the <section> element");
        return;
      }
      //       MessageHandler.Message("tab-name start");
      _currentElement = TabSectionName;
      return;
    }

    if(!strcmp(elementName, "import-template-entry"))
    {
      if (!_sectionElementOpened && !_tabElementOpened)
      {
        MessageHandler.Warning("Element <import-template-entry> is only allowed inside the <section> or <tab> element");
        return;
      }
      //       MessageHandler.Message("import-template-entry start");
      _currentElement = ImportTemplateEntry;
      return;
    }

}

                                             
void fcSAXGuiTemplateFile::endElement(const XMLCh* const uri,
                const XMLCh* const localName,
                const XMLCh* const qName)
{
  const fc_char *const elementName = XMLString::transcode(localName);

  if (!strcmp(elementName, "freeconf-gui-template"))
  {
    //     MessageHandler.Message("window end");
    _currentElement = NoElement;
    _rootElementOpened = false;
    return;
  }
  
  if (!strcmp(elementName, "window"))
  {
//     MessageHandler.Message("window end");
    _currentElement = NoElement;
    return;
  }
  
  if (!strcmp(elementName, "min-width"))
  {
    if (_currentElement == MinWidth)
    {
//       MessageHandler.Message("min-width end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiTemplateFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }

  if (!strcmp(elementName, "min-height"))
  {
    if (_currentElement == MinHeight)
    {
//       MessageHandler.Message("min-height end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiTemplateFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }

  if (!strcmp(elementName, "max-width"))
  {
    if (_currentElement == MaxWidth)
    {
//       MessageHandler.Message("max-width end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiTemplateFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }

  if (!strcmp(elementName, "max-height"))
  {
    if (_currentElement == MaxHeight)
    {
//       MessageHandler.Message("max-height end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiTemplateFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }

  if (!strcmp(elementName, "tab"))
  {
    _currentElement = NoElement;
    _tabElementOpened = false;
    return;
  }

  if (!strcmp(elementName, "tab-name"))
  {
    if (_currentElement == TabName)
    {
      //       MessageHandler.Message("tab-name end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiTemplateFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }

  if (!strcmp(elementName, "tab-icon"))
  {
    if (_currentElement == TabIcon)
    {
      //       MessageHandler.Message("tab-name end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiTemplateFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }

  if (!strcmp(elementName, "tab-section"))
  {
    _currentElement = NoElement;
    _sectionElementOpened = false;
    _sectionStack.Remove();
    return;
  }

  if (!strcmp(elementName, "tab-section-name"))
  {
    if (_currentElement == TabSectionName)
    {
      //       MessageHandler.Message("tab-name end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiTemplateFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }


  if (!strcmp(elementName, "import-template-entry"))
  {
    if (_currentElement == ImportTemplateEntry)
    {
      //       MessageHandler.Message("import-template-entry end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiTemplateFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }

//   MessageHandler.Warning(fcString("unknown end element ") + elementName);

}


void fcSAXGuiTemplateFile::characters(const XMLCh* const chars, const unsigned int length)
{
  const fc_char *data = XMLString::transcode(chars);
  
  if (_currentElement == MinWidth)
  {
   //  MessageHandler.Message("Filling min-width");
    _window->setMinWidth(atoi(data));
    return;
  }

  if (_currentElement == MinHeight)
  {
   //  MessageHandler.Message("Filling min-height");
    _window->setMinHeight(atoi(data));
    return;
  }

  if (_currentElement == MaxWidth)
  {
   //  MessageHandler.Message("Filling max-width");
    _window->setMaxWidth(atoi(data));
    return;
  }
  
  if (_currentElement == MaxHeight)
  {
   //  MessageHandler.Message("Filling max-height");
    _window->setMaxHeight(atoi(data));
    return;
  }

  if (_currentElement == TabName)
  {
  //   MessageHandler.Message("Filling tab name");
     _window->lastTab()->setName(data);
    return;
  }

  if (_currentElement == TabIcon)
  {
    //MessageHandler.Message("Filling tab icon");
    if (!_tabElementOpened)
    {
      MessageHandler.Warning("Element tab-name is allowed only inside the tab element");
      return;
    }

    if (!strcmp(data, "FC::NOICON"))
    {
      _window->lastTab()->setIcon(Fc::NoIcon);
    }
    else if (!strcmp(data, "FC::SECURITY"))
    {
      _window->lastTab()->setIcon(Fc::Security);
    }
    else if (!strcmp(data, "FC::PASSWORD"))
    {
      _window->lastTab()->setIcon(Fc::Password);
    }
    else if (!strcmp(data, "FC::NETWORK"))
    {
      _window->lastTab()->setIcon(Fc::Network);
    }
    else if (!strcmp(data, "FC::TERMINAL"))
    {
      _window->lastTab()->setIcon(Fc::Terminal);
    }
    else if (!strcmp(data, "FC::USER"))
    {
      _window->lastTab()->setIcon(Fc::User);
    }
    else if (!strcmp(data, "FC::TOOLS"))
    {
      _window->lastTab()->setIcon(Fc::Tools);
    }
    else if (!strcmp(data, "FC::DISPLAY"))
    {
      _window->lastTab()->setIcon(Fc::Display);
    }
    else if (!strcmp(data, "FC::PACKAGE"))
    {
      _window->lastTab()->setIcon(Fc::Package);
    }
    else if (!strcmp(data, "FC::FILE"))
    {
      _window->lastTab()->setIcon(Fc::File);
    }
    else if (!strcmp(data, "FC::DIRECTORY"))
    {
      _window->lastTab()->setIcon(Fc::Directory);
    }
    else
    {
      MessageHandler.Warning(fcString("Unknown icon type '") + fcString(data) + "'");
    }
      
    return;
  }

  if (_currentElement == TabSectionName)
  {
  //   MessageHandler.Message("Filling tab section name");
     _sectionStack.Top()->setName(data);
    return;
  }

  if (_currentElement == ImportTemplateEntry)
  {
    //   MessageHandler.Message("Filling import template entry");
    fcCEntry *entry = _configTree->RecursiveFindCEntry(data);
    if (!entry)
    {
      MessageHandler.Warning(fcString("Template entry at '") + fcString(data) + "' not found");
      return;
    }
    // is config entry already connected to gui tree?
    if (entry->GetGuiBuddy())
    {
      // we need to overwrite the rule, that means remove element from tree and place it into another place
      fcCGEntry *guiEntry = entry->GetGuiBuddy();
      // remove from the gui tree
      dynamic_cast<fcCGSEntry*>(guiEntry->parent())->removeChild(guiEntry);
    }
    fcCGEntry *tmpEntry = new fcCGEntry(entry);
    entry->SetGuiBuddy(tmpEntry);
    _sectionStack.Top()->addChild(tmpEntry);
    return;
  }
  
}


fc_bool fcSAXGuiTemplateFile::ParseGuiTemplateFile(const fc_char *fileName, fcCSEntry *configTree, fcGWindow *window)
{
  _rootElementOpened = false;
  _tabElementOpened = false;
  _sectionElementOpened = false;
  _currentElement = NoElement;
  _sectionStack.EraseAll();
  if (!window)
  {
    MessageHandler.Error("Invalid pointer found, expecting existing window object");
    return false;
  }
  if (!configTree)
  {
    MessageHandler.Error("Invalid pointer found, expecting existing config tree object");
    return false;
  }
  _window = window;
  _configTree = configTree;
  return Parse(fileName);
}


                                                              