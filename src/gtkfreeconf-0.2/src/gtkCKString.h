/***************************************************************************
                          gtkCKString.h  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef gtkCKStringH
#define gtkCKStringH

#include <gtk/gtk.h>
#include <fcCKString.h>
#include <fcCSEntry.h>

class gtkCKString;

struct gtkCKStringStructure {
    gtkCKString* entry;
    fcCSEntry* main_entry;
};
//--------------------------------------------------------------------------
class gtkCKString : public fcCKString
{
   //! Strings in the GUI widget
   GList* g_strings;

   //! Widget for this entry
   GtkWidget* widget;

   //! Pointer to gtkCKStringStructure structure
   gtkCKStringStructure *struc;

   //! Say if the widget is GtkCombo or GtkEntry
   fc_bool isCombo;
   
   public:
 
   //! Basic constructor
   gtkCKString();

   //! Copy constructor
   gtkCKString( const gtkCKString& string );

   //! Constructor template entry pointer
   gtkCKString( const fcTEntry* t_entry );
 
   //! Destructor
   ~gtkCKString();

   //! Render the boolean GUI widget
   void Render( GtkWidget* parent_widget,
                GtkTooltips* tooltips,
                const fc_char* help,
                fcCSEntry* main_c_section );

   //! GtkWidget setter
   void SetWidget( GtkWidget* _widget );

   //! GtkWidget getter
   GtkWidget* GetWidget();

   //! isCombo getter 
   /*! Return true, if the widget is GtkCombo,
    *  otherwise return false
    */
   fc_bool GetIsCombo();
};

#endif
