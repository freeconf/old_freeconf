/***************************************************************************
                          fcTKNumber.h  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcTKNumberH
#define fcTKNumberH

#include "fcTKEntry.h"

class fcTKNumber : public fcTKEntry
{
   //! Dependency value 
   fc_real dependency_value;

   //! Maximum and minimum of possible values
   fc_real max, min;

   //! Increment/decrement step
   fc_real step;
   
   public:
   //! Basic constructor
   fcTKNumber();

   //! Copy constructor
   fcTKNumber( const fcTKNumber& entry );

   //! Destructor
   ~fcTKNumber(){};

   //! Entry type getter
   Fc::EntryTypes Type() const;

   //! Allocate new instance and return pointer
   virtual fcBaseEntry* NewEntry() const;

   //! Max getter
   const fc_real& GetMax() const;

   //! Max setter
   void SetMax( const fc_real& _max );

   //! Min getter
   const fc_real& GetMin() const;

   //! Min setter
   void SetMin( const fc_real& _min ); 

   //! Step getter
   const fc_real& GetStep() const;

   //! Step setter
   void SetStep( const fc_real& _step ); 

   //! Dependency_value setter
   void SetDependencyValue( const fc_char* val );

   //! Dependency_value getter
   fc_real GetDependencyValue() const;


#ifdef DEBUG
   //! Print this describer
   void Print( ostream& stream ) const;
#endif

};

#endif
