/***************************************************************************
author           : 2009 by David Fabian
***************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/


#include "fcCGSEntry.h"


fcCGSEntry::fcCGSEntry (fcCGEntry *parent)
  :fcCGEntry(parent),
   _it(_children.begin())
{
  
}


fcCGSEntry::~fcCGSEntry ()
{
  for (_it = _children.begin(); _it != _children.end(); _it++)
  {
    delete *_it;
  }
}


void fcCGSEntry::addChild (fcCGEntry *child)
{
  if (!child) return;
  _children.push_back(child);
  child->setParent(this);
  _it = _children.begin();
}


void fcCGSEntry::removeChild (fcCGEntry *child)
{
  for (std::list<fcCGEntry*>::iterator it = _children.begin(); it != _children.end(); it++)
  {
    if (*it == child)
    {
      _children.erase(it);
      break;
    }
  }
}


fc_uint fcCGSEntry::children () const
{
  return _children.size();
}


fcCGEntry* fcCGSEntry::next () const
{
  fcCGEntry *tmp = 0;
  if (_it != _children.end())
  {
    tmp = *_it;
    _it++;
    return tmp;
  }
  else
  {
    return 0;
  }
}


void fcCGSEntry::skipToFirst () const 
{
  _it = _children.begin();
}


fcCGEntry* fcCGSEntry::findChild (const fcString &childName) const
{
  for (std::list<fcCGEntry*>::const_iterator it = _children.begin();
       it != _children.end(); it++)
  {
    if ((*it)->name() == childName)
    {
      return *it;
    }
  }
  return 0;
}


Fc::EntryTypes fcCGSEntry::type () const
{
  return Fc::Section;
}