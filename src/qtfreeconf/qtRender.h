#ifndef _QT_RENDER_H
#define _QT_RENDER_H

#include <QMainWindow>
#include <fcTypes.h>

class QWidget;
class fcPackage;
class fcTSEntry;
class fcDOMConfigFile;
class QVBoxLayout;
class QLabel;
class QListWidget;
class QListWidgetItem;
class qtCSRootEntry;
class QScrollArea;

class qtRender: public QMainWindow
{
  Q_OBJECT
  
  private:
    QWidget *_main;

    fcPackage *_package;
    QVBoxLayout *_contentLayout;
    QScrollArea *_scroll;
    QListWidget *_iconView;
    QLabel *_tabLabel;
    qtCSRootEntry *_currentConfigPage;
    
    void transform ();

    QIcon loadIcon (Fc::Icon icon);
    
  public:
    qtRender (fcPackage *package);

    ~qtRender ();
    
    void render ();

  public slots:
    void tabChanged(QListWidgetItem *current, QListWidgetItem *previous);

  private slots:
    void apply ();
    void ok ();
};

#endif
