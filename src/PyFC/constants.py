

xmlEscapeTable = {
'"': "&quot;",
"'": "&apos;",
">": "&gt;",
"<": "&lt;"
}

def escapeXML (text):
	"""Produce entities within text."""
	return "".join(xmlEscapeTable.get(c, c) for c in text)

def getXMLIndent (n, string = "\t"):
	"""Generate default XML indent string n-times."""
	return string * n

class FcTypes:
	"""Basic Freeconf types."""
	## Entry Types ##
	UNKNOWNENTRY = 1
	SECTION = 2
	FUZZY = 3
	BOOL = 4
	NUMBER = 5
	STRING = 6
	REFERENCE = 7

class FcSignals:
	"""List of signals, that can be sent from PyFC to GUI Widget"""
	CHANGE_ENABLED = 1	# property enabled of config entry changed
	CHANGE_ACTIVE = 2	# property mandatory of template entry changed
	CHANGE_MANDATORY = 3	# property mandatory of template entry changed
	CHANGE_VALUE = 4	# value of config entry has been changed by a dependency
	CHANGE_MIN = 5		# property min of FcTKNumber has been changed
	CHANGE_MAX = 6		# property max of FcTKNumber has been changed
	CHANGE_STEP = 7		# property step of FcTKNumber has been changed
	CHANGE_STRING_LIST = 8	# property values of FcTKString has been changed
	CHANGE_INCONSISTENCY = 9	# property inconsistency of FcCKEntry has been changed
	CHANGE_EMPTINESS = 10 # this signal is invoked if a section or tab has only inactive or non-mandatory children
	CHANGE_SHOW_ALL = 11 # this signal is called when the show all button has been pressed

