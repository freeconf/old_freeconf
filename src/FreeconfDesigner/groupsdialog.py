#!/usr/bin/python
#
# groupsdialog.py
# begin: 15.10.2011 by Jan Ehrenberger
#
# Dialog for editation of list of groups
#

from ui_groupsdialog import Ui_GroupsDialog
from groupdialog import editGroupDialog
from PyQt4 import QtCore, QtGui


class GroupsListItem(QtGui.QListWidgetItem):
	def __init__(self, group):
		QtGui.QListWidgetItem.__init__(self, group.name)
		self.group = group

class GroupsDialog(QtGui.QDialog):
	def __init__(self, package, parent = None):
		QtGui.QDialog.__init__(self, parent)
		self.package = package

		self.ui = Ui_GroupsDialog()
		self.ui.setupUi(self)

		# Fill dialog with groups
		self.ui.groupsList.clear()
		for group in self.package.groups:
			self.ui.groupsList.addItem(GroupsListItem(group))
	
	def addGroup(self):
		"""Slot for button addGroup."""
		dialog = editGroupDialog(self)
		if dialog.exec_():
			# User confirmed dialog, create the group
			group = dialog.createGroup(self.package)
			self.ui.groupsList.addItem(GroupsListItem(group))

	def deleteGroup(self):
		"""Slot for button deleteGroup."""
		item = self.ui.groupsList.currentItem()
		if item:
			# Confirm group removal
			res = QtGui.QMessageBox.question(
				self,
				"Delte Group?",
				"Do you really want to delete group %s?" % (item.group.name,),
				buttons = QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
				defaultButton = QtGui.QMessageBox.Yes
			)
			if res != QtGui.QMessageBox.Yes:
				return
			# Remove the group
			self.package.removeGroup(item.group)
			row = self.ui.groupsList.row(item)
			self.ui.groupsList.takeItem(row)
			
	def editGroup(self, item):
		if item == None:
			return
		dialog = editGroupDialog()
		dialog.fillFromGroup(item.group)
		if dialog.exec_():
			dialog.saveToGroup(item.group)
			
