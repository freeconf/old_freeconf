/***************************************************************************
                          fcCSEntry.cpp  -  description
                             -------------------
    begin                : 2004/08/13
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/util/XMLString.hpp>
#include "debug.h"
#include "fcCEntry.h"
#include "fcCKBool.h"
#include "fcCKList.h"
#include "fcCKNumber.h"
#include "fcCKString.h"
#include "fcCSEntry.h"
#include "fcMCSContainer.h"
#include "fcMCKContainer.h"
#include "fcMessageHandler.h"
#include "fcTKEntry.h"
#include "fcTSEntry.h"
#include "fcTEntry.h"
#ifdef DEBUG
   #include "fcCKEntry.h"
#endif

//--------------------------------------------------------------------------
static fcList< fcString >* ParseName( const fc_char* name ) {
/*    fcList< fcString >* strlst = new fcList< fcString >;
    fcString part;
    fc_int length = strlen(name);
    const fc_char* slash = "/";
    if(!length) return 0;
    fc_char tmparray [2] = { 'a' , '\0' };
    
    for(fc_int i = 0; i < length; i++) {
        tmparray[0] = (fc_char)name[i];
        const char* tmp = const_cast<char*>(tmparray);
        if( tmp[0] == slash[0] ) {
            strlst -> Append(part); 
            part = 0;
        }
        else {
            part += tmp;
        }
    }
    strlst -> Append(part); 
    return strlst;*/
  fcList<fcString> *stringList = new fcList<fcString>;
  fcString part;
  const fc_char slash = '/';
  fc_int length = strlen(name);
 
  for(fc_int i = 0; i < length; i++)
  {
    if(name[i] == slash)
    {
      if (part)
        stringList->Append(part);
      part.Erase();
     // part = 0;
    }
    else {
      part += name[i];
    }
  }
  if (part) stringList->Append(part);
  return stringList;
}
//--------------------------------------------------------------------------
fcCSEntry :: fcCSEntry()
{
}
//--------------------------------------------------------------------------
fcCSEntry :: fcCSEntry( const fcCSEntry& section_entry )
   : fcCEntry( section_entry ),
     fcSectionEntry( section_entry )
{
  template_entry = section_entry.GetTEntry();
}
//--------------------------------------------------------------------------
fcCSEntry :: fcCSEntry( const fcTEntry* t_entry )
   : fcCEntry( t_entry )
{
}
//--------------------------------------------------------------------------
fcBaseEntry* fcCSEntry :: NewEntry() const
{
   return new fcCSEntry( *this );
}
//--------------------------------------------------------------------------
Fc::EntryTypes fcCSEntry :: Type() const
{
  return Fc::Section;
}
//--------------------------------------------------------------------------
fc_bool fcCSEntry :: IsMultipleEntryContainer() const
{
   return false;
}
//--------------------------------------------------------------------------
fcCEntry* fcCSEntry :: CreateCEntry( const fcTEntry* t_entry, fc_bool ignore_multiple ) const
{
   fcCEntry* new_entry;
   if( !ignore_multiple && t_entry -> GetMultiple() )
   {
      if( t_entry -> IsSection() )
         new_entry = new fcMCSContainer( t_entry );
      else
        new_entry = new fcMCKContainer( t_entry );
   }
   else switch( t_entry -> Type() )
   {
      case Fc::Section:
         new_entry = new fcCSEntry( t_entry );
         break;
      case Fc::Bool:
         new_entry = new fcCKBool( t_entry );
         break;
      case Fc::Number:
         new_entry = new fcCKNumber( t_entry );
         break;
      case Fc::List:
         new_entry = new fcCKList( t_entry );
         break;
      case Fc::String:
         new_entry = new fcCKString( t_entry );
         break;
      default:;
   }
   if( ! new_entry )
   {
      MessageHandler. Error(
            fcString( "Unable to allocate new config entry " ) +
            t_entry -> GetEntryName() );
   }
   return new_entry;
}
//--------------------------------------------------------------------------
fc_bool fcCSEntry :: Init( const fcTEntry* t_entry )
{
   DBG_FUNCTION_NAME( "fcCSEntry", "Init" );
   assert( t_entry -> IsSection() );
   assert( IsEmpty() );
   DBG_COUT( "Set entry name to " << t_entry -> GetEntryName() );
   //SetEntryName( t_entry -> GetEntryName(). Data() );
   const fcTSEntry* t_section = ( const fcTSEntry* ) t_entry;
   for( fc_int i = 0; i < t_section -> Size(); i ++ )
   {
      const fcTEntry* tmp_entry = ( * t_section )[ i ];
      DBG_EXPR( tmp_entry );
      fcCEntry* new_entry = CreateCEntry( tmp_entry );
      if( ! new_entry ) return false;
      Append( new_entry );

      // For multiple entries we do not want to init
      // the new container - it will contain nothing but
      // the multiple entries.
      if( ! tmp_entry -> GetMultiple() )
      if( new_entry -> IsSection() )
        if( ! ( ( fcCSEntry* ) new_entry ) -> Init( tmp_entry ) )
           return false;
   }
   return true;
}
//--------------------------------------------------------------------------
fcCEntry* fcCSEntry :: operator[] ( fc_uint pos )
{
   DBG_FUNCTION_NAME( "fcCSEntry", "operator[]" );  
   fcBaseEntry* entry = fcSectionEntry :: operator[]( pos );
   assert( entry -> IsCEntry() );
   return ( fcCEntry* ) entry;
}
//--------------------------------------------------------------------------
fcCEntry* fcCSEntry :: FindCEntry( const fc_char* entry_name,
                                   fc_uint& pos )
{
   DBG_FUNCTION_NAME( "fcCSEntry", "FindCEntry" );  
   DBG_EXPR( entry_name );
   fcBaseEntry* entry = fcSectionEntry :: FindEntry( entry_name, pos );
   if( ! entry ) return NULL;
   assert( entry -> IsCEntry() );
   return ( fcCEntry* ) entry;
}
//--------------------------------------------------------------------------
fcCEntry* fcCSEntry :: FindCEntry( const fc_char* entry_name )
{
   DBG_FUNCTION_NAME( "fcCSEntry", "FindCEntry" );  
   DBG_EXPR( entry_name );
   fcBaseEntry* entry = fcSectionEntry :: FindEntry( entry_name );
   if( ! entry ) return NULL;
   assert( entry -> IsCEntry() );
   return ( fcCEntry* ) entry;
}
//--------------------------------------------------------------------------
fcCEntry* fcCSEntry :: RecursiveFindCEntry( const fc_char* entry_full_name ) {
   fcList< fcString >* stringList = ParseName( entry_full_name );
/*   for (int i = 0; i < strlst->Size(); i++)
   if(!strlst) return 0;*/
   fcCEntry* entry = FindCEntry(stringList->at(0).Data());
   for(fc_int i = 1; i < stringList->Size(); i++)
   {
     if(!entry)
     {
       delete stringList;
       return 0;
     }
     if (entry->Type() != Fc::Section)
     {
       delete stringList;
       return 0;
     }
     entry = dynamic_cast<fcCSEntry*>(entry)->FindCEntry( stringList->at(i).Data() );
   }
   delete stringList;
   return entry;
}
//--------------------------------------------------------------------------
fcCEntry* fcCSEntry :: RecursiveFindCEntry( const fcString& entry_full_name )
{
    return RecursiveFindCEntry(entry_full_name.Data());
}
//--------------------------------------------------------------------------
void fcCSEntry :: UpdateDOMNode( DOMNode* parent_node, 
                                 DOMDocument* doc )
{
   DBG_FUNCTION_NAME( "fcCSEntry", "UpdateDOMNode" );
   DBG_EXPR( GetEntryName() );
   assert( doc );
   if( ! dom_node )
   {
      dom_node = doc -> createElement(
            XMLString :: transcode( GetEntryName(). Data() ) );
      parent_node -> appendChild( dom_node );

   }
   for( fc_int i = 0; i < Size(); i ++ )
      ( * this )[ i ] -> UpdateDOMNode( dom_node, doc );
}
//--------------------------------------------------------------------------
const fcCEntry* fcCSEntry :: operator[] ( fc_uint pos ) const
{
   return const_cast< fcCSEntry* >( this ) -> operator[]( pos );
}
//--------------------------------------------------------------------------
const fcCEntry* fcCSEntry :: FindCEntry( const fc_char* name ) const
{
   return const_cast< fcCSEntry* >( this ) -> FindCEntry( name );
}
//--------------------------------------------------------------------------
const fcCEntry* fcCSEntry :: FindCEntry( const fc_char* name,
                                         fc_uint& pos ) const
{
   return const_cast< fcCSEntry* >( this ) -> FindCEntry( name, pos );
}
//--------------------------------------------------------------------------
#ifdef DEBUG
void fcCSEntry :: Print( ostream& stream ) const
{
   stream << "CONFIG SECTION: " << GetEntryName() << " at " << this << endl;
   fcSectionEntry :: Print( stream );
}
#endif
//--------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& stream, const fcCSEntry& c_section)
{
#ifdef DEBUG
   c_section. Print( stream );
#else 
    stream << c_section.GetEntryName().Data();
#endif
   return stream;
}
//--------------------------------------------------------------------------
