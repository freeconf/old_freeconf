#!/usr/bin/python
#
# base.py
# begin: 2.5.2010 by Jan Ehrenberger
#
# Freeconf base classes
#

import sys

from log import log
from constants import *
from lists import boolList
from class_visitor import FcVisitee, FcSVisitee
from client_interface import FcGUICommunicator
from designer_interface import FcTemplateCommunicator
from exception import *

class FcBaseEntry (object, FcVisitee):
	"""This is a basic class for all entries."""
	def __init__ (self, name = ""):
		self._name = str(name)
		self.parent = None

	@property
	def type (self):
		return FcTypes.UNKNOWNENTRY

	@property
	def name (self):
		return self._name

	@name.setter
	def name (self, name):
		self._name = str(name)
	
	@property
	def root(self):
		"""Return root of current tree."""
		obj = self
		while obj.parent != None:
			obj = obj.parent
		return obj

	def isSection (self):
		"""Return true if it is a section."""
		raise NotImplementedError

	def isKeyword (self):
		"""Return true if it is a kewyord."""
		raise NotImplementedError

	def isTEntry (self):
		"""Return true if it is a template entry."""
		raise NotImplementedError

	def isCEntry (self):
		"""Return true if it is a config entry."""
		raise NotImplementedError

	def __repr__ (self):
		return self.__class__.__name__ + '(' + self.name + ')'

	def __cmp__ (self, b):
		"""Compare with other FcBaseEntry or with string."""
		if type(b) in (str, unicode):
			# Comparison with string tests name
			return cmp(self.name, b)
		elif b == None:
			# Handle None type. Object and None will never be equal.
			return 1
		else:
			# Otherwise do default compare using hash
			return cmp(hash(self), hash(b))

	def moveToPosition (self, destination, position):
		"""Moves this entry into different position in the parent container.
		You need to call this function for centry and tentry separately."""
		if self.parent == None: return False
		self.parent.disconnect(self)
		if position == -1:
			destination.append(self)
		else:
			destination.insertEntry(self, position)
		return True
		
	def getFullPath(self):
		"""Return full path in current tree in form of: /a/b/c/..."""
		path = self.name
		obj = self
		while obj.parent != None:
			obj = obj.parent
			path = obj.name + "/" + path
		return "/" + path


class FcTEntry(FcBaseEntry):
	"""This is a basic class for all template entries."""

	def __init__ (self, proxy = None):
		FcBaseEntry.__init__(self)
		# Initialize properties:
		self._label = {}
		self.__proxy = proxy
		self._staticActive = True
		self._dynamicActive = True
		self._staticMandatory = False
		self._dynamicMandatory = False
		# Multiple properties
		self.multiple = False
		self._multipleMin = None
		self._multipleMax = None

		self.__group = None # Group of entry
		self.package = None # Plugin or package, from which this entry originates.

	# Override of functions from base class:
	def isTEntry (self):
		"""Return true if it is a template entry."""
		return True

	def isCEntry (self):
		"""Return true if it is a config entry."""
		return False

	def isReference(self):
		"""Return True if object is reference to another keyword."""
		return False

	def keyLabel (self, language = ""):
		"""Returns the correct mutation of the entry's label"""
		try:
			if language == "":
				return self._label[self.package.currentLanguage]
			else:
				return self._label[language]
		except KeyError:
			return ""
			
	def setKeyLabel (self, language, label):
		self._label[language] = label

	@property
	def proxy (self):
		"""Returns the proxy object of this entry"""
		if self.__proxy == None:
			self.__proxy = FcTemplateCommunicator(self)
		return self.__proxy
	
	@property
	def group (self):
		if self.__group == None and self.parent != None:
			# Propagate group from entry's parent
			#self.__group = self.parent.group
			return self.parent.group
		return self.__group
	
	@group.setter
	def group (self, group):
		self.__group = group

	def _makeCEntry (self):
		"""Return CEntry for this template entry. Implement it in subclasses."""
		raise NotImplementedError, "This is abstract method!"

	def createCEntry (self, centry_parent):
		"""Create and add centry to config tree."""

		# Try to find entry in config tree
		centry = None
		if centry_parent != None and self.multiple == False:
			centry = centry_parent.findCEntry(self.name)

		if centry == None:
			# There is no such config entry yet
			if self.multiple == True:
				if centry_parent != None and centry_parent.isMultipleEntryContainer():
					# Check number of children
					n = len(centry_parent.entries)
					if self.multipleMax and n + 1 > self.multipleMax:
						raise FcMultipleError("Can't add child! Maximum number of children (%d) reached." % (self.multipleMax,))
					# Create the entry
					centry = self._makeCEntry()
				else:
					# Create multiple container
					centry = FcMCContainer(self)
			else:
				# Create normal entry
				centry = self._makeCEntry()
			# add the config buddy to the config tree
			if centry_parent != None:
				centry_parent.append(centry)
		return centry

	@property
	def configBuddy (self):
		"""Returns config buddy of this entry. If the buddy does not exist, it is created and added to the config tree"""
		raise FcGeneralError("Property configBuddy is obsolete in template entry.")
	
	@property
	def staticActive (self):
		"""Static activity indicates that the entry is switched on or off. Entries that are off are not processed in
		the client and cannot be turned on by dependencies"""
		return self._staticActive
		
	@staticActive.setter
	def staticActive (self, active):
		self._staticActive = active
		self._dynamicActive =  active

	@property
	def staticMandatory (self):
		"""Static mandatority indicates that the entry is mandatory or not. Entries that are statically mandatory
		cannot be set non-mandatory by dependencies"""
		return self._staticMandatory
		
	@staticMandatory.setter
	def staticMandatory (self, mandatory):
		self._staticMandatory = mandatory
		self._dynamicMandatory = mandatory

	@property
	def dynamicActive (self):
		"""Dynamic activity is the activity set by dependencies"""
		if self._staticActive == False: return False
		return self._dynamicActive
		
	@dynamicActive.setter
	def dynamicActive (self, active):
		if self._staticActive == False and active == True:
			log.error("Key " + self.name + " cannot be set active by a dependency because it is switched off in the template.")
			return
		self._dynamicActive = active

	@property
	def dynamicMandatory (self):
		"""Dynamic mandatority is the mandatority set by dependencies"""
		if self._staticMandatory == True: return True
		return self._dynamicMandatory
		
	@dynamicMandatory.setter
	def dynamicMandatory (self, mandatory):
		if self._staticMandatory == True and mandatory == False:
			log.error("Key " + self.name + " cannot be set non-mandatory by a dependency because it is set mandatory in the template.")
			return
		self._dynamicMandatory = mandatory
	
	@property
	def multipleMin (self):
		return self._multipleMin
	
	@multipleMin.setter
	def multipleMin (self, value):
		if self.multiple == False:
			raise FcGeneralError("Can't set multipleMin property. Entry %s is not multiple!" % (str(self),))
		if value > 0:
			self._multipleMin = value
		else:
			# 0 or less means multipleMin property is disabled
			self._multipleMin = None
	
	@property
	def multipleMax (self):
		return self._multipleMax
	
	@multipleMax.setter
	def multipleMax (self, value):
		if self.multiple == False:
			raise FcGeneralError("Can't set multipleMax property. Entry %s is not multiple!" % (str(self),))
		if value > 0:
			self._multipleMax = value
		else:
			# 0 or less means multipleMax property is disabled
			self._multipleMax = None
	
	def outputMultiple (self, indent = 1):
		"""Generate string in template XML format describing multiple configuration of current entry."""
		string = ""
		if self.multiple:
			indent += 1
			multipleProps = ""
			if self.multipleMin:
				multipleProps += getXMLIndent(indent) + "<min>%d</min>\n" % (self.multipleMin,)
			if self.multipleMax:
				multipleProps += getXMLIndent(indent) + "<max>%d</max>\n" % (self.multipleMax,)
			indent -= 1
			if multipleProps != "":
				string += getXMLIndent(indent) + "<multiple>\n"
				string += multipleProps
				string += getXMLIndent(indent) + "</multiple>\n"
			else:
				string += getXMLIndent(indent) + "<multiple/>\n"
		return string
		
	def convertValue(self, value):
		"""Check if given value can be converted to value for this entry, and if so, return converted value."""
		raise NotImplementedError, "This is abstract method!"


class FcTKEntry (FcTEntry):
	"""This is a class for keyword entries from Template File"""

	def __init__ (self, proxy = None):
		FcTEntry.__init__(self, proxy)
		self._defaultValue = None
		self._help = {}

	@property
	def defaultValue (self):
		return self._defaultValue
	
	@defaultValue.setter
	def defaultValue (self, value):
		"""Set default value."""
		if value != None:
			value = self.convertValue(value)
		self._defaultValue = value
		log.debug("setting default def %s on %s" % (str(self._defaultValue), str(self.name)))

	def keyHelp (self, language = ""):
		"""Returns correct mutation of the entry's help string"""
		try:
			if len(self._help) == 0:
				return ""
			if language == "":
				return self._help[self.package.currentLanguage]
			else:
				return self._help[language]
		except KeyError:
			return ""
			
	def setKeyHelp (self, language, help):
		self._help[language] = help
		
	# Override of functions:
	def isSection (self):
		return False

	def isKeyword (self):
		return True
		
	@property
	def _typeName (self):
		"""Returns type string. Must be implemented in sub-classes"""
		raise NotImplementedError
		
	def outputProperties (self, indent = 1):
		"""Returns property string. Must be implemented in sub-classes"""
		raise NotImplementedError
	
	def getValueRepr (self, value):
		"""Get representation of given value according to settings of template entry."""
		return str(value)
	
	def output (self, indent = 0):
		"""Creates output of the entry which will be saved in the template file"""
		string  = getXMLIndent(indent) + "<%s>\n" % (self._typeName,)
		indent += 1
		string += getXMLIndent(indent) + "<entry-name>%s</entry-name>\n" % (self.name,)
		if self.group:
			string += getXMLIndent(indent) + "<group>%s</group>\n" % (self.group.name,)
		string += self.outputMultiple(indent)
		if self.staticActive == False:
			string += getXMLIndent(indent) + "<active>no</active>\n"
		if self.staticMandatory == True:
			string += getXMLIndent(indent) + "<mandatory>yes</mandatory>\n"
		tmp = self.outputProperties()
		if tmp != "":
			string += "\t<properties>\n%s\t</properties>\n" % tmp
		indent -= 1
		string += getXMLIndent(indent) + "</%s>\n" % self._typeName
		return string
	
	def outputDefault (self, package = None):
		# Filter by package
		if package != None and self.package != package: return ""
		# Generate default value
		if self.defaultValue != None:
			return "<entry name=\"" + self.name + "\">" + self.getValueRepr(self.defaultValue) + "</entry>\n"
		else:
			log.debug(self.name + ": skipping output of default value")
			return ""
	
	def outputHelp (self, language):
		"""Creates output of the entry's help which will be saved in the help file"""
		return "<entry name=\"" + self.name + "\">\n\t<label>" + self.keyLabel(language) + \
					 "</label>\n\t<help>" + escapeXML(self.keyHelp(language)) + "</help>\n</entry>\n"

class FcTKFuzzy(FcTKEntry):
	"""This is a class for keyword entries of type fuzzy from template file."""

	def __init__ (self, proxy = None):
		FcTKEntry.__init__(self, proxy)
		## Initialize Properties
		# Maximum possible value
		self._maxSet = False
		self._max = 1.0
		# Minimum possible value
		self._minSet = False
		self._min = 0.0
		# Property: List of fuzzy values
		self.__list = None

	@property
	def type (self):
		return FcTypes.FUZZY

	@property
	def list(self):
		return self.__list
	
	@list.setter
	def list(self, l):
		if l.type != FcTypes.FUZZY:
			log.error(
				"%s: Incompatible list type! Can't set list to '%s'.",
				str(self), l.name
			)
		else:
			self.__list = l

	@property
	def maxSet (self):
		return self._maxSet

	@property
	def minSet (self):
		return self._minSet

	@property
	def max(self):
		return self._max

	@property
	def min(self):
		return self._min

	@max.setter
	def max(self, m):
		assert type(m) == float and m >= 0.0 and m <= 1.0
		self._maxSet = True
		self._max = m

	@min.setter
	def min(self, m):
		assert type(m) == float and m >= 0.0 and m <= 1.0
		self._minSet = True
		self._min = m

	def _makeCEntry (self):
		return FcCKFuzzy(self)

	@property
	def _typeName (self):
		return "fuzzy"
		
	def outputProperties (self, indent = 1):
		string = ""
		if self.list != None:
			string += getXMLIndent(indent) + "<data>%s</data>\n" % (str(self.list.name),)
		if self._minSet:
			string += getXMLIndent(indent) + "<min>%s</min>\n" % (str(self._min),)
		if self._maxSet:
			string += getXMLIndent(indent) + "<max>%s</max>\n" % (str(self._max),)
		return string

	def valueToGrade(self, value):
		"""Return grade for given value."""
		if value == None:
			return None
		entry = self.list.getEntry(value)
		if entry != None:
			return entry.grade
		else:
			return None
	
	def gradeToValue(self, grade):
		"""Return valid value from fuzzy list with given garde."""
		if grade == None:
			return None
		else:
			# Find value with given grade
			entry = self.list.getExactGrade(grade)
			if entry == None:
				raise FcGeneralError("Value with grade %d was not found in fuzzy list %s!" % (grade, self.list.name))
			return entry.value

	@property
	def defaultGrade(self):
		"""Get grade of default value."""
		return self.valueToGrade(self.defaultValue)
		
	@defaultGrade.setter
	def defaultGrade(self, grade):
		"""Set default value with given garde."""
		self.defaultValue = self.gradeToValue(grade)

	def convertValue (self, v):
		"""Check if given value can be converted to value for this entry, and if so, return converted value."""
		v = str(v)
		entry = self.list.getEntry(v)
		assert entry != None
		return entry.value
	

class FcTKBool(FcTKFuzzy):
	"""This is a class for keyword entries of type bool from template file."""

	def __init__ (self, proxy = None):
		FcTKFuzzy.__init__(self, proxy)
		self.list = boolList

	@property
	def type (self):
		return FcTypes.BOOL
	
	# Min and Max properties are read-only in this class
	@property
	def min(self):
		return self._min
	@property
	def max(self):
		return self._max

	def _makeCEntry (self):
		return FcCKBool(self)

	@property
	def _typeName (self):
		return "bool"
		
	def outputProperties (self, indent = 1):
		return ""
		

class FcTKNumber(FcTKEntry):
	"""This is a class for keyword entries if type number from template file."""

	def __init__(self, proxy = None):
		FcTKEntry.__init__(self, proxy)
		## Initialize Properties
		# Maximum possible value
		self.__maxSet = False
		#self.__max = sys.float_info.max
		self.__max = 999999999.0
		# Minimum possible value
		self.__minSet = False
		#self.__min = sys.float_info.min
		self.__min = -999999999.0
		# Increment / decrement step
		self.__stepSet = False
		self.__step = 1
		# number precision
		self.__precisionSet = False
		self.__precision = 0
		# number format
		self.__printSignSet = False
		self.__printSign = False

		self.__leadingZerosSet = False
		self.__leadingZeros = False

	@property
	def type(self):
		return FcTypes.NUMBER

	@property
	def maxSet (self):
		return self.__maxSet

	@property
	def minSet (self):
		return self.__minSet

	@property
	def stepSet (self):
		return self.__stepSet

	@property
	def precisionSet (self):
		return self.__precisionSet

	@property
	def formatSet (self):
		return self.__formatSet

	@property
	def max (self):
		return self.__max

	@property
	def min (self):
		return self.__min

	@property
	def step (self):
		return self.__step
		
	@property
	def precision (self):
		return self.__precision

	@property
	def printSign (self):
		return self.__printSign

	@property
	def leadingZeros (self):
		return self.__leadingZeros

	@max.setter
	def max(self, max):
		assert type(max) in [float, int]
		self.__maxSet = True
		if self.__precisionSet == True:
			if self.__precision == 0:
				self.__max = int(max)
			else:
				self.__max = round(max, self.__precision)
		else:
			self.__max = max

	@min.setter
	def min(self, min):
		assert type(min) in [float, int]
		self.__minSet = True
		if self.__precisionSet == True:
			if self.__precision == 0:
				self.__min = int(min)
			else:
				self.__min = round(min, self.__precision)
		else:
			self.__min = min

	@step.setter
	def step(self, step):
		assert type(step) in [float, int]
		self.__stepSet = True
		if self.__precisionSet == True:
			if self.__precision == 0:
				self.__step = int(step)
			else:
				self.__step = round(step, self.__precision)
		else:
			self.__step = step

	@precision.setter
	def precision (self, precision):
		self.__precisionSet = True
		self.__precision = precision
		self.max = self.__max
		self.min = self.__min
		self.step = self.__step
		
	@printSign.setter
	def printSign (self, printSign):
		self.__printSignSet = True
		self.__printSign = printSign

	@leadingZeros.setter
	def leadingZeros (self, leadingZeros):
		self.__leadingZerosSet = True
		self.__leadingZeros = leadingZeros

	def _makeCEntry (self):
		return FcCKNumber(self)

	def convertValue(self, v):
		"""Check if given value can be converted to value for this entry, and if so, return converted value."""
		return self.checkValue(float(v))
	
	def checkValue(self, value = None):
		"""Check if entry's value is within permitted range. If not, return nearest value that is in the range."""
		if value == None:
			value = self.value
		if self.maxSet and value > self.max:
			return self.max
		elif self.minSet and value < self.min:
			return self.min
		return value

	def getValueRepr (self, value):
		tmp = ""
		if self.precision == 0:
			size = 0 # no period, it's an integer
			tmp = str(int(value))
		else:
			size = 1 # must add the period
			tmp = "%.*f" % (self.precision, value) # keep trailing zeros
		if self.leadingZeros == True:
			size += max(map(lambda x: len(str(abs(int(x)))), [self.max, self.min])) + self.precision
			if value >= 0.0:
				tmp = tmp.zfill(size)
			else:
				tmp = tmp.zfill(size + 1) # +1 for the - sign
		if self.printSign == True:
			if value >= 0.0:
				tmp = "+" + tmp
		return tmp
		
	@property
	def _typeName (self):
		return "number"
		
	def outputProperties (self, indent = 1):
		string = ""
		if self.__precisionSet:
			string += getXMLIndent(indent) + "<precision>%d</precision>\n" % (self.__precision,)
		if self.__minSet:
			string += getXMLIndent(indent) + "<min>%s</min>\n" % (str(self.__min),)
		if self.__maxSet:
			string += getXMLIndent(indent) + "<max>%s</max>\n" % (str(self.__max),)
		if self.__stepSet:
			string += getXMLIndent(indent) + "<step>%s</step>\n" % (str(self.__step),)
		if self.__printSignSet:
			if self.__printSign == True:
				string += getXMLIndent(indent) + "<print-sign>yes</print-sign>\n"
			else:
				string += getXMLIndent(indent) + "<print-sign>no</print-sign>\n"
		if self.__leadingZerosSet:
			if self.__leadingZeros == True:
				string += getXMLIndent(indent) + "<leading-zeros>yes</leading-zeros>\n"
			else:
				string += getXMLIndent(indent) + "<leading-zeros>no</leading-zeros>\n"
		return string
		

class FcTKString(FcTKEntry):
	"""This is a class for keyword entries if type string from template file."""

	def __init__(self, proxy = None):
		FcTKEntry.__init__(self, proxy)
		# Possible values
		self.__list = None
		# If true, user can insert value which is not in the list
		self.userValues = True
		self.regExp = ""

	@property
	def type(self):
		return FcTypes.STRING
	
	@property
	def list(self):
		return self.__list
	
	@list.setter
	def list(self, l):
		if l.type != FcTypes.STRING:
			log.error(
				"%s: Incompatible list type! Can't set list to '%s'.",
				str(self), l.name
			)
		else:
			self.__list = l

	def _makeCEntry (self):
		return FcCKString(self)

	@property
	def _typeName (self):
		return "string"
		
	def outputProperties (self, indent = 1):
		string = ""
		if self.list:
			string += getXMLIndent(indent) + "<data>%s</data>\n" % (self.list.name,)
		if self.regExp != "":
			string += getXMLIndent(indent) + "<regexp>%s</regexp>\n" % (self.regExp,)
		return string

	def convertValue(self, v):
		"""Check if the given value can be converted to a value for this entry, and if so, return converted value."""
		assert type(v) in (str, unicode)
		return v


class FcSEntry (object, FcSVisitee):
	"""Base class for sections. It is in fact list of FcBaseEntry entries."""

	def __init__ (self):
		self._entries = []

	def getEntry (self, i):
		return self._entries[i]

	def findEntry (self, name, pos = 0):
		"""Find entry with given name."""
		indices = [i for i, x in enumerate(self._entries) if x.name == name]
		if len(indices) == 0:
			return (None, None)
		else:
			#assert len(indices) == 1
			return (indices[0], self._entries[indices[0]])

	def insertEntry (self, entry, position):
		#assert isinstance(entry, FcBaseEntry)
		entry.parent = self
		self._entries.insert(int(position), entry)


	def append (self, entry):
		if entry != None:
			#assert isinstance(entry, FcBaseEntry)
			entry.parent = self
			self._entries.append(entry)


	def replaceEntry (self, old, new):
		(index, node) = self.findEntry(old)
		if index == None: return False
		self._entries[index] = new
		new.parent = self
		return True


	def disconnect (self, entry):
		try:
			self._entries.remove(entry)
			entry.parent = None
			return True
		except ValueError, KeyError:
			return False


	@property
	def entries(self):
		return self._entries

	def size(self):
		return len(self._entries)
	
	def _swap(self, i, j):
		"""Swap entries at given positions."""
		# Check range
		i = max(0, i)
		j = max(0, j)
		i = min(self.size() - 1, i)
		j = min(self.size() - 1, j)
		if i == j:
			return
		# Swap entries
		t = self._entries[i]
		self._entries[i] = self._entries[j]
		self._entries[j] = t
	
	def moveUp(self, entry):
		"""Move given entry up in the list. If it is on the top of the list, nothing happens."""
		i = self._entries.index(entry)
		self._swap(i, i - 1)
	
	def moveDown(self, entry):
		"""Move given entry down in the list. If it is no the bottom of the list, nothing happens."""
		i = self._entries.index(entry)
		self._swap(i, i + 1)
		
		
class FcInconsistency (object):
	"""This class is used for handling inconsistent states in non-section entries. The entry is inconsistent iff
	it is active, mandatory and its value and default value has not been set. Inconsistent entries can not be disabled"""
	def __init__ (self, inconsistent):
		self._inconsistent = inconsistent
		
	@property
	def inconsistent (self):
		return self._inconsistent

	def changeInconsistency (self, value):
		"""This method is called whenever the inconsistency must be changed"""
		if self._inconsistent == value: return
		self._inconsistent = value
		self.evaluateInconsistency()
		
	def evaluateInconsistency (self):
		"""Check if the entry is still inconsistent. Usually reimplemented in sub-class"""
		self.notifyParent(self._inconsistent)
		
	def notifyParent (self, value):
		"""Notify parent entry when the inconsistency has been changed. Must be reimplemented in sub-class"""
		raise NotImplementedError
		
		
class FcSInconsistency (FcInconsistency):
	"""This class is used for handling inconsistent states in section entries. The entry is inconsistent if it contains
	at least one inconsistent child. Otherwise it is consistent"""
	def __init__ (self):
		FcInconsistency.__init__(self, False)
		# number of inconsistent children
		self._inconsistentCount = 0

	def changeInconsistency (self, value):
		if value == True:
			self._inconsistentCount += 1
		else:
			self._inconsistentCount -= 1
		self.evaluateInconsistency()
		
	def evaluateInconsistency (self):
		assert self._inconsistentCount >= 0
		if self._inconsistentCount == 0 and self._inconsistent == True:
			self._inconsistent = False
			self.notifyParent(False)
		elif self._inconsistentCount == 1 and self._inconsistent == False:
			self._inconsistent = True
			self.notifyParent(True)


class FcTSEntry(FcTEntry, FcSEntry):
	"""This is basic class for deriving groups of entries."""
	def __init__(self, proxy = None):
		FcTEntry.__init__(self, proxy)
		FcSEntry.__init__(self)

	@property
	def type(self):
		return FcTypes.SECTION

	def isSection(self):
		return True

	def isKeyword(self):
		return False

	def findTEntry(self, name, pos = 0):
		"""Find template entry with given name."""
		(i, e) = self.findEntry(name, pos)
		assert e == None or e.isTEntry()
		return e

	def recursiveFindTEntry(self, full_name):
		"""Find entry in template tree. Name is given in format: /a/b/c../entry"""
		entry = None
		for name in full_name.split('/')[2:]:
			if entry != None:
				if entry.type != FcTypes.SECTION:
					# There is something inside the path that is not section
					return None

			if entry == None:
				(i, entry) = self.findEntry(name)
			else:
				(i, entry) = entry.findEntry(name)

			if entry == None:
				# Entry was not found
				return None
			assert entry.isTEntry()
		return entry
	
	def output (self, indent = 0):
		string = getXMLIndent(indent) + "<section>\n"
		indent += 1
		string += getXMLIndent(indent) + "<section-name>%s</section-name>\n" % (self.name, )
		if self.group:
			string += getXMLIndent(indent) + "<group>%s</group>\n" % (self.group.name,)
		string += self.outputMultiple(indent)
		for entry in self.entries:
			string += entry.output(indent)
		indent -= 1
		string += getXMLIndent(indent) + "</section>\n"
		return string
	
	def outputDefault(self, package = None):
		string = "<section name=\"" + self.name + "\">\n"
		cache = ""
		for entry in self.entries:
			cache += entry.outputDefault(package)
		# skip empty sections except the top-level one
		if cache == "" and self.parent != None:
			log.debug(self.name + ": skipping empty section")
			return ""
		string += cache
		string += "</section>\n"
		return string

	def _makeCEntry (self):
		return FcCSEntry(self)

	def outputHelp (self, language):
		string = "<section name=\"" + self.name + "\">\n\t<label>" + self.keyLabel(language) + "</label>\n\t"
		for entry in self.entries:
			string += entry.outputHelp(language)
		string += "</section>\n"
		return string

	def disconnect (self, entry):
		#raise FcGeneralError("Zkontroluj jestli se to vsude vola spranve!")
		return FcSEntry.disconnect(self, entry)
		#if FcSEntry.disconnect(self, entry) == True:
		#	self.configBuddy.disconnect(entry.configBuddy)


class FcTReference(FcTEntry):
	"""Reference to template keyword or section. This object redirects calls to it's target."""
	def __init__(self, proxy = None):
		FcTEntry.__init__(self, proxy)
		# Reference to template object
		self.target = None
	
	def isReference(self):
		"""Return True if object is reference to another keyword."""
		return True
	
	@property
	def type(self):
		return self.target.type
	
	def isSection(self):
		return self.target.isSection()
	
	def isKeyword(self):
		return self.target.isKeyword()
	
	@property
	def multiple(self):
		return self.target.multiple
	
	@multiple.setter
	def multiple(self, value):
		pass # Reference does not have it's own multiple property
	
	@property
	def defaultValue(self):
		return self.target.defaultValue()
	
	@defaultValue.setter
	def defaultValue(self, value):
		raise AttributeError("Read only attribute!")
	
	def keyLabel(self, language = ""):
		return self.target.keyLabel(language)
	
	def setKeyLabel(self, language, label):
		raise AttributeError("Read only attribute!")
	
	def _makeCEntry(self):
		return self.target._makeCEntry()
	
	def convertValue(self, value):
		return self.target.convertValue(value)
	
	def __getattr__(self, name):
		return getattr(self.target, name)

	def output(self, indent = 0):
		string = getXMLIndent(indent) + "<reference>\n"
		indent += 1
		string += getXMLIndent(indent) + "<entry-name>%s</entry-name>\n" % (self.name,)
		if self.staticActive == False:
			string += getXMLIndent(indent) + "<active>no</active>\n"
		if self.staticMandatory == True:
			string += getXMLIndent(indent) + "<mandatory>yes</mandatory>\n"
		if self.target != None:
			string += getXMLIndent(indent) + "<target>%s</target>\n" % (self.target.getFullPath(),)
		indent -= 1
		string += getXMLIndent(indent) + "</reference>\n"	
		return string

	def outputDefault(self, package = None):
		"""Stop recursion of outputDefault at reference."""
		return ""

	def outputHelp (self, language):
		"""Stop recursion of outputHelp at reference."""
		return ""

class FcCEntry(FcBaseEntry):
	"""This is basic class for all config entries."""

	def __init__ (self, templateBuddy = None):
		FcBaseEntry.__init__(self)
		self.__templateBuddy = templateBuddy
		self.guiBuddy = None

	def isCEntry(self):
		return True
		
	def isTEntry(self):
		return False

	@property
	def name (self):
		if self.__templateBuddy != None:
			return self.__templateBuddy.name
		else:
			return ""
			
	@property
	def templateBuddy (self):
		return self.__templateBuddy
	

class FcCKEntry (FcCEntry, FcInconsistency):
	"""Base class for configuration values."""
	def __init__(self, templateBuddy = None):
		FcCEntry.__init__(self, templateBuddy)
		if self.templateBuddy != None:
			FcInconsistency.__init__(
				self,
				(self.templateBuddy.dynamicActive == True and self.templateBuddy.dynamicMandatory == True)
				or self.templateBuddy.defaultValue == None
			)
		else:
			FcInconsistency.__init__(self, False)
		self.__dependents = []
		self._value = None
		self._enabled = True

	def isSection(self):
		return False
	def isKeyword(self):
		return True
	def isTEntry(self):
		return False
	def isCEntry(self):
		return True

	@property
	def enabled (self):
		return self._enabled

	@property
	def defaultValue (self):
		"""Get default value from template entry."""
		if self.templateBuddy != None:
			return self.templateBuddy.defaultValue
		else:
			return None

	def initInconsistency (self):
		if self.templateBuddy != None:
			if self.templateBuddy.defaultValue == None: 
				if self._value == None: 
					self.changeInconsistency(True) 
			else: 
				if self.inconsistent == True: 
					self.changeInconsistency(False) 
	
	#@defaultValue.setter
	#def defaultValue (self, value):
		#"""Set default value and change inconsistency if needed"""
		## TODO: Tohle tu je jen docasne pro snadnejsi prechod defaultnich hodnot do template zaznamu. pozdeji by se mela volat primo metoda template zaznamu.
		#if self.templateBuddy != None:
		#	self.templateBuddy.defaultValue = value

	def changeInconsistency (self, value):
		"""Change inconsistency and notify parent"""
		if self.templateBuddy == None: return
		if value == True and self.inconsistent == False and \
		(self.templateBuddy.dynamicActive == True and self.templateBuddy.dynamicMandatory == True): 
			log.info("changing inconsistency of %s to %d" % (self.name, value))
			self._inconsistent = True
		if value == False and self.inconsistent == True:
			log.info("changing inconsistency of %s to %d" % (self.name, value))
			self._inconsistent = False
		self.notifyParent(self._inconsistent)
					
	def evaluateInconsistency (self):
		"""Checks if the entry is still inconsistent when activness or mandatority has changed"""
		if self.templateBuddy == None: return
		log.debug("checking inconsistency of %s, inc %d, man %d, act %d" % (self.name, self.inconsistent, self.templateBuddy.dynamicMandatory, self.templateBuddy.dynamicActive))
		if self.templateBuddy.dynamicMandatory == True and self.inconsistent == True:
			if self.templateBuddy.dynamicActive == True:
				if self._enabled == False:
					raise FcInconsistencyError("There is an unreachable inconsistent key " + self.name + " in the package")
				self.notifyParent(True)
			else:
				self.notifyParent(False)
		else:
			if self.templateBuddy.dynamicActive == True and self.inconsistent == True:
				self.notifyParent(False)
			
	def notifyParent (self, value):
		"""Notifies GUI tree about change of inconsistency"""
		log.debug("notifyParent %s guiBuddy: %s " % (self.name, str(self.guiBuddy)))
		if self.guiBuddy != None and self.guiBuddy.parent != None:
			self.guiBuddy.callWidget(FcSignals.CHANGE_INCONSISTENCY, value)
			self.guiBuddy.parent.changeInconsistency(value)
		
	@property
	def value(self):
		if self._value == None:
			return self.defaultValue
		return self._value

	@value.setter
	def value(self, value):
		self.setValue(value)
	
	@property
	def valueRepr(self):
		"""Returns string representation of value as it will be written to output file."""
		return self.templateBuddy.getValueRepr(self.value)

	def setValue(self, value, informGui = False):
		"""Sets new value and changes inconsistency if needed"""
		if value != None:
			value = self.templateBuddy.convertValue(value)
		if value == self._value: return
		self._value = value
		if self._value != None or self.defaultValue != None:
			if self._inconsistent == True:
				self.changeInconsistency(False)
		else:
			if self._inconsistent == False:
				self.changeInconsistency(True)
		if informGui:
			log.debug("Setting value = " + str(self._value))
			# Inform GUI about change of value
			if self.guiBuddy != None:
				self.guiBuddy.callWidget(FcSignals.CHANGE_VALUE, self._value)
		# update dependencies that depend on this value
		self.updateDependents()

	def handleDependencyEvent (self, event, value):
		"""This property is a brigde between the config tree and the dependency code. Special dependency signals
		can be handled in sub-classes"""
		assert self.templateBuddy != None
		if event == "active":
			if value == self.templateBuddy.dynamicActive: return
			self.templateBuddy.dynamicActive = value
			self.evaluateInconsistency()
			if self.guiBuddy != None:
				self.guiBuddy.callWidget(FcSignals.CHANGE_ACTIVE, value)
				self.guiBuddy.parent.changeActive(value)
		elif event == "mandatory":
			if value == self.templateBuddy.dynamicMandatory: return
			self.templateBuddy.dynamicMandatory = value
			self.evaluateInconsistency()
			if self.guiBuddy != None:
				self.guiBuddy.callWidget(FcSignals.CHANGE_MANDATORY, value)
				self.guiBuddy.parent.changeMandatory(value)
		elif event == "enable":
			if value == self._enabled: return
			self._enabled = value
			self.evaluateInconsistency()
			if self.guiBuddy != None:
				self.guiBuddy.callWidget(FcSignals.CHANGE_ENABLED, value)
		elif event == "value":
			self.element.setValue(self.value, True)

	def addDependent (self, dep):
		"""Add dependency that depends on this entry."""
		self.__dependents.append(dep)

	def removeDependent (self, dep):
		"""Remove given dependency from list of dependent dependencies."""
		self.__dependents.remove(dep)
	
	def updateDependents (self):
		"""Indicates that value of this entry changed. Update all dependent dependencies."""
		log.debug("Updating dependencies on " + str(self))
		for d in self.__dependents:
			d.execute()

	def output (self, package = None, groupName = None, writeHelp = True, writeType = True):
		"""Creates output string of the entry which will be saved in the config file.
		You can select keys that will be written to output file.
		If package is set to None, keys from main package and all plugins will be written
		If package is set, only keys from that package(plugin) will be written.
		You can also select data by groupName. This filter is applied after plugin filter.
		"""
		if self.templateBuddy == None: return ""
		# Compare plugin
		if package != None and self.templateBuddy.package != package: return ""
		# Compare group
		if groupName != None and self.templateBuddy.group.name != groupName: return ""
		# other conditions
		if (self.templateBuddy.group.outputDefaults == False and (self._value in (None, self.defaultValue))) or \
			 (self.templateBuddy.dynamicActive == False) or \
			 (self.templateBuddy.dynamicMandatory == False and self.value == None):
			log.debug(self.name + ": skipping output of value")
			return ""
		if self._inconsistent == True:
			raise FcSaveError("Key " + self.name + " is mandatory but inconsistent")
		log.debug("Outputting element " + self.name + " with value " + str(self.value))
		s = "<entry name=\"%s\">\n\t<value>%s</value>\n" % (self.name, self.valueRepr)
		if writeHelp:
			s = s + "\t<help>%s</help>\n" % (escapeXML(self.templateBuddy.keyHelp()),)
		if writeType:
			s = s + "\t<type>%s</type>\n" % (self.templateBuddy._typeName,)
		s = s + "</entry>\n"
		return s


class FcCKFuzzy(FcCKEntry):
	"""Class for fuzzy config entry."""
	def __init__ (self, t_entry = None):
		FcCKEntry.__init__(self, t_entry)
	
	@property
	def type (self):
		return FcTypes.FUZZY
	
	def checkValue(self, value = None):
		"""Check if entry's value is within permitted range. If not, return nearest value that is in the range."""
		if value == None:
			value = self.value
		if self.templateBuddy == None:
			return value
		e = self.templateBuddy.list.getEntry(value)
		if e == None:
			log.error("Value '%s' was not found in fuzzy list '%s' for entry %s!" % (value, self.templateBuddy.list.name, self.name))
			# Return first value in the list
			first = self.templateBuddy.list.getFirstEntry()
			if first == None:
				log.error("Fuzzy list '%s' is empty!" % (self.templateBuddy.list.name,))
				# TODO:nemelo by to tady vyhodit spis chybu?
				return ''
			else:
				return first.value
		if e.grade > self.templateBuddy.max:
			# Find value with lower grade
			e = self.templateBuddy.list.getMaxGrade(self.templateBuddy.max)
		if e.grade < self.templateBuddy.min:
			# Find value with higher grade
			e = self.templateBuddy.list.getMinGrade(self.templateBuddy.min)
		return e.value
	
	@property
	def list(self):
		if self.templateBuddy == None:
			return None
		return self.templateBuddy.list


	def handleDependencyEvent (self, event, value):
		"""Handles incoming dependency events"""
		FcCKEntry.handleDependencyEvent(self, event, value)
		if event == "min":
			if value == self.templateBuddy.min: return
			self.templateBuddy.min = value
			# Inform GUI about change of property
			if self.guiBuddy != None:
				self.guiBuddy.callWidget(FcSignals.CHANGE_MIN, self.templateBuddy.min)
			self.setValue(self.checkValue(), True)
		elif event == "max":
			if value == self.templateBuddy.max: return
			self.templateBuddy.max = value
			# Inform GUI about change of property
			if self.guiBuddy != None:
				self.guiBuddy.callWidget(FcSignals.CHANGE_MAX, self.templateBuddy.max)
			self.setValue(self.checkValue(), True)

	@property
	def grade(self):
		return self.templateBuddy.valueToGrade(self.value)
	
	@grade.setter
	def grade(self, grade):
		self.value = self.templateBuddy.gradeToValue(grade)
	

class FcCKBool(FcCKFuzzy):
	"""Class for bool config entry."""
	def __init__(self, t_entry = None):
		FcCKFuzzy.__init__(self, t_entry)

	@property
	def type(self):
		return FcTypes.BOOL

	
class FcCKNumber(FcCKEntry):
	"""Class for number config entry."""
	def __init__(self, t_entry = None):
		FcCKEntry.__init__(self, t_entry)

	@property
	def type(self):
		return FcTypes.NUMBER

	def handleDependencyEvent (self, event, value):
		"""Handles incoming dependency events"""
		FcCKEntry.handleDependencyEvent(self, event, value)
		if event == "min":
			if value == self.templateBuddy.min: return
			self.templateBuddy.min = value
			# Inform GUI about change of property
			if self.guiBuddy != None:
				self.guiBuddy.callWidget(FcSignals.CHANGE_MIN, self.templateBuddy.min)
			self.setValue(self.checkValue(), True)
		elif event == "max":
			if value == self.templateBuddy.max: return
			self.templateBuddy.max = value
			# Inform GUI about change of property
			if self.guiBuddy != None:
				self.guiBuddy.callWidget(FcSignals.CHANGE_MAX, self.templateBuddy.max)
			self.setValue(self.checkValue(), informGui = True)
		

class FcCKString(FcCKEntry):
	"""Class for string config entry."""
	def __init__(self, t_entry = None):
		FcCKEntry.__init__(self, t_entry)
		#self.value = "" # Value of config entry

	@property
	def type(self):
		return FcTypes.STRING



class FcCSEntry(FcCEntry, FcSEntry):
	"""Class for groups/sections in config files."""

	def __init__(self, t_entry = None):
		assert t_entry == None or (t_entry.isSection())
		FcCEntry.__init__(self, t_entry)
		FcSEntry.__init__(self)

	@property
	def type(self):
		return FcTypes.SECTION

	def isSection(self):
		return True

	def isKeyword(self):
		return False
	
	def isMultipleEntryContainer(self):
		"""Returns true if it is a multiple config entries container."""
		return False
	
	def findCEntry(self, name, pos = 0):
		"""Find config entry with given name starting at given position."""
		"""Find template entry with given name."""
		(i, e) = self.findEntry(name, pos)
		if e != None:
			assert not e.isTEntry()
		return e

	def recursiveFindCEntry(self, full_name):
		"""Find entry in configuration tree. Name is given in format: /a/b/c../entry"""
		entry = None
		for name in full_name.split('/')[2:]:
			if entry != None:
				if entry.type != FcTypes.SECTION:
					# There is something inside the path that is not section
					return None
				elif entry.templateBuddy.multiple == True:
					# There is multiple entry inside the path.
					raise FcGeneralError("Multiple entry %s inside path %s!" % (entry.name, full_name))

			if entry == None:
				(i, entry) = self.findEntry(name)
			else:
				(i, entry) = entry.findEntry(name)

			if entry == None:
				# Entry was not found
				return None
			assert entry.isCEntry()
		return entry
	
	def fill (self):
		"""Fill section with config entries created from template entry. Can be used to initialize config tree or multiple section."""
		log.debug("Filling config section " + self.name)
		for tentry in self.templateBuddy.entries:
			log.debug("tentry " + tentry.name)
			centry = self.findCEntry(tentry.name)
			if centry == None:
				# Config entry does not exist yet
				log.debug("Creating centry " + tentry.name)
				centry = tentry.createCEntry(self)
			if tentry.type == FcTypes.SECTION:# and centry.isMultipleEntryContainer() == False:
				centry.fill()

	def output (self, package = None, groupName = None, writeHelp = True, writeType = True):
		string = "<section name=\"" + self.name + "\">\n"
		cache = ""
		for entry in self.entries:
			cache += entry.output(package, groupName, writeHelp, writeType)
		# skip empty sections except the top-level one
		if cache == "" and self.parent != None:
			log.debug(self.name + ": skipping empty section")
			return ""
		string += cache
		string += "</section>\n"
		return string

	def initInconsistency (self):
		for entry in self.entries:
			entry.initInconsistency()

#### Multiple sections ####
class FcMCContainer(FcCSEntry):
	"""Container for multiple config entries."""
	def __init__(self, t_entry = None):
		FcCSEntry.__init__(self, t_entry)

	def isMultipleEntryContainer(self):
		return True

	def fill (self):
		"""Propagate filling into all existing multiple sections. If there is no child, it will not fill any further."""
		log.debug("Filling multiple container " + self.name)
		for centry in self.entries:
			centry.fill()

	def output (self, package = None, groupName = None, writeHelp = True, writeType = True):
		cache = ""
		for entry in self.entries:
			cache += entry.output(package, groupName, writeHelp, writeType)
		# skip empty sections except the top-level one
		if cache == "" and self.parent != None:
			log.debug(self.name + ": skipping empty multiple section")
			return ""
		return cache
	
	def createEntry(self):
		return self.templateBuddy.createCEntry(self)
	
	def disconnect(self, entry):
		# Check if we can remove the entry
		n = len(self.entries)
		if self.templateBuddy.multipleMin and n - 1 < self.templateBuddy.multipleMin:
			raise FcMultipleError("Can't remove child! Minimum number of children (%d) reached." % (self.templateBuddy.multipleMin,))
		# Call original method
		super(FcMCContainer, self).disconnect(entry)
	

#### GUI Template Classes ####
class FcCGEntry(object):
	"""Base class for GUI entries"""
	def __init__ (self, configBuddy = None, parent = None):
		self.configBuddy = configBuddy
		# Reference to the communicator that handles communication between the library and the client
		self.__proxy = None
		self.parent = parent
		self._label = ""
		self._name = ""
		self._showAll = False

	@property
	def name (self):
		if self._name != "":
			return self._name
		if self.configBuddy != None:
			return self.configBuddy.name
		else:
			return ""

	@name.setter
	def name (self, name):
		self._name = name
		# implicit fallback to key name
		if self._label == "":
			self._label = name

	@property
	def type (self):
		if self.configBuddy != None:
			return self.configBuddy.type
		return FcTypes.UNKNOWNENTRY
		
	@property
	def inconsistent (self):
		"""GUI entry is inconsistent if the matching config entry is inconsistent"""
		if self.configBuddy != None:
			return self.configBuddy.inconsistent
		else:
			return None

	@property
	def label (self, language = ""):
		if self.configBuddy != None:
			label = self.configBuddy.templateBuddy.keyLabel(language)
			if label == "":
				return self.configBuddy.templateBuddy.name
			else:
				return label
		return self._label

	@label.setter
	def label (self, label):
		self._label = label


	@property
	def proxy (self):
		"""Returns proxy object of this entry"""
		if self.__proxy == None:
			self.__proxy = FcGUICommunicator(self)
		return self.__proxy
		
	def callWidget (self, signal, arg):
		"""Sends a signal to the client widget through the client interface"""
		if self.__proxy != None:
			self.__proxy.processSignal(signal, arg)
			
	def initState (self):
		"""Counts states of all objects in the GUI tree. Number of active and mandatory keys is stored."""
		pass
	
	def showAll (self, value):
		"""Show all non-mandatory entries when "show advanced" button has been toggled"""
		if self.configBuddy == None or self.configBuddy.templateBuddy.dynamicActive == False: return
		if value == True:
			self.callWidget(FcSignals.CHANGE_SHOW_ALL, True)
		elif value == False and self.configBuddy.templateBuddy.dynamicMandatory == False:
			self.callWidget(FcSignals.CHANGE_SHOW_ALL, False)


class FcCGSEntry(FcCGEntry, FcSEntry, FcSInconsistency):
	"""GUI container class"""
	def __init__ (self, configBuddy = None, parent = None):
		FcCGEntry.__init__(self, configBuddy, parent)
		FcSEntry.__init__(self)
		FcSInconsistency.__init__(self)
		self._activeShown = 0
		self._mandatoryShown = 0
		self._sectionShown = 0
		self.empty = None
		self._showAllChildren = False
		if parent != None and isinstance(parent, FcCGSEntry):
			self._showAllChildren = parent._showAllChildren
		# Primary child reference
		if configBuddy != None and configBuddy.templateBuddy.multiple:
			if configBuddy.isMultipleEntryContainer():
				# This section is multiple entry container
				self._primaryChildName = None
			else:
				# This section is instance of multiple section
				self._primaryChild = None

	@property
	def type (self):
		return FcTypes.SECTION

	@property
	def inconsistent (self):
		return FcSInconsistency.inconsistent.fget(self)
	
	@property
	def showAllChildren(self):
		return self._showAllChildren
	
	@property
	def primaryChildName(self):
		raise AttributeError("Property primaryChildName is write only!")
	
	@primaryChildName.setter
	def primaryChildName(self, name):
		self._primaryChildName = name
		self._primaryChild = None
	
	@property
	def primaryChild(self):
		if self.parent == None or self.parent.configBuddy == None or \
		   self.configBuddy == None or not self.configBuddy.templateBuddy.multiple:
			return None
		if self.parent._primaryChildName == None and self._primaryChild == None:
			if len(self._entries) == 0:
				return None
			self._primaryChild = self._entries[0]
		if self._primaryChild == None:
			(index, self._primaryChild) = self.findEntry(self.parent._primaryChildName)
		return self._primaryChild

	def append (self, entry):
		"""Reimplementation of append(). Must count inconsistency of newly added entry"""
		super(FcCGSEntry, self).append(entry)
		if entry != None:
			if entry.inconsistent == True:
				self.changeInconsistency(True)
	
	def disconnect (self, entry):
		"""Reimplementation of disconnect(). Must count inconsistency of removed entry"""
		result = super(FcCGSEntry, self).disconnect (entry)
		if result == False: 
			log.error("removing a non-existing child from the children list")
			return 
	
		if entry.inconsistent == True:
			self.changeInconsistency(False)

	def changeActive (self, value):
		"""Counts active children in the container"""
		if value == True:
			self._activeShown += 1
		else:
			self._activeShown -= 1
		self.evaluateEmptiness()

	def changeMandatory (self, value):
		"""Counts mandatory children in the container"""
		if value == True:
			self._mandatoryShown += 1
		else:
			self._mandatoryShown -= 1
		self.evaluateEmptiness()

	def changeEmptiness (self, value):
		"""Counts empty children in the container"""
		if value == True:
			self._sectionShown -= 1
		else:
			self._sectionShown += 1
		self.evaluateEmptiness()

	def _isEmpty(self):
		"""Evaluates emptiness of the entry."""
		if( # There is section inside, that should be displayed
		    (self._sectionShown > 0) or 
		    # OR ShowAllChildren is on. We display all active entries.
		    (self._showAllChildren == True and self._activeShown > 0) or
		    # OR ShowAllChildren is off. We display only mandatory entries.
		    (self._showAllChildren == False and self._activeShown > 0 and self._mandatoryShown > 0)
		  ):
			return False
		# TODO: Je tady nasledujici podminka vubec potreba? Nestacilo by else?
		elif (self._mandatoryShown == 0 and self._sectionShown == 0) or \
		     (self._activeShown == 0 and self._sectionShown == 0):
			return True
		return None

	def evaluateEmptiness (self):
		"""Evaluates emptiness of the entry. If the entry is empty a signal is sent to the client and parent is notified"""
		# Evaluate emptiness
		e = self._isEmpty()
		if e == False and (self.empty == True or self.empty == None):
			# Emptiness changed to False
			self.callWidget(FcSignals.CHANGE_EMPTINESS, False)
			self.empty = False
			self.parent.changeEmptiness(False)
		elif e == True and (self.empty == False or self.empty == None):
			# Emptiness changed to True
			self.callWidget(FcSignals.CHANGE_EMPTINESS, True)
			self.empty = True
			self.parent.changeEmptiness(True)
	
	def _initMultipleContainerState(self):
		"""Special init method for multiple section containers. We have to initialize number of active
		   and mandatory keys from template tree, because GUI objects might not exist yet."""
		if len(self.entries) > 0:
			# If multiple section container is not empty, it will behave as normal section.
			self.initState()
			return

		# Multiple section container is empty.
		# Recursively browse template tree to count active and mandatory keys.
		def browseTemplateTree(tentry):
			if tentry.type == FcTypes.SECTION and not tentry.isReference():
				for e in tentry.entries:
					browseTemplateTree(e)
			elif tentry.dynamicActive == True:
				self._activeShown += 1
			elif tentry.dynamicMandatory == True:
				self._mandatoryShown += 1

		browseTemplateTree(self.configBuddy.templateBuddy)

		self.evaluateEmptiness()
			

	def initState (self):
		"""Counts states of all objects in the GUI tree. Number of active and mandatory keys is stored."""
		needEvaluation = False
		for entry in self._entries:
			if entry.type == FcTypes.SECTION:
				self._sectionShown += 1
				if entry.configBuddy and entry.configBuddy.isMultipleEntryContainer():
					entry._initMultipleContainerState()
				else:
					entry.initState()
			else:
				needEvaluation = True
				if entry.configBuddy != None:
					if entry.configBuddy.templateBuddy.dynamicActive == True:
						self._activeShown += 1
					if entry.configBuddy.templateBuddy.dynamicMandatory == True:
						self._mandatoryShown += 1
		if needEvaluation == True:
			self.evaluateEmptiness()


	def notifyParent (self, value):
		"""Notifies parent about the change of inconsistency"""
		self.callWidget(FcSignals.CHANGE_INCONSISTENCY, value)
		if self.parent != None:
			self.parent.changeInconsistency(value)
		

	def showAll (self, value):
		"""Shows all entries if the "show advanced" button has been toggled"""
		self._showAllChildren = value
		for entry in self._entries:
			entry.showAll(value)
		self.evaluateEmptiness()
	
	def appendCEntry(self, centry):
		"""Create GUI entry for given config entry and append it to GUI tree."""
		newEntry = None
		if centry.type == FcTypes.SECTION:
			newEntry = FcCGSEntry(centry, self)
		else:
			newEntry = FcCGEntry(centry, self)
		centry.guiBuddy = newEntry
		self.append(newEntry)
		# Fill new entry
		if centry.type == FcTypes.SECTION:
			newEntry.fill()
		return newEntry
	
	def fill (self, centry = None):
		"""Fill section with config entries."""
		log.debug("Filling GUI section " + self.name)

		if centry != None:
			# Set config entry if specified
			if self.configBuddy != None:
				# Remove link to old config buddy
				self.configBuddy.guiBuddy = None
			self.configBuddy = centry
			centry.guiBuddy = self

		if self.configBuddy == None:
			return None

		log.debug("Config buddy: " + str(self.configBuddy))
		log.debug("Filling with config buddy entries: " + str(self.configBuddy.entries))

		# Fill section
		for entry in self.configBuddy.entries:
			gui_entry = self.findEntry(entry.name)[1]
			#if gui_entry == None:
			log.debug("Appending centry " + entry.name + " to GUI section " + self.name)
			self.appendCEntry(entry)
			#elif gui_entry.type == FcTypes.SECTION:
			#	gui_entry.fill()
		

class FcGWindow(FcCGEntry, FcSEntry, FcSInconsistency):
	"""Class that represents the top-level dialogue window"""
	def __init__ (self):
		FcCGEntry.__init__(self)
		FcSEntry.__init__(self)
		FcSInconsistency.__init__(self)
		self._minWidth = 0
		self._minHeight = 0
		self._maxWidth = 0
		self._maxHeight = 0
		self.title = "Freeconf generated config dialog"


	@property
	def minHeight (self):
		return self._minHeight

	@minHeight.setter
	def minHeight (self, height):
		self._minHeight = height
		if self._minHeight > self._maxHeight and self._maxHeight != 0:
			self._maxHeight = self._minHeight

	@property
	def minWidth (self):
		return self._minWidth

	@minWidth.setter
	def minWidth (self, width):
		self._minWidth = width
		if self._minWidth > self._maxWidth and self._maxWidth != 0:
			self._maxWidth = self._minWidth

	@property
	def maxHeight (self):
		return self._maxHeight

	@maxHeight.setter
	def maxHeight (self, height):
		self._maxHeight = height
		if self._maxHeight < self._minHeight and self._minHeight != 0:
			self._minHeight = self._maxHeight

	@property
	def maxWidth (self):
		return self._maxWidth

	@maxWidth.setter
	def maxWidth (self, width):
		self._maxWidth = width
		if self._maxWidth < self._minWidth and self._minWidth != 0:
			self._minWidth = self._maxWidth

	@property
	def inconsistent (self):
		return FcSInconsistency.inconsistent.fget(self)

	def initState (self):
		"""Counts states of all the objects in the GUI tree. Number of active and mandatory keys is stored."""
		for tab in self.entries:
			tab.initState()
			
	def showAll (self, value):
		for tab in self.entries:
			tab.showAll(value)
			
	def notifyParent(self, value):
		self.callWidget(FcSignals.CHANGE_INCONSISTENCY, value)


class FcGTab(FcCGEntry, FcInconsistency):
	"""GUI tab representation class"""
	def __init__ (self, name = "", label = "", description = "", parent = None):
		FcCGEntry.__init__(self, None, parent)
		FcInconsistency.__init__(self, False)
		self.name = name
		self._label = label
		self._empty = None
		self.description = description
		self.icon = "mimetypes/unknown"
		self._content = None

	@property
	def label (self):
		if not self._label:
			return self.name
		return self._label

	@label.setter
	def label (self, label):
		self._label = label
		
	@property
	def content (self):
		return self._content
		
	@content.setter
	def content (self, content):
		if self._content == content: return
		if self._content != None:
			self._content.parent = None
		self._content = content
		self._content.parent = self
		
	def initState (self):
		"""Counts states of all objects in the GUI tree. Number of active and mandatory keys is stored."""
		if self.content != None:
			self.content.initState()
			
	@property
	def empty (self):
		return self._empty
		
	@property
	def inconsistent (self):
		return FcInconsistency.inconsistent.fget(self)

	def changeEmptiness (self, value):
		"""Change emptiness of the tab. If the tab is empty a signal is sent to the client and parent is notified"""
		if value == False and (self._empty == True or self._empty == None):
			self._empty = False
			self.callWidget(FcSignals.CHANGE_EMPTINESS, False)
		elif value == True and (self._empty == False or self._empty == None):
			self._empty = True
			self.callWidget(FcSignals.CHANGE_EMPTINESS, True)
			
	def notifyParent (self, value):
		"""Notify parent about the change of inconsistency"""
		self.callWidget(FcSignals.CHANGE_INCONSISTENCY, value)
		if self.parent != None:
			self.parent.changeInconsistency(value)
			
	def showAll (self, value):
		if self.content != None:
			self.content.showAll(value)
