#ifndef _QT_CKBOOL_H
#define _QT_CKBOOL_H

#include "qtCWidget.h"

class QCheckBox;

class qtCKBool: public qtCWidget
{
  Q_OBJECT
  private:
    //! pointer to Qt widget
    QCheckBox *_check;
    
  public:
    //! constructor
    explicit qtCKBool (fcPackage *package = 0, const qtCWidget *parent = 0);
    //! constructor
    explicit qtCKBool (const fcCGEntry *guiEntry, fcPackage *package = 0, const qtCWidget *parent = 0);

    //! render function
    virtual void render ();
    //! removes any Qt visual widgets
    virtual void invalidateGui ();
    
  public slots:
    //! connects widget with GUI tree
    virtual void attach (fc_bool attached);
  protected slots:
    //! checkbox has changed it's state
    void stateChanged (int state);
};

#endif
