/***************************************************************************
                          fcFiles.h  -  description
                             -------------------
    begin                : 2005/08/18
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcFilesH
#define fcFilesH

#include "fcList.h"
#include "fcString.h"
#include "fcTypes.h"

//! Check for existing files in given directories
/*! This functions goes through all given directories and search for file
    with given file name. If the list with languages is not empty it searches
    also in subdirectories by languages. If the flag write_acces is set
    it the function also checks an acces for writing otherwise just for
    reading. If result file is set the first existing file is set in this string
    and the function ends. If result files list is set all existing files will
    be inserted in.
    The list dirs must be non-empty.
    Function returns false if no file was found.
 */
fc_bool fcCheckFiles( const fcList< fcString >& dirs,
                      const fcList< fcString >* langs, 
                      const fc_char* file_name,
                      fc_bool write_access,
                      fcString* result_file,
                      fcString* result_dir,
                      fcList< fcString >* result_files,
                      fcList< fcString >* result_dirs );

//! At the end of each path in the list dirs appends diven subdir.
void fcAppendSubdir( fcList< fcString >& dirs,
                     const fc_char* subdir );

//! Expands file name
/*! This function replaces character ~ by home directory and
    $ by package directory.
 */
void fcExpandFileName( fcString& file_name,
                       const fcString& home_dir,
                       const fcString& package_dir );
#endif
