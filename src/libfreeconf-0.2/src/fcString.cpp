/***************************************************************************
                          fcString.cpp  -  description
                             -------------------
    begin                : 2004/04/10 16:36
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cstring>
#include <cassert>
#include <iostream>
#include "algorithms.h"
#include "debug.h"
#include "fcString.h"
 
static const unsigned int STRING_PAGE = 256;

//---------------------------------------------------------------------------
fcString :: fcString()
  :string(0),
   capacity(0)
{
//    string = new fc_char[ STRING_PAGE ];
//    string[ 0 ] = 0;
//    length = STRING_PAGE;
}
//---------------------------------------------------------------------------
fcString :: fcString( const fc_char* c, fc_int len /* = 0*/ )
{
   if( ! c )
   {
/*      string = new fc_char[ STRING_PAGE ];
      string[ 0 ] = 0;
      length = STRING_PAGE;*/
      string = 0;
      capacity = 0;
   }
   else
   {
      fc_int length = strlen( c );
      if( len ) length = Min(len, length);
      // alocation of field by multiple of STRING_PAGE(256)
      // length > _length   - this must be
      capacity = STRING_PAGE * ( length / STRING_PAGE + 1 );
      string = new fc_char[ capacity ];
      memcpy( string, c, sizeof( fc_char ) * length );
      string[ length ] = 0;
   }
}
//---------------------------------------------------------------------------
fcString :: fcString( const fcString& str ) : capacity( str. capacity )
{
  if (!capacity)
  {
    string = 0;
    return;
  }
  string = new fc_char[ capacity ];
  fc_uint length = strlen( str.string );
  memcpy( string, str. string, sizeof( fc_char ) * length );
  string[ length ] = 0;
}
//---------------------------------------------------------------------------
fcString :: ~fcString()
{
  DBG_FUNCTION_NAME( "fcString", "~fcString" );
//    assert( string );
  DBG_EXPR( string );
  delete[] string;
}
//---------------------------------------------------------------------------
void fcString :: SetString( const fc_char* c )
{
   DBG_FUNCTION_NAME( "fcString", "SetString" );

   if( ! c )
   {
     if ( capacity )
     {
       string[ 0 ] = 0;
     }
      return;
   }
   fc_uint length = strlen( c );
   //assert( _length );
   //DBG_EXPR( _length );
   //DBG_EXPR( string );

   if( capacity <= length )
       // length must be greather than _length (because of trailing 0)
   {
      delete[] string;
      string = 0;
   }
   if( ! string ) 
   {
      //DBG_COUT( "Reallocating string..." );
      capacity = STRING_PAGE * ( length / STRING_PAGE + 1 );
      string = new fc_char[ capacity ];
   }
   //DBG_EXPR( length );
   memcpy( string, c, sizeof( fc_char ) * ( length ) );
   string[ length ] = 0;
}
//---------------------------------------------------------------------------
fcString& fcString :: operator = ( const fc_char* str )
{
   SetString( str );
   return * this;
}
//---------------------------------------------------------------------------
fcString& fcString :: operator = ( const fcString& str )
{
   SetString( str. Data() );
   return * this;
}
//---------------------------------------------------------------------------
fcString& fcString :: operator += ( const fc_char* str )
{
  if (!str) return *this;
  fc_uint len1 = (string) ? strlen( string ) : 0;
  fc_uint len2 = strlen( str );
  if( len1 + len2 < capacity )
    // (len2 + 1)  => copy str and the ending 0 (+1)
    memcpy( string + len1, str, sizeof( fc_char ) * ( len2 + 1 ) );
  else
  {
    fc_char* tmp_string = string;
    capacity = STRING_PAGE * ( ( len1 + len2 ) / STRING_PAGE + 1 );
    string = new fc_char[ capacity ];
    memcpy( string, tmp_string, sizeof( fc_char ) * len1 );
    memcpy( string + len1, str, sizeof( fc_char ) * ( len2 + 1 ) );
    delete[] tmp_string;
  }
  return * this;
}
//---------------------------------------------------------------------------
fcString& fcString :: operator += ( const fc_char c )
{
  fc_uint length = (string) ? strlen(string) : 0;
  if (length >= capacity)
  {
    fc_char* tmp_string = string;
    capacity += STRING_PAGE;
    string = new fc_char[ capacity ];
    memcpy( string, tmp_string, sizeof( fc_char ) * length );    
    delete[] tmp_string;
  }
  string[length] = c;
  string[length + 1] = 0;
  return * this;
}
//---------------------------------------------------------------------------
fcString& fcString :: operator += ( const fcString& str )
{
   return operator += ( str. Data() );
}
//---------------------------------------------------------------------------
fcString fcString :: operator + ( const fc_char* str )
{
   return fcString( *this ) += str;
}
//---------------------------------------------------------------------------
fcString fcString :: operator + ( const fc_char c )
{
  return fcString( *this ) += c;
}
//---------------------------------------------------------------------------
fcString fcString :: operator + ( const fcString& str )
{
   return fcString( *this ) += str;
}
//---------------------------------------------------------------------------
fc_bool fcString :: operator == ( const fcString& str ) const
{
//    assert( string && str. string );
       // strcmp returns: less than 0 (string is less than str.string)
       //                 equals to 0 (string is equal to str.string)
       //                 greather than 0 (string is greather than str.string)
  if (!string && !str.string) return true;
  if ((str.string && !string) || (!str.string && string)) return false;
  if( strcmp( string, str. string ) == 0 ) return true;
  return false;
}
//---------------------------------------------------------------------------
fc_bool fcString :: operator != ( const fcString& str ) const
{
   return ! operator == ( str );
}
//---------------------------------------------------------------------------
fc_bool fcString :: operator == ( const fc_char* str ) const
{
   //cout << ( void* ) string << " " << ( void* ) str << endl;
//    assert( string && str );
  if (!string && !str) return true;
  if ((str && !string) || (!str && string)) return false;
  if( strcmp( string, str ) == 0 ) return true;
  return false;
}
//---------------------------------------------------------------------------
fcString :: operator bool () const
{
    // this is overloading of bool value
    // i.e. if you use "if(some_instance_of_String_class) 
    //      -- for String this makes sence
    // if string(fc_char *) of this instance isnt empty -> return true
    // otherwise this will return false
   if (!string) return false; 
   if( string[ 0 ] ) return true;
   return false;
}
//---------------------------------------------------------------------------
fc_bool fcString :: operator != ( const fc_char* str ) const
{
   return ! operator == ( str );
}
//---------------------------------------------------------------------------
fc_uint fcString :: Length() const
{
   //return length;
   return strlen( string );
}
//---------------------------------------------------------------------------
fc_bool fcString :: Empty() const
{
  return !string || !string[0];
}
//---------------------------------------------------------------------------
void fcString :: Erase()
{
  capacity = 0;
  delete[] string;
  string = 0;
}
//---------------------------------------------------------------------------
const fc_char* fcString :: Data() const
{
   return string;
}
//---------------------------------------------------------------------------
std::ostream& operator << ( std::ostream& stream, const fcString& str )
{
   stream << str. Data();
   return stream;
}
