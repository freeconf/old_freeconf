#!/usr/bin/python
#
# groupdialog.py
# begin: 15.10.2011 by Jan Ehrenberger
#
# Dialog for editation of group
#

from ui_groupdialog import Ui_editGroupDialog
from PyQt4 import QtCore, QtGui


class editGroupDialog(QtGui.QDialog):
	def __init__(self, parent = None):
		QtGui.QDialog.__init__(self, parent)

		self.ui = Ui_editGroupDialog()
		self.ui.setupUi(self)
	
	def fillFromGroup(self, group):
		self.ui.groupName.setText(group.name)
		self.ui.groupNativeFile.setText(group.nativeFile)
		self.ui.groupTransformFile.setText(group.transformFile)
		if group.outputDefaults:
			self.ui.groupOutputDefaults.setChecked(QtCore.Qt.Checked)
		else:
			self.ui.groupOutputDefaults.setChecked(QtCore.Qt.Unchecked)
	
	def saveToGroup(self, group):
		"""Set values of group from values entered in dialog."""
		group.name = self.ui.groupName.text()
		group.nativeFile = self.ui.groupNativeFile.text()
		group.transformFile = self.ui.groupTransformFile.text()
		if self.ui.groupOutputDefaults.isChecked():
			group.outputDefaults = True
		else:
			group.outputDefaults = False

	def createGroup(self, package):
		"""Create group from dialog entries in given package."""
		group = package.createGroup(self.ui.groupNativeFile.text())
		self.saveToGroup(group)
		return group
		
