/***************************************************************************
                          fcTKString.h  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcTKStringH
#define fcTKStringH

#include "fcTKEntry.h"
#include "fcStringEntry.h"

class fcTKString : public fcTKEntry
{
   //! Dependency value 
   fcString dependency_value;

   //! Possible values
   fcList< fcStringEntry > strings;

   //! It is true if user can insert value which is not in the list
   fc_bool user_values;

   public:
   //! Basic constructor
   fcTKString();

   //! Copy constructor
   fcTKString( const fcTKString& entry );

   //! Destructor
   ~fcTKString();

   //! Entry type getter
   Fc::EntryTypes Type() const;

   //! Allocate new instance and return pointer
   virtual fcBaseEntry* NewEntry() const;

   //! String list size getter
   fc_uint Size() const
      { return strings. Size(); };

   //! A string from the string list getter
   const fcStringEntry& GetString( fc_uint i ) const;

   //! Append new string list
   /*! This methods just appends pointers to fcStringEntry.
    */
   void AppendStrings( fcList< fcStringEntry >& new_strings );

   //! Dependency_value setter
   void SetDependencyValue( const fc_char* val );

   //! Dependency_value getter
   const fc_char* GetDependencyValue() const;


#ifdef DEBUG
   //! Print this keyword
   virtual void Print( ostream& stream ) const;
#endif

};

#endif
