#ifndef _FCGWINDOW_H_
#define _FCGWINDOW_H_

#include <vector>
#include "fcTypes.h"
#include "fcString.h"

class fcGTab;

class fcGWindow
{
  private:
    fcString _title;
    fc_uint _minWidth;
    fc_uint _minHeight;
    fc_uint _maxWidth;
    fc_uint _maxHeight;
    
    std::vector<fcGTab*> _tabs;
  public:
    
    fcGWindow ();
    
    ~fcGWindow ();

    void setTitle (const fcString &title);
    const fcString& title () const;

    void setMinWidth (fc_uint minWidth);
    fc_uint minWidth () const;

    void setMinHeight (fc_uint minHeight);
    fc_uint minHeight () const;

    void setMaxWidth (fc_uint maxWidth);
    fc_uint maxWidth () const;

    void setMaxHeight (fc_uint maxHeight);
    fc_uint maxHeight () const;

    void addTab (fcGTab *tab);
    fcGTab* lastTab () const;

    fcGTab* at (fc_uint index) const;

    fcGTab* findTab (const fcString &tabName) const;
    
    int tabsCount () const;
    
    fc_bool isEmpty () const;

};

#endif
