#include "fcGTab.h"
#include "fcCGSEntry.h"

fcGTab::fcGTab ()
  :_icon(Fc::NoIcon),
   _content(0)
{
  
}


fcGTab::fcGTab (const fcString &name, const fcString &label, const fcString &descr)
  :_icon(Fc::NoIcon),
   _name(name),
   _label (label),
   _descr(descr),
   _content(0)
{
  
}
    

fcGTab::~fcGTab ()
{
  delete _content;
}


void fcGTab::setName (const fcString &name)
{
  _name = name;
  // implicit fallback
  if (!_label) _label = name;
  if (!_descr) _descr = name;
}


const fcString& fcGTab::name () const
{
  return _name;
}

void fcGTab::setIcon (Fc::Icon icon)
{
  _icon = icon;
}


Fc::Icon fcGTab::icon () const
{
  return _icon;
}


void fcGTab::setLabel (const fcString &label)
{
  _label = label;
}


const fcString& fcGTab::label () const
{
  return _label;
}


void fcGTab::setDescription (const fcString &descr)
{
  _descr = descr;
}


const fcString& fcGTab::description () const
{
  return _descr;
}


void fcGTab::setContent (fcCGSEntry *content)
{
  _content = content;
}


fcCGSEntry* fcGTab::content () const
{
  return _content;
}

