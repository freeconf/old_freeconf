/***************************************************************************
                          fcCKEntry.h  -  description
                             -------------------
    begin                : 2004/08/13
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCCKENTRY_H__
#define __FCCKENTRY_H__

#include "fcCEntry.h"

class fcCKEntry : public fcCEntry
{
   protected:
   
   //! Was-some-value-already-set flag
   fc_bool value_set;

   public:

   //! Basic constructor
   fcCKEntry();
   
   //! Copy constructor
   fcCKEntry( const fcCKEntry& entry );

   //! Constructor template entry pointer
   fcCKEntry( const fcTEntry* t_entry )
      : fcCEntry( t_entry ){};

   //! Destructor
   ~fcCKEntry();
   
   //! Say this is not a section
   fc_bool IsSection() const;

   //! Return true if it is a kewyord
   fc_bool IsKeyword() const;

   //! Return true if it is a template entry
   fc_bool IsTEntry() const;

   //! Return true if it is a config entry
   fc_bool IsCEntry() const;
 
   //! Value set getter
   fc_bool ValueSet();
   
   //! Update related DOM node
   /*! @param parent_node, doc for creating new DOM elements
    */
   void UpdateDOMNode( DOMNode* parent_node,
                       DOMDocument* doc );

#ifdef DEBUG
   //! Print this entry
   void Print( ostream& stream ) const;
#endif
};

#endif
