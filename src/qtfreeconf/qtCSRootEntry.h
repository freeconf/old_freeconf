#ifndef _QT_CSROOTENTRY_H
#define _QT_CSROOTENTRY_H

#include "qtCWidget.h"

class QBoxLayout;
class fcGTab;
class qtCSEntry;

class qtCSRootEntry: public qtCWidget
{
  Q_OBJECT
  private:
    //! layout which contains everything
    QBoxLayout *_rootLayout;
    //! pointer to root section
    qtCSEntry *_rootSection;
    //! pointer to active tab
    const fcGTab *_tab;
    
  public:
    //! constructor
    qtCSRootEntry (fcPackage *package, fcGTab *tab, QBoxLayout *rootLayout);

    //! destructor
    virtual ~qtCSRootEntry ();

    //! render function
    virtual void render ();

    //! root layout setter
    void setRootLayout (QBoxLayout *rootLayout);
    //! root layout getter
    QBoxLayout* rootLayout () const;

    //! root section setter
    void setRootSection (qtCSEntry *rootSection);
    //! root section getter
    qtCSEntry* rootSection () const;

    //! tab getter
    const fcGTab* tab () const;

    //! removes any Qt visual widgets
    virtual void invalidateGui ();
    
    
  public slots:
    //! connects widget with GUI tree
    virtual void attach (fc_bool attached);
    
};

#endif
