/***************************************************************************
                          fcList.h  -  description
                             -------------------
    begin                : Sat, 10 Apr 2004 15:58:51 +0100
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/**
  *@author Tomas Oberhuber
  */

#ifndef fcListH
#define fcListH

#include <cassert>
#include <iostream>
#include <ostream>
#include <cstdlib>
#include "fcDataElement.h"
#include "fcTypes.h"

//! Template for double linked lists
/*! To acces elements in the list one can use method Size() and
    operator[](). To add elements there are methods Append(), 
    Prepend() and Insert() to insert an element at given
    position. To erase particular element there is merthod
    Erase() taking the element position. To erase all elements
    there is method EraseAll. There are also alternatives DeepErase()
    and DeepEraseAll() to free dynamicaly allocated data inside the
    data elements.
    The list stores pointer to last accesed element so if one goes
    seqeuntialy through the list there is no inefficiency. The
    accesing algorithm is also able to decide whether to start from
    the last accesed position or from the begining resp. from the end
    of the list. So with common use one does not need to worry about
    efficiency :-)
 */
template< class T > class fcList
{
   //! Pointer to the first element
   fcDataElement< T >* first;

   //! Pointer to the last element
   /*! We use pointer to last element while adding new element to keep order of elements
    */
   fcDataElement< T >* last;
   
   //! List size
   fc_int size;

   //! Iterator
   mutable fcDataElement< T >* iterator;

   //! Iterator index
   mutable fc_int index;

   public:
   //! Basic constructor
   fcList() 
      : first( 0 ),
        last( 0 ),
        size( 0 ),
        iterator( 0 ),
        index( 0 )
    {};

   //! Copy constructor
   fcList( const fcList& lst )
      : first( 0 ),
        last( 0 ),
        size( 0 ),
        iterator( 0 ),
        index( 0 )
   {
      for(fc_int i = 0; i < lst.Size(); i++)
         Append( lst[i] );
   };

   //! Destructor
   ~fcList() { EraseAll(); };

   //! If the list is empty return 'true'
   fc_bool IsEmpty() const { return ! size; };
   
   //! Return size of the list
   fc_int Size() const { return size; };

   //! Assigning operator
   const fcList& operator=(const fcList& lst)
   {
       EraseAll();
       for(fc_int i = 0; i < lst.Size(); i++)
           Append( lst[i] );
   }

   //! Indexing operator
   T& operator[] ( fc_int ind )
   {
      assert( ind < size );
      // find fastest way to element with index i
      // we can use iterator as it is set now or
      // we can start from the first or from the last
      // element
      //cout << "List operator[]: size = " << size
      //     << " current index = " << index
      //     << " index = " << ind << endl;
      fc_int iter_dist = abs( index - ind );
      if( ! iterator ||
          iter_dist > ind || 
          iter_dist > size - ind )
      {
         // it is better to start from the first one or the last one
         if( ind < size - ind )
         {
            // start from the first one
            //cout << "Setting curent index to 0." << endl;
            index = 0;
            iterator = first;
         }
         else
         {
            //cout << "Setting curent index to size - 1." << endl;
            index = size - 1;
            iterator = last;
         }
      }
      while( index != ind )
      {
         //cout << " current index = " << index
         //     << " index = " << ind << endl;
         if( ind < index ) 
         {
            iterator = iterator -> Previous();
            index --;
         }
         else
         {
            iterator = iterator -> Next();
            index ++;
         }
         assert( iterator );
      }
      return iterator -> Data();
   };
   
   const T& operator[] ( fc_int ind ) const
   {
     fc_int iter_dist = abs( index - ind );
     if( ! iterator ||
       iter_dist > ind ||
       iter_dist > size - ind )
     {
       // it is better to start from the first one or the last one
       if( ind < size - ind )
       {
         // start from the first one
         //cout << "Setting curent index to 0." << endl;
         index = 0;
         iterator = first;
       }
       else
       {
         //cout << "Setting curent index to size - 1." << endl;
         index = size - 1;
         iterator = last;
       }
     }
     while( index != ind )
     {
       //cout << " current index = " << index
       //     << " index = " << ind << endl;
       if( ind < index )
       {
         iterator = iterator -> Previous();
         index --;
       }
       else
       {
         iterator = iterator -> Next();
         index ++;
       }
       assert( iterator );
     }
     return iterator -> Data();
   }

   T& at (fc_int ind)
   {
     return (*this)[ind];
   }

   const T& at (fc_int ind) const
   {
     return (*this)[ind];
   }

   //! Append new data element
   void Append( const T& data )
   {
      if( ! first )
      {
         assert( ! last );
         first = last = new fcDataElement< T >( data );
      }
      else last = last -> Next() = new fcDataElement< T >( data, last, 0 ); 
      size ++;
   };

   //! Prepend new data element
   void Prepend( const T& data )
   {
      if( ! first )
      {
         assert( ! last );
         first = last = new fcDataElement< T >( data );
      }
      else first = first -> Previous() = new fcDataElement< T >( data, 0, first ); 
      size ++;
      index ++;
   };

   //! Insert new data element at given position
   void Insert( const T& data, fc_int ind )
   {
      assert( ind <= size);   
      if( ind == 0 ) return Prepend( data );
      if( ind == size ) return Append( data );
      operator[]( ind );
      fcDataElement< T >* new_el = 
         new fcDataElement< T >( data,
                                 iterator -> Previous(),
                                 iterator );
      iterator -> Previous() -> Next() = new_el;
      iterator -> Previous() = new_el;
      iterator = new_el;
      size ++;
   };

   //! Append copy of another list
   void AppendList( fcList& lst )
   {
      for( fc_int i = 0; i < lst. Size(); i ++ )
         Append( lst[ i ] );
   };
   
   //! Prepend copy of another list
   void PrependList( fcList< T >& lst )
   {
      for( fc_int i = lst. Size(); i > 0; i -- )
         Prepend( lst[ i - 1 ] );
   };

   //! Erase data element at given position
   void Erase( fc_int ind )
   {
      operator[]( ind );
      fcDataElement< T >* tmp_it = iterator;
      if( iterator -> Next() )
         iterator -> Next() -> Previous() = iterator -> Previous();
      if( iterator -> Previous() )
        iterator -> Previous() -> Next() = iterator -> Next();
      if( iterator -> Next() ) iterator = iterator -> Next();
      else
      {
         iterator = iterator -> Previous();
         index --;
      }
      if( first == tmp_it ) first = iterator;
      if( last == tmp_it ) last = iterator;
      delete tmp_it;
      size --;
   };

   //! Erase data element with contained data at given position
   void DeepErase( fc_int ind )
   {
      operator[]( ind );
      delete iterator -> Data();
      Erase( ind );
   };

   //! Erase all data elements
   void EraseAll()
   {
      iterator = first;
      fcDataElement< T >* tmp_it;
      while( iterator )
      {
         tmp_it = iterator;
         iterator = iterator -> Next();
         delete tmp_it;
      }
      first = last = 0;
      size = 0;
   };

   //! Erase all data elements with contained data
   void DeepEraseAll()
   {
      iterator = first;
      fcDataElement< T >* tmp_it;
      while( iterator )
      {
         tmp_it = iterator;
         iterator = iterator -> Next();
         delete tmp_it -> Data();
         delete tmp_it;
      }
      first = last = 0;
      size = 0;
   };
};

template< class T > std::ostream& operator << ( std::ostream& stream, fcList< T >& list )
{
   fc_int size = list. Size();
   for( fc_int i = 0; i < size; i ++ )
      stream << list[ i ] << std::endl;
   return stream;
};

#endif
