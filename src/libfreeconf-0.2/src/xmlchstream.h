/***************************************************************************
                          xmlchstream.h  -  description
                             -------------------
    begin                : Sat, 10 Apr 2004 13:41:29 +0100
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : 
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef xmlchstreamH
#define xmlchstreamH

#include <ostream>
#include <xercesc/util/XMLString.hpp>

XERCES_CPP_NAMESPACE_USE

// ---------------------------------------------------------------------------
//  This is a simple class that lets us do easy (though not terribly efficient)
//  trancoding of XMLCh data to local code page for display.
// ---------------------------------------------------------------------------
class StrX
{
public :
    // -----------------------------------------------------------------------
    //  Constructors and Destructor
    // -----------------------------------------------------------------------
    StrX(const XMLCh* const toTranscode)
    {
        // Call the private transcoding method
        fLocalForm = XMLString::transcode(toTranscode);
    }

    ~StrX()
    {
        XMLString::release(&fLocalForm);
    }


    // -----------------------------------------------------------------------
    //  Getter methods
    // -----------------------------------------------------------------------
    const char* localForm() const    // this function is constant => 
    {                                // it doesn't change the components 
        return fLocalForm;
    }

private :
    // -----------------------------------------------------------------------
    //  Private data members
    //
    //  fLocalForm
    //      This is the local code page form of the string.
    // -----------------------------------------------------------------------
    char*   fLocalForm;
};

inline XERCES_STD_QUALIFIER ostream& operator<<(XERCES_STD_QUALIFIER ostream& target, const StrX& toDump)
{
    target << toDump.localForm();
    return target;
}


//  Stream out XMLCh . Doing this requires that we first transcode
//  to char * form in the default code page for the system
inline std::ostream& operator<< ( std::ostream& target, const XMLCh* const s )
{
    char *ch = ( char* ) XMLString :: transcode( s );
    target << *ch;
    XMLString :: release( &ch );
    return target;
}
#endif

