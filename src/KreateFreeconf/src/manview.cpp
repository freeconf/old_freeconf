/***************************************************************************
 *   Copyright (C) 2009 by Jan Ehrenberger   *
 *   lankvil@seznam.cz   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "manview.h"
#include "manviewtab.h"

ManView::ManView(QWidget *parent, const char *name)
 : KTabWidget(parent, name)
{
}


ManView::~ManView()
{
}


void ManView::newTab(const QString &url)
{
    // Create new tab and show it!
    ManViewTab *tab = new ManViewTab(url, this);
    connect(tab, SIGNAL(selectionChanged(KHTMLPart *, const DOM::Range &)),
            this, SLOT(slotSelectionChanged(KHTMLPart*, const DOM::Range &)));

    addTab(tab, "");
    showPage(tab);
}

void ManView::setCurrentURL(const QString &url)
{
    ManViewTab *tab = currentTab();
    if(tab)
        tab->openLocation(url);
    else
        // If there is no tab yet, create new one
        newTab(url);
}

ManViewTab *ManView::currentTab()
{
    return (ManViewTab *)currentPage();
}

void ManView::slotSelectionChanged(KHTMLPart *part, const DOM::Range &range)
{
    // Resend the signal
    emit selectionChanged(part, range);
}



#include "manview.moc"

