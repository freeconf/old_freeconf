/***************************************************************************
                          fcCEntry.h  -  description
                             -------------------
    begin                : 2005/01/02
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcCEntryH
#define fcCEntryH

#include "fcBaseEntry.h"
#include "fcList.h"

class fcTEntry;
class fcCGEntry;
class fcCSEntry;
class fcCDependency;
class fcDActionBaseListItem;

//! This is a basic class for all config entries 
class fcCEntry : public fcBaseEntry
{
   protected:

   //! Pointer to related template entry
   const fcTEntry* template_entry;

   //! Related DOMNode
   fcDOMNode* dom_node;

   //! Related Gui element

   fcCGEntry *gui_buddy;

   //! List of pointers to dependent entries
   /*! List of pointers to entries, that are dependent
    *  on this entry.
    */
   fcList< fcCEntry* > *dependents;

   //! Dependency for the entry
   /*! This is the list of dependency entries for this entry
    */
   fcCDependency* m_pCDep;

   public:

   //! Basic constructor
   fcCEntry();

   //! Constructor with parameter
   fcCEntry( const fcTEntry* tentr );

   //! Destructor
   virtual ~fcCEntry();  

   //! Returns true if it is a template entry
   fc_bool IsCEntry() const { return true; };

   //! Returns true if it is a template entry
   fc_bool IsTEntry() const { return false; };

   //! Template entry getter
   const fcTEntry* GetTEntry() const;

   //! DOM Node getter
   fcDOMNode* GetDOMNode() const;

   //! DOM Node setter
   void SetDOMNode( fcDOMNode* node );
   
   //! Gui element getter
   fcCGEntry* GetGuiBuddy() const;
   
   //! Gui element setter
   void SetGuiBuddy( fcCGEntry* buddy );
   
   //! Update related DOM node
   /*! @param parent_node, doc for creating new DOM elements
    */
   virtual void UpdateDOMNode( DOMNode* parent_node,
                               DOMDocument* doc ) = 0;

   //! dependency pointers setter
   void AddDependents( fcCEntry* c_entry );

   //! dependency pointer getter
   fcCEntry* GetDependents( fc_int pos );

   //! dependency pointer size getter
   fc_int GetDependentsSize();

   //! Returns action corresponding with a dependency condition result
   /*! This function return pointer to an action, whose value 
    *  matches to a dependency result value
    */
   fcDActionBaseListItem* GetCEntryAction(fcCSEntry* pMainCSection);

   //! Set the pointers of dependent entries to this entry
   /*! This function set to the current entry list of pointers
    *  on the dependent entries
    */
   void InitDependents(fcCSEntry* pMainCSection);

   //! Print
   void Print();

   //! Print the fcCDependency class stored int m_pCDep pointer
   void PrintDependency() const;
};

#endif
