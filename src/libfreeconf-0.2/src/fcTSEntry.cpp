/***************************************************************************
                          fcTSEntry.cpp  -  description
                             -------------------
    begin                : 2004/04/10 16:58
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include "debug.h"
#include "fcTSEntry.h"
#include "fcBaseEntry.h"
#include "fcCSEntry.h"
#include "fcCKEntry.h"

//---------------------------------------------------------------------------
fcTSEntry :: fcTSEntry()
{
}
//--------------------------------------------------------------------------
fcTSEntry :: fcTSEntry( const fcTSEntry& section_entry )
   : fcSectionEntry( section_entry ),
     fcTEntry( section_entry ) 
{
}
//---------------------------------------------------------------------------
fcTSEntry :: ~fcTSEntry()
{
}
//--------------------------------------------------------------------------
fcSectionEntry* fcTSEntry :: GetSectionEntryPointer()
{
   return ( fcSectionEntry* ) this;
}
//--------------------------------------------------------------------------
fcBaseEntry* fcTSEntry :: NewEntry() const
{
   fcTSEntry* entry = new fcTSEntry( *this );
   if( ! entry )
   {
      std::cerr << "Can not aloocate new fcTSEntry structure. Possibly out of memory." << std::endl;
   }
   return entry;
}
//--------------------------------------------------------------------------
Fc::EntryTypes fcTSEntry :: Type() const
{
  return Fc::Section;
}
//--------------------------------------------------------------------------
fcTEntry* fcTSEntry :: operator[] ( fc_uint pos )
{
   DBG_FUNCTION_NAME( "fcTSEntry", "operator[]" );  
   fcBaseEntry* entry = fcSectionEntry :: operator[]( pos );
   assert( entry -> IsTEntry() );
   return ( fcTEntry* ) entry;
}
//--------------------------------------------------------------------------
fcTEntry* fcTSEntry :: FindTEntry( const fc_char* name,
                                   fc_uint& pos )
{
   DBG_FUNCTION_NAME( "fcTSEntry", "FindEntry" );  
   fcBaseEntry* entry = fcSectionEntry :: FindEntry( name, pos );
   if( ! entry ) return NULL;
   assert( entry -> IsTEntry() );
   return ( fcTEntry* ) entry;
}
//--------------------------------------------------------------------------
fcTEntry* fcTSEntry :: FindTEntry( const fc_char* name )
{
   DBG_FUNCTION_NAME( "fcTSEntry", "FindEntry" );  
   DBG_EXPR( name );
   DBG_EXPR( Size() );
   fcBaseEntry* entry = fcSectionEntry :: FindEntry( name );
   if( ! entry ) return NULL;
   assert( entry -> IsTEntry() );
   return ( fcTEntry* ) entry;
}
//--------------------------------------------------------------------------
const fcTEntry* fcTSEntry :: operator[] ( fc_uint pos ) const
{
   return const_cast< fcTSEntry* >( this ) -> operator[]( pos );
}
//--------------------------------------------------------------------------
const fcTEntry* fcTSEntry :: FindTEntry( const fc_char* name,
                                         fc_uint& pos ) const
{
   return const_cast< fcTSEntry* >( this ) -> 
      FindTEntry( name, pos );
}
//--------------------------------------------------------------------------
const fcTEntry* fcTSEntry :: FindTEntry( const fc_char* name ) const
{
   return const_cast< fcTSEntry* >( this ) -> FindTEntry( name );
}
//---------------------------------------------------------------------------
#ifdef DEBUG
void fcTSEntry :: Print( ostream& stream ) const
{
   stream << "TEMPLATE SECTION: ";
   fcTEntry :: Print( stream );
   fcSectionEntry :: Print( stream );
}
#endif
