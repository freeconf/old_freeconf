#ifndef _FCGTAB_H_
#define _FCGTAB_H_

#include "fcTypes.h"
#include "fcString.h"

class fcCGSEntry;

class fcGTab
{
  private:
    Fc::Icon _icon;
    fcString _name;
    fcString _label;
    fcString _descr;

    fcCGSEntry *_content;
    
  public:

    fcGTab ();
    fcGTab (const fcString &name, const fcString &label, const fcString &descr);

    ~fcGTab ();

    void setIcon (Fc::Icon icon);
    Fc::Icon icon () const;

    void setName (const fcString &name);
    const fcString& name () const;
    
    void setLabel (const fcString &label);
    const fcString& label () const;
    
    void setDescription (const fcString &descr);
    const fcString& description () const;

    void setContent (fcCGSEntry *content);
    fcCGSEntry* content () const;
    
};

#endif
