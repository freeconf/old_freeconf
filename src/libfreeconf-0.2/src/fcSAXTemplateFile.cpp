/***************************************************************************
                          fcSAXTemplateFile.cpp  -  description
                             -------------------
    begin                : 2004/04/11 13:56
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include "debug.h"
#include "fcMessageHandler.h"
#include "fcSAXTemplateFile.h"
#include "fcDependencyActions.h"
#include "fcTKEntry.h"
#include "fcTSEntry.h"
#include "fcTKBool.h"
#include "fcTKList.h"
#include "fcTKNumber.h"
#include "fcTKString.h"
#include "fcTypes.h"

//---------------------------------------------------------------------------
fcSAXTemplateFile :: fcSAXTemplateFile()
   : current_entry(0),
     string_lists(0),
     enclosing_tag(false),
     dependency_element(false),
     action_element(false),
     value_element(false),
     current_action(0)
{
     template_enum         = fcNO_TMP_ELEMENT;
     property_enum         = fcNO_PROP_ELEMENT;
     dependencies_enum     = fcNO_DEPS_ELEMENT;
     dependency_enum       = fcNO_DEP_ELEMENT;
     action_enum           = fcNO_ACTION_ELEMENT;
     value_enum            = fcNO_VALUE_ELEMENT;
}
//---------------------------------------------------------------------------
fcSAXTemplateFile :: ~fcSAXTemplateFile()
{
}
//---------------------------------------------------------------------------
void fcSAXTemplateFile :: startElement( const XMLCh* const uri,
                                        const XMLCh* const localname,
                                        const XMLCh* const qname,
                                        const Attributes& attributes)
{
   DBG_FUNCTION_NAME( "fcSAXStringListFile", "startElement" );

   const fc_char* const element_name = XMLString :: transcode( localname );
   DBG_EXPR( name );

   //std::cout << "\nSTART_ELEMENT: " << element_name << std::endl;

   if( ! enclosing_tag &&
         strcmp( element_name, "freeconf-template" ) == 0 )
   {
      DBG_COUT( "Freeconf-template tag." );
      enclosing_tag = true;
   }

   if( ! enclosing_tag )
   {
      MessageHandler. Error( "fcSAXTemplateFile.cpp", 70, 
              "You must enclose the Template File with <freeconf-template> and </freeconf-template>." );
      return;
   }

   if( strcmp( element_name, "section" ) == 0 ||
       strcmp( element_name, "freeconf-template" ) == 0 )
   {
      fcTSEntry* new_section = new fcTSEntry();
      section_stack. Top() -> Append( new_section );

      DBG_COUT( "Adding new section " << new_section << " to " << section_stack. Top() );
      
      section_stack. Add( new_section );
      current_entry = new_section;
      //std::cout << "...current_entry (startElement - section ): " << current_entry << std::endl;
      return;
   }

   if( strcmp( element_name, "bool" ) == 0 )
   {
      current_entry = new fcTKBool;
      //std::cout << "...current_entry (startElement - bool ): " << current_entry << std::endl;
      section_stack. Top() -> Append( current_entry );
      DBG_COUT( "Adding bool." );
      return;
   }

   if( strcmp( element_name, "number" ) == 0 )
   {
      current_entry = new fcTKNumber;
      //std::cout << "...current_entry (startElement - number ): " << current_entry << std::endl;
      section_stack. Top() -> Append( current_entry );
      DBG_COUT( "Adding number." );
      return;
   }

   if( strcmp( element_name, "string" ) == 0 )
   {
      current_entry = new fcTKString;
      //std::cout << "...current_entry (startElement - string ): " << current_entry << std::endl;
      section_stack. Top() -> Append( current_entry );
      DBG_COUT( "Adding string." );
      return;
   }

   // at this moment, current_entry must point to an existing entry
   assert( current_entry ); 

   if( strcmp( element_name, "section-name" ) == 0 )
   {
       if( template_enum == fcNO_TMP_ELEMENT )
       {
           template_enum = fcSECTION_NAME;
           //std::cout << ".. fcSECTION_NAME" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"section-name\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "entry-name" ) == 0 ) 
   {
       if( template_enum == fcNO_TMP_ELEMENT ) 
       {
           template_enum = fcKEYWORD_NAME;
           //std::cout << "... fcKEYWORD_NAME" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION && 
           dependency_element              && dependency_enum == fcNO_DEP_ELEMENT ) 
       {
           dependency_enum = fcDEPENDENCY_NAME;
           //std::cout << "... fcDEPENDENCY_NAME" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"entry-name\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "multiple" ) == 0 )
   {
       if( template_enum == fcNO_TMP_ELEMENT ) 
       {
           template_enum = fcMULTIPLE;
           //std::cout << "... fcMULTIPLE" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"multiple\" ) == 0 )"),
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "properties" ) == 0 )
   {
       if( template_enum == fcNO_TMP_ELEMENT )
       {
           template_enum = fcPROPERTIES;
           //std::cout << "... fcPROPERTIES" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&
           action_element                  && action_enum == fcNO_ACTION_ELEMENT )
       {
           action_enum = fcACTION_PROPERTIES;
           //std::cout << "... fcACTION_PROPERTIES" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"properties\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "type" ) == 0 ) 
   {
       if( template_enum == fcPROPERTIES && property_enum == fcNO_PROP_ELEMENT ) 
       {
           property_enum = fcTYPE;
           //std::cout << "... fcTYPE" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES    && dependencies_enum == fcACTIONS && action_element &&
           action_enum == fcACTION_PROPERTIES && property_enum == fcNO_PROP_ELEMENT )
       {
           property_enum = fcTYPE;
           //std::cout << "... fcTYPE" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
               fcString("if( strcmp( element_name, \"type\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "min" ) == 0 ) 
   {
       if( template_enum == fcPROPERTIES && property_enum == fcNO_PROP_ELEMENT ) 
       {
           property_enum = fcMIN;
           //std::cout << "... fcMIN" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES    && dependencies_enum == fcACTIONS && action_element &&
           action_enum == fcACTION_PROPERTIES && property_enum == fcNO_PROP_ELEMENT )
       {
           property_enum = fcMIN;
           //std::cout << "... fcMIN" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"min\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "max" ) == 0 ) 
   {
       if( template_enum == fcPROPERTIES && property_enum == fcNO_PROP_ELEMENT ) 
       {
           property_enum = fcMAX;
           //std::cout << "... fcMAX" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES    && dependencies_enum == fcACTIONS && action_element &&
           action_enum == fcACTION_PROPERTIES && property_enum == fcNO_PROP_ELEMENT )
       {
           property_enum = fcMAX;
           //std::cout << "... fcMAX" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"max\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "step" ) == 0 ) 
   {
       if( template_enum == fcPROPERTIES && property_enum == fcNO_PROP_ELEMENT ) 
       {
           property_enum = fcSTEP;
           //std::cout << "... fcSTEP" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES    && dependencies_enum == fcACTIONS && action_element &&
           action_enum == fcACTION_PROPERTIES && property_enum == fcNO_PROP_ELEMENT )
       {
           property_enum = fcSTEP;
           //std::cout << "... fcSTEP" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"step\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "data" ) == 0 )
   {
       if( template_enum == fcPROPERTIES && property_enum == fcNO_PROP_ELEMENT ) 
       {
           property_enum = fcDATA;
           //std::cout << "... fcDATA" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES    && dependencies_enum == fcACTIONS && action_element &&
           action_enum == fcACTION_PROPERTIES && property_enum == fcNO_PROP_ELEMENT )
       {
           property_enum = fcDATA;
           //std::cout << "... fcDATA" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"data\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "dependencies" ) == 0 )
   {
       if( template_enum == fcNO_TMP_ELEMENT ) 
       {
           template_enum = fcDEPENDENCIES;
           //std::cout << "... fcDEPENDENCIES" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"dependencies\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "condition" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcNO_DEPS_ELEMENT )
       {
           dependencies_enum = fcCONDITION;
           //std::cout << "... fcCONDITION" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"condition\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "actions" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcNO_DEPS_ELEMENT )
       {
           dependencies_enum = fcACTIONS;
           //std::cout << "... fcACTIONS" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"actions\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "relation-and" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION && !dependency_element ) 
       {
           struRPNRead rpn(fcRELATION_AND, false);
           relations_stack.Add(rpn);

           //std::cout << "... relation_and" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"relation-and\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "relation-or" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION && !dependency_element ) 
       {
           struRPNRead rpn(fcRELATION_OR, false);
           relations_stack.Add(rpn);

           //std::cout << "... relation_or" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"relation-or\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "relation-not" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION && !dependency_element ) 
       {
           struRPNRead rpn(fcRELATION_NOT, true);
           relations_stack.Add(rpn);

           //std::cout << "... relation_not" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"relation-not\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "dependency" ) == 0 ) 
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION && !dependency_element ) 
       {
           dependency_element = true;
           //std::cout << "... fcDEPENDENCY" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"dependency\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "values" ) == 0 ) 
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION && 
           dependency_element              && dependency_enum == fcNO_DEP_ELEMENT ) 
       {
           dependency_enum = fcVALUES;
           //std::cout << "... fcVALUES" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"values\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "value" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
           dependency_element              && dependency_enum == fcVALUES      && !value_element )
       {
           value_element = true;
           //std::cout << " ... value_element" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&
           action_element                  && action_enum == fcNO_ACTION_ELEMENT )
       {
           action_enum = fcVALUE;
           //std::cout << "... action::fcVALUE" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),  
               fcString("if( strcmp( element_name, \"value\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "equal" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES  && dependencies_enum == fcCONDITION &&
           dependency_element               && dependency_enum == fcVALUES      && 
           value_element                    && value_enum == fcNO_VALUE_ELEMENT ) 
       {
           value_enum = fcEQUAL;
           //std::cout << "... fcEQUAL" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"equal\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }
    
   if( strcmp( element_name, "not-equal" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES  && dependencies_enum == fcCONDITION &&
           dependency_element               && dependency_enum == fcVALUES      && 
           value_element                    && value_enum == fcNO_VALUE_ELEMENT ) 
       {
           value_enum = fcNOT_EQUAL;
           //std::cout << "... fcNOT_EQUAL" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"not-equal\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "greater" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES  && dependencies_enum == fcCONDITION &&
           dependency_element               && dependency_enum == fcVALUES      && 
           value_element                    && value_enum == fcNO_VALUE_ELEMENT ) 
       {
           value_enum = fcGREATER;
           //std::cout << "... fcGREATER" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"greater\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "greater-or-equal" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES  && dependencies_enum == fcCONDITION &&
           dependency_element               && dependency_enum == fcVALUES      && 
           value_element                    && value_enum == fcNO_VALUE_ELEMENT ) 
       {
           value_enum = fcGREATER_OR_EQUAL;
           //std::cout << "... fcGREATER_OR_EQUAL" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"greater-or-equal\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "lower" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES  && dependencies_enum == fcCONDITION &&
           dependency_element               && dependency_enum == fcVALUES      && 
           value_element                    && value_enum == fcNO_VALUE_ELEMENT ) 
       {
           value_enum = fcLOWER;
           //std::cout << "... fcLOWER" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"lower\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "lower-or-equal" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES  && dependencies_enum == fcCONDITION &&
           dependency_element               && dependency_enum == fcVALUES      && 
           value_element                    && value_enum == fcNO_VALUE_ELEMENT ) 
       {
           value_enum = fcLOWER_OR_EQUAL;
           //std::cout << "... fcLOWER_OR_EQUAL" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"lower-or-equal\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "in" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES  && dependencies_enum == fcCONDITION &&
           dependency_element               && dependency_enum == fcVALUES      && 
           value_element                    && value_enum == fcNO_VALUE_ELEMENT ) 
       {
           value_enum = fcIN;
           //std::cout << "... fcIN" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
               fcString("if( strcmp( element_name, \"in\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "not-in" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES  && dependencies_enum == fcCONDITION &&
           dependency_element               && dependency_enum == fcVALUES      && 
           value_element                    && value_enum == fcNO_VALUE_ELEMENT ) 
       {
           value_enum = fcNOT_IN;
           //std::cout << "... fcNOT_IN" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
               fcString("if( strcmp( element_name, \"not-in\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "action" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&
           !action_element )
       {
           action_element = true;

            switch(current_entry->Type())
            {
                case Fc::Bool:
                    current_action = new fcDActionBoolListItem;
                    break;
                case Fc::Number:
                    current_action = new fcDActionNumberListItem;
                    break;
                case Fc::String:
                    current_action = new fcDActionStringListItem;
                    break;
                default:
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
                            fcString("if( strcmp( element_name, \"action\" ) == 0 )"), 
                            fcString("Wrong type of current entry!"));
                    break;
            }

           //std::cout << "... fcACTION" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
               fcString("if( strcmp( element_name, \"action\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "default-value" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&
           action_element                  && action_enum == fcNO_ACTION_ELEMENT )
       {
           action_enum = fcDEFAULT_VALUE;
           //std::cout << "... fcDEFAULT_VALUE" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"default-value\" ) == 0 )"), 
               fcString("Wrong location of start element: ") + fcString(element_name) );
       return;
   }

   MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
           fcString("end of startElement function"), 
           fcString("startElement: Unknown entry type ") + fcString(element_name) );
   return;
}
//--------------------------------------------------------------------------
void fcSAXTemplateFile :: endElement( const XMLCh* const uri, 
                                      const XMLCh* const localname, 
                                      const XMLCh* const qname)
{
   DBG_FUNCTION_NAME( "fcSAXStringListFile", "endElement" );

   const fc_char* const element_name = XMLString :: transcode( localname );  
   DBG_COUT( name );

   //std::cout << "\nEND_ELEMENT: " << element_name << std::endl;

   if( strcmp( element_name, "section" ) == 0 ||
       strcmp( element_name, "freeconf-template" ) == 0 )
   {
       section_stack.Remove();
       //std::cout << "...current_entry (endElement - section ): " << current_entry << std::endl;
       current_entry = 0;
       control();
       return;
   }

   if( strcmp( element_name, "bool" )   == 0 ||
       strcmp( element_name, "number" ) == 0 ||
       strcmp( element_name, "string" ) == 0 )
   {
       assert( current_entry );

       //std::cout << "...current_entry (endElement - bool || number || string ): " << current_entry << std::endl;
       current_entry = 0;
       control();
       return;
   }

   if( strcmp( element_name, "section-name" ) == 0 )
   {
       if( template_enum == fcSECTION_NAME )
       {
           template_enum = fcNO_TMP_ELEMENT;
           //std::cout << "... fcSECTION_NAME" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"section-name\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "entry-name" ) == 0 ) 
   {
       if( template_enum == fcKEYWORD_NAME ) 
       {
           template_enum = fcNO_TMP_ELEMENT;
           //std::cout << "... fcKEYWORD_NAME" << std::endl;
           return;
       }

       if ( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION && 
            dependency_element              && dependency_enum == fcDEPENDENCY_NAME ) 
       {
           dependency_enum = fcNO_DEP_ELEMENT;
           //std::cout << "... fcDEPENDENCY_NAME" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"entry-name\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "multiple" ) == 0 )
   {
       if( template_enum == fcMULTIPLE ) 
       {
           template_enum = fcNO_TMP_ELEMENT;
           //std::cout << "... fcMULTIPLE" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"multiple\" ) == 0 )"),
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "properties" ) == 0 )
   {
       if( template_enum == fcPROPERTIES )
       {
           template_enum = fcNO_TMP_ELEMENT; 
           //std::cout << "... fcPROPERTIES" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&
           action_element                  && action_enum == fcACTION_PROPERTIES )
       {
           action_enum = fcNO_ACTION_ELEMENT;
           //std::cout << "... fcACTION_PROPERTIES" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"properties\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "type" ) == 0 ) 
   {
       if( template_enum == fcPROPERTIES && property_enum == fcTYPE ) 
       {
           property_enum = fcNO_PROP_ELEMENT;
           //std::cout << "... fcTYPE" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES    && dependencies_enum == fcACTIONS && action_element &&
           action_enum == fcACTION_PROPERTIES && property_enum == fcTYPE )
       {
           property_enum = fcNO_PROP_ELEMENT;
           //std::cout << "... fcTYPE" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
               fcString("if( strcmp( element_name, \"type\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "min" ) == 0 ) 
   {
       if( template_enum == fcPROPERTIES && property_enum == fcMIN ) 
       {
           property_enum = fcNO_PROP_ELEMENT;
           //std::cout << "... fcMIN" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES    && dependencies_enum == fcACTIONS && action_element &&
           action_enum == fcACTION_PROPERTIES && property_enum == fcMIN )
       {
           property_enum = fcNO_PROP_ELEMENT;
           //std::cout << "... fcMIN" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"min\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "max" ) == 0 ) 
   {
       if( template_enum == fcPROPERTIES && property_enum == fcMAX ) 
       {
           property_enum = fcNO_PROP_ELEMENT;
           //std::cout << "... fcMAX" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES    && dependencies_enum == fcACTIONS && action_element &&
           action_enum == fcACTION_PROPERTIES && property_enum == fcMAX )
       {
           property_enum = fcNO_PROP_ELEMENT;
           //std::cout << "... fcMAX" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"max\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "step" ) == 0 ) 
   {
       if( template_enum == fcPROPERTIES && property_enum == fcSTEP ) 
       {
           property_enum = fcNO_PROP_ELEMENT;
           //std::cout << "... fcSTEP" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES    && dependencies_enum == fcACTIONS && action_element &&
           action_enum == fcACTION_PROPERTIES && property_enum == fcSTEP )
       {
           property_enum = fcNO_PROP_ELEMENT;
           //std::cout << "... fcSTEP" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"step\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "data" ) == 0 )
   {
       if( template_enum == fcPROPERTIES && property_enum  == fcDATA ) 
       {
           property_enum = fcNO_PROP_ELEMENT; 
           //std::cout << "... fcDATA" << std::endl;
           return;
       }

       if( template_enum == fcDEPENDENCIES    && dependencies_enum == fcACTIONS && action_element &&
           action_enum == fcACTION_PROPERTIES && property_enum == fcDATA )
       {
           property_enum = fcNO_PROP_ELEMENT; 
           //std::cout << "... fcDATA" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"data\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "dependencies" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES ) 
       {
           template_enum = fcNO_TMP_ELEMENT; 
           //std::cout << "... fcDEPENDENCIES" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"dependencies\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "condition" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION )
       {
           dependencies_enum = fcNO_DEPS_ELEMENT;
           //std::cout << "... fcCONDITION" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"condition\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "actions" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS )
       {
           dependencies_enum = fcNO_DEPS_ELEMENT;
           //std::cout << "... fcACTIONS" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"actions\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "relation-and" ) == 0 )
   {

       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION && !dependency_element && 
           relations_stack.Top().relation_enum == fcRELATION_AND ) 
       {
           relations_stack.Remove();

           if( relations_stack.IsEmpty() )
               return;

           if( relations_stack.Top().is_registrable == false ) 
           {
               relations_stack.Top().is_registrable = true;
           } else {
               // Append relation AND into a final dependency list
               current_entry->AddDependencyListItem(relations_stack.Top().relation_enum);
           }

           //std::cout << "... relation_and" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"relation-and\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "relation-or" ) == 0 )
   {
       assert( !relations_stack.IsEmpty() );

       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION && !dependency_element && 
           relations_stack.Top().relation_enum == fcRELATION_OR ) 
       {
           relations_stack.Remove();

           if( relations_stack.IsEmpty() )
               return;

           if( relations_stack.Top().is_registrable == false )
           {
               relations_stack.Top().is_registrable = true;
           } else {
               // Append relation OR into a final dependency list
               current_entry->AddDependencyListItem(relations_stack.Top().relation_enum);
           }

           //std::cout << "... relation_or" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"relation-or\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "relation-not" ) == 0 )
   {
       assert( !relations_stack.IsEmpty() );

       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION && !dependency_element && 
           relations_stack.Top().relation_enum == fcRELATION_NOT ) 
       {
           relations_stack.Remove();

           if( relations_stack.IsEmpty() )
               return;

           if( relations_stack.Top().is_registrable == false )
           {
               relations_stack.Top().is_registrable = true;
           } else {
               // Append relation NOT into a final dependency list
               current_entry->AddDependencyListItem(relations_stack.Top().relation_enum);
           }

           //std::cout << "... relation_not" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"relation-not\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "dependency" ) == 0 ) 
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION && dependency_element ) 
       {
           dependency_element = false;

           if( relations_stack.IsEmpty() )
           {            
               // Append dependency_values and dependency_name into a final dependency list
               current_entry->AddDependencyListItem(dependency_name, dependency_values);
               dependency_name.Erase();
               dependency_values.EraseAll();
           } else {
               assert( relations_stack.Top().relation_enum != fcNO_RELATION );

               if( relations_stack.Top().is_registrable == false )
               {
                   relations_stack.Top().is_registrable = true;

                   // Append dependency_values and dependency_name into a final dependency list
                   current_entry->AddDependencyListItem(dependency_name, dependency_values);
                   dependency_name.Erase();
                   dependency_values.EraseAll();
               } else {
                   // Append dependency_vaules and dependency_name into a final dependency list
                   current_entry->AddDependencyListItem(dependency_name, dependency_values);
                   dependency_name.Erase();
                   dependency_values.EraseAll();

                   // Append relation AND/OR/NOT (relations_stack.Top().relation_enum) into a final dependency list
                   current_entry->AddDependencyListItem(relations_stack.Top().relation_enum);
               }
           }
           //std::cout << "... fcDEPENDENCY" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"dependency\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "values" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION && 
           dependency_element              && dependency_enum == fcVALUES ) 
       {
           dependency_enum = fcNO_DEP_ELEMENT;
           //std::cout << "... fcVALUES" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"values\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;

   }

   if( strcmp( element_name, "value" ) == 0 ) 
   {
        if ( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
             dependency_element              && dependency_enum == fcVALUES      && value_element ) 
        {
            value_element = false;

            if( tmpStruct.dep_value_type == fcVAL_EMPTY ) 
            {
                if(current_entry)
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),  
                            fcString("if( strcmp( element_name, \"value\" ) == 0 )"), 
                            fcString("NO value condition for the ") + current_entry->GetEntryName() + 
                            fcString(" element was given!" ) );
                else 
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),  
                            fcString("if( strcmp( element_name, \"value\" ) == 0 )"), 
                            fcString( "NO value condition was given!" ) );
                return;
            }
            if( tmpStruct.dep_values.IsEmpty() )
            {
                if(current_entry)
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),  
                            fcString("if( strcmp( element_name, \"value\" ) == 0 )"), 
                            fcString("NO value data for the ") + current_entry->GetEntryName() + 
                            fcString(" element was given!" ) );
                else 
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),  
                            fcString("if( strcmp( element_name, \"value\" ) == 0 )"), 
                            fcString( "NO value data was given!" ) );
                return;
            }

            dependency_values.Append(tmpStruct); // Append supplementary tmpStruct into a dependency_vaules list 
            tmpStruct.Init();

            //std::cout << "... fcVALUE" << std::endl;
            return;
        }

        if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&
            action_element                  && action_enum == fcVALUE )
        {
            action_enum = fcNO_ACTION_ELEMENT;
            //std::cout << "... action::fcVALUE" << std::endl;
            return;
        }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),  
               fcString("if( strcmp( element_name, \"value\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
        return;
   }

   if( strcmp( element_name, "equal" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
           dependency_element              && dependency_enum == fcVALUES      && 
           value_element                   && value_enum == fcEQUAL ) 
       {
           value_enum = fcNO_VALUE_ELEMENT;

           if( tmpStruct.dep_value_type != fcVAL_EMPTY ) {
               if(current_entry)
                   MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                           fcString("if( strcmp( element_name, \"equal\" ) == 0 )"), 
                           fcString("Illegal use of equal condition in ") +
                           current_entry->GetEntryName() + fcString(" element!" ) );
               else 
                   MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                           fcString("if( strcmp( element_name, \"equal\" ) == 0 )"), 
                           fcString( "Illegal use of equal condition!" ) );
               return;
           }

           tmpStruct.dep_value_type = fcVAL_EQUAL; 
           //std::cout << "... fcEQUAL" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"equal\" ) == 0 )"), 
               fcString( "Wrong location of end element: " ) + fcString( element_name ) );
       return;
   }
    
   if( strcmp( element_name, "not-equal" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
           dependency_element              && dependency_enum == fcVALUES      && 
           value_element                   && value_enum == fcNOT_EQUAL ) 
       {
           value_enum = fcNO_VALUE_ELEMENT;

           if( tmpStruct.dep_value_type != fcVAL_EMPTY ) {
               if(current_entry)
                   MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                           fcString("if( strcmp( element_name, \"not-equal\" ) == 0 )"), 
                           fcString("Illegal use of not-equal condition in ") +
                           current_entry->GetEntryName() + fcString(" element!" ) );
               else 
                   MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                           fcString("if( strcmp( element_name, \"not-equal\" ) == 0 )"), 
                           fcString( "Illegal use of not-equal condition!" ) );
               return;
           }

           tmpStruct.dep_value_type = fcVAL_NOT_EQUAL; 
           //std::cout << "... fcNOT_EQUAL" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"not-equal\" ) == 0 )"), 
               fcString( "Wrong location of end element: " ) + fcString( element_name ) );
       return;
   }

   if( strcmp( element_name, "greater" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
           dependency_element              && dependency_enum == fcVALUES      && 
           value_element                   && value_enum == fcGREATER ) 
       {
           value_enum = fcNO_VALUE_ELEMENT; 

           if( tmpStruct.dep_value_type == fcVAL_EMPTY )
           {
               tmpStruct.dep_value_type = fcVAL_GREATER;
           } else 
           if( tmpStruct.dep_value_type == fcVAL_LOWER ) 
           {
               tmpStruct.dep_value_type = fcVAL_LOWER_GREATER;
           } else 
           if( tmpStruct.dep_value_type == fcVAL_LOWER_EQ ) 
           {
               tmpStruct.dep_value_type = fcVAL_LOWER_EQ_GREATER;
           } else {
               if(current_entry)
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                         fcString("if( strcmp( element_name, \"greater\" ) == 0 )"), 
                         fcString("Illegal use of greater condition in ") +
                         current_entry->GetEntryName() + fcString(" element!" ) );
               else
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                         fcString("if( strcmp( element_name, \"greater\" ) == 0 )"), 
                         fcString("Illegal use of greater condition!") );
               return;
           }

           //std::cout << "... fcGREATER" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"greater\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "greater-or-equal" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
           dependency_element              && dependency_enum == fcVALUES      && 
           value_element                   && value_enum == fcGREATER_OR_EQUAL ) 
       {
           value_enum = fcNO_VALUE_ELEMENT; 

           if( tmpStruct.dep_value_type == fcVAL_EMPTY )
           {
               tmpStruct.dep_value_type = fcVAL_GREATER_EQ;
           } else 
           if( tmpStruct.dep_value_type == fcVAL_LOWER ) 
           {
               tmpStruct.dep_value_type = fcVAL_LOWER_GREATER_EQ;
           } else 
           if( tmpStruct.dep_value_type == fcVAL_LOWER_EQ ) 
           {
               tmpStruct.dep_value_type = fcVAL_LOWER_EQ_GREATER_EQ;
           } else {
               if(current_entry)
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                         fcString("if( strcmp( element_name, \"greater-or-equal\" ) == 0 )"), 
                         fcString("Illegal use of greater-or-equal condition in ") +
                         current_entry->GetEntryName() + fcString(" element!" ) );
               else
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                         fcString("if( strcmp( element_name, \"greater-or-equal\" ) == 0 )"), 
                         fcString("Illegal use of greater-or-equal condition!") );
               return;
           }

           //std::cout << "... fcGREATER_OR_EQUAL" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"greater-or-equal\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "lower" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
           dependency_element              && dependency_enum == fcVALUES      && 
           value_element                   && value_enum == fcLOWER ) 
       {
           value_enum = fcNO_VALUE_ELEMENT; 

           if( tmpStruct.dep_value_type == fcVAL_EMPTY )
           {
               tmpStruct.dep_value_type = fcVAL_LOWER;
           } else 
           if( tmpStruct.dep_value_type == fcVAL_GREATER ) 
           {
               tmpStruct.dep_value_type = fcVAL_GREATER_LOWER;
           } else 
           if( tmpStruct.dep_value_type == fcVAL_GREATER_EQ ) 
           {
               tmpStruct.dep_value_type = fcVAL_GREATER_EQ_LOWER;
           } else {
               if(current_entry)
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                         fcString("if( strcmp( element_name, \"lower\" ) == 0 )"), 
                         fcString("Illegal use of lower condition in ") +
                         current_entry->GetEntryName() + fcString(" element!") );
               else
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                         fcString("if( strcmp( element_name, \"lower\" ) == 0 )"), 
                         fcString("Illegal use of lower condition!") );
               return;
           }

           //std::cout << "... fcLOWER" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"lower\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "lower-or-equal" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
           dependency_element              && dependency_enum == fcVALUES      && 
           value_element                   && value_enum == fcLOWER_OR_EQUAL ) 
       {
           value_enum = fcNO_VALUE_ELEMENT; 

           if( tmpStruct.dep_value_type == fcVAL_EMPTY )
           {
               tmpStruct.dep_value_type = fcVAL_LOWER_EQ;
           } else 
           if( tmpStruct.dep_value_type == fcVAL_GREATER ) 
           {
               tmpStruct.dep_value_type = fcVAL_GREATER_LOWER_EQ;
           } else 
           if( tmpStruct.dep_value_type == fcVAL_GREATER_EQ ) 
           {
               tmpStruct.dep_value_type = fcVAL_GREATER_EQ_LOWER_EQ;
           } else {
               if(current_entry)
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                         fcString("if( strcmp( element_name, \"lower-or-equal\" ) == 0 )"), 
                         fcString("Illegal use of lower-or-equal condition in ") +
                         current_entry->GetEntryName() + fcString(" element!" ) );
               else
                    MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                         fcString("if( strcmp( element_name, \"lower-or-equal\" ) == 0 )"), 
                         fcString("Illegal use of lower-or-equal condition!") );
               return;
           }

           //std::cout << "... fcLOWER_OR_EQUAL" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"lower-or-equal\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "in" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
           dependency_element              && dependency_enum == fcVALUES      && 
           value_element                   && value_enum == fcIN ) 
       {
           value_enum = fcNO_VALUE_ELEMENT; 

           //if( tmpStruct.dep_value_type == fcVAL_IN )
           //{
           //} else {
           if( tmpStruct.dep_value_type != fcVAL_IN )
           {
               if( tmpStruct.dep_value_type != fcVAL_EMPTY ) {
                   if(current_entry)
                       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
                               fcString("if( strcmp( element_name, \"in\" ) == 0 )"), 
                               fcString("Illegal use of in condition in ") +
                               current_entry->GetEntryName() + fcString(" element!") );
                   else
                       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
                               fcString("if( strcmp( element_name, \"in\" ) == 0 )"), 
                               fcString("Illegal use of in condition!") );
                   return;
               }

               tmpStruct.dep_value_type = fcVAL_IN;
           }

           //std::cout << "... fcIN" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
               fcString("if( strcmp( element_name, \"in\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "not-in" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
           dependency_element              && dependency_enum == fcVALUES      && 
           value_element                   && value_enum == fcNOT_IN ) 
       {
           value_enum = fcNO_VALUE_ELEMENT;

           //if( tmpStruct.dep_value_type == fcVAL_NOT_IN )
           //{
           //} else {
           if( tmpStruct.dep_value_type != fcVAL_NOT_IN )
           {
               if( tmpStruct.dep_value_type != fcVAL_EMPTY ) {
                   if(current_entry)
                       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
                               fcString("if( strcmp( element_name, \"not-in\" ) == 0 )"), 
                               fcString("Illegal use of not-in condition in ") +
                               current_entry->GetEntryName() + fcString(" element!") );
                   else
                       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
                               fcString("if( strcmp( element_name, \"not-in\" ) == 0 )"), 
                               fcString("Illegal use of not-in condition!") );
                   return;
               }

               tmpStruct.dep_value_type = fcVAL_NOT_IN;
           }

           //std::cout << "... fcNOT_IN" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
               fcString("if( strcmp( element_name, \"not-in\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "action" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&
           action_element )
       {
           action_element = false;

           current_entry->AddActionListItem(current_action);
           current_action = 0;

           //std::cout << "... fcACTION" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"),
               fcString("if( strcmp( element_name, \"action\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   if( strcmp( element_name, "default-value" ) == 0 )
   {
       if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&
           action_element                  && action_enum == fcDEFAULT_VALUE )
       {
           action_enum = fcNO_ACTION_ELEMENT;
           //std::cout << "... fcDEFAULT_VALUE" << std::endl;
           return;
       }

       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( strcmp( element_name, \"default-value\" ) == 0 )"), 
               fcString("Wrong location of end element: ") + fcString(element_name) );
       return;
   }

   MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
           fcString("end of endElement function"), 
           fcString("endElement: Unknown entry type ") + fcString(element_name) );
   return;

}
//--------------------------------------------------------------------------
void fcSAXTemplateFile :: characters( const XMLCh* const chars, 
                                      const unsigned int length)
{
   DBG_FUNCTION_NAME( "fcSAXStringListFile", "characters" );

   const fc_char* data = XMLString :: transcode( chars );
   DBG_EXPR( data );

   //std::cout << "\nCHARACTERS: " << data << std::endl;

   if( template_enum == fcSECTION_NAME )
   {
       assert( current_entry );

       DBG_COUT( "Setting section name to " << data );
       //std::cout << "... fcSECTION_NAME: " << data << "\n" << std::endl;
       current_entry->SetEntryName( data );
       // We set the label just for the case it is missing in the help file
       current_entry->SetLabel( data );
       return;
   }

   if( template_enum == fcKEYWORD_NAME ) 
   {
       assert( current_entry );

       DBG_COUT( "Setting entry name to " << data );
       //std::cout << "... fcKEYWORD_NAME: " << data << "\n" << std::endl;
       current_entry->SetEntryName( data );
       // We set the label just for the case it is missing in the help file
       current_entry->SetLabel( data );
       return;
   }

   if( template_enum == fcMULTIPLE ) 
   {
       assert( current_entry );

       //std::cout << "... fcMULTIPLE: " << data << "\n" << std::endl;
       if( current_entry->Type() != Fc::Section )
       {
           MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                   fcString("if( template_enum == fcMULTIPLE )"), 
                   fcString("Element <multiple> is allowed only for section!") );
           return;
       }

       DBG_COUT( "Setting multiple to " << data );
       if( strcmp( data, "yes" ) == 0 || strcmp( data, "true" ) == 0 || strcmp( data, "1" ) == 0 )
       {
           current_entry->SetMultiple( true );
           return;
       } else 
       if( strcmp( data, "no" ) == 0 || strcmp( data, "false" ) == 0 || strcmp( data, "0" ) == 0 )
       {
           current_entry->SetMultiple( false );
           return;
       }
       
       MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( template_enum == fcMULTIPLE )"), 
               fcString("Unknown data ") + fcString( data ) + 
               fcString("for 'multiple'. Should be only 'yes' or 'no'.") );
       return;
   }

   //    if( template_enum == fcPROPERTIES && property_enum == fcTYPE ) 
   //    {
   //        assert( current_entry );
   //       
   //        if( current_entry->Type() != Fc::Number )
   //        {
   //                MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
   //                       fcString("if( template_enum == fcPROPERTIES && property_enum == fcTYPE )"), 
   //                       fcString("Element <type> is allowed only for section!") );
   //                return;
   //        }
   //
   //        return;
   //    }
   //
   //    MessageHandler. Error( fcString( "Wrong location of end element: " ) + fcString( element_name ) );
   //    return;

   if( template_enum == fcPROPERTIES && property_enum == fcMIN ) 
   {
       assert( current_entry );

       //std::cout << "... fcMIN: " << data << "\n" << std::endl;
       if( current_entry->Type() != Fc::Number )
       {
           MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                   fcString("if( template_enum == fcPROPERTIES && property_enum == fcMIN )"), 
                   fcString("Element <min> is allowed only for number!") );
           return;
       }
   
       DBG_COUT( "Setting number min to " << data );
       //( (fcTKNumber *) current_entry ) -> SetMin( atof( data ) );
       fcTKNumber *pNum = dynamic_cast<fcTKNumber*>(current_entry);
       if(!pNum)
       {
           MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                   fcString("if( template_enum == fcPROPERTIES && property_enum == fcMIN )"), 
                   fcString("Overcasting error!") );
           return;
       }
       pNum->SetMin( atof(data) );

       return;
   }

   if( template_enum == fcPROPERTIES && property_enum == fcMAX ) 
   {
       assert( current_entry );

       //std::cout << "... fcMAX: " << data "\n" << std::endl;
       if( current_entry->Type() != Fc::Number )
       {
           MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                   fcString("if( template_enum == fcPROPERTIES && property_enum == fcMAX )"), 
                   fcString("Element <max> is allowed only for number!") );
           return;
       }

       DBG_COUT( "Setting number max to " << data );
       //( (fcTKNumber *) current_entry ) -> SetMax( atof( data ) );
       fcTKNumber *pNum = dynamic_cast<fcTKNumber*>(current_entry);
       if(!pNum)
       {
           MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                   fcString("if( template_enum == fcPROPERTIES && property_enum == fcMAX )"), 
                   fcString("Overcasting error!") );
           return;
       }
       pNum->SetMax( atof(data) );

       return;
   }

   if( template_enum == fcPROPERTIES && property_enum == fcSTEP ) 
   {
       assert( current_entry );

       //std::cout << "... fcSTEP: " << data "\n" << std::endl;
       if( current_entry->Type() != Fc::Number )
       {
           MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                   fcString("if( template_enum == fcPROPERTIES && property_enum == fcSTEP )"), 
                   fcString("Element <step> is allowed only for number!") );
           return;
       }
       
       DBG_COUT( "Setting number step to " << data ); 
       //( (fcTKNumber *) current_entry ) -> SetStep( atof( data ) );
       fcTKNumber *pNum = dynamic_cast<fcTKNumber*>(current_entry);
       if(!pNum)
       {
           MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                   fcString("if( template_enum == fcPROPERTIES && property_enum == fcSTEP )"), 
                   fcString("Overcasting error!") );
           return;
       }
       pNum->SetStep( atof(data) );

       return;
   }

   if( template_enum == fcPROPERTIES && property_enum == fcDATA ) 
   {
       assert( current_entry );

       //std::cout << "... fcDATA: " << data << "\n" << std::endl;
       if( current_entry->Type() != Fc::String )
       {
           MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                   fcString("if( template_enum == fcPROPERTIES && property_enum == fcDATA )"), 
                   fcString("Element <data> is allowed only for string!") );
           return;
       }

       DBG_COUT( "Looking for list: " << data );
       fcList< fcStringEntry > *strings = (*string_lists)( data );
       assert( strings );
       
       DBG_COUT( "Adding string list: " << strings );
       //( (fcTKString *) current_entry ) -> AppendStrings( *strings );
       fcTKString *pString = dynamic_cast<fcTKString*>(current_entry);
       if(!pString)
       {
           MessageHandler. Error( fcString("fcSAXTemplateFile.cpp"), 
                   fcString("if( template_enum == fcPROPERTIES && property_enum == fcDATA )"), 
                   fcString("Overcasting error!") );
           return;
       }
       pString->AppendStrings( *strings );

       return;
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
       dependency_element              && dependency_enum == fcDEPENDENCY_NAME )
   {
       dependency_name.SetString( data );
       return; 
   }


   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
       dependency_element              && dependency_enum == fcVALUES      && 
       value_element                   && value_enum == fcEQUAL )
   {
       tmpStruct.dep_values.Append( data );
       return; 
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
       dependency_element              && dependency_enum == fcVALUES      && 
       value_element                   && value_enum == fcNOT_EQUAL )
   {
       tmpStruct.dep_values.Append( data );
       return; 
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
       dependency_element              && dependency_enum == fcVALUES      && 
       value_element                   && value_enum == fcGREATER ) 
   {
       tmpStruct.dep_values.Append( data );
       return; 
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
       dependency_element              && dependency_enum == fcVALUES      && 
       value_element                   && value_enum == fcGREATER_OR_EQUAL ) 
   {
       tmpStruct.dep_values.Append( data );
       return; 
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
       dependency_element              && dependency_enum == fcVALUES      && 
       value_element                   && value_enum == fcLOWER ) 
   {
       tmpStruct.dep_values.Append( data );
       return; 
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
       dependency_element              && dependency_enum == fcVALUES      && 
       value_element                   && value_enum == fcLOWER_OR_EQUAL ) 
   {
       tmpStruct.dep_values.Append( data );
       return; 
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
       dependency_element              && dependency_enum == fcVALUES      && 
       value_element                   && value_enum == fcIN ) 
   {
       tmpStruct.dep_values.Append( data );
       return; 
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcCONDITION &&
       dependency_element              && dependency_enum == fcVALUES      && 
       value_element                   && value_enum == fcNOT_IN ) 
   {
       tmpStruct.dep_values.Append( data );
       return; 
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&
       action_element                  && action_enum == fcVALUE )
   {
       assert(current_action);

       if( strcmp(data, "yes") == 0 || strcmp(data, "true") == 0)
       {
           current_action->SetActionValue( 1 );
           return;
       } else
       if( strcmp(data, "no") == 0 || strcmp(data, "false") == 0)
       {
           current_action->SetActionValue( 0 );
           return;
       }

       current_action->SetActionValue( atof(data) ); 
       return;
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&
       action_element                  && action_enum == fcDEFAULT_VALUE )
   {
       assert(current_action);

       fcDActionBoolListItem* pActionBool;
       fcDActionNumberListItem * pActionNum;
       fcDActionStringListItem * pActionStr;
       fc_bool bDefValue;

       switch(current_action->ActionType())
       {
           case fcBOOL_ACTION:
               pActionBool = dynamic_cast<fcDActionBoolListItem*>(current_action);
               if(!pActionBool)
                   break;
               if( strcmp(data, "yes") == 0 || strcmp(data, "true") == 0 || strcmp(data, "1") == 0)
                   bDefValue = true;
               else 
                   bDefValue = false;
               pActionBool->SetDefaultValue( bDefValue );
               return;
           case fcNUMBER_ACTION:
               pActionNum = dynamic_cast<fcDActionNumberListItem*>(current_action);
               if(!pActionNum)
                   break;
               pActionNum->SetDefaultValue( atof(data) );
               return;
           case fcSTRING_ACTION:
               pActionStr = dynamic_cast<fcDActionStringListItem*>(current_action);
               if(!pActionStr)
                   break;
               pActionStr->SetDefaultValue( data );
               return;
           case fcNO_ACTION:
           default:
               break;
       }

       MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
               fcString("action_element && action_enum == fcDEFAULT_VALUE )"),
               fcString("unknown instance of action pointer!") );
       return;
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS     &&
       action_element                  && action_enum == fcACTION_PROPERTIES && property_enum == fcTYPE )
   {
       assert(current_action);
        
       fcDActionNumberListItem * pActionNum;

       switch(current_action->ActionType())
       {
           case fcBOOL_ACTION:
               MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
                       fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
                       fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcTYPE )"),
                       fcString("Properties setting is not allowed for bool!") );
               return;
           case fcNUMBER_ACTION:
               pActionNum = dynamic_cast<fcDActionNumberListItem*>(current_action);
               if(!pActionNum)
                   break;
               pActionNum->SetType( data );
               return;
            case fcSTRING_ACTION:
               MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
                       fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
                       fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcTYPE )"),
                       fcString("Property type setting is not allowed for string!") );
               return;
            case fcNO_ACTION:
            default:
               break;
       }

       MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
               fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcTYPE )"),
               fcString("unknown instance of action pointer!") );
       return;
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS     &&
       action_element                  && action_enum == fcACTION_PROPERTIES && property_enum == fcMIN )
   {
       assert(current_action);

       fcDActionNumberListItem * pActionNum;

       switch(current_action->ActionType())
       {
           case fcBOOL_ACTION:
               MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
                       fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
                       fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcMIN )"),
                       fcString("Properties setting is not allowed for bool!") );
               return;
           case fcNUMBER_ACTION:
               pActionNum = dynamic_cast<fcDActionNumberListItem*>(current_action);
               if(!pActionNum)
                   break;
               pActionNum->SetMin( atof(data) );
               return;
           case fcSTRING_ACTION:
               MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
                       fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
                       fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcMIN )"),
                       fcString("Property min setting is not allowed for string!") );
               return;
           case fcNO_ACTION:
           default:
               break;
       }

       MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
               fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcMIN )"),
               fcString("unknown instance of action pointer!") );
       return;
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS     &&
       action_element                  && action_enum == fcACTION_PROPERTIES && property_enum == fcMAX )
   {
       assert(current_action);

       fcDActionNumberListItem * pActionNum;

       switch(current_action->ActionType())
       {
           case fcBOOL_ACTION:
               MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
                       fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
                       fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcMAX )"),
                       fcString("Properties setting is not allowed for bool!") );
               return;
           case fcNUMBER_ACTION:
               pActionNum = dynamic_cast<fcDActionNumberListItem*>(current_action);
               if(!pActionNum)
                   break;
               pActionNum->SetMax( atof(data) );
               return;
           case fcSTRING_ACTION:
               MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
                       fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
                       fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcMAX )"),
                       fcString("Property max setting is not allowed for string!") );
               return;
           case fcNO_ACTION:
           default:
               break;
       }

       MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
               fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcMAX )"),
               fcString("unknown instance of action pointer!") );
       return;
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS     &&
       action_element                  && action_enum == fcACTION_PROPERTIES && property_enum == fcSTEP )
   {
       assert(current_action);

       fcDActionNumberListItem * pActionNum;

       switch(current_action->ActionType())
       {
           case fcBOOL_ACTION:
               MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
                       fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
                       fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcSTEP )"),
                       fcString("Properties setting is not allowed for bool!") );
               return;
           case fcNUMBER_ACTION:
               pActionNum = dynamic_cast<fcDActionNumberListItem*>(current_action);
               if(!pActionNum)
                   break;
               pActionNum->SetStep( atof(data) );
               return;
           case fcSTRING_ACTION:
               MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
                       fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
                       fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcSTEP )"),
                       fcString("Property step setting is not allowed for string!") );
               return;
           case fcNO_ACTION:
           default:
               break;

       }

       MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
               fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcSTEP )"),
               fcString("unknown instance of action pointer!") );
       return;
   }

   if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS     &&
       action_element                  && action_enum == fcACTION_PROPERTIES && property_enum == fcDATA )
   {
       assert(current_action);

       fcDActionStringListItem * pActionStr;
       fcList< fcStringEntry > * pStringData;

       switch(current_action->ActionType())
       {
           case fcBOOL_ACTION:
               MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
                       fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
                       fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcDATA )"),
                       fcString("Properties setting is not allowed for bool!") );
               return;
           case fcNUMBER_ACTION:
               MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
                       fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
                       fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcDATA )"),
                       fcString("Property data setting is not allowed for number!") );
           case fcSTRING_ACTION:
               pActionStr = dynamic_cast<fcDActionStringListItem*>(current_action);
               if(!pActionStr)
                   break;

               pStringData = (*string_lists)(data);
               assert(pStringData);

               pActionStr->AppendDataList( *pStringData );
               return;
           case fcNO_ACTION:
           default:
               break;
       }

       MessageHandler.Error(fcString("fcSAXTemplateFile.cpp"), 
               fcString("if( template_enum == fcDEPENDENCIES && dependencies_enum == fcACTIONS &&") +
               fcString("action_element && action_enum == fcACTION_PROPERTIES && property_enum == fcDATA )"),
               fcString("unknown instance of action pointer!") );
       return;
   }
}
//---------------------------------------------------------------------------
void fcSAXTemplateFile :: control()
{
    assert( template_enum       == fcNO_TMP_ELEMENT    ||
            property_enum       == fcNO_PROP_ELEMENT   ||
            dependencies_enum   == fcNO_DEPS_ELEMENT   ||
            dependency_enum     == fcNO_DEP_ELEMENT    ||
            action_enum         == fcNO_ACTION_ELEMENT ||
            value_enum          == fcNO_VALUE_ELEMENT  ||
            dependency_element  == false               ||
            action_element      == false               ||
            value_element       == false                ); 
}
//---------------------------------------------------------------------------
fc_bool fcSAXTemplateFile :: ParseTemplateFile( const fc_char* file_name,
                             fcTSEntry* t_section,
                             fcHash< fcList< fcStringEntry > >* _string_lists )
{
   string_lists = _string_lists;
   enclosing_tag = false;
   section_stack.EraseAll();
   section_stack.Add( t_section );
   relations_stack.EraseAll();
   return Parse( file_name );
}
