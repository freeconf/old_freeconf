/***************************************************************************
                          fcCKBool.h  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcCKBoolH
#define fcCKBoolH

#include "fcCKEntry.h"

class fcCKBool : public fcCKEntry
{
   //! Entry value
   fc_bool value;
   
   public:
   //! Basic constructor
   fcCKBool();

   //! Copy constructor
   fcCKBool( const fcCKBool& config_value );

   //! Constructor template entry pointer
   fcCKBool( const fcTEntry* t_entry )
      : fcCKEntry( t_entry ){};

   //! Destructor
   ~fcCKBool();
   
   //! Allocate new instance and return pointer
   virtual fcBaseEntry* NewEntry() const;

   //! Value type getter
   Fc::EntryTypes Type() const;
   
   //! Value getter
   fc_bool GetValue() const;

   //! Value setter
   void SetValue( fc_bool val );

#ifdef DEBUG
   //! Print this structure
   void Print( ostream& stream ) const;
#endif
};

#endif
