/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : 2004/04/12 14:18
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include <iostream>
#include <cstdlib>
#include <gtk/gtk.h>
#include <getopt.h>
#include <fcHeaderStructure.h>
#include <fcInit.h>
#include <fcMessageHandler.h>
#include <fcSAXHeaderFile.h>
#include <fcSAXHelpFile.h>
#include <fcSAXTemplateFile.h>
#include <fcStringEntry.h>
#include <fcHash.h>
#include <fcDOMConfigFile.h>
#include <fcTSEntry.h>
#include "debug.h"
#include "gtkCSEntry.h"
#include "gtk_render.h"

//------------------------------------------------------------------------------------------
int main( int argc, char *argv[] )
{
   DBG_INIT( "debug.xml" );
   DBG_FUNCTION_NAME( "", "main" );

   std::cout << "GTK client for FreeConf - Copyright (C) 2004 Tomas Oberhuber" << std::endl;
  
   gtk_init (&argc, &argv);

   if (argc <= 1)
   {
     std::cerr << "No package name was given." << std::endl;
     return -1;
   }
   
   const fc_char* package_name = argv[ 1 ];

   fcHeaderStructure header_structure;  // Structure that describes files to parse
   //fcSAXHeaderFile sax_header_file;

   fcTSEntry t_section;
   //fcSAXTemplateFile fc_sax_template_file;

   gtkCSEntry c_section;
   fcDOMConfigFile main_config_file;
   main_config_file. SetConfigSection( &c_section );

   fcHash< fcList< fcStringEntry > > string_lists;
   
   if( ! fcInit( package_name,
                 header_structure,
                 t_section, 
                 string_lists,
                 main_config_file ) ) return -1;
   DBG_COUT( main_config_file );  

   std::cout << "Starting GTK rendering ... " << std::endl;
   Gtk_Render( &t_section,
               &header_structure,
               &main_config_file );
   
   return 0;
}

