/************************************************************************
 *                      fcCDependency.cpp
 *      author: Frantisek Rolinek
 ************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <cstring>
#include "fcMessageHandler.h"
#include "fcCDependency.h"
#include "fcCKDependency.h"

//-------------------------------------------------------------
fcCDependencyListItem :: fcCDependencyListItem()
{
}
//-------------------------------------------------------------
fcCDependencyListItem :: fcCDependencyListItem(const fcTDependencyListItem& item)
{
}
//-------------------------------------------------------------
fcCDependencyListItem :: fcCDependencyListItem(const fcCDependencyListItem& item)
{
}
//-------------------------------------------------------------
fcCDependencyListItem :: ~fcCDependencyListItem()
{
}
//-------------------------------------------------------------
fcCDRelationListItem :: fcCDRelationListItem()
{
    m_relation = fcNO_RELATION;
}
//-------------------------------------------------------------
fcCDRelationListItem :: fcCDRelationListItem(const fcRelationEnum& _enum)
{
    m_relation = _enum;
}
//-------------------------------------------------------------
fcCDRelationListItem :: fcCDRelationListItem(const fcTDRelationListItem& item)
{
    m_relation = item.GetRelation();
}
//-------------------------------------------------------------
fcCDRelationListItem :: fcCDRelationListItem(const fcCDRelationListItem& item)
    : fcCDependencyListItem(item)
{
    m_relation = item.m_relation;
}
//-------------------------------------------------------------
fcCDRelationListItem :: ~fcCDRelationListItem()
{
}
//-------------------------------------------------------------
fcCDependencyListItem* fcCDRelationListItem :: NewDependencyListItem() const 
{
    return new fcCDRelationListItem ( *this );
}
//-------------------------------------------------------------
fc_bool fcCDRelationListItem :: IsDependencyListItemKeyword() const
{
    return false;
}
//-------------------------------------------------------------
fc_bool fcCDRelationListItem :: IsDependencyListItemRelation() const
{
    return true;
}
//-------------------------------------------------------------
const fcCDRelationListItem& fcCDRelationListItem :: operator=(const fcCDRelationListItem& item)
{
    m_relation = item.m_relation;

    return *this;
}
//-------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const fcCDRelationListItem& fcRel)
{
    if(fcRel.m_relation == fcRELATION_AND)
        stream << " ==> C_AND ";
    else if(fcRel.m_relation == fcRELATION_OR)
        stream << " ==> C_OR ";
    else if(fcRel.m_relation == fcRELATION_NOT)
        stream << " ==> C_NOT ";
    else 
        std::cout << "fcCDependency.cpp " 
                  << "Item from the dependency list is neither relation-and nor relation-or nor relation-not!" 
                  << std::endl;

    return stream;
}
//-------------------------------------------------------------
void fcCDRelationListItem :: SetRelation(const fcRelationEnum& relation)
{
    m_relation = relation;
}
//-------------------------------------------------------------
const fcRelationEnum& fcCDRelationListItem :: GetRelation() const
{
    return m_relation;
}
//-------------------------------------------------------------
fcCDKeywordListItem :: fcCDKeywordListItem()
{
}
//-------------------------------------------------------------
fcCDKeywordListItem :: fcCDKeywordListItem(const fc_char* name)
{
    m_strEntryName = name;
}
//-------------------------------------------------------------
fcCDKeywordListItem :: fcCDKeywordListItem(const fcString& name)
{
    m_strEntryName = name;
}
//-------------------------------------------------------------
fcCDKeywordListItem :: fcCDKeywordListItem(const fcTDKeywordListItem& item)
{
    m_strEntryName = item.GetKeywordName();
}
//-------------------------------------------------------------
fcCDKeywordListItem :: fcCDKeywordListItem(const fcCDKeywordListItem& item)
    : fcCDependencyListItem(item)
{
    m_strEntryName = item.m_strEntryName;
}
//-------------------------------------------------------------
fcCDKeywordListItem :: ~fcCDKeywordListItem()
{
}
//-------------------------------------------------------------
fc_bool fcCDKeywordListItem :: IsDependencyListItemKeyword() const 
{
    return true;
}
//-------------------------------------------------------------
fc_bool fcCDKeywordListItem :: IsDependencyListItemRelation() const 
{
    return false;
}
//-------------------------------------------------------------
//const fcCDKeywordListItem& fcCDKeywordListItem :: operator=(const fcCDKeywordListItem& item)
//{
//    m_strEntryName      = item.m_strEntryName;
//    
//    return *this;
//}
//-------------------------------------------------------------
//std::ostream& operator<< (std::ostream& stream, const fcCDKeywordListItem& fcKey)
//{
//    stream << " ==> " << fcKey.m_strEntryName << " = ";
//
//    for(fc_uint i = 0; i < fcKey.m_dependencyValues.Size(); i++)
//    {
//        stream << "[ ";
//        struDependencyValue stru = fcKey.m_dependencyValues[i];
//        stream << stru << " ]";
//    }
//
//    return stream;
//}
//-------------------------------------------------------------
void fcCDKeywordListItem :: SetKeywordName(const fc_char* name)
{
    m_strEntryName = name;
}
//-------------------------------------------------------------
void fcCDKeywordListItem :: SetKeywordName(const fcString& name)
{
    m_strEntryName = name;
}
//-------------------------------------------------------------
const fcString& fcCDKeywordListItem :: GetKeywordName() const
{
    return m_strEntryName;
}
//-------------------------------------------------------------
fcCDependency :: fcCDependency()
{
   m_pDep = new fcList< fcCDependencyListItem* >;
}
//-------------------------------------------------------------
fcCDependency :: fcCDependency(fcList< fcCDependencyListItem* > *pDep, fc_bool bDeepCopy /*= true*/)
{
    if(bDeepCopy)
    {
        m_pDep = new fcList< fcCDependencyListItem* >;
        if(pDep)
        {
            for(fc_int i = 0; i < pDep->Size(); i++)
                m_pDep->Append(pDep->at(i));
        }
    } else {
        if(pDep)
        {
            m_pDep = pDep;
        } else { 
            m_pDep = new fcList< fcCDependencyListItem* >;
        }
    }
}
//-------------------------------------------------------------
fcCDependency :: fcCDependency(const fcCDependency& item)
{
    m_pDep = new fcList< fcCDependencyListItem* >;
    if(item.m_pDep)
    {
        for(fc_int i = 0; i < item.m_pDep->Size(); i++)
            m_pDep->Append(item.m_pDep->at(i));
    }
}
//-------------------------------------------------------------
fcCDependency :: ~fcCDependency()
{
    EraseDependencyList();
    if(m_pDep)
        delete m_pDep;
}
//-------------------------------------------------------------
const fcCDependency& fcCDependency :: operator= (const fcCDependency& item)
{
    if(!m_pDep)
    {
        m_pDep = new fcList< fcCDependencyListItem* >;
    } else {
        EraseDependencyList();
    }

    if(item.m_pDep)
    {
        for(fc_int i = 0; i < item.m_pDep->Size(); i++)
            m_pDep->Append(item.m_pDep->at(i));
        //for(fc_int i = 0; i < item.GetDependencyListSize(); i++)
        //    m_pDep->Append(item.GetDependencyListItem(i));
    }

    return *this;
}
//-------------------------------------------------------------
void fcCDependency :: AddDependencyListItem(fcCDependencyListItem* pItem)
{
    if(!m_pDep)
    {
        MessageHandler.Error("fcCDependency.cpp","AddDependencyListItem(fcCDependencyListItem* pItem)",
                "m_pDep pointer is not allocated!");
        return;
    }

    if(!pItem)
    {
        MessageHandler.Error("fcCDependency.cpp","AddDependencyListItem(fcCDependencyListItem* pItem)",
                "the given pointer (pItem) is not allocated!");
        return;
    }

    m_pDep->Append(pItem);
}
//-------------------------------------------------------------
void fcCDependency :: SetDependencyListItem(fcCDependencyListItem* pItem, fc_uint pos)
{
    if(!m_pDep)
    {
        MessageHandler.Error("fcCDependency.cpp","SetDependencyListItem(fcCDependencyListItem* pItem, fc_uint pos)",
                "m_pDep pointer is not allocated!");
        return;
    }

    if( IsPositionNotValid(pos) )
    {
        MessageHandler.Error("fcCDependency.cpp","SetDependencyListItem(fcCDependencyListItem* pItem, fc_uint pos)",
                "pos index is out of range!");
        return;
    }

    fcCDependencyListItem* pOldItem = m_pDep->at(pos);
    m_pDep->at(pos) = pItem;

    delete pOldItem;
}
//-------------------------------------------------------------
fcCDependencyListItem* fcCDependency :: GetDependencyListItem(fc_uint pos)
{
    if(!m_pDep)
    {
        MessageHandler.Error("fcCDependency.cpp", "GetDependencyListItem(fc_uint pos)", "m_pDep is not allocated!");
        return (fcCDependencyListItem*) 0;
    }

    if( IsPositionNotValid(pos) )
    {
        MessageHandler.Warning("fcCDependency.cpp", "GetDependencyListItem(fc_uint pos)", "pos index is out of range!");
        return (fcCDependencyListItem*) 0;
    }

    return m_pDep->at(pos);
}
//-------------------------------------------------------------
fcCDependencyListItem* fcCDependency :: GetDependencyListItem(fc_uint pos) const
{
    if(!m_pDep)
    {
        MessageHandler.Error("fcCDependency.cpp", "GetDependencyListItem(fc_uint pos) const", 
                "m_pDep is not allocated!");
        return (fcCDependencyListItem*) 0;
    }

    if( IsPositionNotValid(pos) )
    {
        MessageHandler.Warning("fcCDependency.cpp", "GetDependencyListItem(fc_uint pos) const", 
                "pos index is out of range!");
        return (fcCDependencyListItem*) 0;
    }

    return m_pDep->at(pos);
}
//-------------------------------------------------------------
fc_uint fcCDependency :: GetDependencyListSize() const
{
    if(!m_pDep)
    {
        MessageHandler.Warning("fcCDependency.cpp", "GetDependencyListSize() const",
                "m_pDep is not allocated!");
        return 0;
    }

    return m_pDep->Size();
}
//-------------------------------------------------------------
fc_bool fcCDependency :: IsPositionValid(fc_uint pos) const
{
    if(!m_pDep)
    {
        MessageHandler.Error("fcCDependency.cpp", "IsPositionValid(fc_uint pos) const",
                "m_pDep is not allocated!");
        return false;
    }

    if( pos < 0 || pos > m_pDep->Size() -1 )
        return false;
    return true;
}
//-------------------------------------------------------------
fc_bool fcCDependency :: IsPositionNotValid(fc_uint pos) const
{
    if(!m_pDep)
    {
        MessageHandler.Error("fcCDependency.cpp", "IsPositionNotValid(fc_uint pos) const",
                "m_pDep is not allocated!");
        return true;
    }

    if( pos < 0 || pos > m_pDep->Size() -1 )
        return true;
    return false;
}
//-------------------------------------------------------------
void fcCDependency :: EraseDependencyList()
{
    if(m_pDep)
        m_pDep->EraseAll();
}
//-------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const fcCDependency& fcCDep)
{
    if(!fcCDep.m_pDep)
        return stream;

    fcCDependencyListItem    * pItem     = 0;
    fcCDKeywordListItem      * pKeyword  = 0;
    fcCDRelationListItem     * pRelation = 0;

    fc_int nSize = fcCDep.m_pDep->Size();
    for(fc_int i = 0; i < nSize; i++)
    {
        pItem = fcCDep.m_pDep->at(i); //(*(fcDep.dep))[i];

        if(!pItem)
        {
            break;
        }

        if(pItem->IsDependencyListItemKeyword())
        {
            pKeyword = dynamic_cast<fcCDKeywordListItem*>(pItem);

            if(!pKeyword)
                break;

            switch(pKeyword->DKeywordType())
            {
                case fcDK_BOOL:
                    stream << (*dynamic_cast<fcCDKBoolListItem*>(pKeyword));
                    break;
                case fcDK_NUMBER:
                    stream << (*dynamic_cast<fcCDKNumberListItem*>(pKeyword));
                    break;
                case fcDK_STRING:
                    stream << (*dynamic_cast<fcCDKStringListItem*>(pKeyword));
                    break;
                default:
                    std::cerr << "fcCDependency.cpp " <<
                        "std::ostream& operator<< (std::ostream& stream, const fcDependency& fcDep)" <<
                        "Item from the dependency list at position " << i <<
                        "is neither bool keyword, nor number keyword nor string keywrod!" << std::endl;
                    break;
            }

        } else
        if(pItem->IsDependencyListItemRelation())
        {
            pRelation = dynamic_cast<fcCDRelationListItem*>(pItem);

            if(!pRelation)
                break;

            stream << (*pRelation);

        } else {
            std::cerr << "fcCDependency.cpp " << 
                         "std::ostream& operator<< (std::ostream& stream, const fcDependency& fcDep)" <<
                         "Item from the dependency list at position " << i << 
                         " is neither keyword nor relation!" << std::endl;

            break;
        }

        pItem     = 0;
        pKeyword  = 0;
        pRelation = 0;
    }

    return stream;
}
