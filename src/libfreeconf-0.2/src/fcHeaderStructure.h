/***************************************************************************
                          fcHeaderStructure.h  -  description
                             -------------------
    begin                : 2004/10/14
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCHEADERSTRUCTURE_H__
#define __FCHEADERSTRUCTURE_H__

#include "fcString.h"
#include "fcList.h"

//! Structure that describes files to parse
/*! This structure containts file names for
    template file, help files, files with default
    values etc. which should be parsed to get all
    necesary information.
 */
class fcHeaderStructure
{
   //! Package name is necesary for constructing file name pathes
   fcString package_name;

   //! Freeconf template file name
   fcString template_file;

   //! Freeconf gui template file name
   fcString gui_template_file;

   //! Help file
   fcString help_file;

   //! Gui label file
   fcString gui_label_file;
   
   //! File with default values
   fcString default_values_file;

   //! List of string lists files
   /*! String lists usualy contains possible values for
       string config values (like screen resolutions,
       text coding pages etc.). 
    */
   fcList< fcString > string_list_files;

   //! XSL file for transformation
   fcString transform_file;

   //! Output file in freeconf format
   fcString output_file;

   //! Output file in native format
   fcString native_file;

   public:

   //! Basic constructor
   fcHeaderStructure();

   //! Destructor
   ~fcHeaderStructure();

   //! Package name setter
   void SetPackageName( const fc_char* name );
   
   //! Package name getter
   const fcString& GetPackageName() const;

   //! Template file setter
   void SetTemplateFile( const fc_char* name );

   //! Template file getter
   const fcString& GetTemplateFile() const;

   //! Gui template file setter
   void SetGuiTemplateFile( const fc_char* name );
   
   //! Gui template file getter
   const fcString& GetGuiTemplateFile() const;

   //! Help file setter
   void SetHelpFile( const fc_char* name );

   //! Help file getter
   const fcString& GetHelpFile() const;

   //! Gui label file setter
   void SetGuiLabelFile( const fc_char* name );
   
   //! Gui label file getter
   const fcString& GetGuiLabelFile() const;

   //! Default values file setter
   void SetDefaultValuesFile( const fc_char* name );

   //! Default values file getter
   const fcString& GetDefaultValuesFile() const;

   //! Acces to string list files
   fcList< fcString >& StringListFiles();

   //! Acces to constant default values files
   //const fcList< fcString >& DefaultValuesFiles() const;

   //! XSLT file name setter
   void SetTransformFile( const fc_char* name );
   
   //! XSLT file name getter
   const fcString& GetTransformFile() const;

   //! Output file setter
   void SetOutputFile( const fc_char* name );

   //! Output file getter
   const fcString& GetOutputFile() const;

   //! Native output file setter
   void SetNativeFile( const fc_char* name );

   //! Native output file getter
   const fcString& GetNativeFile() const;
};

#endif
