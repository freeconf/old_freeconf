/***************************************************************************
                          fcTKEntry.cpp  -  description
                             -------------------
    begin                : 2004/04/10 16:22
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cassert>
#include "fcTKEntry.h"

//---------------------------------------------------------------------------
fcTKEntry :: fcTKEntry()
   : aliases( NULL )
{
}
//---------------------------------------------------------------------------
fcTKEntry :: fcTKEntry( const fcTKEntry& entry )
   : fcTEntry( entry ),
     aliases( 0 )
{
}
//---------------------------------------------------------------------------
fcTKEntry :: ~fcTKEntry()
{
   if( aliases )
   {
      aliases -> EraseAll();
      delete aliases;
   }
}
//---------------------------------------------------------------------------
fc_bool fcTKEntry :: IsSection() const
{ 
   return false;
}
//---------------------------------------------------------------------------
fc_bool fcTKEntry :: IsKeyword() const
{ 
   return true;
}
//---------------------------------------------------------------------------
fc_bool fcTKEntry :: IsTEntry() const 
{ 
   return true;
}
//---------------------------------------------------------------------------
fc_bool fcTKEntry :: IsCEntry() const
{ 
   return false;
}
//---------------------------------------------------------------------------
//void fcTKEntry :: SetDependencyValue( const fc_char* val ) {} 
//---------------------------------------------------------------------------
void fcTKEntry :: AddAlias( const fc_char* str )
{
   aliases -> Append( fcString( str ) );
}
//---------------------------------------------------------------------------
void fcTKEntry :: SetCommandLine( const fc_char* str )
{
   command_line. SetString( str );
}
//---------------------------------------------------------------------------
#ifdef DEBUG
void fcTKEntry :: Print( ostream& stream )
{
   stream << " TEMPLATE KEYWORD: " << GetEntryName() << " -> ";
   fcTEntry :: Print( stream );
}
#endif
