#include <QVBoxLayout>
#include <QFrame>
#include <QDebug>
#include <fcCGEntry.h>
#include <fcCGSEntry.h>
#include <fcPackage.h>
#include <fcGTab.h>

#include "qtCSEntry.h"
#include "qtCSRootEntry.h"
#include "qtCWidget.h"


qtCSRootEntry::qtCSRootEntry (fcPackage *package, fcGTab *tab, QBoxLayout *rootLayout)
  :qtCWidget(package),
  _rootLayout(rootLayout),
  _tab(tab)
{
  _layout = new QVBoxLayout();
  _layout->setSpacing(0);
  _rootSection = new qtCSEntry(dynamic_cast<const fcCGEntry*>(_tab->content()), _package, this);
  _parent = 0;
}


qtCSRootEntry::~qtCSRootEntry ()
{
  QLayoutItem *child;
  while ((child = _rootLayout->takeAt(0)) != 0)
  {
    delete child->widget();
    delete child->layout();
  }
  delete _rootSection;
  _rootLayout->update();
}


void qtCSRootEntry::render ()
{
  if (!_rootLayout || !_rootSection)
  {
    qDebug() << "Root section and layout must be set before render";
    return;
  }
  QFrame *frame = new QFrame;
  frame->setLayout(_layout);
  _rootLayout->addWidget(frame);
  _rootLayout->setSpacing(0);
  _rootSection->setFrameLess(true);
  _rootSection->render();
}


void qtCSRootEntry::invalidateGui ()
{

}


void qtCSRootEntry::attach (fc_bool attached)
{
  
}


void qtCSRootEntry::setRootSection (qtCSEntry *rootSection)
{
  _rootSection = rootSection;
}


qtCSEntry* qtCSRootEntry::rootSection () const
{
  return _rootSection;
}


void qtCSRootEntry::setRootLayout (QBoxLayout *rootLayout)
{
  _rootLayout = rootLayout;
}


QBoxLayout* qtCSRootEntry::rootLayout () const
{
  return _rootLayout;
}


const fcGTab* qtCSRootEntry::tab () const
{
  return _tab;
}

  