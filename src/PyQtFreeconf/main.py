#!/usr/bin/python

from PyQt4 import QtCore, QtGui
from renderer import GuiHandler

from PyFC import client_interface
import sys

if __name__ == "__main__":

	if (len(sys.argv) < 2) :
		print "No package name was given."
		exit(2)

	app = QtGui.QApplication(sys.argv)
	package = client_interface.PackageInterface()
	package.loadPackage(sys.argv[1])
	#try:
	#p.loadPackage()
	#except:
		#print "Freeconf library init failed!"
		#exit(-1)


	widget = GuiHandler(package)
	widget.show()
	app.exec_()