#!/usr/bin/python
#
# lists.py
# begin: 18.12.2010 by Jan Ehrenberger
#
# PyFC: String Lists and Fuzzy Lists
#

# Freeconf libs
from log import log
from constants import FcTypes


class FcList:
	"""Class for string lists and fuzzy lists."""
	def __init__ (self, name):
		self.name = name
		self.values = {}
	
	def append(self, entry):
		self.values[entry.value] = entry
	
	def contains(self, value):
		"""Return True/False if this list contains given value or not."""
		return value in self.values
	
	@property
	def entries(self):
		"""Return list of entries."""
		return self.values.values()
	
	def getEntry(self, value):
		"""Return entry by value."""
		try:
			return self.values[value]
		except KeyError:
			return None
	
	def getFirstEntry(self):
		"""Return first entry in the list. If there is no entry, return None"""
		if len(self.values) == 0:
			return None
		return self.values[min(self.values.keys())]


class FcStringList (FcList):
	"""Class for string lists."""

	class Entry:
		"""This class describes particular string for string config keyword. It
			 contains string value, optional label and help as an explanation for
			 meaning of given string value."""

		def __init__(self, value = "", label = "", help = ""):
			self.value = value
			self.help = help
			self.label = label

		def __repr__(self):
			return 'StringEntry(%s, "%s", "%s")' % (self.value, self.label, self.help)

	def __init__(self, name):
		FcList.__init__(self, name)
	
	@property
	def type(self):
		"""Return type of this list entries."""
		return FcTypes.STRING


class FcFuzzyList (FcList):
	"""Class for fuzzy lists."""

	class Entry:
		"""This class describes particular value for fuzzy config keyword."""

		def __init__(self, grade, value = "", label = "", help = ""):
			assert grade >= 0 and grade <= 1
			self.grade = grade
			self.value = value
			self.help = help
			self.label = label

		def __repr__(self):
			return 'FuzzyEntry(%s, "%s", "%s")' % (self.value, self.label, self.help)

	def __init__(self, name):
		FcList.__init__(self, name)
	
	@property
	def type(self):
		"""Return type of this list entries."""
		return FcTypes.FUZZY
	
	def getExactGrade(self, grade):
		"""Return entry with given grade. If such entry is not found, None is returned."""
		for e in self.values.values():
			if e.grade == grade:
				return e
		return None
	
	def getMaxGrade(self, maxGrade = 1.0):
		"""Return entry with maximum grade that is lower or equal than maxGrade."""
		maxEntry = None
		for e in self.values.values():
			if e.grade <= maxGrade and (maxEntry == None or maxEntry.grade < e.grade):
				maxEntry = e
		return maxEntry

	def getMinGrade(self, minGrade = 0.0):
		"""Return entry with minimum grade that is higher or equal than minGrade."""
		minEntry = None
		for e in self.values.values():
			if e.grade >= minGrade and (minEntry == None or minEntry.grade > e.grade):
				minEntry = e
		return minEntry
	
	def joinList(self, new_list):
		"""Add new value list to this fuzzy list."""
		for key in new_list.values:
			if not key in self.values:
				self.values[key] = new_list.values[key]
			else:
				# Key is already in the list. Check if it has same grade
				if new_list.values[key].grade != self.values[key].grade:
					log.error(
						"FcFuzzyList: joinList: Joining %s with %s. Value '%s' is already in the list but with different grade!"
						% (self.name, new_list.name, key)
					)
		return self


class FcBoolList(FcFuzzyList):
	"""Constant fuzzy list of bool's values."""

	def __init__(self):
		FcList.__init__(self, "bool")
		# Define bool values
		self.append(FcFuzzyList.Entry(0.0, "no", "No"))
		self.append(FcFuzzyList.Entry(1.0, "yes", "Yes"))
	

# Create constant list of boolean values
boolList = FcBoolList()

