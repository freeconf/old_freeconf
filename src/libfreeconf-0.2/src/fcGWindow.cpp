#include "fcGTab.h"
// #include "fcGSEntry.h"
#include "fcGWindow.h"

fcGWindow::fcGWindow ()
  :_minWidth(0),
  _minHeight(0),
  _maxWidth(0),
  _maxHeight(0),
  _title("Freeconf generated config dialog")
{
  
}


fcGWindow::~fcGWindow ()
{
  for (std::vector<fcGTab*>::iterator it = _tabs.begin(); it != _tabs.end(); it++)
  {
    delete *it;
  }
}


void fcGWindow::setTitle (const fcString &title)
{
  _title = title;
}


const fcString& fcGWindow::title () const
{
  return _title;
}


void fcGWindow::setMinWidth (fc_uint minWidth)
{
  _minWidth = minWidth;
  if (_maxWidth && minWidth > _maxWidth)
  {
    _minWidth = _maxWidth;
  }
}


fc_uint fcGWindow::minWidth () const
{
  return _minWidth;
}


void fcGWindow::setMinHeight (fc_uint minHeight)
{
  _minHeight = minHeight;
  if (_maxHeight && minHeight > _maxHeight)
  {
    _minHeight = _maxHeight;
  }
}


fc_uint fcGWindow::minHeight () const
{
  return _minHeight;
}


void fcGWindow::setMaxWidth (fc_uint maxWidth)
{
  _maxWidth = maxWidth;
  if (_minWidth && maxWidth < _minWidth)
  {
    _maxWidth = _minWidth;
  }
}


fc_uint fcGWindow::maxWidth () const
{
  return _maxWidth;
}


void fcGWindow::setMaxHeight (fc_uint maxHeight)
{
  _maxHeight = maxHeight;
  if (_minHeight && maxHeight < _minHeight)
  {
    _maxHeight = _minHeight;
  }
}


fc_uint fcGWindow::maxHeight () const
{
  return _maxHeight;
}


void fcGWindow::addTab (fcGTab *tab)
{
  _tabs.push_back(tab);
}


fcGTab* fcGWindow::lastTab () const
{
  if (_tabs.empty()) return 0;
  return _tabs.back();
}


fcGTab* fcGWindow::at (fc_uint index) const
{
  return _tabs[index];
}


fcGTab* fcGWindow::findTab (const fcString &tabName) const
{
  for (std::vector<fcGTab*>::iterator it = const_cast<fcGWindow*>(this)->_tabs.begin();
       it != const_cast<fcGWindow*>(this)->_tabs.end(); it++)
  {
    if ((*it)->name() == tabName) return *it;
  }
  return 0;
}


int fcGWindow::tabsCount () const
{
  return _tabs.size();
}


fc_bool fcGWindow::isEmpty () const
{
  return _tabs.empty();
}
