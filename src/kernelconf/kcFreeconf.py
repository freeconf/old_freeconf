#!/usr/bin/python
#
# Kernel Config Free conf classes
#
# begin: 12.11.2008 by Jan Ehrenberger <lankvil@seznam.cz>
#
#


class Element:
	name = None
	xml_name = None # XML name of element. Can be different from official name of element
	label = None
	default = None
	elt_type = 'element' # static variable describing xml element name
	
	def __init__(self, name):
		self.name = name
		self.xml_name = self.get_xml_name(name)
	
	def get_xml_name(self, name):
		if not name[0].isalpha():
			# Elements should start only with alphabetic characters
			return '_' + name
		return name
	
	def get_default_str(self):
		return self.default

	
class Keyword(Element):
	help = None
	dependency = None

	def accept(self, visitor):
		return visitor.visit(self)


class Bool(Keyword):
	elt_type = 'bool'

	def __init__(self, name):
		Element.__init__(self, name)

	def get_default_str(self):
		if self.default == True:
			return 'Yes'
		else:
			return 'No'
		


class Number(Keyword):
	type = None
	min = None
	max = None
	step = None
	elt_type = 'number'

	def __init__(self, name):
		Element.__init__(self, name)


class String(Keyword):
	elt_type = 'string'
	data = None

	def __init__(self, name, data = None):
		Element.__init__(self, name)
		self.data = data

class Tristate(String):
	def __init__(self, name):
		String.__init__(self, name, "tristate")


class Section(Element):
	content = None
	multiple = False
	elt_type = 'section'

	def __init__(self, name):
		Element.__init__(self, name)
		self.content = []
	
	def append(self, o):
		self.content.append(o)
	def concat(self, l):
		self.content = self.content + l

	def accept(self, visitor):
		if visitor.enter(self):
			for elt in self.content:
				elt.accept(visitor)
		return visitor.leave(self)
			

