/***************************************************************************
                          gtkCKNumber.cpp  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <gtk/gtk.h>
#include <fcTKNumber.h>
#include "gtkCKNumber.h"
#include "gtkCSEntry.h"
#include "debug.h"
#include "widget.h"

//--------------------------------------------------------------------------
static void cb_number_set( GtkWidget *widget,
                           gtkCKNumberStructure* struc )
{
   DBG_FUNCTION_NAME( "", "gtk_fc_number_set" );
   assert( (struc -> entry) -> Type() == fcNUMBER );
   DBG_EXPR( struc -> entry -> GetEntryName() );
   DBG_EXPR( struc -> entry -> GetValue() );
   fc_real number = gtk_spin_button_get_value_as_float( GTK_SPIN_BUTTON( widget ) );
   //gdouble number = gtk_spin_button_get_value( GTK_SPIN_BUTTON( widget ) );
   DBG_COUT( "NUMBER value set to '" << number << "'" );
   struc -> entry -> SetValue( number );
   for( fc_int i = 0; i < ((fcCEntry*)(struc->entry))->GetDependentsSize() ; i++) {
       fcCEntry* c_entry = ((fcCEntry*)(struc->entry))->GetDependents( i );
       fc_bool isSensitive =  c_entry -> CEntryIsActive( struc->main_entry );
       compileEntry( isSensitive, c_entry );
   }
}
//--------------------------------------------------------------------------
gtkCKNumber :: gtkCKNumber() {
    struc = new gtkCKNumberStructure();
}
//--------------------------------------------------------------------------
gtkCKNumber :: gtkCKNumber( const fcCKNumber& ck_number )
   : fcCKNumber( ck_number )
{
    struc = new gtkCKNumberStructure();
}
//--------------------------------------------------------------------------
gtkCKNumber :: gtkCKNumber( const fcTEntry* t_entry )
   : fcCKNumber( t_entry )
{
    struc = new gtkCKNumberStructure();
}
//--------------------------------------------------------------------------
gtkCKNumber :: ~gtkCKNumber() {
    if( struc ) 
        delete struc;
}
//--------------------------------------------------------------------------
void gtkCKNumber :: Render( GtkWidget* parent_widget,
                          GtkTooltips* tooltips,
                          const fc_char* help,
                          fcCSEntry* main_c_section )
{
   DBG_FUNCTION_NAME( "gtkCKNumber", "Render" );
   const fcTKNumber& t_entry = * ( fcTKNumber* ) GetTEntry();
   const fc_char* label = t_entry. GetLabel(). Data();
   GtkWidget* hbox = gtk_hbox_new( FALSE, 0 );
   gtk_box_pack_start( GTK_BOX( parent_widget ),
                       hbox,
                       false,
                       false,
                       0 );
   
   GtkWidget* label_widget = gtk_label_new( label );
   gtk_label_set_justify( GTK_LABEL( label_widget ),
                          GTK_JUSTIFY_LEFT );
   
   fc_real x = GetValue();
   fc_real min = t_entry. GetMin();
   fc_real max = t_entry. GetMax();
   fc_real step = t_entry. GetStep();
   
   GtkObject* adjustment = gtk_adjustment_new( 
                                    x,
                                    min,
                                    max,
                                    step,   // step increment
                                    step,   // page increment
                                    0 ); // page size
   
   GtkWidget* value_widget = gtk_spin_button_new( 
                                GTK_ADJUSTMENT( adjustment ),
                                0.5,
                                4 );
    
    this -> SetWidget( value_widget );
    fcCEntry* current_entry = (fcCEntry*)(this);
    fc_bool isSensitive = current_entry -> CEntryIsActive( main_c_section );
    compileEntry( isSensitive, current_entry );

    struc -> entry = this;
    struc -> main_entry = main_c_section;

   gtk_box_pack_start( GTK_BOX( hbox ),
                       label_widget,
                       false,
                       false,
                       10 );
   
   gtk_box_pack_start( GTK_BOX( hbox ),
                       value_widget,
                       true,
                       true,
                       10 );
   
   gtk_signal_connect( GTK_OBJECT( value_widget ),
                       "changed",
                       GTK_SIGNAL_FUNC( cb_number_set ),
                       struc );
   // Set tooltips
   DBG_COUT( " setting tooltips to ... " << t_entry. GetHelp() );
   gtk_tooltips_set_tip( tooltips,
                         value_widget,
                         help,
                         NULL );
}
//--------------------------------------------------------------------------
void gtkCKNumber :: SetWidget( GtkWidget* _widget ) {
    widget = _widget;
}
//--------------------------------------------------------------------------
GtkWidget* gtkCKNumber :: GetWidget() {
    return widget; 
}
