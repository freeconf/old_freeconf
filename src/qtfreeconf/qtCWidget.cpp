#include <QBoxLayout>
#include <fcCGEntry.h>

#include "qtCWidget.h"

qtCWidget::qtCWidget (fcPackage *package, const qtCWidget *parent)
  :_package(package),
   _parent(parent), 
   _layout(0),
   _guiEntry(0),
   _attached(true),
   _rendered(false)
{
  if (!package && parent) _package = parent->package();
}

qtCWidget::qtCWidget (const fcCGEntry *guiEntry, fcPackage *package, const qtCWidget *parent)
  :_package(package),
   _parent(parent),
   _layout(0),
   _guiEntry(guiEntry),
   _attached(true),
   _rendered(false)
{
  if (!package && parent) _package = parent->package();
}


qtCWidget::~qtCWidget ()
{

}


void qtCWidget::setPackage (fcPackage *package)
{
  _package = package;
}


fcPackage* qtCWidget::package () const
{
  return _package;
}


void qtCWidget::setGuiEntry (fcCGEntry *guiEntry)
{
  _guiEntry = guiEntry;
}


const fcCGEntry* qtCWidget::guiEntry () const
{
  return _guiEntry;
}


void qtCWidget::setFcParent (const qtCWidget *parent)
{
  _parent = parent;
}


const qtCWidget* qtCWidget::fcParent () const
{
  return _parent;
}


void qtCWidget::setFcLayout (QBoxLayout *layout)
{
  _layout = layout;
}


QBoxLayout* qtCWidget::fcLayout () const
{
  return _layout;
}


fc_bool qtCWidget::attached () const
{
  return _attached;
}


fc_bool qtCWidget::rendered () const
{
  return _rendered;
}


void qtCWidget::attach (fc_bool attached)
{
  if (!_guiEntry) return;
  _attached = attached;
}
