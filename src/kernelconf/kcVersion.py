#!/usr/bin/python
#
# Kernel Config Parser
#
# begin: 27.10.2008 by Jan Ehrenberger <lankvil@seznam.cz>
#
# Extract kernel version from Makefile
#


class Makefile:
	
	def __init__(self, filename):
		self.filename = filename
		self.variables = {}

	def parse_var(self, line, names):
		"""Parse line and if it contains desired variable(it's name is listed in names), then the variable and it's value will be stored."""
		if line.find('=') == -1: return
		a = line.split('=', 1)
		if len(a) != 2: return
		name = a[0].strip()
		if name in names:
			var = a[1].strip()
			self.variables[name] = var

	def parse(self, names):
		f = open(self.filename)
		try:
			while 1:
				l = f.readline()
				if not l: break
				self.parse_var(l, names)
		finally:
			f.close()


class Version:
	# Default values
	major = 2
	patchlevel = 6
	sublevel = 1
	extraversion = ""
	name = "" # Kernel release name

	def __init__(self, path):
		self.makefile = Makefile(path + '/Makefile')
		self.makefile.parse(('VERSION', 'PATCHLEVEL', 'SUBLEVEL', 'EXTRAVERSION', 'NAME'))		
	
		try:	
			self.major = int(self.makefile.variables['VERSION'])
			self.patchlevel = int(self.makefile.variables['PATCHLEVEL'])
			self.sublevel = int(self.makefile.variables['SUBLEVEL'])
			self.extraversion = self.makefile.variables['EXTRAVERSION']
			self.name = self.makefile.variables['NAME']
		except KeyError:
			pass

	def __str__(self):
		return "%d.%d.%d%s" % (self.major, self.patchlevel, self.sublevel, self.extraversion)

