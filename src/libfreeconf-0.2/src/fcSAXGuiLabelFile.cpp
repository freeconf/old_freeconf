/***************************************************************************
author           : 2009 by David Fabian
***************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/


#include "fcMessageHandler.h"
#include "fcCGSEntry.h"
#include "fcGWindow.h"
#include "fcGTab.h"
#include "fcSAXGuiLabelFile.h"


fcSAXGuiLabelFile::fcSAXGuiLabelFile()
  :_rootElementOpened(false),
   _currentTab(0),
   _sectionElementOpened(false),
   _currentElement(NoElement)
{
  
}


fcSAXGuiLabelFile::~fcSAXGuiLabelFile()
{
  
}



void fcSAXGuiLabelFile::startElement(const XMLCh* const uri,
                                    const XMLCh* const localName,
                                    const XMLCh* const qName,
                                    const Attributes& attributes)
{
  const fc_char *const elementName = XMLString::transcode(localName);
  
  if(!_rootElementOpened && strcmp(elementName, "freeconf-gui-label") == 0)
  {
    //       DBG_COUT( "Freeconf-gui tag." );
    _rootElementOpened = true;
    _currentElement = RootElement;
    return;
  }
  
  if(!_rootElementOpened)
  {
    MessageHandler.Error("fcSAXGuiLabelFile.cpp", "startElement",
                         "You must enclose the Gui label file with <freeconf-gui-label> and </freeconf-gui-label>.");
    return;
  }
  
  if(!strcmp(elementName, "window"))
  {
    //       MessageHandler.Message("window start");
    _currentElement = Window;
    return;
  }
  
  if(!strcmp(elementName, "title"))
  {
   //        MessageHandler.Message("title start");
    _currentElement = Title;
    return;
  }

  if(!strcmp(elementName, "tab"))
  {
   //        MessageHandler.Message("tab start");
    _sectionStack.EraseAll();
    _currentElement = Tab;
    _currentTab = 0;
    return;
  }

  if(!strcmp(elementName, "tab-name"))
  {
    if (_currentElement != Tab)
    {
      MessageHandler.Warning("Element <tab-name> is only allowed as the first child element of the <tab> element");
      return;
    }
    //       MessageHandler.Message("tab-name start");
    _currentElement = TabName;
    return;
  }

  if(!strcmp(elementName, "tab-label"))
  {
    if (_currentElement == Tab)
    {
      MessageHandler.Warning("Element <tab-name> must be first child element of the <tab> element");
      return;
    }

    if (!_currentTab)
    {
      MessageHandler.Warning("Element <tab-label> is only allowed inside the <tab> element");
      return;
    }
    //       MessageHandler.Message("tab-label start");
    _currentElement = TabLabel;
    return;
  }

  if(!strcmp(elementName, "tab-description"))
  {
    if (_currentElement == Tab)
    {
      MessageHandler.Warning("Element <tab-name> must be first child element of the <tab> element");
      return;
    }

    if (!_currentTab)
    {
      MessageHandler.Warning("Element <tab-description> is only allowed inside the <tab> element");
      return;
    }
    //       MessageHandler.Message("tab-description start");
    _currentElement = TabDescription;
    return;
  }

  if(!strcmp(elementName, "tab-section"))
  {
    if (_currentElement == Tab)
    {
      MessageHandler.Warning("Element <tab-name> must be first child element of the <tab> element");
      // temp value, signaling new section entry
      // need to be done, end element always removes section from the stack
      _sectionStack.Add(0);
      return;
    }
    
    if (!_currentTab)
    {
      MessageHandler.Warning("Element <tab-section> is only allowed inside the <tab> element");
      // temp value, signaling new section entry
      // need to be done, end element always removes section from the stack
      _sectionStack.Add(0);
      return;
    }

    if (!_sectionStack.Top())
    {
      MessageHandler.Warning("Found <tab-section> child element before it's parent's <tab-section-name>");
      // temp value, signaling new section entry
      // need to be done, end element always removes section from the stack
      _sectionStack.Add(0);
      return;
    }
    //       MessageHandler.Message("tab-section start");
    // temp value, signaling new section entry
    // need to be done, end element always removes section from the stack
    _sectionStack.Add(0);
    _currentElement = TabSection;
    _sectionElementOpened = true;
    return;
  }
  
  if(!strcmp(elementName, "tab-section-name"))
  {
    // at the top of the stack must be 0 when tab-section-name is found
    if (!_sectionElementOpened || _sectionStack.Top())
    {
      MessageHandler.Warning("Element <tab-section-name> is only allowed as the first child element of the <tab-section> element");
      return;
    }
    //       MessageHandler.Message("tab-section-name start");
    _currentElement = TabSectionName;
    return;
  }

  if(!strcmp(elementName, "tab-section-label"))
  {
    // at the top of the stack must be valid tab-section
    if (!_sectionElementOpened || !_sectionStack.Top())
    {
      MessageHandler.Warning("Element <tab-section-label> is only allowed inside the <tab-section> element");
      return;
    }
    //       MessageHandler.Message("tab-section-label start");
    _currentElement = TabSectionLabel;
    return;
  }

}


void fcSAXGuiLabelFile::endElement(const XMLCh* const uri,
                                  const XMLCh* const localName,
                                  const XMLCh* const qName)
{
  const fc_char *const elementName = XMLString::transcode(localName);
  
  if (!strcmp(elementName, "freeconf-gui-label"))
  {
    //     MessageHandler.Message("window end");
    _currentElement = NoElement;
    _rootElementOpened = false;
    return;
  }
  
  if (!strcmp(elementName, "window"))
  {
    //     MessageHandler.Message("window end");
    _currentElement = NoElement;
    return;
  }
  
  if (!strcmp(elementName, "title"))
  {
    if (_currentElement == Title)
    {
      //       MessageHandler.Message("title end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiLabelFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }

  if (!strcmp(elementName, "tab"))
  {
    //       MessageHandler.Message("tab end");
    _currentElement = NoElement;
    _currentTab = 0;
    return;
  }

  if (!strcmp(elementName, "tab-name"))
  {
    if (_currentElement == TabName)
    {
      //       MessageHandler.Message("tab-name end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiLabelFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }

  if (!strcmp(elementName, "tab-label"))
  {
    if (_currentElement == TabLabel)
    {
      //       MessageHandler.Message("tab-description end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiLabelFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }

  if (!strcmp(elementName, "tab-description"))
  {
    if (_currentElement == TabDescription)
    {
      //       MessageHandler.Message("tab-description end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiLabelFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }
  
  if (!strcmp(elementName, "tab-section"))
  {
    _currentElement = NoElement;
    _sectionElementOpened = false;
    _sectionStack.Remove();
    return;
  }

  if (!strcmp(elementName, "tab-section-name"))
  {
    if (_currentElement == TabSectionName)
    {
      //       MessageHandler.Message("tab-name end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiLabelFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }

  if (!strcmp(elementName, "tab-section-label"))
  {
    if (_currentElement == TabSectionLabel)
    {
      //       MessageHandler.Message("tab-name end");
      _currentElement = NoElement;
      return;
    }
    MessageHandler.Warning("fcSAXGuiLabelFile.cpp", "endElement", fcString("Wrong location of end element: ") + fcString(elementName));
    return;
  }

}


void fcSAXGuiLabelFile::characters(const XMLCh* const chars, const unsigned int length)
{
  const fc_char *data = XMLString::transcode(chars);
  
  if (_currentElement == Title)
  {
    //   //  MessageHandler.Message("Filling title");
    _window->setTitle(data);
    return;
  }

  if (_currentElement == TabName)
  {
  //   finding tab
     _currentTab = _window->findTab(data);
     if (!_currentTab)
     {
       MessageHandler.Warning(fcString("Tab element '") + data + "' not found in the gui tree");
       return;
     }
     _sectionStack.Add(_currentTab->content());
    return;
  }

  if (_currentElement == TabLabel)
  {
     if (!_currentTab) return;
     _currentTab->setLabel(data);
    return;
  }

  if (_currentElement == TabDescription)
  {
     if (!_currentTab) return;
     _currentTab->setDescription(data);
     return;
  }

  if (_currentElement == TabSectionName)
  {
     if (_sectionStack.Top()) return;
     _sectionStack.Remove();
     fcCGEntry *entry = _sectionStack.Top()->findChild(data);
     if (!entry)
     {
       MessageHandler.Warning(fcString("Section element '") + data + "' not found in the gui tree");
       _sectionStack.Add(0);
       return;
     }
     fcCGSEntry *section;
     if (section = dynamic_cast<fcCGSEntry*>(entry))
     {
       _sectionStack.Add(section);
     }
     else
     {
       MessageHandler.Warning(fcString("Element '") + data + "' is not a section");
       return;
     }
    return;
  }

  if (_currentElement == TabSectionLabel)
  {
     if (!_sectionStack.Top()) return;
     _sectionStack.Top()->setLabel(data);
     return;
  }


}


fc_bool fcSAXGuiLabelFile::ParseGuiLabelFile(const fc_char* fileName, fcGWindow* window)
{
  _rootElementOpened = false;
  _currentTab = false;
  _sectionElementOpened = false;
  _currentElement = NoElement;
  _sectionStack.EraseAll();
  if (!window)
  {
    MessageHandler.Error("Invalid pointer found, expecting existing window object");
    return false;
  }

  _window = window;
  return Parse(fileName);
}