/***************************************************************************
                          fcCKString.cpp  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "fcCKString.h"

//--------------------------------------------------------------------------
fcCKString :: fcCKString()
{
}
//--------------------------------------------------------------------------
fcCKString :: fcCKString( const fcCKString& entry  )
   : fcCKEntry( entry ),
     value( entry. value )
{
}
//--------------------------------------------------------------------------
fcCKString :: ~fcCKString()
{
}
//--------------------------------------------------------------------------
fcBaseEntry* fcCKString :: NewEntry() const
{
   return new fcCKString( * this );
}
//--------------------------------------------------------------------------
Fc::EntryTypes fcCKString :: Type() const
{ 
   return Fc::String;
}
//--------------------------------------------------------------------------
const fcString& fcCKString :: GetValue() const 
{ 
   return value;
}
//--------------------------------------------------------------------------
void fcCKString :: SetValue( const fc_char* val )
{
   value = val;
   value_set = true;
}
//--------------------------------------------------------------------------
#ifdef DEBUG
void fcCKString :: Print( ostream& stream ) const
{
   stream << "STRING " << GetEntryName() << " = " << value;
}
#endif
