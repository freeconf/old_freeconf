/***************************************************************************
                          fcCSEntry.h  -  description
                             -------------------
    begin                : 2004/08/12
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCCSENTRY_H__
#define __FCCSENTRY_H__

#include "fcBaseEntry.h"
#include "fcSectionEntry.h"
#include "fcCKEntry.h"
#include "fcList.h"

class fcTSEntry;
class fcCEntry;
class fcTemplateStructure;

//! Class for groups/sections in config files
class fcCSEntry : public fcCEntry, public fcSectionEntry
{

   public:

   //! Basic constructor
   fcCSEntry();

   //! Copy constructor
   fcCSEntry( const fcCSEntry& section_entry );

   //! Constructor template entry pointer
   fcCSEntry( const fcTEntry* t_entry );

   //! Destructor
   ~fcCSEntry(){};
   
   //! Allocate new instance and return pointer
   virtual fcBaseEntry* NewEntry() const;
   
   //! Return section entry pointer
   fcSectionEntry* GetSectionEntryPointer()
      { return ( fcSectionEntry* ) this; }  

   //! Entry type getter
   Fc::EntryTypes Type() const;

   //! Returns true if it is a section
   fc_bool IsSection() const { return true; };

   //! Returns true if it is a kewyord
   fc_bool IsKeyword() const { return false; };

   //! Returns true if it is a template entry
   //fc_bool IsTEntry() const { return false; };
  
   //! Returns true if it is a multiple config entries conatianer
   virtual fc_bool IsMultipleEntryContainer() const;

   //! Creates new config entry by the type of template entry
   virtual fcCEntry* CreateCEntry( const fcTEntry* t_entry, fc_bool ignore_multiple = false ) const;

   //! Allocating entries by given template section
   fc_bool Init( const fcTEntry* t_entry );

   //! Indexing operator with re-typing
   fcCEntry* operator[] ( fc_uint pos );

   //! Find entry with given name starting at given position
   /*! If the entry is found 'pos' is set to its position
    */
   fcCEntry* FindCEntry( const fc_char* entry_name,
                         fc_uint& pos );
   
   //! Find entry with given name
   fcCEntry* FindCEntry( const fc_char* entry_name );

   //! Recursive find entry with full name
   /*! Full name is name with form that is similar to XPath language
    *  (e.g. /freeconf/section/entry )
    */
   fcCEntry* RecursiveFindCEntry( const fc_char* entry_full_name );
   fcCEntry* RecursiveFindCEntry( const fcString& entry_full_name );
  
   //! Return if the given entry should be visible or not 
   /*! This function returns true, if the given entry should 
    *  be visible in the UI and return false if not.
    */
   //fc_bool CEntryIsVisible( fcCEntry* c_entry );

   //! Set the pointers of dependent entries to this entry
   /*! This function set to the current entry list of pointers
    *  on the dependent entries
    */
   //void SetDependencyPointers( fcCSEntry* main_c_section );

   //! Return section entry pointer
   const fcSectionEntry* GetSectionEntryPointer() const
      { return ( fcSectionEntry* ) this; }  

   //! Indexing operator for constant instances
   const fcCEntry* operator[] ( fc_uint pos ) const;
   
   //! Find entry with given name starting at given position for constant instances
   const fcCEntry* FindCEntry( const fc_char* name,
                               fc_uint& pos ) const;

   //! Find entry with given name for constant instances
   const fcCEntry* FindCEntry( const fc_char* name ) const;

   //! Update DOM nodes
   void UpdateDOMNode( fcDOMNode* parent_node,
                       fcDOMDocument* doc );
   
#ifdef DEBUG
   //! Print this section
   void Print( ostream& stream ) const;
#endif

   friend std::ostream& operator<<(std::ostream& stream, const fcCSEntry& c_section);
};

#endif
