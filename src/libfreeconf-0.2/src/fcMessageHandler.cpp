/***************************************************************************
                          fcMessageHandler.cpp  -  description
                             -------------------
    begin                : 2004/10/21
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include "fcMessageHandler.h"
#include "fcString.h"

fcMessageHandler MessageHandler;

//--------------------------------------------------------------------------
fcMessageHandler :: fcMessageHandler() :
   messages_num( 0 ), warnings_num( 0 ), errors_num( 0 )
{
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Message( const fc_char* message )
{
   std::cout << "MSG: " << ++ messages_num
        << " " << message << std::endl;
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Message( const fc_char* file, const fc_char* function, const fc_char* message )
{
   std::cout << "MSG: " << ++ messages_num
        << " " << file << "::" << function << " \"" << message << "\"" << std::endl;
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Message( const fc_char* file, fc_uint line, const fc_char* message )
{
   std::cout << "MSG: " << ++ messages_num
        << " " << file << ":" << line << " \"" << message << "\"" << std::endl;
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Message( const fcString& message )
{
   std::cout << "MSG: " << ++ messages_num
        << " " << message << std::endl;
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Message( const fcString& file, const fcString& function, const fcString& message )
{
   std::cout << "MSG: " << ++ messages_num
        << " " << file << "::" << function << " \"" << message << "\"" << std::endl;
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Message( const fcString& file, fc_uint line, const fcString& message )
{
   std::cout << "MSG: " << ++ messages_num
        << " " << file << ":" << line << " \"" << message << "\"" << std::endl;
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Warning( const fc_char* warning )
{
   std::cout << "WRN: " << ++ warnings_num
        << " " << warning << std::endl;
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Warning( const fc_char* file, const fc_char* function, const fc_char* warning )
{
   std::cout << "WRN: " << ++ warnings_num
        << " " << file << "::" << function << " \"" << warning  << "\"" << std::endl;
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Warning( const fc_char* file, fc_uint line, const fc_char* warning )
{
   std::cout << "WRN: " << ++ warnings_num
        << " " << file << ":" << line << " \"" << warning  << "\"" << std::endl;
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Warning( const fcString& warning )
{
   std::cout << "WRN: " << ++ warnings_num
        << " " << warning << std::endl;
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Warning( const fcString& file, const fcString& function, const fcString& warning )
{
   std::cout << "WRN: " << ++ warnings_num
        << " " << file << "::" << function << " \"" << warning  << "\"" << std::endl;
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Warning( const fcString& file, fc_uint line, const fcString& warning )
{
   std::cout << "WRN: " << ++ warnings_num
        << " " << file << ":" << line << " \"" << warning  << "\"" << std::endl;
   return true;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Error( const fc_char* error )
{
   std::cerr << "ERR: " << ++ errors_num
        << " " << error << std::endl;
   return false;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Error( const fc_char* file, const fc_char* function, const fc_char* error )
{
   std::cerr << "ERR: " << ++ errors_num
        << " " << file << "::" << function << " \"" << error << "\"" << std::endl;
   return false;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Error( const fc_char* file, fc_uint line, const fc_char* error )
{
   std::cout << "ERR: " << ++ errors_num
        << " " << file << ":" << line << " \"" << error << "\"" << std::endl;
   return false;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Error( const fcString& error )
{
   std::cerr << "ERR: " << ++ errors_num
        << " " << error << std::endl;
   return false;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Error( const fcString& file, const fcString& function, const fcString& error )
{
   std::cerr << "ERR: " << ++ errors_num
        << " " << file << "::" << function << " \"" << error << "\"" << std::endl;
   return false;
}
//--------------------------------------------------------------------------
fc_bool fcMessageHandler :: Error( const fcString& file, fc_uint line, const fcString& error )
{
   std::cout << "ERR: " << ++ errors_num
        << " " << file << ":" << line << " \"" << error << "\"" << std::endl;
   return false;
}
//--------------------------------------------------------------------------
fc_uint fcMessageHandler :: GetMessagesNumber()
{
   return messages_num;
}
//--------------------------------------------------------------------------
fc_uint fcMessageHandler :: GetWarningsNumber()
{
   return warnings_num;
}
//--------------------------------------------------------------------------
fc_uint fcMessageHandler :: GetErrorsNumber()
{
   return errors_num;
}
//--------------------------------------------------------------------------
void fcMessageHandler :: Init()
{
   messages_num = warnings_num = errors_num = 0;
}
//--------------------------------------------------------------------------
