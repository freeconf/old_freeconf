/***************************************************************************
                          fcBaseEntry.cpp  -  description
                             -------------------
    begin                : 2004/04/10 16:58
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "debug.h"
#include "fcBaseEntry.h"
#include "fcTypes.h"

//---------------------------------------------------------------------------
fcBaseEntry :: fcBaseEntry()
{
}
//---------------------------------------------------------------------------
fcBaseEntry :: fcBaseEntry( const fcBaseEntry& entry )
   : entry_name( entry. entry_name )
{
}
//---------------------------------------------------------------------------
fcBaseEntry :: fcBaseEntry( const fc_char* name )
   : entry_name( name )
{
}
//---------------------------------------------------------------------------
fcBaseEntry :: ~fcBaseEntry()
{
}
//---------------------------------------------------------------------------
void fcBaseEntry :: SetEntryName( const fc_char* str )
{
   DBG_FUNCTION_NAME( "fcBaseEntry", "SetEntryName" );
   DBG_COUT( "Setting entry name to " << str );

   entry_name. SetString( str );
}
//---------------------------------------------------------------------------
const fcString& fcBaseEntry :: GetEntryName() const
{
   return entry_name;
}
//---------------------------------------------------------------------------
fcSectionEntry* fcBaseEntry :: GetSectionEntryPointer()
{
  return 0;
}
//---------------------------------------------------------------------------
