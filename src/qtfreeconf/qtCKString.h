#ifndef _QT_CKSTRING_H
#define _QT_CKSTRING_H

#include <QWidget>
#include "qtCWidget.h"

class QComboBox;
class QLineEdit;

class qtCKString: public qtCWidget
{
  Q_OBJECT
  
  private:
    //! is string list  or normal string keyword?
    fc_bool _dropDown;
    //! pointer to Qt widget
    QComboBox *_combo;
    //! pointer to Qt widget
    QLineEdit *_line;
    
  public:
    //! constructor
    explicit qtCKString (fcPackage *package = 0, const qtCWidget *parent = 0);
    //! constructor
    explicit qtCKString (const fcCGEntry *guiEntry, fcPackage *package = 0, const qtCWidget *parent = 0);
    

    //! render function
    virtual void render ();
    //! removes any Qt visual widgets
    virtual void invalidateGui ();
    
  public slots:
    //! connects widget with GUI tree
    virtual void attach (fc_bool attached);
  protected slots:
    //! textwidget has changed it's text
    void textChanged (const QString &text);
    
};

#endif