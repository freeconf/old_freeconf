/***************************************************************************
                          fcSAXFile.cpp  -  description
                             -------------------
    begin                : 2004/04/11 11:37
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include <iostream>
#include <xercesc/sax2/SAX2XMLReader.hpp>
#include <xercesc/sax2/XMLReaderFactory.hpp>
#include "fcSAXFile.h"
#include "xmlchstream.h"
#include "debug.h"

//--------------------------------------------------------------------------
fcSAXFile :: fcSAXFile() :
   errors( 0 )
{
}
//--------------------------------------------------------------------------
fcSAXFile :: ~fcSAXFile()
{
}
//--------------------------------------------------------------------------
fc_bool fcSAXFile :: Parse( const fc_char* file_name )
{
   DBG_FUNCTION_NAME( "fcSAXFile", "Parse" );
   // Initialize the XML4C2 system
   try
   {
        XMLPlatformUtils::Initialize();
   }

   catch (const XMLException& toCatch)
   {
        std::cerr << "Error during initialization of SAX2 parser! :\n"
             << toCatch. getMessage() << std::endl;
        return false;
   }

   //  Create a SAX parser object.
   SAX2XMLReader* parser = XMLReaderFactory::createXMLReader();

   parser -> setFeature( XMLString :: transcode( "http://xml.org/sax/features/validation" ), false );

   //
   //  Create the handler object and install it as the document and error
   //  handler for the parser. Then parse the file and catch any exceptions
   //  that propogate out
   //
   
   //const char* encodingName = "ISO8859-1";
   Errors() = 0;         //it is equivalent to errors = 0
   try
   {
       parser -> setContentHandler( this );
       parser -> setErrorHandler( this );
       parser -> parse( file_name );
   }

   catch (const XMLException& toCatch)
   {
       std::cerr << "\nAn error occured\n  Error: "
            << toCatch. getMessage()
            << "\n" << std::endl;
       XMLPlatformUtils :: Terminate();
       return 0;
   }
 
   DBG_EXPR( Errors() );
   if( ! Errors() )
      return true;
   return false;
}
//--------------------------------------------------------------------------
void fcSAXFile :: warning( const SAXParseException& exception )
{
    std::cerr << "\nWarning at file " << StrX( exception. getSystemId() )
                 << ", line " << exception. getLineNumber()
                 << ", char " << exception. getColumnNumber()
         << "\n  Message: " << StrX( exception. getMessage() ) << std::endl;
}
//--------------------------------------------------------------------------
void fcSAXFile :: error( const SAXParseException& exception )
{
    std::cerr << "\nError at file " << StrX( exception. getSystemId() )
                 << ", line " << exception. getLineNumber()
                 << ", char " << exception. getColumnNumber()
         << "\n  Message: " << StrX( exception. getMessage() ) << std::endl;
    errors ++;
}
//--------------------------------------------------------------------------
void fcSAXFile :: fatalError( const SAXParseException& exception )
{
    std::cerr << "\nFatal Error at file " <<  StrX( exception. getSystemId() )
                 << ", line " << exception. getLineNumber()
                 << ", char " << exception. getColumnNumber()
         << "\n  Message: " << StrX( exception. getMessage() ) << std::endl;
    errors ++;
}

