/***************************************************************************
                          fcDOMConfigFile.cpp  -  description
                             -------------------
    begin                : 2004/08/12
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include <iostream>
#include <cstring>
#include <xercesc/dom/DOMBuilder.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOMImplementationRegistry.hpp>
#include <xercesc/dom/DOMWriter.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/util/XMLChar.hpp>
#include <xercesc/util/XMLUni.hpp>
#include "fcDOMConfigFile.h"
#include "xmlchstream.h"
#include "fcCSEntry.h"
#include "fcDOMErrorHandler.h"
#include "fcMessageHandler.h"
#include "fcTKEntry.h"
#include "fcTSEntry.h"
#include "fcCKBool.h"
#include "fcCKList.h"
#include "fcCKNumber.h"
#include "fcCKString.h"
#include "fcCSEntry.h"
#include "debug.h"

static const XMLCh  gUTF8[] =
{
    chLatin_U, chLatin_T, chLatin_F, chDash, chDigit_8, chNull
};
//--------------------------------------------------------------------------
fcDOMConfigFile :: fcDOMConfigFile()
   : config_section( 0 ),
     config_file( 0 )
{
}
//--------------------------------------------------------------------------
fcDOMConfigFile::fcDOMConfigFile (fcCSEntry *c_section)
  :config_section(c_section),
   config_file(0)
{
}
//--------------------------------------------------------------------------
fcDOMConfigFile :: ~fcDOMConfigFile()
{
   if( config_file )
      config_file -> release();
}
//--------------------------------------------------------------------------
void fcDOMConfigFile :: ProcessElementNode( fcDOMNode* node, fcCSEntry* c_section,
                                            const fcTSEntry* t_section,
                                            fc_bool default_setting )
{
   DBG_FUNCTION_NAME( "fcDOMConfigFile", "ProcessElementNode" );  
   const fc_char *name = XMLString :: transcode( node -> getNodeName() );
 
   DBG_COUT( "Current config section is " << c_section -> GetEntryName() );
   DBG_COUT( "Processing node " << name );

   // find given entry
   fcCEntry* c_entry = dynamic_cast<fcCEntry*>(c_section-> FindEntry(name));

   // check if everything is ok
   if( ! c_entry )
   {
      MessageHandler. Error( 
            fcString( "There is no entry " ) +
            fcString( name ) +
            fcString( " defined in the template file." ) );
      return;
   }
   
   // first handle multiple entries
   if( c_entry -> IsSection() && dynamic_cast<fcCSEntry*>(c_entry) -> IsMultipleEntryContainer() )
   {
      // assert( c_entry -> IsSection() );
      fcCSEntry* cont = dynamic_cast<fcCSEntry*>(c_entry);
      const fcTEntry* tmp_entry = c_entry -> GetTEntry();
      assert( tmp_entry );
      // we must create new entry
      fcCEntry* new_entry;
      DBG_EXPR( tmp_entry );
      // CreateCEntry in fcCSEntry is virtual 
      new_entry = config_section -> CreateCEntry( tmp_entry, true );
      if( ! new_entry ) return;
      assert( new_entry -> IsSection() );
      dynamic_cast<fcCSEntry*>(new_entry) -> Init( tmp_entry );
      // insert it in the curent config section serving as a
      // container for the multiple entries
      cont -> Append( new_entry );
     
      // finish this method with the new entry
      c_entry = new_entry;
   }
   
   if( ! default_setting )
     c_entry -> SetDOMNode( node );
   if( c_entry -> IsSection() )
   {
      DBG_COUT( "Processing config section " << name );
      
      // in the case of section procced further recursively
      fcCSEntry* new_c_section = ( fcCSEntry* ) c_entry;  
      ProcessDOMNode( node -> getFirstChild(),
                      new_c_section,
                      ( const fcTSEntry* ) t_section,
                      default_setting );
      return;
   }
   assert( c_entry -> IsKeyword() );
   // and for the case of keyword set its value
   fcCKEntry* new_ck_entry = dynamic_cast<fcCKEntry*>(c_entry);
   SetKeywordValue( node -> getFirstChild(), 
                    new_ck_entry ); 
}                 
//--------------------------------------------------------------------------
fc_bool fcDOMConfigFile :: ProcessDOMNode( fcDOMNode* node,
                                           fcCSEntry* c_section,
                                           const fcTSEntry* t_section,
                                           fc_bool default_setting )
{
   DBG_FUNCTION_NAME( "fcDOMConfigFile", "ProcessDOMNode" );  
   
   const fcTEntry* t_entry;
   fcCSEntry* tmp_section_entry;
   while( node )
   {
      switch( node -> getNodeType() )
      {
         case DOMNode :: DOCUMENT_NODE:

            DBG_COUT( "Omitting document node... " );
            
            if( ! default_setting )
              c_section -> SetDOMNode( node );
            ProcessDOMNode( node -> getFirstChild(),
                            c_section,
                            t_section,
                            default_setting );
            break;

         case DOMNode :: ELEMENT_NODE:
            ProcessElementNode( node,
                                c_section,
                                t_section,
                                default_setting );
            break;
         default:
            break;
      }
      node = node -> getNextSibling();
   }
   return 1;
}
//--------------------------------------------------------------------------
void fcDOMConfigFile :: SetKeywordValue( DOMNode* node,
                                         fcCKEntry* entry )
{
   DBG_FUNCTION_NAME( "fcDOMConfigFile", "SetKeywordValue" );  
   if( ! node || node -> getNodeType() != DOMNode :: TEXT_NODE )
   {
      MessageHandler. Warning( "A config value expected!" );
      return;
   }
   fc_char* value = XMLString :: transcode( node -> getNodeValue() );

   DBG_EXPR( value );

   switch( entry -> Type() )
   {
     case Fc::Bool:
         if( strcasecmp( value, "yes" ) == 0 ||
             strcasecmp( value, "true" ) == 0 ||
             strcasecmp( value, "1" ) == 0 )
             dynamic_cast<fcCKBool*>(entry)->SetValue(true);
         else
         {
            if( strcasecmp( value, "no" ) == 0 ||
                strcasecmp( value, "false" ) == 0 ||
                strcasecmp( value, "0" ) == 0 )
                dynamic_cast<fcCKBool*>(entry)->SetValue(false);
            else
            {
               MessageHandler. Error( fcString( "Uknown value " ) +
                     fcString( value ) +
                     fcString( ". Yes/no, true/false or 1/0 expected for bool type. " ) );
            }
         }
         break;
     case Fc::String:
          dynamic_cast<fcCKString*>(entry)->SetValue(value);
          break;
     case Fc::Number:
          dynamic_cast<fcCKNumber*>(entry)->SetValue( atof(value) );
          break;
     case Fc::List:
          while( node )
          {
            if( strcmp( XMLString :: transcode( node -> getNodeValue() ), "item" ) != 0 )
            {
               MessageHandler. Error( "Use tag item to define list of values. " );
               return;
            }
            value = XMLString :: transcode( node -> getFirstChild() -> getNodeValue() );           
            dynamic_cast<fcCKList*>(entry)->Append( fcString(value) );
            node = node -> getNextSibling();
          }
          break;
       default:;
    }
}
//--------------------------------------------------------------------------
void fcDOMConfigFile :: SetConfigSection( fcCSEntry* c_section )
{
   config_section = c_section;
}
//--------------------------------------------------------------------------
fcCSEntry* fcDOMConfigFile :: GetConfigSection()
{
   return config_section;
}
//--------------------------------------------------------------------------
fc_bool fcDOMConfigFile :: Parse( const fc_char* file_name, 
                                  const fcTSEntry* t_section,
                                  fc_bool default_setting )
{
   assert( config_section );
   fc_bool errorOccurred = false;

   // Instantiate the DOM parser.
   static const XMLCh gLS[] = { chLatin_L, chLatin_S, chNull };
   DOMImplementation *impl = DOMImplementationRegistry::getDOMImplementation(gLS);
   DOMBuilder        *parser = ((DOMImplementationLS*)impl)->createDOMBuilder(DOMImplementationLS::MODE_SYNCHRONOUS, 0);

   parser->setFeature(XMLUni::fgDOMNamespaces, false );
   parser->setFeature(XMLUni::fgXercesSchema, false );
   parser->setFeature(XMLUni::fgXercesSchemaFullChecking, false );
    
   // enable datatype normalization - default is off
   parser->setFeature(XMLUni::fgDOMDatatypeNormalization, true);
 
   try
   {
     // reset document pool
     parser -> resetDocumentPool();
     config_file = parser -> parseURI( file_name );
   }
   catch (const XMLException& toCatch)
   {
      std::cerr << "\nError during parsing: '" << file_name << "'\n"
           << "Exception message is:  \n"
           << StrX(toCatch.getMessage()) << "\n" << std::endl;
      errorOccurred = true;
   }
   catch (const DOMException& toCatch)
   {
      const unsigned int maxChars = 2047;
      XMLCh errText[maxChars + 1];

      std::cerr << "\nDOM Error during parsing: '" << config_file << "'\n"
           << "DOMException code is:  " << toCatch.code << std::endl;

      if (DOMImplementation::loadDOMExceptionMsg(toCatch.code, errText, maxChars))
           std::cerr << "Message is: " << StrX(errText) << std::endl;

      errorOccurred = true;
   }
   catch (...)
   {
      std::cerr << "\nUnexpected exception during parsing: '" << config_file << "'\n";
      errorOccurred = true;
   }
   if( ! config_file )
   {
      // TODO: create new DOM document
      return 0;
   }

   if( errorOccurred ) return 0;

   #ifdef DEBUG
      //PrintDOMNode( config_file );
   #endif
   assert( config_file );
   ProcessDOMNode( config_file,
                   config_section,
                   t_section,
                   default_setting ); 
   return 1;
}
//--------------------------------------------------------------------------
fc_bool fcDOMConfigFile :: Write(const fc_char* output_file) 
{
   DBG_FUNCTION_NAME( "fcDOMConfigFile", "Write" );
   DBG_COUT( "Updating nodes..." );
   assert( config_section );

   config_section -> UpdateDOMNode( ( fcDOMNode* ) config_file,
                  config_file );
   DBG_COUT( "...done." );
   
   // get a serializer, an instance of DOMWriter
   XMLCh tempStr[3];
   XMLString::transcode( "LS", tempStr, 2);
   DOMImplementation *impl = DOMImplementationRegistry::getDOMImplementation( tempStr);
   DOMWriter *theSerializer = ((DOMImplementationLS*)impl)->createDOMWriter();

   // set user specified end of line sequence and output encoding
   //@@theSerializer->setNewLine( XMLUni::fgXMLStringLF);
   theSerializer->setEncoding( gUTF8);

   // plug in user's own error handler
   DOMErrorHandler *myErrorHandler = new fcDOMErrorHandler();
   theSerializer->setErrorHandler( myErrorHandler);

   // set feature if the serializer supports the feature/mode
   if( theSerializer->canSetFeature( XMLUni::fgDOMWRTSplitCdataSections, true))
      theSerializer->setFeature( XMLUni::fgDOMWRTSplitCdataSections, true);

   if( theSerializer->canSetFeature( XMLUni::fgDOMWRTDiscardDefaultContent, true))
      theSerializer->setFeature( XMLUni::fgDOMWRTDiscardDefaultContent, true);

   //@@if( theSerializer->canSetFeature( XMLUni::fgDOMWRTFormatPrettyPrint, false))
   //  theSerializer->setFeature( XMLUni::fgDOMWRTFormatPrettyPrint, false);
   if( theSerializer->canSetFeature( XMLUni::fgDOMWRTFormatPrettyPrint, true))
      theSerializer->setFeature( XMLUni::fgDOMWRTFormatPrettyPrint, true);

   //@@theSerializer->getFeature( XMLUni::fgDOMWRTFormatPrettyPrint);

   //
   // Plug in a format target to receive the resultant
   // XML stream from the serializer.
   //
   // StdOutFormatTarget prints the resultant XML stream
   // to stdout once it receives any thing from the serializer.
   //
   //@@XMLFormatTarget *myFormTarget = new StdOutFormatTarget();
   //XMLFormatTarget *myFormTarget = new LocalFileFormatTarget( cStdFile::getOSNativePath( location));
   XMLFormatTarget *myFormTarget = new LocalFileFormatTarget( output_file );
   //
   // do the serialization through DOMWriter::writeNode();
   //
   theSerializer->writeNode( myFormTarget, *config_file );

   delete theSerializer;

   //
   // Filter, formatTarget and error handler
   // are NOT owned by the serializer.
   //
   delete myFormTarget;
   delete myErrorHandler;
   return true;

}
//--------------------------------------------------------------------------
#ifdef DEBUG
void fcDOMConfigFile :: PrintDOMNode( DOMNode* node )
{
   while( node )
   {
      fc_char *name = XMLString :: transcode( node -> getNodeName() );
      fc_char *value = XMLString :: transcode( node -> getNodeValue() );
      switch( node -> getNodeType() )
      {
         case DOMNode :: DOCUMENT_NODE:
            cout << "Document node... " << endl;
            break;
         case DOMNode :: ELEMENT_NODE:
            cout << "Element node... " << name << endl;
            break;
         case DOMNode :: TEXT_NODE:
            cout << "Text node... " << value << endl;
            break;
         default:
            break;
      }
      if( node -> hasChildNodes() )
      {
         cout << "Has child node..." << endl;
         PrintDOMNode( node -> getFirstChild() );
      }
      node = node -> getNextSibling();
   }

}
#endif
//--------------------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, fcDOMConfigFile& conf_file)
{
   if( conf_file. config_section )
      stream << * conf_file.config_section; 
   return stream;
}
