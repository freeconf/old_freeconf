/***************************************************************************
author           : 2009 by David Fabian
***************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/

#ifndef _FCCGSENTRY_H_
#define _FCCGSENTRY_H_

#include "fcCGEntry.h"
#include <list>

class fcCGSEntry: public fcCGEntry
{
  private:
    std::list<fcCGEntry*> _children;
    mutable std::list<fcCGEntry*>::const_iterator _it;
    
  public:
    
    fcCGSEntry (fcCGEntry *parent = 0);
    
    ~fcCGSEntry ();

    void addChild (fcCGEntry *child);

    void removeChild (fcCGEntry *child);

    fc_uint children () const;

    fcCGEntry* next () const;

    void skipToFirst () const;

    fcCGEntry* findChild (const fcString &childName) const;

    virtual Fc::EntryTypes type () const;
};

#endif