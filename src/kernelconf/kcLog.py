#!/usr/bin/python
#
# Kernel Config Logger
#
# begin: 2.3.2009 by Jan Ehrenberger <lankvil@seznam.cz>
#
# Setup logging object 
#

import logging

# Setup logging to stderr
handler = logging.StreamHandler()
format = logging.Formatter("%(asctime)s %(levelname)s: %(message)s")
handler.setFormatter(format)

# Create log object
log = logging.getLogger('KernelConf')
log.addHandler(handler)
log.setLevel(logging.DEBUG)

