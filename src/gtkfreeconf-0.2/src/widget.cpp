/************************************************************************
 *                      widget.cpp
 *      author: Frantisek Rolinek
 ************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/

#include <gtk/gtk.h>
#include <fcTEntry.h>
#include <fcTKBool.h>
#include <fcTKNumber.h>
#include <fcTKString.h>
#include <fcCKBool.h>
#include <fcCKNumber.h>
#include <fcCKString.h>
#include "gtkCKBool.h"
#include "gtkCKNumber.h"
#include "gtkCKString.h"
#include "gtkCSEntry.h"
#include "widget.h"

void compileEntry( fc_bool isSensitive, fcCEntry* c_entry ) {
    /*
    if( !isSensitive ) {
        const fcTEntry* t_entry = c_entry -> GetTEntry();
        fc_bool bool_val; 
        fc_real real_val; 
        const fc_char* char_val;
        switch( c_entry -> Type() ) {
            case fcBOOL:
                bool_val = ((fcTKBool*)t_entry) -> GetDependencyValue();
                ((fcCKBool*)c_entry) -> SetValue( bool_val );
                gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( ((gtkCKBool*)c_entry) -> GetWidget() ), bool_val );
                break;
            case fcNUMBER:
                real_val = ((fcTKNumber*)t_entry) -> GetDependencyValue();
                ((fcCKNumber*)c_entry) -> SetValue( real_val );
                // TODO:
                //gtk_spin_button_set_value( GTK_SPIN_BUTTON( ((gtkCKNumber*)c_entry) -> GetWidget() ), real_val );
                break;
            case fcSTRING:
                char_val = ((fcTKString*)t_entry) -> GetDependencyValue();
                ((fcCKString*)c_entry) -> SetValue( char_val );
                // TODO:
                //gtk_entry_set_text( GTK_ENTRY( ((gtkCKString*)c_entry) -> GetWidget() ), char_val );
                break;
            default:
                break;
        }
    }
    switch( c_entry -> Type() ) {
        case fcBOOL:
            gtk_widget_set_sensitive( ((gtkCKBool*)c_entry) -> GetWidget(), isSensitive );
            break;
        case fcNUMBER:
            gtk_widget_set_sensitive( ((gtkCKNumber*)c_entry) -> GetWidget(), isSensitive );
            break;
        case fcSTRING:
            gtk_widget_set_sensitive( ((gtkCKString*)c_entry) -> GetWidget(), isSensitive );
            break;
        default:
            break;
    }
    */

    const fcTEntry* t_entry = c_entry -> GetTEntry();
    fc_bool bool_val;
    fc_real real_val;
    const fc_char* char_val;
    switch( c_entry -> Type() ) {
        case fcBOOL:
            if( !isSensitive ) {
                bool_val = ((fcTKBool*)t_entry) -> GetDependencyValue();
                ((fcCKBool*)c_entry) -> SetValue( bool_val );
                gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( ((gtkCKBool*)c_entry) -> GetWidget() ), bool_val );
            }
            gtk_widget_set_sensitive( ((gtkCKBool*)c_entry) -> GetWidget(), isSensitive );
            break;
        case fcNUMBER:
            if( !isSensitive ) {
                real_val = ((fcTKNumber*)t_entry) -> GetDependencyValue();
                ((fcCKNumber*)c_entry) -> SetValue( real_val );
                // TODO:
                gtk_spin_button_set_value( GTK_SPIN_BUTTON( ((gtkCKNumber*)c_entry) -> GetWidget() ), real_val );
            }
            // TODO
            gtk_widget_set_sensitive( ((gtkCKNumber*)c_entry) -> GetWidget(), isSensitive );
            break;
        case fcSTRING:
            if( !isSensitive ) {
                char_val = ((fcTKString*)t_entry) -> GetDependencyValue();
                ((fcCKString*)c_entry) -> SetValue( char_val );
                if( ((gtkCKString*)c_entry) -> GetIsCombo() ) {
                    gtk_entry_set_text( GTK_ENTRY( GTK_COMBO(((gtkCKString*)c_entry) -> GetWidget()) -> entry ), char_val );
                } 
                else {
                    gtk_entry_set_text( GTK_ENTRY( ((gtkCKString*)c_entry) -> GetWidget() ), char_val );
                }
                //====================================================================================
                //gtk_entry_set_text( GTK_ENTRY( value_widget ), text );
                //// TODO:
                //  typedef struct {
                //    	    GtkWidget *entry;
                //	    GtkWidget *list;
                //  } GtkCombo;
                //gtk_entry_set_text( GTK_ENTRY( GTK_COMBO( value_widget ) -> entry ), text );

            }
            // TODO
            gtk_widget_set_sensitive( ((gtkCKString*)c_entry) -> GetWidget(), isSensitive );
            break;
        default:
            break;
    }
}

