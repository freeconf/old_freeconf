/***************************************************************************
                          fcSAXHelpFile.h  -  description
                             -------------------
    begin                : 2004/08/20
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCSAXHELPFILE_H__
#define __FCSAXHELPFILE_H__

#include "fcSAXFile.h"
#include "fcStack.h"

class fcTSEntry;
class fcTEntry;

//! This class fills help file to the template structure
/*!
 */
class fcSAXHelpFile : public fcSAXFile
{
   //! Pointer to fcTemplateStructure being created
   /*! It is set in ParseTemplateFile method.
    */
   fcStack< fcTSEntry* > t_section_stack;

   //! Element flags
   fc_bool label_element,
           help_element;

   //! Check for enclosing tag
   fc_bool enclosing_tag;

   //! Current entry
   fcTEntry* current_entry;

   public:

   //! Basic constructor
   fcSAXHelpFile();

   //! Destructor
   ~fcSAXHelpFile();

   protected:
   //! Implementations of the SAX DocumentHandler start element
   void startElement(  const   XMLCh* const    uri,
                       const   XMLCh* const    localname,
                       const   XMLCh* const    qname,
                       const   Attributes&     attributes);

   //! Implementations of the SAX DocumentHandler end element
   void endElement( const XMLCh* const uri, 
                    const XMLCh* const localname, 
                    const XMLCh* const qname);

   //! Implementations of the SAX DocumentHandler characters
   void characters( const XMLCh* const chars, 
                    const unsigned int length);

   public:

   //! Method for parsing help file
   /*! This method just sets pointer this -> template_structure
       and the calls fcSAXFile :: Parse()
    */
   fc_bool ParseHelpFile( const fc_char* file_name,
                          fcTSEntry* t_section );

};

#endif
