/***************************************************************************
                          fcSAXStringListFile.cpp  -  description
                             -------------------
    begin                : 2004/12/29
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "debug.h"
#include "fcSAXStringListFile.h"
#include "fcMessageHandler.h"
#include "fcList.h"

//--------------------------------------------------------------------------
fcSAXStringListFile :: fcSAXStringListFile()
   : string_lists( NULL ),
     current_list( NULL ),
     enclosing_tag( false )
{
}
//--------------------------------------------------------------------------
fcSAXStringListFile :: ~fcSAXStringListFile()
{
}
//--------------------------------------------------------------------------
void fcSAXStringListFile :: startElement( const XMLCh* const uri,
                                          const XMLCh* const localname,
                                          const XMLCh* const qname,
                                          const Attributes& attributes)
{
   DBG_FUNCTION_NAME( "fcSAXStringListFile", "startElement" );

   const fc_char* const name = XMLString :: transcode( localname );
   DBG_EXPR( name );

   if( strcmp( name, "freeconf-strings" ) == 0 )
   {
      enclosing_tag = true;
      return;
   }
   if( ! enclosing_tag )   
   {
      MessageHandler. Error( "You must enclose the string list file with <freeconf-string-list-file> and </freeconf-string-list-file>." );
      return;
   }
   
   if( strcmp( name, "string-list" ) == 0 )
   {
      fc_uint attr_len = attributes. getLength();
      for( fc_uint i = 0; i < attr_len; i ++ )
      {
         const fc_char* const attr_name = XMLString :: transcode( attributes. getLocalName( i ) );
         const fc_char* const attr_val = XMLString :: transcode( attributes. getValue( i ) );
         if( strcmp( attr_name, "name" ) == 0 )
         {
            fcList< fcStringEntry > lst;
            string_lists -> Insert( attr_val, lst );
            current_list = ( * string_lists )( attr_val );
         }
         else
         {
            MessageHandler. Error( fcString( "Uknown attribute " )
                                             + attr_name + "." );
         }
      }
      xml_element = fcSTRING_LIST;
      return;
   }
   if( strcmp( name, "string" ) == 0 )
   {
      xml_element = fcSTRING_LIST_STRING;
      return;
   }
   if( strcmp( name, "help" ) == 0 )
   {
      xml_element = fcSTRING_LIST_HELP;
      return;
   }
   if( strcmp( name, "label" ) == 0 )
   {
      xml_element = fcSTRING_LIST_LABEL;
      return;
   }
}
//--------------------------------------------------------------------------
void fcSAXStringListFile :: endElement( const XMLCh* const uri, 
                                        const XMLCh* const localname, 
                                        const XMLCh* const qname)
{
   DBG_FUNCTION_NAME( "fcSAXStringListFile", "endElement" );

   const fc_char* const name = XMLString :: transcode( localname );  
   DBG_COUT( name );

   switch( xml_element )
   {
      case fcSTRING_LIST:
         assert( current_list );
         current_list = NULL;
         break;
      case fcSTRING_LIST_STRING: 
         assert( current_list );
         current_list -> Append( current_entry );
         current_entry. SetValue( "" );
         current_entry. SetHelp( "" );
         current_entry. SetLabel( "" );
         break;
      default:;
   }
   xml_element = fcNO_STR_LIST_ELEMENT;
}
//--------------------------------------------------------------------------
void fcSAXStringListFile :: characters( const XMLCh* const chars, 
                                        const unsigned int length)
{
   DBG_FUNCTION_NAME( "fcSAXStringListFile", "characters" );

   const fc_char* data = XMLString :: transcode( chars );
   DBG_EXPR( data );

   switch( xml_element )
   {
      case fcSTRING_LIST_STRING:
         current_entry. SetValue( data );
         break;
      case fcSTRING_LIST_HELP:
         current_entry. SetHelp( data );
         break;
      case fcSTRING_LIST_LABEL:
         current_entry. SetLabel( data );
         break;
      default:;
   }        
}
//--------------------------------------------------------------------------
fc_bool fcSAXStringListFile :: ParseStringListFile( const fc_char* file_name,
                                                    fcHash< fcList< fcStringEntry > >* _string_lists )
{
   string_lists = _string_lists;
   current_list = NULL;
   enclosing_tag = false;
   return Parse( file_name );
}
