#!/usr/bin/python

from PyQt4 import QtCore, QtGui
from mainwindow import MainWindow

import sys

if __name__ == "__main__":

	app = QtGui.QApplication(sys.argv)

	widget = MainWindow()
	widget.show()
	app.exec_()