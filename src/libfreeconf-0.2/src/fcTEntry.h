/***************************************************************************
                          fcTEntry.h  -  description
                             -------------------
    begin                : 2004/08/28
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCTENTRY_H__
#define __FCTENTRY_H__

#include "fcBaseEntry.h"
#include "fcString.h"
#include "fcStringEntry.h"
#include "fcSAXTemplateConst.h"
#include "fcDependencyStruct.h"

class fcTDependency;
class fcTDependencyListItem;
class fcDActionBaseListItem;

//! This is a basic class for all template entries
class fcTEntry : public fcBaseEntry
{
    //! Class members
    private:
        //! Entry label
        /*! It should be short description of the entry meaning 
        */
        fcString m_strLabel;

        //! Help for the entry
        /*! This is full and detailed description of the entry meaning 
        */
        fcString m_strHelp;

        //! Dependency for the entry
        /*! This is the list of dependency entries for this entry
        */
        fcTDependency* m_pTDep;
   
        //! It is true if the entry is required to be in the config file
        fc_bool m_bRequired;
   
        //! It is true if the entry can occure more the once in the config file
        fc_bool m_bMultiple;

   //! Constructors & destructor
   public:
        fcTEntry();
        fcTEntry(fcTDependency* pDep, fc_bool bRequired = false, fc_bool bMultiple = false);
        fcTEntry(const fcTEntry& item);
        virtual ~fcTEntry(); 

   //! Overrided functions from base class
   public:
        //! Returns true if it is a template entry
        virtual fc_bool IsTEntry() const/* { return true; }*/;
        //! Returns true if it is a config entry
        virtual fc_bool IsCEntry() const/* { return false; }*/;

    //! Setters
    public:
        //! Label setter
        void SetLabel(const fc_char* strLabel);
        //! Label setter
        void SetLabel(const fcString& strLabel);
        //! Help setter
        void SetHelp(const fc_char* strHelp);
        //! Help setter
        void SetHelp(const fcString& strHelp);
        //! Required setter
        void SetRequired(fc_bool bRequired);
        //! Multiple entry setter
        void SetMultiple(fc_bool bMultiple);

   //! Getters
    public:
        //! Label getter
        const fcString& GetLabel() const;
        //! Help getter
        const fcString& GetHelp() const;
        //! Required getter
        fc_bool GetRequired() const;
        //! Multiple entry getter
        fc_bool GetMultiple() const;


   //! Functions for fcTDependency class
   public:
        //! Adders
        //  ------
            //! Add fcTDRelationListItem or fcTDKeywordListItem
        void AddDependencyListItem(fcTDependencyListItem *pItem);
            //! Add fcTDRelationListItem
        void AddDependencyListItem(const fcRelationEnum& _enum);
            //! Add fcTDKeywordListItem
        void AddDependencyListItem(const fc_char* name, const fcList<struDependencyValue>& lst);
            //! Add fcTDKeywordListItem
        void AddDependencyListItem(const fcString& name, const fcList<struDependencyValue>& lst);
            //! Add fcDActionBoolListItem or fcDActionNumberListItem or fcDActionStringListItem
        void AddActionListItem(fcDActionBaseListItem *pItem);
            //! Add fcDActionBoolListItem 
        void AddActionListItem(fc_int nActionValue, fc_bool bDefValue);
            //! Add fcDActionNumberListItem 
        void AddActionListItem(fc_int nActionValue, fc_int nDefValue);
            //! Add fcDActionNumberListItem 
        void AddActionListItem(fc_int nActionValue, fc_int nMin, fc_int nMax, fc_int nStep, const fc_char* strType);
            //! Add fcDActionNumberListItem 
        void AddActionListItem(fc_int nActionValue, fc_int nMin, fc_int nMax, fc_int nStep, const fcString& strType);
            //! Add fcDActionStringListItem
        void AddActionListItem(fc_int nActionValue, const fc_char* strDefValue);
            //! Add fcDActionStringListItem
        void AddActionListItem(fc_int nActionValue, const fcString& strDefValue);
            //! Add fcDActionStringListItem
        void AddActionListItem(fc_int nActionValue, const fcList< fcStringEntry >& lstData);
        
        //! Setters
        //  ------- 
            //! Set fcTDRelationListItem or fcTDKeywordListItem
        void SetDependencyListItem(fc_uint pos, fcTDependencyListItem *pItem);
            //! Set fcTDRelationListItem or fcTDKeywordListItem
        void SetDependencyListItem(fcTDependencyListItem *pItem, fc_uint pos);
            //! Set fcDActionBoolListItem or fcDActionNumberListItem or fcDActionStringListItem
        void SetActionListItem(fc_uint pos, fcDActionBaseListItem *pItem);
            //! Set fcDActionBoolListItem or fcDActionNumberListItem or fcDActionStringListItem
        void SetActionListItem(fcDActionBaseListItem *pItem, fc_uint pos);
       
        //! Getters
        //  ------- 
            //! Get fcTDRelationListItem or fcTDKeywordListItem
        fcTDependencyListItem* GetDependencyListItem(fc_uint pos) const;
            //! Get fcDActionBoolListItem or fcDActionNumberListItem or fcDActionStringListItem
        fcDActionBaseListItem* GetActionListItem(fc_uint pos) const;

        //! Other functions
        //  --------------- 
        //! Dependency list size
        fc_uint GetDependencyListSize() const;
        //! Action list size
        fc_uint GetActionListSize() const;
        //! Erase m_pTDep pointer
        void EraseDependency();

        //! Prints the fcTDependency class stored in m_pTDep pointer
        void PrintDependency() const;

#ifdef DEBUG
   //! Print this template entry
   void Print( ostream& stream ) const;
#endif
};

#endif
