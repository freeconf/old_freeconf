/***************************************************************************
                          fcStringEntry.h  -  description
                             -------------------
    begin                : 2005/01/01
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcStringEntryH
#define fcStringEntryH

#include "fcString.h"

//! String list entry
/*! This class describes particular string for string config
    keyword. It contains string value, optional label and help
    as an explanation for meaning of given string value.
 */

class fcStringEntry
{
   //! Class members
   private:
       //! String value
       fcString m_strValue;

       //! Help for this value
       fcString m_strHelp;

       //! Optional label
       /*! This label can appear for example in the UI
         instead of real valeu.
         */
       fcString m_strLabel;
   
   //! Constructors & destructor
   public:
       //! Basic constructor
       fcStringEntry();
       //! Constructors with parameters
       fcStringEntry(const fc_char* strValue, const fc_char* strHelp = 0, const fc_char* strLabel = 0);
       fcStringEntry(const fcString& strValue, const fcString& strHelp, const fcString& strLabel);
       //! Copy constructor
       fcStringEntry(const fcStringEntry& item);
       //! Destructor
       ~fcStringEntry();

   //! Overrides operators
   const fcStringEntry& operator=(const fcStringEntry& item);

   //! Setters
   public:
        //! Value setter
        void SetValue(const fc_char* strValue);
        //! Value setter
        void SetValue(const fcString& strValue);
        //! Help setter
        void SetHelp(const fc_char* strHelp);
        //! Help setter
        void SetHelp(const fcString& strHelp);
        //! Label setter
        void SetLabel(const fc_char* strLabel);
        //! Label setter
        void SetLabel(const fcString& strLabel);
   
   //! Getters
   public:
        //! Value getter
        const fcString& GetValue() const;
        //! Help getter
        const fcString& GetHelp() const;
        //! Label getter
        const fcString& GetLabel() const;
};

#endif
