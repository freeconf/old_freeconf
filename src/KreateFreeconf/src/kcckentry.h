/***************************************************************************
 *   Copyright (C) 2009 by Jan Ehrenberger   *
 *   lankvil@seznam.cz   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef KCCKENTRY_H
#define KCCKENTRY_H

#include <klistview.h>
#include "packagetreeview.h"


class kcCKEntryProperty;

/**
	@author Jan Ehrenberger <lankvil@seznam.cz>
*/
class kcCKEntry : public PackageTreeViewItem
{
public:
    kcCKEntry(PackageTreeView *tree, const QString &name = "", const QString &type = "");
    kcCKEntry(kcCKEntry *section, const QString &name = "", const QString &type = "");

    ~kcCKEntry();

    bool isRenameable(int column);
    bool dropTextEvent(const QString &text);

protected:
    kcCKEntryProperty *m_type;
    kcCKEntryProperty *m_label;
    kcCKEntryProperty *m_description;
};


class kcCKEntryProperty : public PackageTreeViewItem
{
    int m_order;
public:
    kcCKEntryProperty(kcCKEntry *entry, const QString &name, const QString &value = "", int order = 1);
    ~kcCKEntryProperty();

    QString getName() const;
    QString getValue() const;
    void setValue(const QString &);

    int order() const { return m_order; }
    bool isRenameable(int column);
};

/** kcCKEntryText
 * Represents text property of kcCKEntry.
 * Accepts text drop events.
 */
class kcCKEntryText : public kcCKEntryProperty
{
public:
    kcCKEntryText(kcCKEntry *entry, const QString &name, const QString &value = "", int order = 1);
    ~kcCKEntryText();

    bool dropTextEvent(const QString &text);
};

#endif
