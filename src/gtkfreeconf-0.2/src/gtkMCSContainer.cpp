/***************************************************************************
                          gtkMCSContainer.cpp  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include "gtkMCSContainer.h"
#include <fcTSEntry.h>
#include "gtkCKString.h"
#include "gtkCSEntry.h"
#include "debug.h"

struct gtkMCSContainerStructure {
    gtkMCSContainer* container_entry;
    fcCSEntry* main_entry;
};

const fc_char* no_name = "(No name)";

// we need this for adding rows to CList (there cannot be NULL pointer)
static fc_char* clist_titles[] = {"text for (bugy) GTK"}; 

static void cb_clist_row_selection( GtkWidget* widget,
                                    gint row,
                                    gint column,
                                    GdkEventButton *event,
                                    gtkMCSContainer* container )
{
   DBG_FUNCTION_NAME( "", "cb_clist_row_selection" );
   DBG_EXPR( row );
   container -> SetRowSelection( row );
}
                                    
static void cb_multiple_section_add_dialog_ok( GtkWidget* widget,
                                               gtkMCSContainer* container )
{
   DBG_FUNCTION_NAME( "", "cb_multiple_section_add_dialog_ok" );
   container -> CBAddSectionOK( widget );
}

static void cb_multiple_section_add_dialog_cancel( GtkWidget* widget,
                                                   gtkMCSContainer* conatiner )
{
   conatiner -> CBAddSectionCancel( widget );
}

static void cb_multiple_section_modify_dialog_ok( GtkWidget* widget,
                                                  gtkMCSContainer* conatiner )                                            
{
   DBG_FUNCTION_NAME( "", "cb_multiple_section_modify_dialog_ok" );
   conatiner -> CBModifySectionOK( widget );
}

static void cb_dialog_destroy( GtkWidget* widget,
                               GtkWidget* dialog )
{
  gtk_widget_destroy( dialog );
}

static void cb_fc_multiple_section_add( GtkWidget* widget,
                                        gtkMCSContainerStructure* hlp )
{
   DBG_FUNCTION_NAME( "", "gtk_fc_multiple_section_add" );
  
   (hlp -> container_entry) -> CBAddSection( widget, hlp -> main_entry );
}

static void cb_fc_multiple_section_modify( GtkWidget* widget,      
                                           gtkMCSContainerStructure* hlp )
{
   DBG_FUNCTION_NAME( "", "gtk_fc_multiple_section_modify" );
   (hlp -> container_entry) -> CBModifySection( widget, hlp -> main_entry );
}


static void cb_fc_multiple_section_remove( GtkWidget* widget,
                                           gtkMCSContainer* container )
{
   DBG_FUNCTION_NAME( "", "gtk_fc_multiple_section_modify" );
   container -> CBRemoveSection( widget );
}
//--------------------------------------------------------------------------
gtkMCSContainer :: gtkMCSContainer()
   : gtk_clist( 0 ),
     select_row( 0 ),
     edited_section( 0 )
{
}
//--------------------------------------------------------------------------
gtkMCSContainer :: gtkMCSContainer( const gtkMCSContainer& mcs_container )
   : fcMCSContainer( mcs_container ),
     gtk_clist( 0 ),
     select_row( 0 ),
     edited_section( 0 )
{
}
//--------------------------------------------------------------------------
gtkMCSContainer :: gtkMCSContainer( const fcTEntry* t_entry )
   : fcMCSContainer( t_entry ),
     gtk_clist( 0 ),
     select_row( 0 ),
     edited_section( 0 )
{
}
//--------------------------------------------------------------------------
gtkMCSContainer :: ~gtkMCSContainer() {
}
//--------------------------------------------------------------------------
void gtkMCSContainer :: Render( GtkWidget* parent_widget,
                                GtkTooltips* tooltips,
                                const fc_char* help,
                                fcCSEntry* main_c_section )
{
   DBG_FUNCTION_NAME( "gtkMCSContainer", "Render" );
   DBG_EXPR( GetEntryName() );
   
   // Create new frame
   const fcTSEntry* ts_entry = ( const fcTSEntry* ) GetTEntry();
   
   GtkWidget* section_frame = 
      gtk_frame_new( ts_entry -> GetLabel(). Data() );
   gtk_container_set_border_width( GTK_CONTAINER( section_frame ), 5 );
   gtk_box_pack_start( GTK_BOX( parent_widget ), section_frame, false, false, 10 );

   GtkWidget* section_hbox = gtk_hbox_new( FALSE, 0 );
   gtk_container_add( GTK_CONTAINER( section_frame ), section_hbox );
   
   // Create new clist
   gtk_clist = GTK_CLIST( gtk_clist_new( 1 ) );
   GtkWidget* button_vbox = gtk_vbox_new( TRUE, 0 );
   gtk_box_pack_start( GTK_BOX( section_hbox ),
                       GTK_WIDGET( gtk_clist ),
                       true, true, 10 );
   gtk_box_pack_start( GTK_BOX( section_hbox ), button_vbox, false, false, 10 );

   // Create editing buttons
   GtkWidget* button_add = gtk_button_new_with_label( "Add" );
   GtkWidget* button_modify = gtk_button_new_with_label( "Modify" );
   GtkWidget* button_remove = gtk_button_new_with_label( "Remove" );

   // Pack the buttons to a vbox
   gtk_box_pack_start( GTK_BOX( button_vbox ), button_add, true, true, 10 );
   gtk_box_pack_start( GTK_BOX( button_vbox ), button_modify, true, true, 10 );
   gtk_box_pack_start( GTK_BOX( button_vbox ), button_remove, true, true, 10 );

   gtkMCSContainerStructure *hlp = new gtkMCSContainerStructure();
   hlp -> container_entry = this;
   hlp -> main_entry = main_c_section;

   // Connect signals for editing multiple section
   gtk_signal_connect( GTK_OBJECT( gtk_clist ),
                       "select_row",
                       GTK_SIGNAL_FUNC( cb_clist_row_selection ),
                       this );
   gtk_signal_connect( GTK_OBJECT( button_add ),
                       "clicked",
                       GTK_SIGNAL_FUNC( cb_fc_multiple_section_add ),
                       hlp );
   gtk_signal_connect( GTK_OBJECT( button_modify ),
                       "clicked",
                       GTK_SIGNAL_FUNC( cb_fc_multiple_section_modify ),
                       hlp );
   gtk_signal_connect( GTK_OBJECT( button_remove ),
                       "clicked",
                       GTK_SIGNAL_FUNC( cb_fc_multiple_section_remove ),
                       this );
  
   gtk_clist_column_titles_show( GTK_CLIST( gtk_clist ) );
   gtk_clist_set_column_title( GTK_CLIST( gtk_clist ), 0, "Name" );

   for( fc_int i = 0; i < Size(); i ++ )
   {
      gtkCSEntry* c_section = ( gtkCSEntry* ) ( *this )[ i ];
      
      // The first entry of the multiple section is
      // expected to be its ID
      fcCEntry* id_entry = ( fcCEntry* ) ( *c_section )[ 0 ];

      const fc_char* section_id( 0 );
      if( id_entry -> Type() == fcSTRING ) 
         section_id = ( ( gtkCKString* ) id_entry ) -> GetValue(). Data(); 
      
      // Add multiple section to the GUI clist
      gtk_clist_append( GTK_CLIST( gtk_clist ),
                        clist_titles );
      gtk_clist_set_text( GTK_CLIST( gtk_clist ),
                          i,
                          0,
                          section_id );
   }
}
//--------------------------------------------------------------------------
void gtkMCSContainer :: SetRowSelection( fc_uint row )
{
   select_row = row;
}
//--------------------------------------------------------------------------
void gtkMCSContainer :: CBAddSection( GtkWidget* widget, fcCSEntry* main_c_section )
{
   DBG_FUNCTION_NAME( "gtkMCSContainer", "CBAddSection" );
   const fcTSEntry* t_section = ( fcTSEntry* ) GetTEntry();  
   const fc_char* name = t_section -> GetEntryName(). Data();
   // create new config section
   edited_section = new gtkCSEntry( t_section );
   edited_section -> Init( t_section );

   // display dialog for setting the section up
   GtkWidget* section_dialog = gtk_dialog_new();
   gtk_window_set_title( GTK_WINDOW( section_dialog ),
                         "Adding new config section");
   gtk_window_set_modal( GTK_WINDOW( section_dialog ),
                         true );
  
   gtk_container_border_width( GTK_CONTAINER( GTK_DIALOG( section_dialog ) -> vbox ),
                               10 );

   
   edited_section -> Render( GTK_DIALOG( section_dialog ) -> vbox,
                             true , main_c_section);
   
   GtkWidget* ok_button = gtk_button_new_with_label( "OK" );
   GtkWidget* cancel_button = gtk_button_new_with_label( "Cancel" );
   
   gtk_box_pack_start( GTK_BOX( GTK_DIALOG( section_dialog ) -> action_area ),
                       ok_button, true, true, 0 );
   gtk_box_pack_start( GTK_BOX( GTK_DIALOG( section_dialog ) -> action_area ),
                       cancel_button, true, true, 0 );
   gtk_signal_connect( GTK_OBJECT( ok_button ),
                       "clicked",
                       GTK_SIGNAL_FUNC( cb_multiple_section_add_dialog_ok ),
                       this );
   gtk_signal_connect( GTK_OBJECT( ok_button ),
                       "clicked",
                       GTK_SIGNAL_FUNC( cb_dialog_destroy ),
                       section_dialog );
   
   gtk_signal_connect( GTK_OBJECT( cancel_button ),
                       "clicked",
                       GTK_SIGNAL_FUNC( cb_multiple_section_add_dialog_cancel ),
                       this );
   gtk_signal_connect( GTK_OBJECT( cancel_button ),
                       "clicked",
                       GTK_SIGNAL_FUNC( cb_dialog_destroy ),
                       section_dialog );

   gtk_widget_show_all( section_dialog ); 
}
//--------------------------------------------------------------------------
void gtkMCSContainer :: CBAddSectionOK( GtkWidget* widget )
{
   DBG_FUNCTION_NAME( "gtkMCSContainer", "CBAddSectionOK" );

   assert( edited_section );
   Append( edited_section );

   // the first entry is supposed to be string with the
   // multiple section id
   fcCEntry* c_entry = ( * edited_section )[ 0 ];

   const fc_char* section_name;
   if( c_entry -> Type() != fcSTRING ) section_name = no_name;
   else section_name = 
      ( ( gtkCKString* ) c_entry ) -> GetValue(). Data(); 

   //DBG_EXPR( * config_section );
   
   // add new row to the clist widget
   gtk_clist_freeze( gtk_clist );


   DBG_EXPR( GTK_CLIST( gtk_clist ) -> rows );
   gtk_clist_append( gtk_clist,
                     clist_titles );
   gtk_clist_set_text( gtk_clist,
                       gtk_clist -> rows - 1,
                       0,
                       section_name );
   gtk_clist_thaw( gtk_clist );
}
//--------------------------------------------------------------------------
void gtkMCSContainer :: CBAddSectionCancel( GtkWidget* widget )
{
   assert( edited_section );
   delete edited_section;
}
//--------------------------------------------------------------------------
void gtkMCSContainer :: CBModifySection( GtkWidget* widget, fcCSEntry* main_c_section )
{
   DBG_FUNCTION_NAME( "gtkMCSContainer", "CBModifySection" );
   
   assert( select_row < Size() );
   edited_section = dynamic_cast<gtkCSEntry*>(( * this )[ select_row ]);

   std::cout << edited_section << std::endl;
   // display dialog for setting the section up
   GtkWidget* section_dialog = gtk_dialog_new();
   gtk_window_set_title( GTK_WINDOW( section_dialog ),
                         "Modifying config section");
   gtk_window_set_modal( GTK_WINDOW( section_dialog ),
                         true );

   gtk_container_border_width( GTK_CONTAINER( GTK_DIALOG( section_dialog ) -> vbox ),
                               10 );
   edited_section -> Render( GTK_DIALOG( section_dialog ) -> vbox,
                        true , main_c_section);
   
   GtkWidget* ok_button = gtk_button_new_with_label( "OK" );
   //GtkWidget* cancel_button = gtk_button_new_with_label( "Cancel" );
   // TODO: add cancel button (it is not trivial)

   gtk_box_pack_start( GTK_BOX( GTK_DIALOG( section_dialog ) -> action_area ),
                       ok_button, true, true, 0 );
   //gtk_box_pack_start( GTK_BOX( GTK_DIALOG( section_dialog ) -> action_area ),
   //                    cancel_button, true, true, 0 );
   gtk_signal_connect( GTK_OBJECT( ok_button ),
                       "clicked",
                       GTK_SIGNAL_FUNC( cb_multiple_section_modify_dialog_ok ),
                       this );
   gtk_signal_connect( GTK_OBJECT( ok_button ),
                       "clicked",
                       GTK_SIGNAL_FUNC( cb_dialog_destroy ),
                       section_dialog );
   
   //gtk_signal_connect( GTK_OBJECT( cancel_button ),
   //                    "clicked",
   //                    GTK_SIGNAL_FUNC( cb_multiple_section_add_dialog_cancel ),
   //                    section_pointer );
   //gtk_signal_connect( GTK_OBJECT( cancel_button ),
   //                    "clicked",
   //                    GTK_SIGNAL_FUNC( cb_dialog_destroy ),
   //                    section_dialog );

   gtk_widget_show_all( section_dialog );
}
//--------------------------------------------------------------------------
void gtkMCSContainer :: CBModifySectionOK( GtkWidget* widget )
{
   DBG_FUNCTION_NAME( "gtkMCSContainer", "CBModifySectionOK" );
   
   // get new multiple section name
   assert( edited_section );
   fcCEntry* id_entry = ( * edited_section )[ 0 ];
   
   assert( id_entry && id_entry -> Type() == fcSTRING );
   const fc_char* section_name = 
      ( ( gtkCKString* ) id_entry ) -> GetValue(). Data();

   // add new row to the clist widget
   gtk_clist_freeze( gtk_clist );


   gtk_clist_set_text( gtk_clist,
                       select_row,
                       0,
                       section_name );
   gtk_clist_thaw( gtk_clist );
}
//--------------------------------------------------------------------------
void gtkMCSContainer :: CBRemoveSection( GtkWidget* widget )
{
   DBG_FUNCTION_NAME( "gtkMCSContainer", "CBRemoveSection" );

   assert( select_row < Size() );
   
   // remove the section from the container
   Erase( select_row );
   
   // remove selected row from the clist
   gtk_clist_remove( gtk_clist,
                     select_row );
}
