/***************************************************************************
                          fcTypes.h  -  description
                             -------------------
    begin                : Fri,  9 Apr 2004 15:12:46 +0100
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcTypesH
#define fcTypesH

#include <xercesc/dom/DOMDocument.hpp>
XERCES_CPP_NAMESPACE_USE

typedef bool fc_bool;
typedef int fc_int;
typedef unsigned int fc_uint;
typedef char fc_char;
typedef unsigned char fc_uchar;
typedef double fc_real;

typedef DOMNode fcDOMNode;
typedef DOMElement fcDOMElement;
typedef DOMDocument fcDOMDocument;

namespace Fc
{
  enum EntryTypes
  {
    UnknownEntry,
    Section,
    Bool,
    Number,
    List,
    String
  };
  
  enum Icon
  {
    NoIcon,
    User,
    Security,
    Password,
    Terminal,
    Network,
    Tools,
    Display,
    Package,
    File,
    Directory
  };
}

#endif
