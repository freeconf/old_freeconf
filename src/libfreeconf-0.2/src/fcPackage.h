/***************************************************************************
author           : 2009 by David Fabian
***************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/

#ifndef __FCPACKAGE_H__
#define __FCPACKAGE_H__

#include "fcTypes.h"
#include "fcHash.h"
#include "fcList.h"
#include "fcStringEntry.h"
#include "fcStringEntry.h"

class fcHeaderStructure;
class fcGWindow;
class fcStringListContainer;
class fcDOMConfigFile;
class fcCSEntry;
class fcCGSEntry;
class fcTSEntry;


class fcPackage
{
  private:
    fcHeaderStructure *_header;
    fcGWindow *_window;
    fcTSEntry *_tTree;
    fcCSEntry *_cTree;
    fcHash<fcList <fcStringEntry> > *_stringLists;
    fcDOMConfigFile *_DOM;
    fcCSEntry *_mainCSection;

    fcString *_packageName;

    void init (const fcString &packageName);

    void fillSection (fcCGSEntry *guiEntry, const fcCSEntry *configEntry);
    
  public:
    explicit fcPackage (const fcString &packageName);

    explicit fcPackage (const char *packageName);
    
    ~fcPackage ();

    fc_bool loadPackage ();
    
    const fcTSEntry* tSectionEntry () const;

    const fcCSEntry* cSectionEntry () const;

    const fcGWindow* window () const;

    fcCSEntry* cSectionEntry ();

    const fcHeaderStructure* header () const;

    fcDOMConfigFile* configFile ();

    fcCSEntry* mainCSection ();

    //! This function initialize the Freeconf structures using data in HeaderStructure
//   fc_bool fcInit( const fc_char* package_name,
//                 fcHeaderStructure& header_structure,
//                 fcTSEntry& t_section,
//                 fcHash< fcList< fcStringEntry > >& string_lists,
//                 fcDOMConfigFile& config_file );

};
#endif
