/***************************************************************************
 *   Copyright (C) 2009 by Jan Ehrenberger   *
 *   lankvil@seznam.cz   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "manviewtab.h"
#include "manview.h"
#include <khtml_events.h>
#include <qapplication.h>
#include <qdragobject.h>


ManPart::ManPart(QWidget *parent, const char *name)
    :KHTMLPart(parent), m_drag(false)
{
    // Connect selectionChanged signal to myself
    connect(this, SIGNAL(selectionChanged()),
            this, SLOT(slotSelectionChanged()));

}

ManPart::~ManPart()
{
}

void ManPart::khtmlMousePressEvent(khtml::MousePressEvent *event)
{
    QMouseEvent *mouseevent = event->qmouseEvent();
    KHTMLPart::khtmlMousePressEvent(event); // Call std implementation of event
    
    if(mouseevent->button() == Qt::MidButton)
            m_dragStartPoint = mouseevent->pos();
    mouseevent->accept();
}

void ManPart::khtmlMouseMoveEvent(khtml::MouseMoveEvent *event)
{
    QMouseEvent *mouseevent = event->qmouseEvent();
    KHTMLPart::khtmlMouseMoveEvent(event);

    if(!(mouseevent->state() & Qt::MidButton)) // User must keep middle button pressed
        return;
    // Cursor must move at least QApplication::startDragDistance() to start drag
    if((mouseevent->pos() - m_dragStartPoint).manhattanLength() < QApplication::startDragDistance())
        return;
    if(!hasSelection())
        return;

    //mouseevent->accept();
    QDragObject *d = new QTextDrag(selectedText(), widget());
    d->dragCopy();
}


void ManPart::slotSelectionChanged()
{
    if(hasSelection())
        // Enable drag only when there is some text selete
        m_drag = true;
    else
        m_drag = false;
}


ManViewTab::ManViewTab(const QString &url, ManView *man_view)
    : QHBox(man_view), m_man_view(man_view)
{
    m_html = new ManPart(this);
    m_html->setDNDEnabled(true);
    openLocation(url);


    // Connect KHTMLPart signals
    connect(m_html, SIGNAL(setWindowCaption(const QString&)),
            this,   SLOT(slotSetTitle(const QString&)));
    connect(m_html, SIGNAL(selectionChanged()),
            this, SLOT(slotSelectionChanged()));
/*  khtml_ext.h is missing
    // Create Browser Extension
    KHTMLPartBrowserExtension *browser = new KHTMLPartBrowserExtension(m_html);
    connect(browser, SIGNAL(openURLRequestDelayed (const KURL &, const KParts::URLArgs &)),
            this, SLOT(slotOpenURLRequest(const KURL &, const KParts::URLArgs &)));
    */
}


ManViewTab::~ManViewTab()
{
}


KHTMLPart *ManViewTab::getHTMLPart()
{
    return m_html;
}

void ManViewTab::openLocation(const QString &location)
{
    if(location.isEmpty())
        return; // TODO: Show blank page
    // Search man pages by default
    KURL url;
    if(location.find(':') == -1 && location.find('/') == -1)
        url = KURL("man:" + location);
    else
        url = KURL(location);
    m_html->openURL(url);
}

void ManViewTab::slotSetTitle(const QString& title)
{
    m_man_view->setTabLabel(this, title);
}

void ManViewTab::slotSelectionChanged()
{
    DOM::Range range = m_html->selection();
    if(range.isNull() || range.toString().isEmpty())
        return;
    emit selectionChanged(m_html, range);
}


/** Slot Open URL Request
 */
void ManViewTab::slotOpenURLRequest(const KURL &url, const KParts::URLArgs &args)
{
    m_html->openURL(url);
}



#include "manviewtab.moc"
