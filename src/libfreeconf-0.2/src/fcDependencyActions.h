/***************************************************************************
                          fcDependencyActions.h  -  description
                             -------------------
    copyright            : (C) 2009 by Frantisek Rolinek
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCDEPENDENCYACTIONS_H__
#define __FCDEPENDENCYACTIONS_H__

#include <ostream>
#include "fcTypes.h"
#include "fcList.h"
#include "fcString.h"
#include "fcStringEntry.h"

enum fcActionType { fcNO_ACTION,
                    fcBOOL_ACTION,
                    fcNUMBER_ACTION,
                    fcSTRING_ACTION
};

class fcDActionBaseListItem
{
    //! Class members
    protected:
        //! Dependency condition value
        fc_int m_nActionValue;

    //! Constructors & destructor
    public:
        fcDActionBaseListItem();
        fcDActionBaseListItem(fc_int nActionValue);
        fcDActionBaseListItem(const fcDActionBaseListItem& item);
        virtual ~fcDActionBaseListItem();

    //! Pure virtual functions
    public:
        //! Allocates and returns new instance of this class
        virtual fcDActionBaseListItem* NewActionListItem() const = 0;
        //! Returns true, if the default value for this action is set
        virtual fc_bool IsDefaultValueSet() const = 0;
        //! Returns true, if properties for this action are set
        virtual fc_bool IsPropertiesSet() const = 0;
        //! Returns type of action
        virtual fcActionType ActionType() const = 0;

    //! Setter
    public:
        void SetActionValue(fc_int nActionValue);

    //! Getter
    public:
        fc_int GetActionValue() const;
};

class fcDActionBoolListItem : public fcDActionBaseListItem
{
    //! Class members
    private:
        //! Default value for given action
        fc_bool m_bDefValue;
        //! Flag for default value
        fc_bool m_bFlagDefValue;

    //! Constructors & destructor
    public:
        fcDActionBoolListItem();
        fcDActionBoolListItem(fc_int nActionValue, fc_bool bDefValue);
        fcDActionBoolListItem(const fcDActionBoolListItem& item);
        virtual ~fcDActionBoolListItem();

    //! Overrides from base class
    public:
        //! Allocates and returns new instance of this class
        virtual fcDActionBaseListItem* NewActionListItem() const;
        //! Returns true, if the default value for this action is set
        virtual fc_bool IsDefaultValueSet() const;
        //! Returns true, if properties for this action are set
        /*! This function returns always false, because 
         *  bool keywords cannot have any property value
         */
        virtual fc_bool IsPropertiesSet() const;
        //! Returns type of action
        virtual fcActionType ActionType() const;

    //! Override operators
    public:
        const fcDActionBoolListItem& operator=(const fcDActionBoolListItem& item);
        friend std::ostream& operator<< (std::ostream& stream, const fcDActionBoolListItem& fcBoolAction);

    //! Setters
    public:
        //! Set default value for given action
        void SetDefaultValue(fc_bool bDefValue);

    //! Getters
    public:
        //! Get default value for given action
        fc_bool GetDefaultValue() const;
};

class fcDActionNumberListItem : public fcDActionBaseListItem
{
    //! Class members
    private:
        //! Default value for given action
        fc_int m_nDefValue;
        //! Flag for default value
        fc_bool m_bFlagDefValue;

        //! Property: min for given action
        fc_int m_nMin;
        //! Flag for property min
        fc_bool m_bFlagMin;

        //! Property: max for given action
        fc_int m_nMax;
        //! Flag for property max
        fc_bool m_bFlagMax;

        //! Property: step for given action
        fc_int m_nStep;
        //! Flag for property step
        fc_bool m_bFlagStep;

        //! Property: type for given action
        fcString m_strType;
        //! Flag for property type
        fc_bool m_bFlagType;

    //! Constructors & destructor
    public:
        fcDActionNumberListItem();
        fcDActionNumberListItem(fc_int nActionValue);
        fcDActionNumberListItem(fc_int nActionValue, fc_int nDefValue);
        fcDActionNumberListItem(fc_int nActionValue, fc_int nMin, fc_int nMax, fc_int nStep, const fc_char* strType);
        fcDActionNumberListItem(fc_int nActionValue, fc_int nMin, fc_int nMax, fc_int nStep, const fcString& strType);
        fcDActionNumberListItem(const fcDActionNumberListItem& item);
        virtual ~fcDActionNumberListItem();

    //! Overrides from base class
    public:
        //! Allocates and returns new instance of this class
        virtual fcDActionBaseListItem* NewActionListItem() const;
        //! Returns true, if the default value for this action is set
        virtual fc_bool IsDefaultValueSet() const;
        //! Returns true, if properties for this action are set
        virtual fc_bool IsPropertiesSet() const;
        //! Returns type of action
        virtual fcActionType ActionType() const;

    //! Override operators
    public:
        const fcDActionNumberListItem& operator=(const fcDActionNumberListItem& item);
        friend std::ostream& operator<< (std::ostream& stream, const fcDActionNumberListItem& fcNumberAction);

    //! Setters
    public:
        //! Set default value for given action
        void SetDefaultValue(fc_int nDefValue);
        //! Set property values for given action
        void SetProperties(fc_int nMin, fc_int nMax, fc_int nStep, const fc_char* strType);
        //! Set property values for given action
        void SetProperties(fc_int nMin, fc_int nMax, fc_int nStep, const fcString& strType);
        //! Set property min for given action
        void SetMin(fc_int nMin);
        //! Set property max for given action
        void SetMax(fc_int nMax);
        //! Set property step for given action
        void SetStep(fc_int nStep);
        //! Set property type for given action
        void SetType(const fc_char* strType);
        //! Set property type for given action
        void SetType(const fcString& strType);

    //! Getters
    public:
        //! Get default value for given acton
        fc_int GetDefaultValue() const;
        //! Get property values for given action
        void GetProperties(fc_int& nMin, fc_int& nMax, fc_int& nStep, fcString& strType);
        //! Get property min for given action
        fc_int GetMin() const;
        //! Get property max for given action
        fc_int GetMax() const;
        //! Get property step for given action
        fc_int GetStep() const;
        //! Get property type for given action
        const fcString& GetType() const;

    //! Others
    public:
        //! Returns true, if the property min is set
        fc_bool IsMinSet() const;
        //! Returns true, if the property max is set
        fc_bool IsMaxSet() const;
        //! Returns true, if the property step is set
        fc_bool IsStepSet() const;
        //! Returns true, if the property type is set
        fc_bool IsTypeSet() const;
};

class fcDActionStringListItem : public fcDActionBaseListItem
{
    //! Class members
    private:
        //! Default value for given action
        fcString m_strDefValue;
        //! Flag for default value
        fc_bool m_bFlagDefValue;

        //! Property: list of possible data for given action
        fcList< fcStringEntry > m_lstData;
        //! Flag for property data
        fc_bool m_bFlagData;

    //! Constructors & destructor
    public:
        fcDActionStringListItem();
        fcDActionStringListItem(fc_int nActionValue, const fc_char* strDefValue);
        fcDActionStringListItem(fc_int nActionValue, const fcString& strDefValue);
        fcDActionStringListItem(fc_int nActionValue, const fcList< fcStringEntry >& lstData);
        fcDActionStringListItem(const fcDActionStringListItem& item);
        virtual ~fcDActionStringListItem();

    //! Overrides from base class
    public:
        //! Allocates and returns new instance of this class
        virtual fcDActionBaseListItem* NewActionListItem() const;
        //! Returns true, if the default value for this action is set
        virtual fc_bool IsDefaultValueSet() const;
        //! Returns true, if properties for this action are set
        virtual fc_bool IsPropertiesSet() const;
        //! Returns type of action
        virtual fcActionType ActionType() const;

    //! Override operators
    public:
        const fcDActionStringListItem& operator=(const fcDActionStringListItem& item);
        friend std::ostream& operator<< (std::ostream& stream, const fcDActionStringListItem& fcStringAction);

    //! Appenders
    public:
        //! Append item to the property data list for given action
        void AppendData(const fcStringEntry& strEntry);
        //! Append list ot items to the property data list for diven action
        void AppendDataList(const fcList< fcStringEntry >& lstData);

    //! Setters
    public:
        //! Set default value for given action
        void SetDefaultValue(const fc_char* strDefValue);
        //! Set default value for given action
        void SetDefaultValue(const fcString& strDefValue);
        //! Set property data list for given action
        void SetData(const fcList< fcStringEntry >& lstData);
        //! Set item to the property data list for given action
        void SetData(const fcStringEntry& strEntry, fc_uint pos);
        //! Set item to the property data list for given action
        void SetData(fc_uint pos, const fcStringEntry& strEntry);

    //! Getters
    public:
        //! Get default value for given action
        const fcString& GetDefaultValue() const;
        //! Get property list data for given action
        const fcList<fcStringEntry>& GetData() const;
        //! Get item of property data for given action
        const fcStringEntry& GetData(fc_uint pos) const;

    //! Other functions
    public:
        //! Erase m_lstData list
        void EraseDataList();
        //! Get m_lstData size
        fc_int GetDataListSize() const;
        //! Returns true if the position in m_lstData list is valid
        fc_bool IsPositionValid(fc_uint pos) const;
        //! Returns true if the position in m_lstData list is not valid
        fc_bool IsPositionNotValid(fc_uint pos) const;
};

#endif
