/***************************************************************************
                          fcDOMErrorHandler.h  -  description
                             -------------------
    begin                : 2004/10/09
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCDOMERRORHANDLER_H__
#define __FCDOMERRORHANDLER_H__

#include <xercesc/dom/DOMErrorHandler.hpp>

XERCES_CPP_NAMESPACE_USE

class fcDOMErrorHandler : public DOMErrorHandler
{
   bool handleError( const DOMError& domError );
};

#endif
