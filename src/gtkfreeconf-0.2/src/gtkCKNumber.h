/***************************************************************************
                          gtkCKNumber.h  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef gtkCKNumberH
#define gtkCKNumberH

#include <gtk/gtk.h>
#include <fcCKNumber.h>
#include <fcCSEntry.h>

class gtkCKNumber;

struct gtkCKNumberStructure {
    gtkCKNumber* entry;
    fcCSEntry* main_entry;
};
//--------------------------------------------------------------------------
class gtkCKNumber : public fcCKNumber
{
   //! Widget for this entry
   GtkWidget* widget; 

   //! Pointet to gtkCKNumberStructure structure
   gtkCKNumberStructure *struc;

   public:

   //! Basic constructor
   gtkCKNumber();

   //! Copy constructor
   gtkCKNumber( const fcCKNumber& ck_number );

   //! Constructor template entry pointer
   gtkCKNumber( const fcTEntry* t_entry );

   //! Destructor
   ~gtkCKNumber();
  
   //! Render the number GUI widget
   void Render( GtkWidget* parent_widget,
                GtkTooltips* tooltips,
                const fc_char* help,
                fcCSEntry* main_c_section );

   //! GtkWidget setter
   void SetWidget( GtkWidget* _widget );

   //! GtkWidget getter
   GtkWidget* GetWidget();
};

#endif
