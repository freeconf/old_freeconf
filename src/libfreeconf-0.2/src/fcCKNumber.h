/***************************************************************************
                          fcCKNumber.h  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcCKNumberH
#define fcCKNumberH

#include "fcCKEntry.h"

class fcCKNumber : public fcCKEntry
{
   //! Entry value
   fc_real value;
   
   public:
   //! Basic constructor
   fcCKNumber();

   //! Copy constructor
   fcCKNumber( const fcCKNumber& config_value );

   //! Constructor template entry pointer
   fcCKNumber( const fcTEntry* t_entry )
      : fcCKEntry( t_entry ){};

   //! Destructor
   ~fcCKNumber();
   
   //! Allocate new instance and return pointer
   virtual fcBaseEntry* NewEntry() const;

   //! Value type getter
   Fc::EntryTypes Type() const;
   
   //! Value getter
   const fc_real& GetValue() const;

   //! Value setter
   void SetValue( fc_real val );
   
   //! Update DOM node
   /*void UpdateDOMNode( fcDOMNode* parent_node,
                       fcDOMDocument* doc );*/

#ifdef DEBUG
   //! Print this structure
   void Print( ostream& stream ) const;
#endif
};

#endif
