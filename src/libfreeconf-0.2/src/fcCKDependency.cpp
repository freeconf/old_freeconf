/***************************************************************************
                          fcCKDependency.cpp  -  description
                             -------------------
    copyright            : (C) 2009 by Frantisek Rolinek
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cstring>
#include "fcCKDependency.h"

//---------------------------------------------------------------------------
fcCDKBoolListItem :: fcCDKBoolListItem()
{
    m_dependencyValues.EraseAll();
}
//---------------------------------------------------------------------------
fcCDKBoolListItem :: fcCDKBoolListItem(const fc_char* name, const fcList<struDBoolValue>& lst)
    : fcCDKeywordListItem(name),
      m_dependencyValues(lst)
{
}
//---------------------------------------------------------------------------
fcCDKBoolListItem :: fcCDKBoolListItem(const fcString& name, const fcList<struDBoolValue>& lst)
    : fcCDKeywordListItem(name),
      m_dependencyValues(lst)
{
}
//---------------------------------------------------------------------------
fcCDKBoolListItem :: fcCDKBoolListItem(const fcTDKeywordListItem& item)
    : fcCDKeywordListItem(item)
{
    fcList<struDependencyValue> lst = item.GetDependencyValuesList();

    for(fc_int i = 0; i < lst.Size(); i++)
    {
        struDBoolValue struBool(lst[i]);

        m_dependencyValues.Append(struBool);
    }
}
//---------------------------------------------------------------------------
fcCDKBoolListItem :: fcCDKBoolListItem(const fcCDKBoolListItem& item)
    : fcCDKeywordListItem(item),
      m_dependencyValues(item.m_dependencyValues)
{
}
//---------------------------------------------------------------------------
fcCDKBoolListItem :: ~fcCDKBoolListItem()
{
}
//---------------------------------------------------------------------------
fcCDependencyListItem* fcCDKBoolListItem :: NewDependencyListItem() const
{
    return new fcCDKBoolListItem(*this);
}
//---------------------------------------------------------------------------
fcDKeywordTypes fcCDKBoolListItem :: DKeywordType() const
{
    return fcDK_BOOL;
}
//---------------------------------------------------------------------------
const fcCDKBoolListItem& fcCDKBoolListItem :: operator= (const fcCDKBoolListItem& item)
{
    m_strEntryName     = item.m_strEntryName;
    m_dependencyValues = item.m_dependencyValues;

    return *this;
}
//---------------------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const fcCDKBoolListItem& fcBool)
{
    stream << " ==> Bool_C: " << fcBool.m_strEntryName << " = ";

    for(fc_int i = 0; i < fcBool.m_dependencyValues.Size(); i++)
    {
        stream << "[ ";
        struDBoolValue stru = fcBool.m_dependencyValues[i];
        stream << stru << " ]";
    }

    return stream;
}
//---------------------------------------------------------------------------
void fcCDKBoolListItem :: SetKeywordValues(const fcList<struDBoolValue>& lst)
{
    m_dependencyValues = lst;
}
//---------------------------------------------------------------------------
const fcList<struDBoolValue>& fcCDKBoolListItem :: GetDependencyValuesList() const
{
    return m_dependencyValues;
}
//---------------------------------------------------------------------------
fc_int fcCDKBoolListItem :: IsDependencyConditionFulfill(fc_bool bRealValue) const
{
    for(fc_int i = 0; i < m_dependencyValues.Size(); i++)
    {
        struDBoolValue stru = m_dependencyValues[i];

        switch(stru.m_depValueType)
        {
            case fcVAL_EQUAL:
                if(stru.m_bValue == bRealValue)
                    return 1;
                break;
            case fcVAL_NOT_EQUAL:
                if(stru.m_bValue != bRealValue)
                    return 1;
                break;
            default:
                break;
        }
    }

    return 0;
}
//---------------------------------------------------------------------------
fcCDKNumberListItem :: fcCDKNumberListItem()
{
}
//---------------------------------------------------------------------------
fcCDKNumberListItem :: fcCDKNumberListItem(const fc_char* name, const fcList<struDNumberValue>& lst)
    : fcCDKeywordListItem(name),
      m_dependencyValues(lst)
{
}
//---------------------------------------------------------------------------
fcCDKNumberListItem :: fcCDKNumberListItem(const fcString& name, const fcList<struDNumberValue>& lst)
    : fcCDKeywordListItem(name),
      m_dependencyValues(lst)
{
}
//---------------------------------------------------------------------------
fcCDKNumberListItem :: fcCDKNumberListItem(const fcTDKeywordListItem& item)
    : fcCDKeywordListItem(item)
{
    fcList<struDependencyValue> lst = item.GetDependencyValuesList();

    for(fc_int i = 0; i < lst.Size(); i++)
    {
        struDNumberValue struNumber(lst[i]);

        m_dependencyValues.Append(struNumber);
    }
}
//---------------------------------------------------------------------------
fcCDKNumberListItem :: fcCDKNumberListItem(const fcCDKNumberListItem& item)
    : fcCDKeywordListItem(item),
      m_dependencyValues(item.m_dependencyValues)
{
}
//---------------------------------------------------------------------------
fcCDKNumberListItem :: ~fcCDKNumberListItem()
{
}
//---------------------------------------------------------------------------
fcCDependencyListItem* fcCDKNumberListItem :: NewDependencyListItem() const
{
    return new fcCDKNumberListItem(*this);
}
//---------------------------------------------------------------------------
fcDKeywordTypes fcCDKNumberListItem :: DKeywordType() const
{
    return fcDK_NUMBER;
}
//---------------------------------------------------------------------------
const fcCDKNumberListItem& fcCDKNumberListItem :: operator= (const fcCDKNumberListItem& item)
{
    m_strEntryName     = item.m_strEntryName;
    m_dependencyValues = item.m_dependencyValues;

    return *this;
}
//---------------------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const fcCDKNumberListItem& fcNum)
{
    stream << " ==> Number_C: " << fcNum.m_strEntryName << " = ";

    for(fc_int i = 0; i < fcNum.m_dependencyValues.Size(); i++)
    {
        stream << "[ ";
        struDNumberValue stru = fcNum.m_dependencyValues[i];
        stream << stru << " ]";
    }

    return stream;
}
//---------------------------------------------------------------------------
void fcCDKNumberListItem :: SetKeywordValues(const fcList<struDNumberValue>& lst)
{
    m_dependencyValues = lst;
}
//---------------------------------------------------------------------------
const fcList<struDNumberValue>& fcCDKNumberListItem :: GetDependencyValuesList() const
{
    return m_dependencyValues;
}
//---------------------------------------------------------------------------
fc_int fcCDKNumberListItem :: IsDependencyConditionFulfill(fc_int nRealValue) const
{
    for(fc_int i = 0; i < m_dependencyValues.Size(); i++)
    {
        struDNumberValue stru = m_dependencyValues[i];

        fc_int nValue1 = 0;
        fc_int nValue2 = 0;
        fc_int nValue  = 0;

        switch(stru.m_depValueType)
        {
            case fcVAL_EQUAL:
                if(stru.m_lstValues.Size() < 1)
                    return 0;
                nValue = stru.m_lstValues[0];
                if(nValue == nRealValue)
                    return 1;
                break;
            case fcVAL_NOT_EQUAL:
                if(stru.m_lstValues.Size() < 1)
                    return 0;
                nValue = stru.m_lstValues[0];
                if(nValue != nRealValue)
                    return 1;
                break;
            case fcVAL_GREATER:
                if(stru.m_lstValues.Size() < 1)
                    return 0;
                nValue = stru.m_lstValues[0];
                if(nValue < nRealValue)
                    return 1;
                break;
            case fcVAL_GREATER_EQ:
                if(stru.m_lstValues.Size() < 1)
                    return 0;
                nValue = stru.m_lstValues[0];
                if(nValue <= nRealValue)
                    return 1;
                break;
            case fcVAL_LOWER:
                if(stru.m_lstValues.Size() < 1)
                    return 0;
                nValue = stru.m_lstValues[0];
                if(nValue > nRealValue)
                    return 1;
                break;
            case fcVAL_LOWER_EQ:
                if(stru.m_lstValues.Size() < 1)
                    return 0;
                nValue = stru.m_lstValues[0];
                if(nValue >= nRealValue)
                    return 1;
                break;
            case fcVAL_GREATER_LOWER:
                if(stru.m_lstValues.Size() < 2)
                    return 0;
                nValue1 = stru.m_lstValues[0];
                nValue2 = stru.m_lstValues[1];
                if(nValue1 < nRealValue && nRealValue < nValue2)
                    return 1;
                break;
            case fcVAL_GREATER_LOWER_EQ:
                if(stru.m_lstValues.Size() < 2)
                    return 0;
                nValue1 = stru.m_lstValues[0];
                nValue2 = stru.m_lstValues[1];
                if(nValue1 < nRealValue && nRealValue <= nValue2)
                    return 1;
                break;
            case fcVAL_GREATER_EQ_LOWER:
                if(stru.m_lstValues.Size() < 2)
                    return 0;
                nValue1 = stru.m_lstValues[0];
                nValue2 = stru.m_lstValues[1];
                if(nValue1 <= nRealValue && nRealValue < nValue2)
                    return 1;
                break;
            case fcVAL_GREATER_EQ_LOWER_EQ:
                if(stru.m_lstValues.Size() < 2)
                    return 0;
                nValue1 = stru.m_lstValues[0];
                nValue2 = stru.m_lstValues[1];
                if(nValue1 <= nRealValue && nRealValue <= nValue2)
                    return 1;
                break;
            case fcVAL_LOWER_GREATER:
                if(stru.m_lstValues.Size() < 2)
                    return 0;
                nValue1 = stru.m_lstValues[0];
                nValue2 = stru.m_lstValues[1];
                if(nValue2 < nRealValue && nRealValue < nValue1)
                    return 1;
                break;
            case fcVAL_LOWER_GREATER_EQ:
                if(stru.m_lstValues.Size() < 2)
                    return 0;
                nValue1 = stru.m_lstValues[0];
                nValue2 = stru.m_lstValues[1];
                if(nValue2 <= nRealValue && nRealValue < nValue1)
                    return 1;
                break;
            case fcVAL_LOWER_EQ_GREATER:
                if(stru.m_lstValues.Size() < 2)
                    return 0;
                nValue1 = stru.m_lstValues[0];
                nValue2 = stru.m_lstValues[1];
                if(nValue2 < nRealValue && nRealValue <= nValue1)
                    return 1;
                break;
            case fcVAL_LOWER_EQ_GREATER_EQ:
                if(stru.m_lstValues.Size() < 2)
                    return 0;
                nValue1 = stru.m_lstValues[0];
                nValue2 = stru.m_lstValues[1];
                if(nValue2 <= nRealValue && nRealValue <= nValue1)
                    return 1;
                break;
            case fcVAL_IN:
                for(fc_int j = 0; j < stru.m_lstValues.Size(); j++)
                {
                    nValue = stru.m_lstValues[j];
                    if(nValue == nRealValue)
                        return 1;
                }
                break;
            case fcVAL_NOT_IN:
                for(fc_int j = 0; j < stru.m_lstValues.Size(); j++)
                {
                    nValue = stru.m_lstValues[j];
                    if(nValue != nRealValue)
                        return 1;
                }
                break;
            default:
                break;
        }
    }

    return 0;
}
//---------------------------------------------------------------------------
fcCDKStringListItem :: fcCDKStringListItem()
{
}
//---------------------------------------------------------------------------
fcCDKStringListItem :: fcCDKStringListItem(const fc_char* name, const fcList<struDStringValue>& lst)
    : fcCDKeywordListItem(name),
      m_dependencyValues(lst)
{
}
//---------------------------------------------------------------------------
fcCDKStringListItem :: fcCDKStringListItem(const fcString& name, const fcList<struDStringValue>& lst)
    : fcCDKeywordListItem(name),
      m_dependencyValues(lst)
{
}
//---------------------------------------------------------------------------
fcCDKStringListItem :: fcCDKStringListItem(const fcTDKeywordListItem& item)
    : fcCDKeywordListItem(item)
{
    fcList<struDependencyValue> lst = item.GetDependencyValuesList();

    for(fc_int i = 0; i < lst.Size(); i++)
    {
        struDStringValue struString(lst[i]);

        m_dependencyValues.Append(struString);
    }
}
//---------------------------------------------------------------------------
fcCDKStringListItem :: fcCDKStringListItem(const fcCDKStringListItem& item)
    : fcCDKeywordListItem(item),
      m_dependencyValues(item.m_dependencyValues)
{
}
//---------------------------------------------------------------------------
fcCDKStringListItem :: ~fcCDKStringListItem()
{
}
//---------------------------------------------------------------------------
fcCDependencyListItem* fcCDKStringListItem :: NewDependencyListItem() const
{
    return new fcCDKStringListItem(*this);
}
//---------------------------------------------------------------------------
fcDKeywordTypes fcCDKStringListItem :: DKeywordType() const
{
    return fcDK_STRING;
}
//---------------------------------------------------------------------------
const fcCDKStringListItem& fcCDKStringListItem :: operator= (const fcCDKStringListItem& item)
{
    m_strEntryName     = item.m_strEntryName;
    m_dependencyValues = item.m_dependencyValues;

    return *this;
}
//---------------------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const fcCDKStringListItem& fcStr)
{
    stream << " ==> String_C: " << fcStr.m_strEntryName << " = ";

    for(fc_int i = 0; i < fcStr.m_dependencyValues.Size(); i++)
    {
        stream << "[ ";
        struDStringValue stru = fcStr.m_dependencyValues[i];
        stream << stru << " ]";
    }

    return stream;
}
//---------------------------------------------------------------------------
void fcCDKStringListItem :: SetKeywordValues(const fcList<struDStringValue>& lst)
{
    m_dependencyValues = lst;
}
//---------------------------------------------------------------------------
const fcList<struDStringValue>& fcCDKStringListItem :: GetDependencyValuesList() const
{
    return m_dependencyValues;
}
//---------------------------------------------------------------------------
fc_int fcCDKStringListItem :: IsDependencyConditionFulfill(const fc_char* strRealValue) const
{
    for(fc_int i = 0; i < m_dependencyValues.Size(); i++)
    {
        struDStringValue stru = m_dependencyValues[i];

        const fc_char* strValue;

        switch(stru.m_depValueType)
        {
            case fcVAL_EQUAL:
                if(stru.m_lstValues.Size() < 1)
                    return 0;
                strValue = stru.m_lstValues[0].Data();
                if( strcmp(strValue, strRealValue) == 0)
                    return 1;
                break;
            case fcVAL_NOT_EQUAL:
                if(stru.m_lstValues.Size() < 1)
                    return 0;
                strValue = stru.m_lstValues[0].Data();
                if( strcmp(strValue, strRealValue) != 0)
                    return 1;
                break;
            case fcVAL_IN:
                for(fc_int j = 0; j < stru.m_lstValues.Size(); j++)
                {
                    strValue = stru.m_lstValues[j].Data();
                    if( strcmp(strValue, strRealValue) == 0)
                        return 1;
                }
                break;
            case fcVAL_NOT_IN:
                for(fc_int j = 0; j < stru.m_lstValues.Size(); j++)
                {
                    strValue = stru.m_lstValues[j].Data();
                    if( strcmp(strValue, strRealValue) != 0)
                        return 1;
                }
                break;
            default:
                break;
        }
    }

    return 0;
}
//---------------------------------------------------------------------------
fc_int fcCDKStringListItem :: IsDependencyConditionFulfill(const fcString& strRealValue) const
{
    return IsDependencyConditionFulfill(strRealValue.Data());
}
//---------------------------------------------------------------------------

