/***************************************************************************
                          fcCEntry.cpp  -  description
                             -------------------
    begin                : 2005/01/06
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include "debug.h"
#include "fcTypes.h"
#include "fcCEntry.h"
#include "fcTEntry.h"
#include "fcCSEntry.h"
#include "fcCKBool.h"
#include "fcCKNumber.h"
#include "fcCKString.h"
#include "fcTDependency.h"
#include "fcCDependency.h"
#include "fcCKDependency.h"
#include "fcDependencyActions.h"
#include "fcStack.h"

//--------------------------------------------------------------------------
fcCEntry :: fcCEntry()
   : template_entry( 0 ),
     dom_node( 0 ),
     gui_buddy(0)
{
    dependents = new fcList< fcCEntry *>;
    m_pCDep    = new fcCDependency;
}
//--------------------------------------------------------------------------
fcCEntry :: fcCEntry( const fcTEntry* tentr )
   : fcBaseEntry( *tentr ),
     template_entry( tentr ),
     dom_node( 0 ),
     gui_buddy(0)
{
    dependents = new fcList< fcCEntry *>;
    m_pCDep    = new fcCDependency;
    DBG_FUNCTION_NAME( "fcCEntry", "fcCEntry" );
    DBG_EXPR( template_entry );
}
//--------------------------------------------------------------------------
fcCEntry :: ~fcCEntry() {
   delete dependents;
   delete m_pCDep;
}
//--------------------------------------------------------------------------
const fcTEntry* fcCEntry :: GetTEntry() const
{ 
   DBG_FUNCTION_NAME( "fcCEntry", "GetTEntry" );
   DBG_EXPR( template_entry );
   return template_entry;
}
//--------------------------------------------------------------------------
fcDOMNode* fcCEntry :: GetDOMNode() const
{ 
   return dom_node; 
}
//--------------------------------------------------------------------------
void fcCEntry :: SetDOMNode( fcDOMNode* node )
{ 
   dom_node = node; 
}
//--------------------------------------------------------------------------
fcCGEntry* fcCEntry :: GetGuiBuddy() const
{
  return gui_buddy;
}
//--------------------------------------------------------------------------
void fcCEntry :: SetGuiBuddy( fcCGEntry* buddy )
{
  gui_buddy = buddy;
}
//--------------------------------------------------------------------------
void fcCEntry :: AddDependents( fcCEntry* c_entry ) {
    dependents-> Append( c_entry );
}
//--------------------------------------------------------------------------
fcCEntry* fcCEntry :: GetDependents( fc_int pos ) {
    return (*dependents)[pos];
}
//--------------------------------------------------------------------------
fc_int fcCEntry :: GetDependentsSize() {
    return dependents -> Size();
}
//--------------------------------------------------------------------------
fcDActionBaseListItem* fcCEntry :: GetCEntryAction(fcCSEntry* pMainCSection)
{
    fcStack<fc_int> stack; 

    for(fc_int i = 0; i < m_pCDep->GetDependencyListSize(); i++)
    {
        fcCDependencyListItem* pListItem = m_pCDep->GetDependencyListItem(i);
        if(!pListItem)
            continue;

        if(pListItem->IsDependencyListItemKeyword())
        {
            fcCDKeywordListItem* pKeyword = dynamic_cast<fcCDKeywordListItem*>(pListItem);
            if(!pKeyword)
                continue;

            fcCEntry* pFindCEntry = pMainCSection->RecursiveFindCEntry(pKeyword->GetKeywordName());
            if(!pFindCEntry)
                continue;

            fcCKBool*            pKBool    = 0;
            fcCKNumber*          pKNumber  = 0;
            fcCKString*          pKString  = 0;
            fcCDKBoolListItem*   pDKBool   = 0;
            fcCDKNumberListItem* pDKNumber = 0;
            fcCDKStringListItem* pDKString = 0;

            switch(pFindCEntry->Type())
            {
                case Fc::Bool:
                    assert(pKeyword->DKeywordType() == fcDK_BOOL);
                    pDKBool = dynamic_cast<fcCDKBoolListItem*>(pKeyword);
                    if(!pDKBool)
                        continue;
                    pKBool = dynamic_cast<fcCKBool*>(pFindCEntry);
                    if(!pKBool)
                        continue;

                    stack.Add(pDKBool->IsDependencyConditionFulfill(pKBool->GetValue()));
                    break;
                case Fc::Number:
                    assert(pKeyword->DKeywordType() == fcDK_NUMBER);
                    pDKNumber = dynamic_cast<fcCDKNumberListItem*>(pKeyword);
                    if(!pDKNumber)
                        continue;
                    pKNumber = dynamic_cast<fcCKNumber*>(pFindCEntry);
                    if(!pKNumber)
                        continue;

                    stack.Add(pDKNumber->IsDependencyConditionFulfill(pKNumber->GetValue()));
                    break;
                case Fc::String:
                    assert(pKeyword->DKeywordType() == fcDK_STRING);
                    pDKString = dynamic_cast<fcCDKStringListItem*>(pKeyword);
                    if(!pDKString)
                        continue;
                    pKString = dynamic_cast<fcCKString*>(pFindCEntry);
                    if(!pKString)
                        continue;

                    stack.Add(pDKString->IsDependencyConditionFulfill(pKString->GetValue()));
                    break;
                default:
                    break;
            }
        } else 
        if(pListItem->IsDependencyListItemRelation())
        {
            fcCDRelationListItem* pRelation = dynamic_cast<fcCDRelationListItem*>(pListItem);
            if(!pRelation)
                continue;

            fc_int nValue1 = 0;
            fc_int nValue2 = 0;
            fc_int nValue  = 0;

            switch(pRelation->GetRelation())
            {
                case fcRELATION_AND:
                    nValue1 = stack.Remove();
                    nValue2 = stack.Remove();
                    nValue  = 0;
                    if(nValue1 == 1 && nValue2 == 1)
                        nValue = 1;
                    stack.Add(nValue);
                    break;
                case fcRELATION_OR:
                    nValue1 = stack.Remove();
                    nValue2 = stack.Remove();
                    nValue  = 0;
                    if(nValue1 == 1 || nValue2 == 1)
                        nValue = 1;
                    stack.Add(nValue);
                    break;
                case fcRELATION_NOT:
                    if(stack.Top() == 0)
                    {
                        stack.Top() = 1;
                    } else
                    if(stack.Top() == 1)
                    {
                        stack.Top() = 0;
                    } 
                    break;
                default:
                    break;
            }
        } else {
            assert(false);
        }
    }

    if(stack.IsEmpty())
        return (fcDActionBaseListItem*)0;

    fc_int nResult = stack.Top();
    stack.EraseAll();

    //std::cout << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" << std::endl;
    //std::cout << "fcCEntry::GetCEntryAction(fcCSEntry* pMainCSection): " << std::endl;
    //std::cout << ".... pCEntry=" << this << ", name=" << GetEntryName() << std::endl;
    //std::cout << ".... nResult = " << nResult << std::endl;
    const fcTEntry* pTEntry = GetTEntry();

    for(fc_int i = 0; i < pTEntry->GetActionListSize(); i++)
    {
        fcDActionBaseListItem* pListItem = pTEntry->GetActionListItem(i);

        if(nResult == pListItem->GetActionValue())
        {
            /* 
            switch(pListItem->ActionType())
            {
                case fcBOOL_ACTION:
                    std::cout <<".... " << *dynamic_cast<fcDActionBoolListItem*>(pListItem) << std::endl;
                    break;
                case fcNUMBER_ACTION:
                    std::cout << ".... " << *dynamic_cast<fcDActionNumberListItem*>(pListItem) << std::endl;
                    break;
                case fcSTRING_ACTION:
                    std::cout << ".... " << *dynamic_cast<fcDActionStringListItem*>(pListItem) << std::endl;
                    break;
            }
            */
            return pListItem;
        }
    }

    return (fcDActionBaseListItem*)0;
}
//--------------------------------------------------------------------------
void fcCEntry :: InitDependents( fcCSEntry* pMainCSection ) {
    fcCSEntry *pCSection = dynamic_cast<fcCSEntry*>(this);   //(fcCSEntry*)this;
    for(fc_int i = 0 ; i < pCSection->Size() ; i++) 
    {
        fcCEntry *pCEntry = dynamic_cast<fcCEntry*>(pCSection->at(i));
        const fcTEntry *pTEntry = pCEntry->GetTEntry();
        for(fc_int j = 0; j < pTEntry->GetDependencyListSize(); j++)
        {
            fcTDependencyListItem *pListItem = pTEntry->GetDependencyListItem(j);
            if(!pListItem)
                continue;

            if(pListItem->IsDependencyListItemRelation())
            {
                fcTDRelationListItem *pRelationItem = dynamic_cast<fcTDRelationListItem*>(pListItem);
                fcCDRelationListItem *pItem = new fcCDRelationListItem(*pRelationItem);
                //std::cout << "C_relation: " << pCEntry->GetEntryName() << ": " << (*pItem) << std::endl;
                (pCEntry->m_pCDep)->AddDependencyListItem(pItem);

                continue;
            }

            assert(pListItem->IsDependencyListItemKeyword());

            fcTDKeywordListItem *pKeywordItem = dynamic_cast<fcTDKeywordListItem*>(pListItem);
            if(!pKeywordItem)
                continue;

            fcCEntry *pFindCEntry = pMainCSection->RecursiveFindCEntry(pKeywordItem->GetKeywordName());
            if(!pFindCEntry)
                continue;
           
            // to pFindCEntry->dependents add pCEntry, because pCEntry is dependent on pFindCEntry
            pFindCEntry->AddDependents(pCEntry);

            fcCDependencyListItem *pItem;

            switch(pFindCEntry->Type())
            {
                case Fc::Bool:
                    pItem = new fcCDKBoolListItem(*pKeywordItem);
                    //std::cout << "C_keyword# " << pCEntry->GetEntryName() << ": " << *dynamic_cast<fcCDKBoolListItem*>(pItem) << std::endl;
                    break;
                case Fc::Number:
                    pItem = new fcCDKNumberListItem(*pKeywordItem);
                    //std::cout << "C_keyword# " << pCEntry->GetEntryName() << ": " << *dynamic_cast<fcCDKNumberListItem*>(pItem) << std::endl;
                    break;
                case Fc::String:
                    pItem = new fcCDKStringListItem(*pKeywordItem);
                    //std::cout << "C_keyword# " << pCEntry->GetEntryName() <<": " << *dynamic_cast<fcCDKStringListItem*>(pItem) << std::endl;
                    break;
                default:
                    pItem = 0;
                    break;
            }

            //to pCEntry->m_pCDep add pItem, because pCEntry is dependent on pFindCEntry
            if(pItem)
                (pCEntry->m_pCDep)->AddDependencyListItem(pItem);

            pItem = 0;
        }

        if(pCEntry->Type() == Fc::Section)
            pCEntry->InitDependents(pMainCSection);
    }
}
//--------------------------------------------------------------------------
void fcCEntry :: Print()
{
    fcCSEntry * pCSection = dynamic_cast<fcCSEntry*>(this);
    for(fc_int i = 0; i < pCSection->Size(); i++)
    {
        //fcCEntry * pCEntry = (*pCSection)[i];
        fcCEntry *pCEntry = dynamic_cast<fcCEntry*>(pCSection->at(i));
        const fcTEntry * pTEntry = pCEntry->GetTEntry();
        pTEntry->PrintDependency();
        pCEntry->PrintDependency();
        
        if(pCEntry->Type() == Fc::Section)
            pCEntry->Print();
    } //for(fc_int i = 0; i < pCSection->Size(); i++)
}
//--------------------------------------------------------------------------
void fcCEntry :: PrintDependency() const
{
    if(!m_pCDep)
        return;

    if(m_pCDep->GetDependencyListSize() > 0)
    {
        std::cout << std::endl;
        std::cout << GetEntryName() << std::endl;
        std::cout << "------------------------------" << std::endl;
        std::cout << (*m_pCDep) << std::endl;
        std::cout << std::endl;
        std::cout << "************************************************************************************" << std::endl;
        std::cout << "************************************************************************************" << std::endl;
        std::cout << "************************************************************************************" << std::endl;
        std::cout << std::endl;
    }
}
//--------------------------------------------------------------------------
