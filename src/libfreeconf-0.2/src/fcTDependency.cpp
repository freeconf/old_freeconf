/************************************************************************
 *                      fcTDependency.cpp
 *                      -------------------
 *   copyright          : (C) 2009 by Frantisek Rolinek
 ************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <cstring>
#include "fcMessageHandler.h"
#include "fcTDependency.h"

//-------------------------------------------------------------
fcTDependencyListItem :: fcTDependencyListItem()
{
}
//-------------------------------------------------------------
fcTDependencyListItem :: fcTDependencyListItem(const fcTDependencyListItem& item)
{
}
//-------------------------------------------------------------
fcTDependencyListItem :: ~fcTDependencyListItem()
{
}
//-------------------------------------------------------------
fcTDRelationListItem :: fcTDRelationListItem()
{
    m_relation = fcNO_RELATION;
}
//-------------------------------------------------------------
fcTDRelationListItem :: fcTDRelationListItem(const fcRelationEnum& _enum)
{
    m_relation = _enum;
}
//-------------------------------------------------------------
fcTDRelationListItem :: fcTDRelationListItem(const fcTDRelationListItem& item)
    : fcTDependencyListItem(item)
{
    m_relation = item.m_relation;
}
//-------------------------------------------------------------
fcTDRelationListItem :: ~fcTDRelationListItem()
{
}
//-------------------------------------------------------------
fcTDependencyListItem* fcTDRelationListItem :: NewDependencyListItem() const 
{
    return new fcTDRelationListItem(*this);
}
//-------------------------------------------------------------
fc_bool fcTDRelationListItem :: IsDependencyListItemKeyword() const
{
    return false;
}
//-------------------------------------------------------------
fc_bool fcTDRelationListItem :: IsDependencyListItemRelation() const
{
    return true;
}
//-------------------------------------------------------------
const fcTDRelationListItem& fcTDRelationListItem :: operator=(const fcTDRelationListItem& item)
{
    m_relation = item.m_relation;

    return *this;
}
//-------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const fcTDRelationListItem& fcRel)
{
    if(fcRel.m_relation == fcRELATION_AND)
        stream << " ==> AND ";
    else if(fcRel.m_relation == fcRELATION_OR)
        stream << " ==> OR ";
    else if(fcRel.m_relation == fcRELATION_NOT)
        stream << " ==> NOT ";
    else 
        std::cout << "fcTDependency.cpp " 
                  << "Item from the dependency list is neither relation-and nor relation-or nor relation-not!" 
                  << std::endl;

    return stream;
}
//-------------------------------------------------------------
void fcTDRelationListItem :: SetRelation(const fcRelationEnum& relation)
{
    m_relation = relation;
}
//-------------------------------------------------------------
const fcRelationEnum& fcTDRelationListItem :: GetRelation() const
{
    return m_relation;
}
//-------------------------------------------------------------
fcTDKeywordListItem :: fcTDKeywordListItem()
{
}
//-------------------------------------------------------------
fcTDKeywordListItem :: fcTDKeywordListItem(const fc_char* name, const fcList<struDependencyValue>& lst)
    : m_dependencyValues(lst)
{
    m_strEntryName      = name;
}
//-------------------------------------------------------------
fcTDKeywordListItem :: fcTDKeywordListItem(const fcString& name, const fcList<struDependencyValue>& lst)
    : m_dependencyValues(lst)
{
    m_strEntryName      = name;
}
//-------------------------------------------------------------
fcTDKeywordListItem :: fcTDKeywordListItem(const fcTDKeywordListItem& item)
    : fcTDependencyListItem(item),
      m_dependencyValues(item.m_dependencyValues)
{
    m_strEntryName      = item.m_strEntryName;
}
//-------------------------------------------------------------
fcTDKeywordListItem :: ~fcTDKeywordListItem()
{
}
//-------------------------------------------------------------
fcTDependencyListItem* fcTDKeywordListItem :: NewDependencyListItem() const 
{
    return new fcTDKeywordListItem(*this);
}
//-------------------------------------------------------------
fc_bool fcTDKeywordListItem :: IsDependencyListItemKeyword() const 
{
    return true;
}
//-------------------------------------------------------------
fc_bool fcTDKeywordListItem :: IsDependencyListItemRelation() const 
{
    return false;
}
//-------------------------------------------------------------
const fcTDKeywordListItem& fcTDKeywordListItem :: operator=(const fcTDKeywordListItem& item)
{
    m_strEntryName     = item.m_strEntryName;
    m_dependencyValues = item.m_dependencyValues;
    
    return *this;
}
//-------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const fcTDKeywordListItem& fcKey)
{
    stream << " ==> " << fcKey.m_strEntryName << " = ";

    for(fc_uint i = 0; i < fcKey.m_dependencyValues.Size(); i++)
    {
        stream << "[ ";
        struDependencyValue stru = fcKey.m_dependencyValues[i];
        stream << stru << " ]";
    }

    return stream;
}
//-------------------------------------------------------------
void fcTDKeywordListItem :: SetKeyword(const fc_char* name, const fcList<struDependencyValue>& lst) 
{
    SetKeywordName(name);
    SetKeywordValues(lst);
}
//-------------------------------------------------------------
void fcTDKeywordListItem :: SetKeyword(const fcString& name, const fcList<struDependencyValue>& lst) 
{
    SetKeywordName(name);
    SetKeywordValues(lst);
}
//-------------------------------------------------------------
void fcTDKeywordListItem :: SetKeywordName(const fc_char* name)
{
    m_strEntryName = name;
}
//-------------------------------------------------------------
void fcTDKeywordListItem :: SetKeywordName(const fcString& name)
{
    m_strEntryName = name;
}
//-------------------------------------------------------------
void fcTDKeywordListItem :: SetKeywordValues(const fcList<struDependencyValue>& lst)
{
    m_dependencyValues = lst;
}
//-------------------------------------------------------------
const fcString& fcTDKeywordListItem :: GetKeywordName() const
{
    return m_strEntryName;
}
//-------------------------------------------------------------
const fcList<struDependencyValue>& fcTDKeywordListItem :: GetDependencyValuesList() const
{ 
    return m_dependencyValues;
}
//-------------------------------------------------------------
fcTDependency :: fcTDependency()
{
    m_pDep    = new fcList< fcTDependencyListItem* >;
    m_pAction = new fcList< fcDActionBaseListItem* >;
}
//-------------------------------------------------------------
fcTDependency:: fcTDependency(fcList<fcTDependencyListItem*> *pDep, fcList<fcDActionBaseListItem*> *pAction,
                              fc_bool bDeepCopy/*=true*/)
{
    if(bDeepCopy)
    {
        m_pDep    = new fcList< fcTDependencyListItem* >;
        if(pDep)
        {
            for(fc_int i = 0; i < pDep->Size(); i++)
                m_pDep->Append(pDep->at(i));
        } 

        m_pAction = new fcList< fcDActionBaseListItem* >;
        if(pAction)
        {
            for(fc_int i = 0; i < m_pAction->Size(); i++)
                m_pAction->Append(pAction->at(i));
        }
    } else {
        if(pDep)
        {
            m_pDep    = pDep;
        } else { 
            m_pDep    = new fcList< fcTDependencyListItem* >;
        }

        if(pAction)
        {
            m_pAction = pAction;
        } else { 
            m_pAction = new fcList< fcDActionBaseListItem* >;
        }
    }
}
//-------------------------------------------------------------
fcTDependency :: fcTDependency(const fcTDependency& item)
{
    m_pDep    = new fcList< fcTDependencyListItem* >;
    if(item.m_pDep)
    {
        for(fc_int i = 0; i < item.m_pDep->Size(); i++)
            m_pDep->Append(item.m_pDep->at(i));
    }

    m_pAction = new fcList< fcDActionBaseListItem* >;
    if(item.m_pAction)
    {
        for(fc_int i = 0; i < item.m_pAction->Size(); i++)
            m_pAction->Append(item.m_pAction->at(i));
    }
}
//-------------------------------------------------------------
fcTDependency :: ~fcTDependency()
{
    EraseDependencyList();
    if(m_pDep)
        delete m_pDep;

    EraseActionList();
    if(m_pAction)
        delete m_pAction;
}
//-------------------------------------------------------------
const fcTDependency& fcTDependency :: operator=(const fcTDependency& item)
{
    if(!m_pDep)
    {
        m_pDep    = new fcList< fcTDependencyListItem* >;
    } else {
        EraseDependencyList();
    }

    if(!m_pAction)
    {
        m_pAction = new fcList< fcDActionBaseListItem* >;
    } else {
        EraseActionList();
    }

    if(item.m_pDep)
    {
        for(fc_int i = 0; i < item.m_pDep->Size(); i++)
            m_pDep->Append(item.m_pDep->at(i));
        //for(fc_int i = 0; i < item.GetDependencyListSize(); i++)
        //    m_pDep->Append(item.GetDependencyListItem(i));
    }

    if(item.m_pAction)
    {
        for(fc_int i = 0; i < item.m_pAction->Size(); i++)
            m_pAction->Append(item.m_pAction->at(i));
        //for(fc_int i = 0; i < item.GetActionListSize(); i++)
        //    m_pAction->Append(item.GetActionListItem(i));
    }

    return *this;
}
//-------------------------------------------------------------
void fcTDependency :: AddDependencyListItem(fcTDependencyListItem* pItem)
{
    if(!m_pDep)
    {
        MessageHandler.Error("fcTDependency.cpp","AddDependencyListItem(fcTDependencyListItem* pItem)",
                "m_pDep pointer is not allocated!");
        return;
    }
    
    if(!pItem)
    {
        MessageHandler.Error("fcTDependency.cpp","AddDependencyListItem(fcTDependencyListItem* pItem)",
                "the given pointer (pItem) is not allocated!");
        return;
    }

    m_pDep->Append(pItem);
}
//-------------------------------------------------------------
void fcTDependency :: AddActionListItem(fcDActionBaseListItem* pItem)
{
    if(!m_pAction)
    {
        MessageHandler.Error("fcTDependency.cpp","AddActionListItem(fcDActionBaseListItem* pItem)",
                "m_pAction pointer is not allocated!");
        return;
    }

    if(!pItem)
    {
        MessageHandler.Error("fcTDependency.cpp","AddActionListItem(fcDActionBaseListItem* pItem)",
                "the given pointer (pItem) is not allocated!");
        return;
    }

    m_pAction->Append(pItem);
}
//-------------------------------------------------------------
void fcTDependency :: SetDependencyListItem(fcTDependencyListItem* pItem, fc_uint pos)
{
    if(!m_pDep)
    {
        MessageHandler.Error("fcTDependency.cpp", 
                "SetDependencyListItem(fcTDependencyListItem* pItem, fc_uint pos)", 
                "m_pDep is not allocated!");
        return;
    }

    if( IsPositionInDependencyListNotValid(pos) )
    {
        MessageHandler.Warning("fcTDependency.cpp", 
                "SetDependencyListItem(fcTDependencyListItem* pItem, fc_uint pos)", 
                "pos index is out of range!");
        return;
    }

    fcTDependencyListItem *pOldItem  = m_pDep->at(pos);
    m_pDep->at(pos) = pItem;

    delete pOldItem;
}
//-------------------------------------------------------------
void fcTDependency :: SetActionListItem(fcDActionBaseListItem* pItem, fc_uint pos)
{
    if(!m_pAction)
    {
        MessageHandler.Error("fcTDependency.cpp", 
                "SetActionListItem(fcDActionBaseListItem* pItem, fc_uint pos)",
                "m_pAction is not allocated!");
        return;
    }

    if( IsPositionInActionListNotValid(pos) )
    {
        MessageHandler.Warning("fcTDependency.cpp", 
                "SetActionListItem(fcDActionBaseListItem* pItem, fc_uint pos)",
                "pos index is out of range!");
        return;
    }

    fcDActionBaseListItem *pOldItem = m_pAction->at(pos);
    m_pAction->at(pos) = pItem;

    delete pOldItem;
}
//-------------------------------------------------------------
fcTDependencyListItem* fcTDependency:: GetDependencyListItem(fc_uint pos)
{
    if(!m_pDep)
    {
        MessageHandler.Error("fcTDependency.cpp", "GetDependencyListItem(fc_uint pos)", "m_pDep is not allocated!");
        return (fcTDependencyListItem*) 0;
    }

    if( IsPositionInDependencyListNotValid(pos) )
    {
        MessageHandler.Warning("fcTDependency.cpp", "GetDependencyListItem(fc_uint pos)", "pos index is out of range!");
        return (fcTDependencyListItem*) 0;
    }

    //return (*dep)[pos];
    return m_pDep->at(pos);
}
//-------------------------------------------------------------
fcTDependencyListItem* fcTDependency:: GetDependencyListItem(fc_uint pos) const
{
    if(!m_pDep)
    {
        MessageHandler.Error("fcTDependency.cpp", "GetDependencyListItem(fc_uint pos) const", 
                "m_pDep is not allocated!");
        return (fcTDependencyListItem*) 0;
    }

    if( IsPositionInDependencyListNotValid(pos) )
    {
        MessageHandler.Warning("fcTDependency.cpp", "GetDependencyListItem(fc_uint pos) const", 
                "pos index is out of range!");
        return (fcTDependencyListItem*) 0;
    }

    //return (*dep)[pos];
    return m_pDep->at(pos);
}
//-------------------------------------------------------------
fcDActionBaseListItem* fcTDependency :: GetActionListItem(fc_uint pos)
{
    if(!m_pAction)
    {
        MessageHandler.Error("fcTDependency.cpp", "GetActionListItem(fc_uint pos)",
                "m_pAction is not allocated!");
        return (fcDActionBaseListItem*) 0;
    }

    if( IsPositionInActionListNotValid(pos) )
    {
        MessageHandler.Warning("fcTDependency.cpp", "GetActionListItem(fc_uint pos)",
                "pos index is out of range!");
        return (fcDActionBaseListItem*) 0;
    }
    
    return m_pAction->at(pos);
}
//-------------------------------------------------------------
fcDActionBaseListItem* fcTDependency :: GetActionListItem(fc_uint pos) const
{
    if(!m_pAction)
    {
        MessageHandler.Error("fcTDependency.cpp", "GetActionListItem(fc_uint pos) const",
                "m_pAction is not allocated!");
        return (fcDActionBaseListItem*) 0;
    }

    if( IsPositionInActionListNotValid(pos) )
    {
        MessageHandler.Warning("fcTDependency.cpp", "GetActionListItem(fc_uint pos) const",
                "pos index is out of range!");
        return (fcDActionBaseListItem*) 0;
    }
    
    return m_pAction->at(pos);
}
//-------------------------------------------------------------
fc_uint fcTDependency:: GetDependencyListSize() const
{
    if(!m_pDep)
    {
        MessageHandler.Warning("fcTDependency.cpp", "GetDependencyListSize() const",
                "m_pDep is not allocated!");
        return 0;
    }

    return m_pDep->Size();
}
//-------------------------------------------------------------
fc_uint fcTDependency :: GetActionListSize() const
{
    if(!m_pAction)
    {
        MessageHandler.Warning("fcTDependency.cpp", "GetActionListSize() const",
                "m_pAction is not allocated!");
        return 0;
    }

    return m_pAction->Size();
}
//-------------------------------------------------------------
fc_bool fcTDependency :: IsPositionInDependencyListValid(fc_uint pos) const
{
    if(!m_pDep)
    {
        MessageHandler.Error("fcTDependency.cpp", "IsPositionInDependencyListValid(fc_uint pos) const",
                "m_pDep is not allocated!");
        return false;
    }

    if( pos < 0 || pos > m_pDep->Size() -1 )
        return false;
    return true;
}
//-------------------------------------------------------------
fc_bool fcTDependency :: IsPositionInDependencyListNotValid(fc_uint pos) const
{
    if(!m_pDep)
    {
        MessageHandler.Error("fcTDependency.cpp", "IsPositionInDependencyListNotValid(fc_uint pos) const",
                "m_pDep is not allocated!");
        return true;
    }

    if( pos < 0 || pos > m_pDep->Size() -1 )
        return true;
    return false;
}
//-------------------------------------------------------------
fc_bool fcTDependency :: IsPositionInActionListValid(fc_uint pos) const
{
    if(!m_pAction)
    {
        MessageHandler.Error("fcTDependency.cpp", "IsPositionInActionListValid(fc_uint pos) const",
                "m_pAction is not allocated!");
        return false;
    }

    if( pos < 0 || pos > m_pAction->Size() -1 )
        return false;
    return true;
}
//-------------------------------------------------------------
fc_bool fcTDependency :: IsPositionInActionListNotValid(fc_uint pos) const
{
    if(!m_pAction)
    {
        MessageHandler.Error("fcTDependency.cpp", "IsPositionInActionListNotValid(fc_uint pos) const",
                "m_pAction is not allocated!");
        return true;
    }

    if( pos < 0 || pos > m_pAction->Size() -1 )
        return true;
    return false;
}
//-------------------------------------------------------------
void fcTDependency :: EraseDependencyList()
{
    if(m_pDep)
        m_pDep->EraseAll();
}
//-------------------------------------------------------------
void fcTDependency :: EraseActionList()
{
    if(m_pAction)
        m_pAction->EraseAll();
}
//-------------------------------------------------------------
std::ostream& operator<< (std::ostream& stream, const fcTDependency& fcTDep)
{
    if(!fcTDep.m_pDep && !fcTDep.m_pAction)
        return stream;

    if(fcTDep.m_pDep)
    {
        fcTDependencyListItem    * pItem     = 0;
        fcTDKeywordListItem      * pKeyword  = 0;
        fcTDRelationListItem     * pRelation = 0;

        fc_int nSize = fcTDep.m_pDep->Size();
        for(fc_int i = 0; i < nSize; i++)
        {
            pItem = fcTDep.m_pDep->at(i); //pItem = (*(fcTDep.m_pDep))[i];

            if(!pItem)
            {
                break;
            } 

            if(pItem->IsDependencyListItemKeyword())
            {
                pKeyword = dynamic_cast<fcTDKeywordListItem*>(pItem);

                if(!pKeyword)
                    break;   // continue; 

                stream << (*pKeyword);

            } else {
                if(pItem->IsDependencyListItemRelation())
                {
                    pRelation = dynamic_cast<fcTDRelationListItem*>(pItem);

                    if(!pRelation)
                        break;   // continue;

                    stream << (*pRelation);

                } else {
                    std::cerr << "fcTDependency.cpp " << 
                        "std::ostream& operator<< (std::ostream& stream, const fcTDependency& fctDep)" <<
                        "Item from the m_pDep list at position " << i << 
                        " is neither keyword nor relation!" << std::endl;

                    break;   // continue;
                }
            }

            pItem     = 0;
            pKeyword  = 0;
            pRelation = 0;
        }
    }

    std::cout << "\n    " << std::endl;

    if(fcTDep.m_pAction)
    {
        fcDActionBaseListItem   * pItem   = 0;
        fcDActionBoolListItem   * pBool   = 0;
        fcDActionNumberListItem * pNumber = 0;
        fcDActionStringListItem * pString = 0;

        fc_int nSize = fcTDep.m_pAction->Size();
        for(fc_int i = 0; i < nSize; i++)
        {
            pItem = fcTDep.m_pAction->at(i);

            switch(pItem->ActionType())
            {
                case fcBOOL_ACTION:
                    pBool = dynamic_cast<fcDActionBoolListItem*>(pItem);
                    if(!pBool)
                        break; // continue;
                    stream << (*pBool);
                    break;
                case fcNUMBER_ACTION:
                    pNumber = dynamic_cast<fcDActionNumberListItem*>(pItem);
                    if(!pNumber)
                        break; // continue;
                    stream << (*pNumber);
                    break;
                case fcSTRING_ACTION:
                    pString = dynamic_cast<fcDActionStringListItem*>(pItem);
                    if(!pString)
                        break;  // continue;
                    stream << (*pString);
                    break;
                case fcNO_ACTION:
                default:
                    std::cerr << "fcTDependency.cpp " <<
                        "std::ostream& operator<< (std::ostream& stream, const fcTDependency& fcTDep)" <<
                        "Item from the m_pAction list at position " << i << 
                        " is neither bool action nor number action nor string action!" << std::endl;
                    break;
            }

            pItem   = 0;
            pBool   = 0;
            pNumber = 0;
            pString = 0;
        }
    }

    return stream;
}
