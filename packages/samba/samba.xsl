<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text" indent="yes"/>
    
<xsl:template match="samba-config">
<xsl:apply-templates/>
</xsl:template>

<xsl:template match="global-settings">
[global]<xsl:apply-templates/>
</xsl:template>

<xsl:template match="workgroup">
   workgroup = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="server-string">
   server string = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="client-code-page">
   client code page = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="character-set">
   character set = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="log-file">
   log file = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="max-log-size">
   max log size = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="encrypt-passwords">
   encrypt passwords = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="smb-passwords-file">
   smb passwords file = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="share-definition">
[<xsl:value-of select="name"/>]<xsl:apply-templates/>
</xsl:template>

<xsl:template match="name">
   <xsl:apply-templates/>
</xsl:template>

<xsl:template match="comment">
   comment = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="writable">
   writable = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="locking">
   locking = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="path">
   path = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="public">
   public = <xsl:apply-templates/>
</xsl:template>

</xsl:stylesheet>
