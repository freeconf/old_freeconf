<?xml version="1.0"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text" indent="yes"/>

<xsl:template match="mplayer-config">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="general-options">
    <!--    # general options -->
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="quiet">
    quiet = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="really-quiet">
    really-quiet = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="decoding_filtering-options">
    <!-- # decoding/filtering options -->
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="nosound">
    nosound = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="novideo">
    novideo = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="x">
    x = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="y">
    y = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="gui">
    <!-- # gui -->
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="skin">
    skin = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="OSD_subtitle-options">
    <!-- #OSD/subtitle options -->
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="fontconfig">
    fontconfig = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="font">
    font = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="subfont-encoding">
    subfont-encoding = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="subcp">
    subcp = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="subfont-text-scale">
    subfont-text-scale = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="sub-fuzziness">
    sub-fuzziness = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="subpos">
    subpos = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="subalign">
    subalign = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="noautosub">
    noautosub = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="osdlevel">
    osdlevel = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="video-output-drivers">
    <!-- #video output drivers -->
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="vo">
    vo = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="player-options">
    <!-- #player options -->
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="fixed-vo">
    fixed-vo = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="video-output-options">
    <!-- #video output options -->
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="brightness">
    brightness = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="contrast">
    contrast = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="hue">
    hue = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="saturation">
    saturation = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="stop-xscreensaver">
    stop-xscreensaver = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="fs">
    fs = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="audio-output-drivers">
    <!-- #audio output drivers -->
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="ao">
    ao = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="DEMUXER_stream-options">
    <!-- #DEMUXER/stream options -->
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="channels">
    channels = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="cdrom-device">
    cdrom-device = <xsl:apply-templates/>
</xsl:template>

<xsl:template match="dvd-device">
    dvd-device = <xsl:apply-templates/>
</xsl:template>

</xsl:stylesheet>
