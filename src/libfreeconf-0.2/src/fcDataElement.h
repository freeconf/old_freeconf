/***************************************************************************
                          fcDataElement.h  -  description
                             -------------------
    begin                : 2004/04/11 14:01
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCDATAELEMENT_H__
#define __FCDATAELEMENT_H__

#define DE_DATA( data_element ) ( * ( data_element -> Data() ) )
#define DE_NEXT( data_element ) data_element = data_element -> Next()

//! Data element for fcList and fcStack
template< class T > class fcDataElement
{
   //! Main data
   T data;

   //! Pointer to the next element
   fcDataElement< T >* next;

   //! Pointer to the previous element
   fcDataElement< T >* previous;

   public:
   //! Basic constructor
   fcDataElement()
      : next( 0 ),
        previous( 0 ){};

   //! Constructor with given data and possibly pointer to next or previous element
   fcDataElement( const T& dt, 
                  fcDataElement< T >* prv = 0,
                  fcDataElement< T >* nxt = 0 )
      : data( dt ), 
        next( nxt ),
        previous( prv ){};

   //! Destructor
   ~fcDataElement(){};

   //! Return data for non-const instances
   T& Data() { return data; };

   //! Return data for const instances
   const T& Data() const { return data; };

   //! Return pointer to the next element for non-const instances
   fcDataElement< T >*& Next() { return next; };

   //! Return pointer to the next element for const instances
   const fcDataElement< T >* Next() const { return next; };

   //! Return pointer to the previous element for non-const instances
   fcDataElement< T >*& Previous() { return previous; };

   //! Return pointer to the previous element for const instances
   const fcDataElement< T >* Previous() const { return previous; };

};

#endif
