/***************************************************************************
                          fcCKDependency.h  -  description
                             -------------------
    copyright            : (C) 2009 by Frantisek Rolinek
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCCKDEPENDENCY_H__
#define __FCCKDEPENDENCY_H__

#include <ostream>
#include "fcTypes.h"
#include "fcString.h"
#include "fcList.h"
#include "fcSAXTemplateConst.h"
#include "fcDependencyStruct.h"
#include "fcCDependency.h"

class fcCDKBoolListItem : public fcCDKeywordListItem
{
    //! Class members
    private:
        fcList<struDBoolValue> m_dependencyValues;

    //! Constructors & destructor
    public:
        fcCDKBoolListItem();
        fcCDKBoolListItem(const fc_char* name, const fcList<struDBoolValue>& lst);
        fcCDKBoolListItem(const fcString& name, const fcList<struDBoolValue>& lst);
        fcCDKBoolListItem(const fcTDKeywordListItem& item);
        fcCDKBoolListItem(const fcCDKBoolListItem& item);
        virtual ~fcCDKBoolListItem();

    //! Overrided functions from base class
    public:
        //! Allocates and returns new instance of this class
        virtual fcCDependencyListItem* NewDependencyListItem() const;
        //! Dependency keyword type getter
        virtual fcDKeywordTypes DKeywordType() const;

    //! Overrided operator
    public:
        const fcCDKBoolListItem& operator= (const fcCDKBoolListItem& item);
        friend std::ostream& operator<< (std::ostream& stream, const fcCDKBoolListItem& fcBool);

    //! Setters
    public:
        //! Dependency values setter
        void SetKeywordValues(const fcList<struDBoolValue>& lst);

    //! Getters
    public:
        //! Dependency values getter
        const fcList<struDBoolValue>& GetDependencyValuesList() const;
        //! Function for comparing real and dependency value 
        /*! This function takes a real value (value, that is set on a current entry)
         *  and compares it with the values, stored in m_dependencyValues list.
         *  The return value is 1 if the real value correspond (fit) with values in the
         *  m_dependencyValues list. If there is no correspond (no fit), the return value is 0.
         */
        fc_int IsDependencyConditionFulfill(fc_bool bRealValue) const;
};

class fcCDKNumberListItem : public fcCDKeywordListItem
{
    //! Class members
    private:
        fcList<struDNumberValue> m_dependencyValues;

    //! Constructors & destructor
    public:
        fcCDKNumberListItem();
        fcCDKNumberListItem(const fc_char* name, const fcList<struDNumberValue>& lst);
        fcCDKNumberListItem(const fcString& name, const fcList<struDNumberValue>& lst);
        fcCDKNumberListItem(const fcTDKeywordListItem& item);
        fcCDKNumberListItem(const fcCDKNumberListItem& item);
        virtual ~fcCDKNumberListItem();

    //! Overrided functions from base class
    public:
        //! Allocates and returns new instance of this class
        virtual fcCDependencyListItem* NewDependencyListItem() const;
        //! Dependency keyword type getter
        virtual fcDKeywordTypes DKeywordType() const;

    //! Overrided operator
    public:
        const fcCDKNumberListItem& operator= (const fcCDKNumberListItem& item);
        friend std::ostream& operator<< (std::ostream& stream, const fcCDKNumberListItem& fcNum);

    //! Setters
    public:
        //! Dependency values setter
        void SetKeywordValues(const fcList<struDNumberValue>& lst);

    //! Getters
    public:
        //! Dependency values getter
        const fcList<struDNumberValue>& GetDependencyValuesList() const;
        //! Function for comparing real and dependency value 
        /*! This function takes a real value (value, that is set on a current entry)
         *  and compares it with the values, stored in m_dependencyValues list.
         *  The return value is 1 if the real value correspond (fit) with values in the
         *  m_dependencyValues list. If there is no correspond (no fit), the return value is 0.
         */
        fc_int IsDependencyConditionFulfill(fc_int nRealValue) const;
};

class fcCDKStringListItem : public fcCDKeywordListItem
{
    //! Class members
    private:
        fcList<struDStringValue> m_dependencyValues;

    //! Constructors & destructor
    public:
        fcCDKStringListItem();
        fcCDKStringListItem(const fc_char* name, const fcList<struDStringValue>& lst);
        fcCDKStringListItem(const fcString& name, const fcList<struDStringValue>& lst);
        fcCDKStringListItem(const fcTDKeywordListItem& item);
        fcCDKStringListItem(const fcCDKStringListItem& item);
        virtual ~fcCDKStringListItem();

    //! Overrided functions from base class
    public:
        //! Allocates and returns new instance of this class
        virtual fcCDependencyListItem* NewDependencyListItem() const;
        //! Dependency keyword type getter
        virtual fcDKeywordTypes DKeywordType() const;

    //! Overrided operator
    public:
        const fcCDKStringListItem& operator= (const fcCDKStringListItem& item);
        friend std::ostream& operator<< (std::ostream& stream, const fcCDKStringListItem& fcStr);

    //! Setters
    public:
        //! Dependency values setter
        void SetKeywordValues(const fcList<struDStringValue>& lst);

    //! Getters
    public:
        //! Dependency values getter
        const fcList<struDStringValue>& GetDependencyValuesList() const;
        //! Function for comparing real and dependency value 
        /*! This function takes a real value (value, that is set on a current entry)
         *  and compares it with the values, stored in m_dependencyValues list.
         *  The return value is 1 if the real value correspond (fit) with values in the
         *  m_dependencyValues list. If there is no correspond (no fit), the return value is 0.
         */
        fc_int IsDependencyConditionFulfill(const fc_char* strRealValue) const;
        fc_int IsDependencyConditionFulfill(const fcString& strRealValue) const;
};

#endif
