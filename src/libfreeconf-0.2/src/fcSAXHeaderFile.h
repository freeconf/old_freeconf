/***************************************************************************
                          fcSAXHeaderFile.h  -  description
                             -------------------
    begin                : 2004/10/15
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCSAXHEADERFILE_H__
#define __FCSAXHEADERFILE_H__

#include "fcSAXFile.h"
#include "fcHeaderStructure.h"

enum fcHeaderEnum { fcNO_HDR_ELEMENT,
                    fcTEMPLATE_FILE,
                    fcGUI_TEMPLATE_FILE,
                    fcSTRING_LISTS_FILE,
                    fcHELP_FILE,
                    fcGUI_LABEL_FILE,
                    fcDEFAULT_VALUES_FILE,
                    fcTRANSFORM_FILE,
                    fcOUTPUT_FILE,
                    fcNATIVE_OUTPUT_FILE,
                    fcHeaderEnumEnd };

class fcSAXHeaderFile : protected fcSAXFile
{
   //! Pointer to fcHeaderStructure being created
   /*! It is set in ParseHeaderFile() method
    */
   fcHeaderStructure* header_structure;

   //! Flag for the tag freeconf-header
   fc_bool enclosing_tag;

   //! Id of current xml header element
   fcHeaderEnum header_element;

   //! String storing help file language
   fcString help_language;

   public:
   //! Basic constructor
   fcSAXHeaderFile();
   
   //! Destructor
   ~fcSAXHeaderFile();
   
   protected:
   //! Implementations of the SAX DocumentHandler start element
   void startElement(  const   XMLCh* const    uri,
                       const   XMLCh* const    localname,
                       const   XMLCh* const    qname,
                       const   Attributes&     attributes);

   //! Implementations of the SAX DocumentHandler end element
   void endElement( const XMLCh* const uri, 
                    const XMLCh* const localname, 
                    const XMLCh* const qname);

   //! Implementations of the SAX DocumentHandler characters
   void characters( const XMLCh* const chars, 
                    const unsigned int length);

   public:
   //! Method for parsing header file
   /*! This method just sets pointer this -> header_structure
       and the calls fcSAXFile :: Parse()
    */
   fc_bool ParseHeaderFile( const fc_char* file_name,
                            fcHeaderStructure* fc_header_structure );

};

#endif
