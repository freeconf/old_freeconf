#!/usr/bin/python

from PyQt4 import QtCore, QtGui

class MyTextEdit (QtGui.QTextEdit):
	"""Class overridden from QTextEdit. It emits a signal everytime the text is changed"""
	def __init__ (self, parent):
		QtGui.QTextEdit.__init__(self, parent)
		QtCore.QObject.connect(self, QtCore.SIGNAL("textChanged()"), self.__emitChange)

	def __emitChange (self):
		self.emit(QtCore.SIGNAL("textEdited(QString)"), self.toPlainText())