/***************************************************************************
                          fcCKList.cpp  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "fcCKList.h"

//--------------------------------------------------------------------------
fcCKList :: fcCKList()
{
}
//--------------------------------------------------------------------------
fcCKList :: fcCKList( const fcCKList& entry  )
   : fcCKEntry( entry ),
     fcList< fcString >( entry )
{
}
//--------------------------------------------------------------------------
fcCKList :: ~fcCKList()
{
}
//--------------------------------------------------------------------------
fcBaseEntry* fcCKList :: NewEntry() const
{
   return new fcCKList( * this );
}
//--------------------------------------------------------------------------
Fc::EntryTypes fcCKList :: Type() const
{ 
   return Fc::List;
}
//--------------------------------------------------------------------------
/*void fcCKList :: UpdateDOMNode( fcDOMNode* parent_node,
                                fcDOMDocument* doc )
{
   assert( 0 );
}*/
//--------------------------------------------------------------------------
#ifdef DEBUG
void fcCKList :: Print( ostream& stream ) const
{
   stream << "LIST " << GetEntryName() << ":" << endl;
   for( fc_int i = 0; i < Size(); i ++ )
      stream << "+ " << operator[]( i ) << endl;
}
#endif
