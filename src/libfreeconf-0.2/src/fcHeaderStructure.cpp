/***************************************************************************
                          fcHeaderStructure.cpp  -  description
                             -------------------
    begin                : 2004/10/14
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "debug.h"
#include "fcHeaderStructure.h"
#include "fcDataElement.h"

//--------------------------------------------------------------------------
fcHeaderStructure :: fcHeaderStructure()
{
}
//--------------------------------------------------------------------------
fcHeaderStructure :: ~fcHeaderStructure()
{
}
//--------------------------------------------------------------------------
void fcHeaderStructure :: SetPackageName( const fc_char* name )
{
   package_name. SetString( name );
}
//--------------------------------------------------------------------------
const fcString& fcHeaderStructure :: GetPackageName() const
{
   return package_name;
}
//--------------------------------------------------------------------------
void fcHeaderStructure :: SetTemplateFile( const fc_char* name )
{
   template_file. SetString( name );
}
//--------------------------------------------------------------------------
const fcString& fcHeaderStructure :: GetTemplateFile() const
{
   return template_file;
}
//--------------------------------------------------------------------------
void fcHeaderStructure :: SetGuiTemplateFile( const fc_char* name )
{
  gui_template_file. SetString( name );
}
//--------------------------------------------------------------------------
const fcString& fcHeaderStructure :: GetGuiTemplateFile() const
{
  return gui_template_file;
}
//--------------------------------------------------------------------------
void fcHeaderStructure :: SetHelpFile( const fc_char* name )
{
   help_file. SetString( name );
}
//--------------------------------------------------------------------------
const fcString& fcHeaderStructure :: GetHelpFile() const
{
   return help_file;
}
//--------------------------------------------------------------------------
void fcHeaderStructure :: SetGuiLabelFile( const fc_char* name )
{
  gui_label_file. SetString( name );
}
//--------------------------------------------------------------------------
const fcString& fcHeaderStructure :: GetGuiLabelFile() const
{
  return gui_label_file;
}
//--------------------------------------------------------------------------
void fcHeaderStructure :: SetDefaultValuesFile( const fc_char* name )
{
   default_values_file. SetString( name );
}
//--------------------------------------------------------------------------
const fcString& fcHeaderStructure :: GetDefaultValuesFile() const
{
   return default_values_file;
}
//--------------------------------------------------------------------------
fcList< fcString >& fcHeaderStructure :: StringListFiles()
{
   return string_list_files;
}
//--------------------------------------------------------------------------
void fcHeaderStructure :: SetTransformFile( const fc_char* name )
{
   transform_file. SetString( name );
}
//--------------------------------------------------------------------------
const fcString& fcHeaderStructure :: GetTransformFile() const
{
   return transform_file;
}
//--------------------------------------------------------------------------
void fcHeaderStructure :: SetOutputFile( const fc_char* name )
{
   output_file. SetString( name );
}
//--------------------------------------------------------------------------
const fcString& fcHeaderStructure :: GetOutputFile() const
{
   return output_file;
}
//--------------------------------------------------------------------------
void fcHeaderStructure :: SetNativeFile( const fc_char* name )
{
   native_file. SetString( name );
}
//--------------------------------------------------------------------------
const fcString& fcHeaderStructure :: GetNativeFile() const
{
   return native_file;
}
//--------------------------------------------------------------------------

