#!/usr/bin/python
#
# Kernel Config Dependencies
#
# begin: 8.3.2008 by Jan Ehrenberger <lankvil@seznam.cz>
#
# Parse kernel configuration files dependencies using simple Top-Down parser
#

import re
from kcLog import log


class TopDownParser:
	"""Abstract class defining Top Down parser"""

	symbol_table = {}
	token = None

	class SymbolBase:
		id = None # Token name
		value = None # used by literals
		first = second = None # used by tree nodes

		def nud(self):
			raise SyntaxError("Syntax error (%r)." % (self.id, ))

		def led(self, left):
			raise SyntaxError("Unknown operator (%r)." % (self.id, ))

		def __repr__(self):
			if self.id == "(name)" or self.id == "(literal)":
				return "(%s %s)" % (self.id[1:-1], self.value)
			out = [self.id, self.first, self.second]
			out = map(str, filter(None, out))
			return "(" + " ".join(out) + ")"
	
	def create_symbol(self, id, bp=0):
		"""Symbol class factory function. Add new symbol to lookup table."""
		if id in self.symbol_table:
			s = self.symbol_table[id]
			s.lbp = max(bp, s.lbp)
		else:
			# Create new class derivec from SymbolBase
			class s(self.SymbolBase):
				pass
			s.__name__ = "symbol-" + id
			s.id = id
			s.lbp = bp
			self.symbol_table[id] = s
		return s
	
	def tokenize(self, text):
		raise TypeError("Abstract method tokenize called!")

	def expression(self, rbp=0):
		t = self.token
		self.token = self.next()
		left = t.nud()
		while rbp < self.token.lbp:
			t = self.token
			self.token = self.next()
			left = t.led(left)
		return left
	
	def infix(self, id, bp):
		def led(this, left):
			this.first = left
			this.second = self.expression(bp)
			return this
		self.create_symbol(id, bp).led = led

	def prefix(self, id, bp):
		def nud(this):
			this.first = self.expression(bp)
			this.second = None
			return this
		self.create_symbol(id).nud = nud

	def infix_r(self, id, bp):
		def led(this, left):
			this.first = left
			this.second = self.expression(bp-1)
			return this
		self.create_symbol(id, bp).led = led
	

	def parse(self, text):
		log.debug("Parsing dependency: " + text)
		try:
			self.next = self.tokenize(text).next
			self.token = self.next()
			return self.expression()
		except StopIteration:
			# Parsing of empty string should result in None
			return None
	
	def advance(self, id):
		"""Set expectation to next token and go to next token if it occured in the code."""
		if id and self.token.id != id:
			raise SyntaxError("Expected (%r)" % (id,))
		self.token = self.next()
	
	def __init__(self):
		self.create_symbol("(literal)").nud = lambda self: self
		self.create_symbol("(name)").nud = lambda self: self
		self.create_symbol("(end)")

class DependencyParser(TopDownParser):
	# Regular expression dividing text into tokens.
	# Should return tuple of (name, literal, operator) for each token of input text.
	r_token = re.compile("\s*(?:(\w+)|('[^']*'|\"[^\"]*\")|(&&|[|]{2}|!=|=|\(|\)|!))")

	def strip_quotes(self, text):
		return text.strip("'\"")

	def tokenize(self, text):
		"""Generator that will tokenize text and return symbol objects"""
		for name, literal, operator in self.r_token.findall(text):
			if literal:
				# Process litelral token. par.ex.: 'y'
				symbol = self.symbol_table["(literal)"]
				s = symbol()
				s.value = self.strip_quotes(literal)
				yield s
			elif name:
				# Process name token. par.ex.: MODULES
				symbol = self.symbol_table["(name)"]
				s = symbol()
				s.value = name
				yield s
			else:
				# Process operator token. par.ex.: &&
				symbol = self.symbol_table.get(operator)
				if not symbol:
					raise SyntaxError("Unknown operator (%s)" % (operator, ))
				yield symbol()
		symbol = self.symbol_table["(end)"]
		yield symbol()

	def __init__(self):
		TopDownParser.__init__(self)

		#### Define our dependency language ################
		# Here is the definition of dependencies from
		# kernel documentation:
		#
		# <expr> ::= <symbol>                            (1)
		#	    <symbol> '=' <symbol>                (2)
		#	    <symbol> '!=' <symbol>               (3)
		#	    '(' <expr> ')'                       (4)
		#	    '!' <expr>                           (5)
		#	    <expr> '&&' <expr>                   (6)
		#	    <expr> '||' <expr>                   (7)

		self.infix("=", 10)
		self.infix("!=", 10)
		self.infix("&&", 20)
		self.infix("||", 20)
		self.prefix("!", 100)

		# Create parthesis rules
		def nud(this):
			expr = self.expression()
			self.advance(")")
			return expr
		self.create_symbol("(", 200).nud = nud # ( Should have highest priority
		self.create_symbol(")")


	def concatenate_and(self, dep_a, dep_b):
		"""Concatenate two dependency parse trees with relation &&"""
		if dep_a == None:
			return dep_b
		if dep_b == None:
			return dep_a
		symbol = self.symbol_table["&&"] # Find && object
		op_and = symbol()
		op_and.first = dep_a
		op_and.second = dep_b
		return op_and
		

