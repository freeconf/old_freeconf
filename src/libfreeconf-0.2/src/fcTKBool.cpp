/***************************************************************************
                          fcTKBool.cpp  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <cstring>
#include "fcTKBool.h"

//--------------------------------------------------------------------------
fcTKBool :: fcTKBool( )
{
}
//--------------------------------------------------------------------------
fcTKBool :: fcTKBool( const fcTKBool& entry )
   : fcTKEntry( entry )
{
}
//--------------------------------------------------------------------------
Fc::EntryTypes fcTKBool :: Type() const
{
   return Fc::Bool;
}
//--------------------------------------------------------------------------
fcBaseEntry* fcTKBool :: NewEntry() const
{
   return new fcTKBool( * this );
}
//--------------------------------------------------------------------------
void fcTKBool :: SetDependencyValue( const fc_char* val ) {
   if( strcmp(val,"true") == 0 || strcmp(val,"yes") == 0 || strcmp(val,"1") == 0 ) 
       dependency_value = true;
   else dependency_value = false;
}
//--------------------------------------------------------------------------
fc_bool fcTKBool :: GetDependencyValue() const {
    return dependency_value;
}
//--------------------------------------------------------------------------
#ifdef DEBUG
void fcTKBool :: Print( ostream& stream ) const
{
   stream << "BOOL ";
   fcTEntry :: Print( stream );
}
#endif
