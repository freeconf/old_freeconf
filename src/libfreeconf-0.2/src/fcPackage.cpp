/***************************************************************************
author           : 2009 by David Fabian
***************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/

#include <iostream>
#include <fstream>
#include <cstdlib>
#include "debug.h"
#include "fcDOMConfigFile.h"
#include "fcFiles.h"
#include "fcHeaderStructure.h"
#include "fcPackage.h"
#include "fcMessageHandler.h"
#include "fcSAXHeaderFile.h"
#include "fcSAXHelpFile.h"
#include "fcSAXStringListFile.h"
#include "fcSAXTemplateFile.h"
#include "fcSAXGuiTemplateFile.h"
#include "fcSAXGuiLabelFile.h"
#include "fcStringEntry.h"
#include "fcTSEntry.h"
#include "fcGWindow.h"
#include "fcGTab.h"
#include "fcCGEntry.h"
#include "fcCGSEntry.h"


//--------------------------------------------------------------------------

fcPackage::fcPackage (const char *packageName)
  :_mainCSection(0)
{
  init(fcString(packageName));
}
//--------------------------------------------------------------------------

fcPackage::fcPackage (const fcString &packageName)
  :_mainCSection(0)
{
  init (packageName);
}
//--------------------------------------------------------------------------

void fcPackage::init (const fcString &packageName)
{
  _header = new fcHeaderStructure;
  _window = new fcGWindow;
  _tTree = new fcTSEntry;
  _cTree = new fcCSEntry;
  _DOM = new fcDOMConfigFile(_cTree);
  _stringLists = new fcHash<fcList<fcStringEntry> >;
  _packageName = new fcString(packageName);
}

//--------------------------------------------------------------------------

fcPackage::~fcPackage ()
{
  delete _header;
  delete _tTree;
  delete _window;
  delete _DOM;
  delete _cTree;
  delete _packageName;
  delete _stringLists;
}
//--------------------------------------------------------------------------

const fcTSEntry* fcPackage::tSectionEntry () const
{
  return _tTree;
}
//--------------------------------------------------------------------------

const fcCSEntry* fcPackage::cSectionEntry () const
{
  return _cTree;
}
//--------------------------------------------------------------------------

fcCSEntry* fcPackage::cSectionEntry ()
{
  return _cTree;
}
//--------------------------------------------------------------------------

const fcGWindow* fcPackage::window () const
{
  return _window;
}
//--------------------------------------------------------------------------

fcCSEntry* fcPackage::mainCSection ()
{
  return _mainCSection;
}
//--------------------------------------------------------------------------

const fcHeaderStructure* fcPackage::header () const
{
  return _header;
}
//--------------------------------------------------------------------------

fcDOMConfigFile* fcPackage::configFile ()
{
  return _DOM;
}
//--------------------------------------------------------------------------
fc_bool fcPackage::loadPackage ()
{
  //    DBG_FUNCTION_NAME( "", "fcInit" );
//    assert( dom_config_file. GetConfigSection() );
 
   const fc_char* home = getenv( "HOME" );
   if( ! home )
      MessageHandler. Warning( "Unable to get value of $HOME." );
//    DBG_EXPR( home );

   const fc_char* lang = getenv( "LANG" );
   if( ! lang )
      MessageHandler. Warning( "Unable to get value of $LANG." );
   //    DBG_EXPR( lang );

   fcList< fcString > dirs;
   dirs.Append( fcString( home ) + "/.freeconf" );
   dirs.Append( "/usr/local/share/freeconf" );
   dirs.Append( "/usr/share/freeconf" );
   
   fcList< fcString > langs;
   if( lang && strlen( lang ) != 0 )
     langs. Append( lang );

   langs. Append( "en" );
   
   fcList< fcString > package_dirs( dirs );

   fcString package_subdir( fcString( "packages/" ) + *_packageName  );
   fcAppendSubdir( package_dirs, package_subdir.Data() );

   fcString real_header_file, header_dir;

   if( ! fcCheckFiles( package_dirs,
                       0, // langs
                       "header.xml",
                       0, 
                       &real_header_file,
                       &header_dir,
                       0,
                       0 ) )
   {
      MessageHandler. Error( "Unable to resolve header file." );
      return false;
   }
//    DBG_EXPR( real_header_file );
//    DBG_EXPR( header_dir );
  
  // ----------------- Header files ----------------------
   fcSAXHeaderFile sax_header_file;
   MessageHandler.Message(fcString( "Parsing header file ... " ) + real_header_file);
   
   if( ! sax_header_file. ParseHeaderFile( real_header_file. Data(),
                                           _header ) )
      return false;

   // ----------------- String lists files ----------------------
   fcList< fcString >& string_list_files = _header->StringListFiles();
   
   fcList< fcString > string_dirs;
   string_dirs.Append( fcString( home ) + "/.freeconf" );
   string_dirs.Append( header_dir );
   string_dirs.AppendList( dirs );
   fcAppendSubdir( string_dirs, "strings" );
   for(fc_int i = 0; i < string_list_files.Size(); i ++ )
   {
      fcString string_list_name = string_list_files[ i ];
      fcString string_list_file = string_list_name + ".xml";
      fcString file;
      
      if( ! fcCheckFiles( string_dirs,
                          0, //langs,
                          string_list_name. Data(),
                          0, 
                          &file,
                          0,
                          0,
                          0 ) )
      {
         MessageHandler. Warning( fcString( "Unable to find " ) +
                                  string_list_name + "." );
         continue;
      }
      
      fcSAXStringListFile sax_string_list;
      MessageHandler. Message( fcString( "Parsing string list file ... " ) +
                               file + "." );
      sax_string_list. ParseStringListFile( file.Data(),
                                            _stringLists );
   }

   // ------------------ Template file --------------------------
   
   const fcString& template_file = header_dir + "/" +
                                   _header->GetTemplateFile();
   if( ! template_file )
      return MessageHandler. Error( "No template file was given!" );
   
   MessageHandler. Message( 
         fcString( "Parsing template file ... " ) + 
         fcString( template_file ) );
   
   fcSAXTemplateFile sax_template_file;
   if( ! sax_template_file. ParseTemplateFile( template_file.Data(),
                                               _tTree,
                                               _stringLists ) )
      return MessageHandler. Error( "Unable to parse template file!" );   
   //DBG_COUT( t_section );


   // -------------------- Help file ----------------------------
     
   fcString help_file;
   fcList< fcString > help_dirs;
   help_dirs. Append( header_dir + "/L10n" );
   if( ! fcCheckFiles( help_dirs,
                       &langs,
                       _header->GetHelpFile().Data(),
                       0,
                       &help_file,
                       0,
                       0,
                       0 ) )
   {
      MessageHandler. Warning( 
            fcString( "Unable to find help file .. " ) +
            _header->GetHelpFile().Data() + "." );
   }
   else
   {
      fcSAXHelpFile sax_help_file;
      MessageHandler. Message( fcString( "Parsing help file ..." ) +
                               help_file );
      if( ! sax_help_file. ParseHelpFile( help_file. Data(),
                                          _tTree ) )
         MessageHandler. Warning( "Unable to parse help file!" );
   }
   
   //DBG_COUT( t_section );

//    DBG_COUT( "Initializing config structure..." );
   if( ! _DOM->GetConfigSection()->Init(_tTree) )
   {
      MessageHandler. 
         Error( "Unable to initialize root config section." );
      return false;  
   }
   // ----------------- Default setting --------------------------
   fcList< fcString > def_val_dirs;
   def_val_dirs. Append( fcString( home ) + 
                         "/.freeconf/default/" +
                         *_packageName );
   def_val_dirs. Append( header_dir );
   
   fcString default_file;
   if( ! fcCheckFiles( def_val_dirs,
                       0, // langs
                       _header->GetDefaultValuesFile(). Data(),
                       0,
                       &default_file,
                       0,
                       0,
                       0 ) )
   {
      MessageHandler. Error( 
            fcString( "Unable to find default values file ..." ) +
               _header->GetDefaultValuesFile() + "." );

   }
   else
   {
      MessageHandler. Message(
            fcString( "Parsing default values file ..." ) +
            default_file + "." );
      fcDOMConfigFile dom_default_file(_DOM->GetConfigSection());
      if( ! dom_default_file. Parse( default_file.Data(),
                                     _tTree,
                                     true // default setting
                                     ) ) 
      {
         MessageHandler. Error( 
                fcString( "Unable to parse file..." ) +
                default_file + "." );
      }
   }
   
   // ----------------- Config file ----------------------------
   fcString out_dir = fcString( home ) + 
                      "/.freeconf/config/" + *_packageName;

   fcString config_file( _header->GetOutputFile() );
                      
//    fcExpandFileName( config_file, home, out_dir );
   fcExpandFileName( config_file, home, header_dir );
   _header->SetOutputFile( config_file. Data() );
      
   MessageHandler. Message( 
         fcString( "Parsing configuration file ..." ) +
         config_file + "." );
   if( ! _DOM->Parse( config_file.Data(),
                      _tTree,
                      false // not default setting
                      ))
    return MessageHandler.
          Error( "Unable to parse configuration file." );

  // ======================================================
  _mainCSection = dynamic_cast<fcCSEntry*>(_DOM->GetConfigSection()->at(0));
  // ======================================================
  
  // ----------------- Gui template file ----------------------------
  bool error = false;
  
  fcString gui_template_file = _header->GetGuiTemplateFile();
  error = gui_template_file.Empty();
  if (error) MessageHandler.Warning("Missing gui template entry in header");
  fcList<fcString> gui_dirs;
  gui_dirs.Append(header_dir);
  // we're looking for gui template file
  if(!error && fcCheckFiles( gui_dirs,
                              0, // langs
                              gui_template_file.Data(), //filename
                              0, // write access
                              &gui_template_file, // result file
                              0, // result dir
                              0, // result files
                              0)) // result dirs
  {
    MessageHandler. Message(fcString("Parsing gui template file ... ") + gui_template_file);
    
    fcSAXGuiTemplateFile sax_gui_template_file;
    error = !sax_gui_template_file.ParseGuiTemplateFile(gui_template_file.Data(), _cTree, _window);
  }
  // file not found
  else
  {
    MessageHandler.Warning(fcString("Gui template file '") + gui_template_file + "' not found");
    error = true;
  }

  // need to reconstruct gui tree from config tree
  if (error)
  {
    MessageHandler.Warning( "Unable to parse gui template file, reverting to fallback!" );
    // creating implicit gui tree, all in one tab
    // cleaning up possible mess
    if (!_window->isEmpty())
    {
      delete _window;
      _window = new fcGWindow();
    }
    _window->setTitle("freeconf generated config dialog");
    fcGTab *tab = new fcGTab();
    tab->setName("all-tab");
    tab->setLabel("All");
    tab->setDescription("General fallback tab");
    _window->addTab(tab);
    fcCGSEntry *rootSection = new fcCGSEntry();
    tab->setContent(rootSection);
    // filling tree
    fillSection(rootSection, _mainCSection);
    
  }

  // ----------------- Gui label file ----------------------------
  error = false;
  
  fcString gui_label_file = _header->GetGuiLabelFile();
  
  error = gui_label_file.Empty();
  if (error) MessageHandler.Warning("Missing gui label entry in header");

  if(!error && fcCheckFiles(help_dirs,
                            &langs,
                            gui_label_file.Data(),
                            0,
                            &gui_label_file,
                            0,
                            0,
                            0))
  {
    MessageHandler.Message(fcString("Parsing gui label file ... ") + gui_label_file);

    fcSAXGuiLabelFile sax_gui_label_file;
    error = !sax_gui_label_file.ParseGuiLabelFile(gui_label_file.Data(), _window);
  }
  // file not found
  else
  {
    MessageHandler.Warning(fcString("Gui label file '") + gui_label_file + "' not found");
  }

  //    DBG_COUT( "Final config file ... " << endl << config_file );
  
   // ==========================================================
    _mainCSection->InitDependents( _DOM->GetConfigSection() );
    // the next function prints the dependencies conditions (temaplate and config) for all entries and 
    //  corresponding actions for these conditions
    ////dynamic_cast<fcCEntry*>(_mainCSection)->Print(); 
   // ==========================================================
   // ------------------- XSLT file ---------------------------
   fcString xslt_file;
   if( ! fcCheckFiles( package_dirs,
                       0, // langs
                       _header->GetTransformFile().Data(),
                       0, 
                       &xslt_file,
                       0,
                       0,
                       0 ) )
   {
      MessageHandler. Error( "Unable to resolve XSLT file." );
      return false;
   }
   _header->SetTransformFile( xslt_file.Data() );

   // out dir is defined if 'Config file' section (the previous one )
   fcString str = _header->GetNativeFile();
   fcExpandFileName( str, home, out_dir );
   _header->SetNativeFile( str.Data() );

//    DBG_EXPR( header_structure. GetTransformFile() );
//    DBG_EXPR( header_structure. GetOutputFile() );
//    DBG_EXPR( header_structure. GetNativeFile() );

   if( MessageHandler. GetErrorsNumber() )
      return false;
   return true;
}


void fcPackage::fillSection (fcCGSEntry *guiEntry, const fcCSEntry *configEntry)
{
  for (int i = 0; i < configEntry->Size(); i++)
  {
    if (configEntry->at(i)->Type() == Fc::Section)
    {
      fcCGSEntry *newEntry = new fcCGSEntry();
      fcCSEntry *tmpEntry = dynamic_cast<fcCSEntry*>(configEntry->at(i));
      newEntry->setConfigBuddy(tmpEntry);
      dynamic_cast<fcCEntry*>(tmpEntry)->SetGuiBuddy(newEntry);
      guiEntry->addChild(newEntry);
      fillSection(newEntry, tmpEntry);
    }
    else
    {
      fcCGEntry *newEntry = new fcCGEntry();
      newEntry->setConfigBuddy(dynamic_cast<fcCEntry*>(configEntry->at(i)));
      dynamic_cast<fcCEntry*>(configEntry->at(i))->SetGuiBuddy(newEntry);
      guiEntry->addChild(newEntry);
    }
  }
}
