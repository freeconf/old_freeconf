/***************************************************************************
                          fcHash.h  -  description
                             -------------------
    begin                : 2005/08/20
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcHashH
#define fcHashH

#include "fcString.h"
#include "fcList.h"

//! Hash element structure
template< class T > struct fcHashElement
{
   //! Hash key
   fcString key;

   //! Value
   T value;

   //! Constructor
   fcHashElement( const fc_char* _key,
                  const T& _value )
      : key( _key ),
        value( _value ){};
};

//! Hash
template< class T > class fcHash
{
   //! Main hash
   fcList< fcHashElement< T > > hash;

   public:

   //! Inserts data into the hash
   void Insert( const fc_char* key, const T& val )
   {
      hash. Append( fcHashElement< T >( key, val ) );
   };

   //! Search for data in hash
   T* operator()( const fc_char* key )
   {
      fc_int i;
      for( i = 0; i < hash. Size(); i ++ )
         if( hash[ i ]. key == key )
            return & hash[ i ]. value;
      return NULL;
   };

   //! Search for data in constant hash
   const T* operator()( const fc_char* key ) const
   {
      return const_cast< fcHash< T >* >( this ) -> Get( key );
   };

   //! Erase entry with given key
   void Erase( const fc_char* key )
   {
      fc_int i;
      for( i = 0; i < hash. Size(); i ++ )
         if( hash[ i ]. key == key )
            hash. Erase( i );
   };

   //! Erase all
   void EraseAll()
   {
      hash. EraseAll();
   };

   //! Erase all including dynamycaly allocated data
   void DeepEraseAll()
   {
      hash. DeepEraseAll();
   };
};

#endif
