/***************************************************************************
                          fcMCKContainer.h  -  description
                             -------------------
    begin                : 2005/08/24
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcMCKContainerH
#define fcMCKContainerH

#include "fcCSEntry.h"
#include "fcCKEntry.h"
#include "fcTEntry.h"

//! Conatiner for multiple config sections
class fcMCKContainer : public fcCSEntry
{
   //! Keyword entry with default values 
   /*! It is usefull for example for creating a new entry by user
       to have some initial setting.
    */
   fcCKEntry* default_values;

   public:

   //! Basic constructor
   fcMCKContainer();

   //! Copy constructor
   fcMCKContainer( const fcMCKContainer& container );

   //! Constructor template entry pointer
   fcMCKContainer( const fcTEntry* t_entry );

   //! Destructor
   ~fcMCKContainer();
   
   //! Returns true if it is a multiple config entries conatianer
   fc_bool IsMultipleEntryContainer() const;
};

#endif
