/***************************************************************************
author           : 2009 by David Fabian
***************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/


#ifndef _FCCGENTRY_H_
#define _FCCGENTRY_H_

#include "fcString.h"

class fcCEntry;

class fcCGEntry
{
  protected:
    fcCEntry *_configBuddy;
    fcCGEntry *_parent;
    fcString _name;
    fcString _label;
    
  public:

    fcCGEntry (fcCGEntry *parent = 0);

    explicit fcCGEntry (fcCEntry *configBuddy, fcCGEntry *parent = 0);

    virtual ~fcCGEntry ();

    void setConfigBuddy (fcCEntry *configBuddy);
    fcCEntry* configBuddy () const;

    void setParent (fcCGEntry *parent);
    fcCGEntry* parent () const;

    void setName (const fcString &name);
    const fcString& name () const;

    void setLabel (const fcString &label);
    const fcString& label () const;
    
    virtual Fc::EntryTypes type () const;
};

#endif
