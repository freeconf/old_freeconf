#include <fcTSEntry.h>
#include <fcTKString.h>
#include <fcMCKContainer.h>
#include <fcCKList.h>
#include <fcMessageHandler.h>
#include <QGroupBox>
#include <QPushButton>
#include <QListWidget>
#include <QBoxLayout>
#include <QDialog>
#include <QMessageBox>
#include <QDialogButtonBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>


#include "qtCSEntry.h"
#include "qtCSRootEntry.h"
#include "qtCKString.h"
#include "qtMCSContainer.h"

#include "qtCKBool.h"
#include "qtCKNumber.h"

qtMCSContainer::qtMCSContainer (const qtCWidget *parent)
  :qtCWidget(parent),
   fcMCSContainer(),
  _list(0)
{
  
}


qtMCSContainer::qtMCSContainer (const fcTEntry *tEntry, const qtCWidget *parent)
  :qtCWidget(parent),
  fcMCSContainer(tEntry),
  _list(0)
{
  
}


void qtMCSContainer::render ()
{
  if (!fcParent() || !fcParent()->fcLayout())
  {
    qDebug() << "Widget's parent and layout must be set before render";
    return;
  }
  const fcTSEntry* tsEntry = (const fcTSEntry*)GetTEntry();
  
  QGroupBox *box = new QGroupBox(tsEntry->GetLabel().Data());
  _list = new QListWidget();
  _list->setMinimumHeight(130);
  if (!Size()) _list->setEnabled(false);
  for(fc_int i = 0; i < Size(); i ++)
  {
    qtCSEntry *cSection = dynamic_cast<qtCSEntry*>((*this)[i]);
    
    // The first entry of the multiple section is
    // expected to be its ID
    fcCEntry *idEntry = (fcCEntry*) (*cSection)[0];

    const fc_char *sectionId = 0;
    
    if(idEntry->Type() == fcSTRING)
      sectionId = ((qtCKString*)idEntry)->GetValue().Data();
    _list->addItem(sectionId);
  }
  

  QHBoxLayout *hLayout = new QHBoxLayout();
  QVBoxLayout *vLayout = new QVBoxLayout();

  _add = new QPushButton(tr("&Add"));
  _modify = new QPushButton(tr("&Modify"));
  _remove = new QPushButton(tr("&Remove"));
  vLayout->addWidget(_add);
  vLayout->addWidget(_modify);
  vLayout->addWidget(_remove);

  connect(_add, SIGNAL(clicked()), this, SLOT(add()));
  connect(_modify, SIGNAL(clicked()), this, SLOT(modify()));
  connect(_remove, SIGNAL(clicked()), this, SLOT(remove()));
  connect(_list, SIGNAL(clicked(const QModelIndex&)), this, SLOT(itemSelected(const QModelIndex&)));

  _modify->setEnabled(_list->currentRow() > -1);
  _remove->setEnabled(_list->currentRow() > -1);
  
  hLayout->addWidget(_list);
  hLayout->addLayout(vLayout);
  
  box->setLayout(hLayout);
  fcParent()->fcLayout()->addWidget(box);
}


void qtMCSContainer::itemSelected (const QModelIndex &index)
{
  _modify->setEnabled(index.row() > -1);
  _remove->setEnabled(index.row() > -1);
}


void qtMCSContainer::attach (fc_bool attached)
{
  
}


void qtMCSContainer::add ()
{
  QDialog *dialog = new QDialog(this);
  dialog->setMinimumSize(QSize(320, 240));
  dialog->setWindowTitle(tr("Adding new config section"));
  QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

  connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
  connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
  
  QVBoxLayout *vLayout = new QVBoxLayout();

  const fcTSEntry* tSection = (fcTSEntry*) GetTEntry();

  qtCSEntry *editedSection = new qtCSEntry(tSection);
  
  qtCSRootEntry *rootSection = new qtCSRootEntry(editedSection, vLayout);
  
  editedSection->Init(tSection);
  rootSection->render();

  vLayout->addWidget(buttonBox);
  dialog->setLayout(vLayout);
  if (dialog->exec() == QDialog::Accepted)
  {
    Append(editedSection);
    
    fcCEntry *idEntry = (*editedSection)[0];

    const fc_char *sectionName;
    
    if(idEntry->Type() != fcSTRING)
      sectionName = tr("no name").toStdString().c_str();
    else
      sectionName = dynamic_cast<qtCKString*>(idEntry)->GetValue().Data();

    _list->setEnabled(true);
    _list->addItem(sectionName);                                         
  }
  delete rootSection;
}

void qtMCSContainer::modify ()
{
  if (!_list)
  {
    qDebug() << "Cannot modify item before render";
    return;
  }

  if (_list->currentRow() >= Size() || _list->currentRow() < 0)
  {
    qDebug() << "Invalid list index";
    return;
  }


  QDialog dialog(this);
  dialog.setMinimumSize(QSize(320, 240));
  dialog.setWindowTitle(tr("Modifying config section"));
  QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
  
  connect(buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));
  connect(buttonBox, SIGNAL(rejected()), &dialog, SLOT(reject()));
  
  QVBoxLayout *vLayout = new QVBoxLayout();

  qtCSEntry *editedSection = dynamic_cast<qtCSEntry*>((*this)[_list->currentRow()]);

  qtCSRootEntry *rootSection = new qtCSRootEntry(editedSection, vLayout);

  editedSection->attach(false);
  rootSection->render();

  vLayout->addWidget(buttonBox);
  dialog.setLayout(vLayout);
  if (dialog.exec() == QDialog::Accepted)
  {
    editedSection->attach(true);
    const fc_char *sectionName = dynamic_cast<qtCKString*>((*editedSection)[0])->GetValue().Data();
    QListWidgetItem *it = _list->currentItem();
    it->setText(sectionName);
    _list->setCurrentItem(it);
    _list->setEnabled(true);
  }
  delete rootSection;
}

void qtMCSContainer::remove ()
{
  if (_list->currentRow() >= Size() || _list->currentRow() < 0) return;

  qtCSEntry *editedSection = dynamic_cast<qtCSEntry*>((*this)[_list->currentRow()]);
  const fc_char *sectionName = dynamic_cast<qtCKString*>((*editedSection)[0])->GetValue().Data();
  
  QMessageBox question(this);
  question.setMinimumSize(QSize(400, 240));
  question.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
  question.setDefaultButton(QMessageBox::Ok);
  question.setText(tr("Remove this section?"));
  QString msg = "The " + QString(sectionName) + " will be permanently removed";
  question.setInformativeText(tr(msg.toStdString().c_str()));
  question.setWindowTitle(tr("Really remove?"));
  question.setIcon(QMessageBox::Question);
  if (question.exec() == QMessageBox::Ok)
  {
    Erase(_list->currentRow());
    delete(_list->currentItem());
  }
}


void qtMCSContainer::invalidateGui()
{
  
}

