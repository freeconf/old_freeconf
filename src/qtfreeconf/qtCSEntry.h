#ifndef _QT_CSENTRY_H
#define _QT_CSENTRY_H


#include "qtCWidget.h"

class vector;

class qtCSEntry: public qtCWidget
{
  Q_OBJECT

  private:
    //! list of children
    std::vector<qtCWidget*> _children;
    //! should the section be rendered with or without a frame
    fc_bool _frameLess;

    //! helper function, fills children list
    void fillChildren();
    //! helper function, deletes all the children
    void destroyChildren();

  protected:
    //! renders children
    virtual void renderChildren ();
 
  public:
    //! constructor
    explicit qtCSEntry (fcPackage *package = 0, const qtCWidget *parent = 0);
    //! constructor
    explicit qtCSEntry (const fcCGEntry *guiEntry, fcPackage *package = 0, const qtCWidget *parent = 0);

    //! destructor
    virtual ~qtCSEntry();

    //! overloaded function, GUI entry setter
    virtual void setGuiEntry (fcCGEntry *guiEntry);

    //! returns number of children
    int numberOfChildren() const;

    //! render fucntion
    virtual void render ();
    //! removes any Qt visual widgets
    virtual void invalidateGui ();

    //! frameless setter
    void setFrameLess (fc_bool frameLess);
    //! frameless getter
    fc_bool frameLess () const;
    
  public slots:
    //! connects widget with GUI tree
    virtual void attach (fc_bool attached);
  
};

#endif
