/***************************************************************************
                          fcTransformer.cpp  -  description
                             -------------------
    begin                : 2004/10/13
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <xalanc/Include/PlatformDefinitions.hpp>  

#if defined(XALAN_CLASSIC_IOSTREAMS)
#include <iostream.h>
#else
#include <iostream>
#endif

#include <xercesc/util/PlatformUtils.hpp>
#include <xalanc/XalanTransformer/XalanTransformer.hpp>   

#include "fcTransformer.h"
#include "debug.h"

//--------------------------------------------------------------------------
void fcTransformer :: Transform( const fc_char* config_file, 
                                 const fc_char* xsl_file,
                                 const fc_char* output_file )
{
   DBG_FUNCTION_NAME( "fcTransformer", "Transform" ); 
   DBG_EXPR( config_file );
   DBG_EXPR( xsl_file );
   DBG_EXPR( output_file );

   try
   {
      XALAN_USING_XERCES(XMLPlatformUtils);
      XALAN_USING_XALAN(XalanTransformer);

      // Call the static initializer for Xerces.
      XMLPlatformUtils :: Initialize();

      // Initialize Xalan.
      XalanTransformer::initialize();

      {
         // Create a XalanTransformer.
         XalanTransformer theXalanTransformer;

         // The assumption is that the executable will be run
         // from same directory as the input files.
         if( theXalanTransformer. transform( config_file,
                                             xsl_file,
                                             output_file ) )

         {
            std::cerr << "Transform Error: \n" << theXalanTransformer.getLastError()
            << std::endl
            << std::endl;
         }
      }

      // Terminate Xalan...
      XalanTransformer :: terminate();

      // Terminate Xerces...
      XMLPlatformUtils :: Terminate();

      // Clean up the ICU, if it's integrated...
      XalanTransformer :: ICUCleanUp();
   }
   catch(...)
   {
     std::cerr << "Initialization failed!" << std::endl;
   }   
}
