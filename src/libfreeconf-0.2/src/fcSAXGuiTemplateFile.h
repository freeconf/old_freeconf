/***************************************************************************
author           : 2009 by David Fabian
***************************************************************************/

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/


#ifndef _FCSAXGUITEMPLATEFILE_H_
#define _FCSAXGUITEMPLATEFILE_H_

#include "fcSAXFile.h"
#include "fcStack.h"
#include "fcHash.h"
#include "fcString.h"

class fcGWindow;
class fcCSEntry;
class fcCGSEntry;

class fcSAXGuiTemplateFile : protected fcSAXFile
{
  private:
    enum Element
    {
      NoElement,
      RootElement,
      Window,
      MinWidth,
      MinHeight,
      MaxWidth,
      MaxHeight,
      Tab,
      TabName,
      TabIcon,
      TabSection,
      TabSectionName,
      ImportTemplateEntry
    } _currentElement;

    fcGWindow *_window;
    fcCSEntry *_configTree;
    fc_bool _rootElementOpened;
    fc_bool _tabElementOpened;
    fc_bool _sectionElementOpened;

    fcStack<fcCGSEntry*> _sectionStack;

  protected:
    
    void startElement(const XMLCh* const uri,
                      const XMLCh* const localName,
                      const XMLCh* const qName,
                      const Attributes& attributes);
                        
    void endElement(const XMLCh* const uri,
                    const XMLCh* const localName,
                    const XMLCh* const qName);
                                         

   void characters(const XMLCh* const chars, const unsigned int length);
                                                          
  public:

    fcSAXGuiTemplateFile ();

    ~fcSAXGuiTemplateFile ();
    
    fc_bool ParseGuiTemplateFile(const fc_char *fileName, fcCSEntry *configTree, fcGWindow *window);
};

#endif
