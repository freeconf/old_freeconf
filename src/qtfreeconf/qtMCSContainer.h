#ifndef _QT_MCSCONTAINER_H
#define _QT_MCSCONTAINER_H

#include <fcMCSContainer.h>
#include "qtCSEntry.h"

class QListWidget;
class QPushButton;
class QModelIndex;
class fcTEntry;

class qtMCSContainer: public qtCWidget, public fcMCSContainer
{
  Q_OBJECT

  private:
    QPushButton *_add, *_modify, *_remove;
    QListWidget *_list;
  public:

    qtMCSContainer (const qtCWidget *parent = 0);
    qtMCSContainer (const fcTEntry *tEntry, const qtCWidget *parent = 0);

    virtual void render ();
    virtual void invalidateGui ();

  public slots:
    virtual void attach (fc_bool attached);
  protected slots:
    void add ();
    void modify ();
    void remove ();
    void itemSelected (const QModelIndex &index);
};


#endif
