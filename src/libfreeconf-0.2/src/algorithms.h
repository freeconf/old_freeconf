#ifndef algorithmsH
#define algorithmsH

template< class T > 
T Min( const T& a, const T& b ) {
return a < b ? a : b;
};

template< class T > 
T Max( const T& a, const T& b ) {
return a > b ? a : b;
};

template< class T > 
void Swap( T& a, T& b ) {
T tmp( a );
a = b;
b = tmp;
};

#endif
