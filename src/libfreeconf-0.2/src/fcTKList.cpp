/***************************************************************************
                          fcTKList.cpp  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "fcTKList.h"

//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
fcTKList :: fcTKList( )
{
}
//--------------------------------------------------------------------------
fcTKList :: fcTKList( const fcTKList& entry )
   : fcTKString( entry )
{
}
//--------------------------------------------------------------------------
fcTKList :: ~fcTKList()
{
}
//--------------------------------------------------------------------------
Fc::EntryTypes fcTKList :: Type() const
{
   return Fc::List;
}
//--------------------------------------------------------------------------
fcBaseEntry* fcTKList :: NewEntry() const
{
   return new fcTKList( * this );
}
//--------------------------------------------------------------------------
#ifdef DEBUG
void fcTKList :: Print( ostream& stream ) const
{
   stream << "LIST ";
   fcTEntry :: Print( stream );
}
#endif
