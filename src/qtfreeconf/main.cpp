#include <QApplication>
#include <QDebug>

#include <fcPackage.h>
#include <ctime>

#include "qtCSEntry.h"
#include "qtRender.h"


int main (int argc, char **argv)
{
  clock_t Ticks = clock();
  
  QApplication app(argc, argv);
  
  qDebug() << "QT FreeConf client - Copyright (C) 2009 David Fabian";
  
  if (argc <= 1)
  {
    qCritical() << "No package name was given.";
    return -1;
  }
  
  fcPackage package(argv[1]);
  
  if (!package.loadPackage())
  {
    qCritical() << "Freeconf library init failed!";
    return -1;
  }
 // clock_t Actual = clock();
//  qDebug() << "Init " << (double)(Actual - Ticks)/CLOCKS_PER_SEC << " s";
//  Ticks = Actual;
  qDebug() << "Starting QT rendering ... ";
  qtRender render(&package);
 // Actual = clock();
 // qDebug() << "Render prepare " << (double)(Actual - Ticks)/CLOCKS_PER_SEC << " s";
 // Ticks = Actual;
  render.render();
//  Actual = clock();
 // qDebug() << "Render " << (double)(Actual - Ticks)/CLOCKS_PER_SEC << " s";
  return app.exec();
}
