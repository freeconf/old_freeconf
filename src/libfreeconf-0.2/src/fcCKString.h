/***************************************************************************
                          fcCKString.h  -  description
                             -------------------
    begin                : 2005/01/04
    copyright            : (C) 2005 by Tomas Oberhuber
    email                : oberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef fcCKStringH
#define fcCKStringH

#include "fcCKEntry.h"
#include "fcString.h"

class fcCKString : public fcCKEntry
{
   //! Entry value
   fcString value;
   
   public:
   //! Basic constructor
   fcCKString();

   //! Copy constructor
   fcCKString( const fcCKString& config_value );

   //! Constructor template entry pointer
   fcCKString( const fcTEntry* t_entry )
      : fcCKEntry( t_entry ){};

   //! Destructor
   ~fcCKString();
   
   //! Allocate new instance and return pointer
   virtual fcBaseEntry* NewEntry() const;

   //! Value type getter
   Fc::EntryTypes Type() const;
   
   //! Value getter
   const fcString& GetValue() const;

   //! Value setter
   void SetValue( const fc_char* val );
   
#ifdef DEBUG
   //! Print this structure
   void Print( ostream& stream ) const;
#endif
};

#endif
