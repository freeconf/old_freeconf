/***************************************************************************
                          fcSAXTemplateFile.h  -  description
                             -------------------
    copyright            : (C) 2009 by Frantisek Rolinek
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCSAXTEMPLATECONST_H__
#define __FCSAXTEMPLATECONST_H__

enum fcTemplateEnum { fcNO_TMP_ELEMENT,
                      fcSECTION_NAME,         // id for <section-name> element nested in <section>
                      fcKEYWORD_NAME,         // id for <entry-name> element nested in <bool>,
                                                //      <number> or <string> element
                      fcMULTIPLE,             // id for <multiple> element nested in <section>
                      fcPROPERTIES,           // id for <properties> element nested in <number> element
                      fcDEPENDENCIES          // id for <dependencies> element nested in <section>, <bool>,
                                                //      <number> or <string> element
};

enum fcPropertyEnum  { fcNO_PROP_ELEMENT,
                       fcTYPE,                 // id of <type> element nested in <properties> element
                       fcMIN,                  // id of <min> element nested in <properties> element
                       fcMAX,                  // id of <max> element nested in <properties> element
                       fcSTEP,                 // id of <step> element nested in <properties> element
                       fcDATA                  // id for <data> element nested in <string> element
};

enum fcDependenciesEnum { fcNO_DEPS_ELEMENT,
                          fcCONDITION,         // id of <condition> element nested id <dependencies> element 
                          fcACTIONS            // id of <actions> element nested in <dependencies> element
};

enum fcDependencyEnum { fcNO_DEP_ELEMENT,
                        fcDEPENDENCY_NAME,      // id of <entry-name> element nested in <dependency> element
                        fcVALUES                // id of <values> element nested in <dependency> element
};

enum fcActionEnum { fcNO_ACTION_ELEMENT,            
                    fcVALUE,                    // id of <value> element nested in <action> element   
                    fcDEFAULT_VALUE,            // id of <default-value> element nested in <action> element
                    fcACTION_PROPERTIES         // id of <properties> element nested in <action> element
};

enum fcValueEnum { fcNO_VALUE_ELEMENT, 
                   fcEQUAL,                    // id of <equal> element nested in <value> element
                   fcNOT_EQUAL,                // id of <not_equal> element nested in <value> element
                   fcGREATER,                  // id of <greater> element nested in <value> element
                   fcGREATER_OR_EQUAL,         // id of <greater_or_equal> element nested in <value> element
                   fcLOWER,                    // id of <lower> element nested in <value> element
                   fcLOWER_OR_EQUAL,           // id of <lower_or_equal> element nested in <value> element
                   fcIN,                       // id of <in> element nested in <value> element
                   fcNOT_IN                    // id of <not_in> element nested in <value> element
};

enum fcValueType { fcVAL_EMPTY,                 
                   fcVAL_EQUAL,                 // id of equal (=) condition for dependency value
                   fcVAL_NOT_EQUAL,             // id of not equal (!=) condition for dependency value
                   fcVAL_GREATER,               // id of greater (>) condition for dependency value
                   fcVAL_GREATER_EQ,            // id of greater or equal (>=) condition for dependency value
                   fcVAL_LOWER,                 // id of lower (<) condition for dependency value
                   fcVAL_LOWER_EQ,              // id of lower or equal (<=) condition for dependecy value
                   fcVAL_GREATER_LOWER,         // id of greater and lower (< >) condition for dependency value
                   fcVAL_GREATER_LOWER_EQ,      // id of greater and lower or equal (<= >) condition for dependency value
                   fcVAL_GREATER_EQ_LOWER,      // id of greater or equal and lower (< >=) condition for dependency value
                   fcVAL_GREATER_EQ_LOWER_EQ,   // id of greater or equal and lower or eaual (<= >=) condition for 
                                                    // dependency value
                   fcVAL_LOWER_GREATER,         // id of lower and greater (< >) condition for dependency value
                   fcVAL_LOWER_GREATER_EQ,      // id of lower and greater or equal (< >=) condition for dependency value
                   fcVAL_LOWER_EQ_GREATER,      // id of lower or equal and greater (<= >) condition for dependency value
                   fcVAL_LOWER_EQ_GREATER_EQ,   // id of lower or equal and greater or equal (<= >=) condition for 
                                                    // dependency value
                   fcVAL_IN,                    // id of in condition for dependency value
                   fcVAL_NOT_IN                 // id of not in condition for dependency value
};

enum fcRelationEnum { fcNO_RELATION,            
                      fcRELATION_AND,          // id of <relation-and> element nested in <dependencies>
                      fcRELATION_OR,           // id of <relation-or> element nested in <dependencies> 
                      fcRELATION_NOT           // id of <relation-not> element nested in <dependencies>
};

#endif
