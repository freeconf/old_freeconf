/***************************************************************************
                          fcSAXTemplateFile.h  -  description
                             -------------------
    begin                : 2004/04/11 13:41
    copyright            : (C) 2004 by Tomas Oberhuber
    email                : tomasoberhuber@seznam.cz
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __FCSAXTEMPLATEFILE_H__
#define __FCSAXTEMPLATEFILE_H__

#include "fcSAXTemplateConst.h"
#include "fcDependencyStruct.h"
#include "fcTypes.h"
#include "fcStringEntry.h"
#include "fcSAXFile.h"
#include "fcStack.h"
#include "fcHash.h"
#include "fcString.h"

class fcDActionBaseListItem;
class fcTEntry;
class fcTSEntry;

//! This class reads Template File and creates Template Structure
class fcSAXTemplateFile : protected fcSAXFile
{
   //! String lists definitions
   /*! They are the lists containing definitions of
       possible string values (screen resolution,
       text codings etc. ).
    */
   fcHash< fcList< fcStringEntry > >* string_lists;

   //! Stack for the Template File sections being created
   fcStack< fcTSEntry* > section_stack;

   //! Entry being currently created
   fcTEntry* current_entry;

   //! Flag for file being enclosed in freeconf-template tags
   fc_bool enclosing_tag;

   //! Id of basic xml element 
   /*! The basic xml elements are: <section-name>, <entry-name>,
    *  <multiple>, <properties> and <dependencies>
    */
   fcTemplateEnum template_enum;

   //! Id of property xml element 
   /*! The property xml elements are: <type>,  
    *  <min>, <max>, <step> and <data>
    */
   fcPropertyEnum property_enum;

   //! Id of dependencies xml element 
   /*! The dependencies xml elements are: 
    *  <condition> and <actions>
    */
   fcDependenciesEnum dependencies_enum;

   //! Id of dependency xml element 
   /*! The dependency xml elements are: 
    *  <entry-name> and <values>
    */
   fcDependencyEnum dependency_enum;

   //! Id of action xml element
   /*! The action xml elements are:
    *  <value>, <default-value> and <properties>
    */
   fcActionEnum action_enum;

   //! Id of value xml element
   /*! The value xml elements are: 
    *  <equal>, <not_equal>, <greater>, <greater_or_equal>
    *  <lower>, <lower_or_equal>, <in> and <not_in>
    */
   fcValueEnum value_enum;

   //! Stack for relations used in transformation into Reverse Polish Notation
   /*! The stack's structure is RPNRead structure. This structure has two 
    *  members: the first one indicates kind of relation (AND, OR, NOT), 
    *  the second one indicates condition, if the first entry 
    *  for the current relation was done or not yet. The first entry in 
    *  Reverse Polish Notation transform is special, because after this 
    *  entry, there is no following relation (AND or OR) sign.  
    */
   fcStack< struRPNRead > relations_stack;
   
   ///! Flag for <entry> element nested in <in> or <not_in> element
   //fc_bool entry_element;

   //! Name of the entry, on which the current entry is dependent
   fcString dependency_name;

   //! structure of dependency value for current entry
   fcList< struDependencyValue > dependency_values;

   //! Supplementary instance of the struDependencyValue structure
   struDependencyValue tmpStruct;

   //! Flag for element <dependency>
   fc_bool dependency_element;

   //! Flag for element <action>
   fc_bool action_element;

   //! Flag for element <value>
   fc_bool value_element;

   //! Action for the dependency condition
   fcDActionBaseListItem* current_action;
   
  public:
   //! Basic constructor
   fcSAXTemplateFile();

   //! Destructor
   ~fcSAXTemplateFile();

   private:
   //! Method for controling proper closing of tags listed in enums
   void control();

   protected:

   //! Implementations of the SAX DocumentHandler start element
   void startElement(  const   XMLCh* const    uri,
                       const   XMLCh* const    localname,
                       const   XMLCh* const    qname,
                       const   Attributes&     attributes);

   //! Implementations of the SAX DocumentHandler end element
   void endElement( const XMLCh* const uri, 
                    const XMLCh* const localname, 
                    const XMLCh* const qname);

   //! Implementations of the SAX DocumentHandler characters
   void characters( const XMLCh* const chars, 
                    const unsigned int length);

   public:

   //! Method for parsing template file
   /*! This method just sets pointer this -> template_structure
       and the calls fcSAXFile :: Parse()
    */
   fc_bool ParseTemplateFile( const fc_char* file_name,
                              fcTSEntry* t_section,
                              fcHash< fcList< fcStringEntry > >* _string_lists );

};

#endif
